import React, { Component } from 'react';
import './App.css';

import Search from './js/components/Search';
import Table from './js/components/Table';
import Result from './js/components/Result';

import GitImage from './img/git.svg';
import TwitterImage from './img/twitter.svg';
import HomeImage from './img/home.svg';

import SCHOOLS_DETAILS from './js/constants/SchoolsDetails';


const isSearched = searchTerm => item => {
  const lowercaseSearch = searchTerm.toLowerCase();
  return [item.rollNumber, item.schoolName, item.addressOne, item.addressTwo, item.addressThree, item.addressFour].some(str => str.toLowerCase().includes(lowercaseSearch));
}

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      searchTerm: '',
      selectedSchool: [],
      SCHOOLS_DETAILS,
    };
    
    this.onSearchChange = this.onSearchChange.bind(this);
    this.onSchoolSelect = this.onSchoolSelect.bind(this);
  }

  onSearchChange(event) {
    this.setState({ searchTerm: event.target.value, selectedSchool: [] });
  }

  onLogoClick() {
    this.setState({ searchTerm: "", selectedSchool: [] });
  }

  onSchoolSelect(schoolArray) {
    const selectedSchool = schoolArray;
    this.setState({ selectedSchool });
  }
  
  render() {
    const { searchTerm, selectedSchool, SCHOOLS_DETAILS } = this.state;

    return (
      <div className="page">
        <div className="interactions">
        <header onClick={() => this.onLogoClick()}>
          <h1 id="pageLogo">
           <span class="blue-text"><b>CEC</b></span><span class="pink-text">Search</span>
          </h1>
          <p id="pageSubtitle"><i>{searchTerm.length > 0 ? "Click here to start again" : "Find your District's INTO Rep"}</i></p>
        </header>
        
        <Search 
          value={searchTerm}
          onChange={this.onSearchChange}
        >
        </Search>

        {selectedSchool.schoolName &&
          <Result
            selectedSchool={selectedSchool}
          />
        }
        
        {(searchTerm.length > 2 && !selectedSchool.schoolName)  && 
          <Table 
            SCHOOLS_DETAILS={SCHOOLS_DETAILS}
            pattern={searchTerm}
            onSchoolSelect={this.onSchoolSelect}
            isSearched={isSearched}

          />
        }
        {searchTerm.length < 3 &&
          <footer>
            <div>
              <a href="http://teacherbuilt.me"><img src={HomeImage} /></a>
              <a href="https://twitter.com/D4RK_ONION"><img src={TwitterImage} /></a>
              <a href="https://gitlab.com/D4RKONION/into-cec-rep-email-finder"><img src={GitImage} /></a>
            </div>
            <em>There could be mistakes... <a href="mailto:teacher@fullmeter.com">Let me know!</a> [v1.1.7]</em>
          </footer>
        }
        </div>
      </div>
    )
  }
}

export default App;