const SCHOOLS_DETAILS = [
  {
    "rollNumber": "01572D",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "Carrowhill",
    "addressTwo": "Drumfries",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "01574H",
    "schoolName": "Scoil Naomh Iósaf",
    "addressOne": "Connaghkinnego",
    "addressTwo": "Ballymagan",
    "addressThree": "Buncrana",
    "addressFour": "Co. Donegal",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "01733B",
    "schoolName": "Ardara Mixed N S",
    "addressOne": "Back Road",
    "addressTwo": "Ardara",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "03294L",
    "schoolName": "S N Caiseal Na Gcorr",
    "addressOne": "Gort a Choirce",
    "addressTwo": "Leitir Ceanainn",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "04809A",
    "schoolName": "Scoil An Aingil Choimheadai",
    "addressOne": "An Céideadh,",
    "addressTwo": "Ailt An Chorráin",
    "addressThree": "Leitir Ceanainn",
    "addressFour": "Co Dhún na nGall",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "05164I",
    "schoolName": "Scoil Naomh Cholmcille",
    "addressOne": "Oileán Thoraí",
    "addressTwo": "Co. Dhún na nGall",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "05230S",
    "schoolName": "Comhscoil Chonmha S N",
    "addressOne": "Main Street",
    "addressTwo": "Convoy",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "07143K",
    "schoolName": "Monreagh N S",
    "addressOne": "Monreagh",
    "addressTwo": "Carrigans",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "07464H",
    "schoolName": "Browneknowe N S",
    "addressOne": "Brownknowe",
    "addressTwo": "Ramelton",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "07626H",
    "schoolName": "S N An Iorball Riabaigh",
    "addressOne": "Urblereagh",
    "addressTwo": "Malin Head",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "09009Q",
    "schoolName": "Rockfield N S",
    "addressOne": "Rockfield National School",
    "addressTwo": "Knocknashangan",
    "addressThree": "Ballyshannon",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "09660R",
    "schoolName": "St Francis Ns",
    "addressOne": "Mullanalamphry",
    "addressTwo": "Barnesmore",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "09748I",
    "schoolName": "Glenmaquin No 2 N S",
    "addressOne": "Knockbrack",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "10062E",
    "schoolName": "Creeslough N S",
    "addressOne": "Creeslough",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "10595Q",
    "schoolName": "Kilbarron N S",
    "addressOne": "Belleek Road",
    "addressTwo": "Ballyshannon",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "11843O",
    "schoolName": "S N Neill Mor",
    "addressOne": "Killybegs",
    "addressTwo": "Co. Donegal",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "12077E",
    "schoolName": "Scoil Naomh Fiachra",
    "addressOne": "Illistrin",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "13563S",
    "schoolName": "S N Chill Coinnigh",
    "addressOne": "Kilkenny",
    "addressTwo": "Glenties",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "13755E",
    "schoolName": "Gartan N S",
    "addressOne": "Gartan",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "13872I",
    "schoolName": "Robertson N S",
    "addressOne": "Lisminton",
    "addressTwo": "Ballintra",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "14194S",
    "schoolName": "Scoil Cholmcille",
    "addressOne": "An Tearmann",
    "addressTwo": "Co. Dhún na nGall",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "14502D",
    "schoolName": "Scoil Mhuire B&c",
    "addressOne": "Na Doirí Beaga",
    "addressTwo": "Leitir Ceanainn",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "14631O",
    "schoolName": "Scoil Cholmchille",
    "addressOne": "Ballymena",
    "addressTwo": "Glengad",
    "addressThree": "Malin",
    "addressFour": "Co. Donegal",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "14704P",
    "schoolName": "Murroe National School",
    "addressOne": "Murroe",
    "addressTwo": "Dunfanaghy",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "14910S",
    "schoolName": "St Mary's National School",
    "addressOne": "Castlefinn",
    "addressTwo": "Lifford",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "14941G",
    "schoolName": "Ray N S",
    "addressOne": "Ray",
    "addressTwo": "Manorcunningham",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "15170J",
    "schoolName": "Cashelshanaghan N S",
    "addressOne": "Ballymaleel",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "15208I",
    "schoolName": "Scoil An Tstratha Mhóir",
    "addressOne": "Mín an Lábáin",
    "addressTwo": "Leitir Ceanainn",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "15532R",
    "schoolName": "Croaghross N S",
    "addressOne": "Portsalon",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "15554E",
    "schoolName": "Gortnacart N S",
    "addressOne": "Gortnacart",
    "addressTwo": "Ardara",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "15627F",
    "schoolName": "St Muras N S",
    "addressOne": "Burnfoot",
    "addressTwo": "Lifford",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "15729N",
    "schoolName": "Rathmullen N S",
    "addressOne": "RATHMULLEN",
    "addressTwo": "CO DONEGAL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "15763N",
    "schoolName": "Moville N S",
    "addressOne": "Greencastle Road",
    "addressTwo": "Moville",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "15770K",
    "schoolName": "S N Naomh Naille",
    "addressOne": "Na Caologa",
    "addressTwo": "Inbhear",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "15847T",
    "schoolName": "S N Leitir Mhic An Bhaird",
    "addressOne": "Leitir Mhic a' Bhaird",
    "addressTwo": "Lettermacaward",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "15955W",
    "schoolName": "Sn Arainn Mhor I",
    "addressOne": "Arainn Mór",
    "addressTwo": "Co. Dhún na nGall",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16054M",
    "schoolName": "St Patricks N S",
    "addressOne": "MURLOG",
    "addressTwo": "LIFFORD",
    "addressThree": "CO DONEGAL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16108J",
    "schoolName": "Scoil Naomh Treasa C",
    "addressOne": "TIERNASLIGO URRIS CLONMANY",
    "addressTwo": "LIFFORD",
    "addressThree": "CO DONEGAL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16137Q",
    "schoolName": "Drumfad N S",
    "addressOne": "Kerrykeel P O",
    "addressTwo": "LETTERKENNY",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16138S",
    "schoolName": "Raphoe Central N S",
    "addressOne": "RAPHOE",
    "addressTwo": "LIFFORD",
    "addressThree": "CO DONEGAL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16242N",
    "schoolName": "S N Dumhach Beag",
    "addressOne": "Doaghbeg",
    "addressTwo": "Portalson",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16279N",
    "schoolName": "Scoil Choluim",
    "addressOne": "Ballyheerin",
    "addressTwo": "Fanad",
    "addressThree": "Letterkenny",
    "addressFour": "Co. Donegal",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16349I",
    "schoolName": "S N An Droim Mor",
    "addressOne": "An Droim Mór",
    "addressTwo": "Killygordeon",
    "addressThree": "Lifford",
    "addressFour": "Co. Donegal",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16357H",
    "schoolName": "S N Fhionntra",
    "addressOne": "Fintra",
    "addressTwo": "Killybegs",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16375J",
    "schoolName": "Frosses N S",
    "addressOne": "Frosses",
    "addressTwo": "Co. Donegal",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16384K",
    "schoolName": "Sn Arainn Mhor Ii",
    "addressOne": "Arainn Mhór",
    "addressTwo": "Co. Dhún na nGall",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16458N",
    "schoolName": "Inver N S",
    "addressOne": "INVER",
    "addressTwo": "CO DONEGAL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16471F",
    "schoolName": "St Davadogs N S",
    "addressOne": "Tamney",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16603T",
    "schoolName": "S N An Chillin",
    "addressOne": "Killian",
    "addressTwo": "Inver",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16608G",
    "schoolName": "Killybegs Common N S",
    "addressOne": "The Commons",
    "addressTwo": "Killybegs",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16611S",
    "schoolName": "Glentogher Con N S",
    "addressOne": "Mullins",
    "addressTwo": "Carndonagh",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16642G",
    "schoolName": "St Francis' National School",
    "addressOne": "Clonmaney",
    "addressTwo": "Lifford",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16664Q",
    "schoolName": "Bruckless N S",
    "addressOne": "Bruckless",
    "addressTwo": "Co. Donegal",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16671N",
    "schoolName": "S N Cnoc Na Naomh",
    "addressOne": "Doire Chonaire",
    "addressTwo": "Gort a Choirce",
    "addressThree": "Leitir Ceanainn",
    "addressFour": "Co. Dhún na nGall",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16672P",
    "schoolName": "St Patricks N S",
    "addressOne": "Lurgybrack",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16763S",
    "schoolName": "S N Glasain",
    "addressOne": "Glassan",
    "addressTwo": "Croasloch",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16789N",
    "schoolName": "St Orans N S",
    "addressOne": "Cockhill",
    "addressTwo": "Buncrana",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16819T",
    "schoolName": "S N Gort An Choirce",
    "addressOne": "LEITIR CEANAINN",
    "addressTwo": "CO DHUN NA NGALL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16820E",
    "schoolName": "Scoil Cholmcille",
    "addressOne": "Newtowncunningham",
    "addressTwo": "Co. Donegal",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16821G",
    "schoolName": "Clochar Padraig Naofa",
    "addressOne": "Convent Rd.,",
    "addressTwo": "Carndonagh",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16823K",
    "schoolName": "Min A Ghabhann N S",
    "addressOne": "LETTERMACAWARD",
    "addressTwo": "DONEGAL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16829W",
    "schoolName": "S N Loch An Iubhair",
    "addressOne": "Loch an Iúir",
    "addressTwo": "Anagaire",
    "addressThree": "Leitir Ceanainn",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16836T",
    "schoolName": "Naomh Bridhid",
    "addressOne": "Collon",
    "addressTwo": "Carndonagh",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16837V",
    "schoolName": "S N Duchoraidh",
    "addressOne": "DUCHORAIDH",
    "addressTwo": "CO DHUN NA NGALL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16850N",
    "schoolName": "St Garvans N.s.",
    "addressOne": "Drumhalla",
    "addressTwo": "Rathmullan",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16854V",
    "schoolName": "Buncrana N S",
    "addressOne": "ST. MARY'S ROAD",
    "addressTwo": "BUNCRANA",
    "addressThree": "CO. DONEGAL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16869L",
    "schoolName": "S N An Bhreacaigh",
    "addressOne": "ARD A RATHA",
    "addressTwo": "CO DHUN NA NGALL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16880W",
    "schoolName": "Scoil Naomh Colmchille",
    "addressOne": "CRAIGTOWN",
    "addressTwo": "CARNDONAGH",
    "addressThree": "CO DONEGAL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16903I",
    "schoolName": "S N Fothar",
    "addressOne": "PORT NA BLAITHCHE",
    "addressTwo": "LEITIR CEANAINN",
    "addressThree": "CO DUN NA NGALL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16995Q",
    "schoolName": "S N Naomh Colmchille",
    "addressOne": "Drumoghill",
    "addressTwo": "Manorcunningham",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17018N",
    "schoolName": "Scoil Phadraig",
    "addressOne": "Dobhar",
    "addressTwo": "An Bun Beag",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17035N",
    "schoolName": "S N Mhin Teineadh De",
    "addressOne": "Mín Tine Dé",
    "addressTwo": "Ard an Rátha",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17036P",
    "schoolName": "S N Naomh Colmchille",
    "addressOne": "Kilmacrennan",
    "addressTwo": "Co. Donegal",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17040G",
    "schoolName": "Sn Naomh Samhthann",
    "addressOne": "Drumdoit",
    "addressTwo": "Castlefin",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17057A",
    "schoolName": "Scoil Phádraig",
    "addressOne": "Scoil Phádraig",
    "addressTwo": "Drumkeen",
    "addressThree": "Ballybofey",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17122I",
    "schoolName": "Sn Eadan Fhionnfhaoich",
    "addressOne": "NA GLEANNTA",
    "addressTwo": "CO DHUN NA NGALL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17130H",
    "schoolName": "Scoil Naomh Dubhthach",
    "addressOne": "Machaire Rabhartaigh",
    "addressTwo": "Gort a Choirce",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17180W",
    "schoolName": "S N Na Gcluainte",
    "addressOne": "Cloontagh",
    "addressTwo": "Clonmany",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17241Q",
    "schoolName": "S N Domhnach Mor",
    "addressOne": "Liscooley",
    "addressTwo": "Castlefin",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17260U",
    "schoolName": "Scoil An Linbh Íosa",
    "addressOne": "Killymard",
    "addressTwo": "Donegal Town",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17268N",
    "schoolName": "Sn An Br M O Cleirigh",
    "addressOne": "Creevy",
    "addressTwo": "Ballyshannon",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17328F",
    "schoolName": "Scoil Roisin",
    "addressOne": "An Clochán Liath",
    "addressTwo": "Co. Dhún na nGall",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17447N",
    "schoolName": "S N Crannaighe Buidhe",
    "addressOne": "Crannóg Buí",
    "addressTwo": "Ardara",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17469A",
    "schoolName": "Scoil Cuilm Cille",
    "addressOne": "BALLINDRAIT",
    "addressTwo": "LIFFORD",
    "addressThree": "CO DONEGAL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17503U",
    "schoolName": "S N Adhamhnain",
    "addressOne": "An Luinnigh",
    "addressTwo": "Doire Beaga",
    "addressThree": "Leitir Ceanainn",
    "addressFour": "Co. Dhun na nGall",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17549V",
    "schoolName": "S N Ceathru Caol",
    "addressOne": "KERRYKEEL",
    "addressTwo": "LETTERKENNY",
    "addressThree": "CO DONEGAL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17553M",
    "schoolName": "S N Taodhbhog",
    "addressOne": "AN CLOCHAN",
    "addressTwo": "LEIFEARR",
    "addressThree": "CO DHUN NA NGALL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17564R",
    "schoolName": "S N An Choimin",
    "addressOne": "An Gharbháin",
    "addressTwo": "An Clochán",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17575W",
    "schoolName": "S N Gleann Coimhead",
    "addressOne": "BALLYBOFEY",
    "addressTwo": "CO DONEGAL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17598L",
    "schoolName": "Sn An Leinbh Iosa",
    "addressOne": "Carrigans",
    "addressTwo": "Lifford",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17607J",
    "schoolName": "S N Seiseadh Ui Neill",
    "addressOne": "Sessiaghoneill",
    "addressTwo": "Ballybofey",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17704H",
    "schoolName": "Scoil Fhionáin",
    "addressOne": "Baile Chonaill",
    "addressTwo": "An Fál Carrach",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17716O",
    "schoolName": "St Riaghans Ns",
    "addressOne": "DRIMNACROSH",
    "addressTwo": "GLENTIES",
    "addressThree": "CO DONEGAL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17721H",
    "schoolName": "Scoil Treasa Naofa",
    "addressOne": "Drumcarbit",
    "addressTwo": "Malin",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17728V",
    "schoolName": "S N Talamh Na Coille",
    "addressOne": "Rough Park",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17729A",
    "schoolName": "Scoil Naomh Proinnseas",
    "addressOne": "Magherabeg",
    "addressTwo": "Manorcunningham",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17822N",
    "schoolName": "Scoil Bhrighde",
    "addressOne": "Mín an Chladaigh",
    "addressTwo": "Gort a Choirce",
    "addressThree": "Leitir Ceanainn",
    "addressFour": "Co. Dhún na nGall",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17828C",
    "schoolName": "Scoil Adhamhnain",
    "addressOne": "Meetinghouse Street",
    "addressTwo": "Raphoe",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17831O",
    "schoolName": "Glebe Ns",
    "addressOne": "The Glebe",
    "addressTwo": "Donegal",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17837D",
    "schoolName": "Scoil Mhuire",
    "addressOne": "PETTIGO",
    "addressTwo": "CO DONEGAL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17945G",
    "schoolName": "Scoil Naomh Chaitriona",
    "addressOne": "College Street",
    "addressTwo": "Ballyshannon",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17956L",
    "schoolName": "Scoil Cholmcille Naofa",
    "addressOne": "Ballylast",
    "addressTwo": "Castlefin",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18007N",
    "schoolName": "S N Olibhear Pluinceid",
    "addressOne": "Rann na Feirsde",
    "addressTwo": "Anagaire",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18052S",
    "schoolName": "S N Muire Gan Smal",
    "addressOne": "Sentry Hill",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18058H",
    "schoolName": "Scoil Naomh Seosamh",
    "addressOne": "Rathdonnell",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18076J",
    "schoolName": "Scoil Náisiúnta Muire Gan Smál",
    "addressOne": "Townparks",
    "addressTwo": "Lifford",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18086M",
    "schoolName": "Dunfanaghy N S",
    "addressOne": "Hornhead Rd",
    "addressTwo": "Dunfanaghy",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18114O",
    "schoolName": "S N Naomh Eighneach",
    "addressOne": "DISEART EIGHNIGH",
    "addressTwo": "BUNCRANNACH",
    "addressThree": "CO DUN NA NGALL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18120J",
    "schoolName": "Scoil Mhuire",
    "addressOne": "CAISEAL",
    "addressTwo": "CEANNDROMA O.P.",
    "addressThree": "FÁNAID",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18129E",
    "schoolName": "Scoil Naomh Peadar",
    "addressOne": "MOUNTCHARLES",
    "addressTwo": "CO DUN NA NGALL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18131O",
    "schoolName": "S N Muire Gan Smal",
    "addressOne": "Hillhead",
    "addressTwo": "Ardara",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18151U",
    "schoolName": "S N Mhuire",
    "addressOne": "Convent Road",
    "addressTwo": "Milford",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18172F",
    "schoolName": "Gleneely N S",
    "addressOne": "Gleneely",
    "addressTwo": "Killygordon",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18219F",
    "schoolName": "Sn Chonaill",
    "addressOne": "Machaire Chlochair",
    "addressTwo": "Bun Beag",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18241V",
    "schoolName": "Scoil Naomh Cholmcille",
    "addressOne": "DRUMMAN",
    "addressTwo": "RAMELTON",
    "addressThree": "CO DONEGAL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18250W",
    "schoolName": "S N Baile Mor",
    "addressOne": "Ballymore",
    "addressTwo": "Portnablagh",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18251B",
    "schoolName": "Ayr Hill N S Ramelton",
    "addressOne": "Tank Road",
    "addressTwo": "Ramelton",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18286U",
    "schoolName": "S N Na Hacrai",
    "addressOne": "Acres",
    "addressTwo": "Burtonport",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18295V",
    "schoolName": "S N Min An Aoire",
    "addressOne": "Meenaneary",
    "addressTwo": "Carrick",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18319J",
    "schoolName": "S N Trianta",
    "addressOne": "Triantagh",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18371L",
    "schoolName": "Scoil Mhuire",
    "addressOne": "AN CRAOSLOCH",
    "addressTwo": "LETTERKENNY",
    "addressThree": "CO DONEGAL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18421A",
    "schoolName": "Sn Dun Ceannfhaolaidh",
    "addressOne": "Dunkineely",
    "addressTwo": "Co. Donegal",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18446Q",
    "schoolName": "Scoil Naomh Mhuire",
    "addressOne": "Carnmalin",
    "addressTwo": "Malin",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18508M",
    "schoolName": "Naomh Adhamhnain",
    "addressOne": "Laghey",
    "addressTwo": "Co. Donegal",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18517N",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Dristernan",
    "addressTwo": "Gleneely",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18520C",
    "schoolName": "Scoil Phadraig",
    "addressOne": "Rashenny",
    "addressTwo": "Clonmany",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18605K",
    "schoolName": "Scoil Naomh Padraig Boys",
    "addressOne": "CARNDONAGH",
    "addressTwo": "CO DONEGAL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18611F",
    "schoolName": "S N Na Carraige",
    "addressOne": "An Charraig",
    "addressTwo": "Co. Dhún na nGall",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18625Q",
    "schoolName": "Scoil Choilmcille",
    "addressOne": "Convent Road",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18652T",
    "schoolName": "S N An Chaiseal",
    "addressOne": "An Caiseal",
    "addressTwo": "Gleann Cholmcille",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18700E",
    "schoolName": "S N Baile An Caislean",
    "addressOne": "Castletown",
    "addressTwo": "St Johnston",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18703K",
    "schoolName": "S N Baile An Bhailsig",
    "addressOne": "Welchtown",
    "addressTwo": "Ballybofey",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18710H",
    "schoolName": "Sn Na Croise Naofa",
    "addressOne": "DUNFANAGHY",
    "addressTwo": "Letterkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18731P",
    "schoolName": "Robertson N S",
    "addressOne": "Church Road",
    "addressTwo": "Stranordlar",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18737E",
    "schoolName": "Scoil Bhride",
    "addressOne": "Convoy",
    "addressTwo": "Lifford",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18766L",
    "schoolName": "Scoil Cholmcille",
    "addressOne": "DUBHLINN RIABHACH",
    "addressTwo": "CARRAIG AIRT",
    "addressThree": "LEITIR CEANAINN",
    "addressFour": "Co. Dhún na nGall",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18791K",
    "schoolName": "Scoil Cholmcille",
    "addressOne": "DROIM AN MHAOIR",
    "addressTwo": "MOVILLE",
    "addressThree": "CO DONEGAL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18844F",
    "schoolName": "Scoil Mhuire",
    "addressOne": "BELCRUIT",
    "addressTwo": "KINCASSLAGH",
    "addressThree": "LETTERKENNY",
    "addressFour": "CO. DONEGAL",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18874O",
    "schoolName": "Killygordan N S",
    "addressOne": "Main Street",
    "addressTwo": "Killygordon",
    "addressThree": "Lifford",
    "addressFour": "Co. Donegal",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18939Q",
    "schoolName": "S N Donaigh",
    "addressOne": "CARNDONAGH",
    "addressTwo": "CO DONEGAL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19009W",
    "schoolName": "Cranford N S",
    "addressOne": "Cranford",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19089A",
    "schoolName": "Killaghtee N S",
    "addressOne": "KILLAGHTEE",
    "addressTwo": "DUNKINEELY",
    "addressThree": "CO DONEGAL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19228L",
    "schoolName": "S N Naomh Brid",
    "addressOne": "Na Dúnaibh",
    "addressTwo": "Leitir Ceanainn",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19235I",
    "schoolName": "Portlean N S",
    "addressOne": "KILMACRENNAN",
    "addressTwo": "LETTERKENNY",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19252I",
    "schoolName": "Scoil Eoin Baiste",
    "addressOne": "Umlagh",
    "addressTwo": "Carrigart",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19260H",
    "schoolName": "S N Cholmcille",
    "addressOne": "BAILE NA FINNE",
    "addressTwo": "CO DHUN NA NGALL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19280N",
    "schoolName": "Scoil Naomh Brid",
    "addressOne": "Muff",
    "addressTwo": "Lifford",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19310T",
    "schoolName": "Scoil Naomh Earnan",
    "addressOne": "Lisminton",
    "addressTwo": "Ballintra",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19313C",
    "schoolName": "Glenswilly N S",
    "addressOne": "Newmills",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19333I",
    "schoolName": "Dooish N S",
    "addressOne": "BALLYBOFEY",
    "addressTwo": "CO DONEGAL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19343L",
    "schoolName": "S N Dhubhthaigh",
    "addressOne": "ANAGAIRE",
    "addressTwo": "LEITIR CEANAINN",
    "addressThree": "CO DHUN NA NGALL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19359D",
    "schoolName": "S N Naomh Aonghus",
    "addressOne": "Bunnmayne",
    "addressTwo": "Bridge End",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19411C",
    "schoolName": "S N Baile Raighin",
    "addressOne": "Letterkenny",
    "addressTwo": "Co. Donegal",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19491D",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Ramelton",
    "addressTwo": "Co. Donegal",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19518U",
    "schoolName": "S N Naomh Baoithin",
    "addressOne": "St Johnston",
    "addressTwo": "Lifford",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19614Q",
    "schoolName": "Naomh Bodain",
    "addressOne": "Culdaff",
    "addressTwo": "Lifford",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19633U",
    "schoolName": "Moyle N S",
    "addressOne": "Moyle National School",
    "addressTwo": "Newtowncunningham",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19685Q",
    "schoolName": "Scoil Chartha Naofa",
    "addressOne": "CHILL CHARTHA",
    "addressTwo": "CO DHUN NA NGALL",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19686S",
    "schoolName": "St Macartans Central",
    "addressOne": "Bundoran",
    "addressTwo": "Co. Donegal",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19756N",
    "schoolName": "St Conals",
    "addressOne": "NARIN",
    "addressTwo": "PORTNOO",
    "addressThree": "CO DONEGAL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19912B",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Glenties",
    "addressTwo": "Co. Donegal",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19927O",
    "schoolName": "Scoil Mhuire B & C",
    "addressOne": "Main Street",
    "addressTwo": "Stranorlar",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19959E",
    "schoolName": "Scoil Naomh Fionan",
    "addressOne": "Whitecastle",
    "addressTwo": "Quigley's Point",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19963S",
    "schoolName": "Scoil Aodh Rua&nuala",
    "addressOne": "Upper Main Street",
    "addressTwo": "Donegal Town",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19967D",
    "schoolName": "Scoil Iosagain",
    "addressOne": "St Mary's Road",
    "addressTwo": "Buncrana",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19971R",
    "schoolName": "Gaelscoil Adhamhnain",
    "addressOne": "Gleann Cearra",
    "addressTwo": "Leitir Ceanainn",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "20054L",
    "schoolName": "Scoil Eoghan",
    "addressOne": "Bath Terrace Lane",
    "addressTwo": "Moville",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "20096E",
    "schoolName": "Gaelscoil Na Gceithre Maistri",
    "addressOne": "Páirc an tSrutháin",
    "addressTwo": "Baile Dhún na nGall",
    "addressThree": "Co Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "20097G",
    "schoolName": "Gaelscoil Bhun Crannach",
    "addressOne": "ASCAILL AN CHAISLEAIN",
    "addressTwo": "BUN CRANNCHA",
    "addressThree": "CO. DHÚN NA nGALL",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "20144M",
    "schoolName": "Gaelscoil Chois Feabhaill",
    "addressOne": "Carn na Gairbhe",
    "addressTwo": "Bun an Phobail",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "20150H",
    "schoolName": "Holy Family National School",
    "addressOne": "East Rock",
    "addressTwo": "Ballyshannon",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "20167B",
    "schoolName": "Gaelscoil Eirne",
    "addressOne": "Fearainn an Bhaile",
    "addressTwo": "Béal Átha Seannaigh",
    "addressThree": "Co. Dhún na nGall",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "20235P",
    "schoolName": "Letterkenny Educate Together",
    "addressOne": "Kiltoy",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "20421M",
    "schoolName": "Scoil Chróine",
    "addressOne": "An Clochán Liath",
    "addressTwo": "Co. Dhún na nGall",
    "addressThree": "",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19592J",
    "schoolName": "St Bernadette Spec Sch",
    "addressOne": "College Farm Road",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19724A",
    "schoolName": "Little Angels Spec Sch",
    "addressOne": "Knocknamona",
    "addressTwo": "Letterkenny",
    "addressThree": "Co. Donegal",
    "addressFour": "",
    "county": "Donegal",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "01125B",
    "schoolName": "Leitrim Mxd N S",
    "addressOne": "Leitrim Village",
    "addressTwo": "Carrick-on-Shannon",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "08390J",
    "schoolName": "Mastersons N S",
    "addressOne": "Church Lane",
    "addressTwo": "Manorhamilton",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "08673V",
    "schoolName": "The Hunt N S",
    "addressOne": "Castle Street",
    "addressTwo": "Mohill",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "09353I",
    "schoolName": "Newtowngore N S 1",
    "addressOne": "Mullyaster",
    "addressTwo": "Newtowngore",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "13656C",
    "schoolName": "Annaduff Mxd N S",
    "addressOne": "Antfield",
    "addressTwo": "Aughamore",
    "addressThree": "Carrick on Shannon",
    "addressFour": "Co. Leitrim",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "13908D",
    "schoolName": "Ballaghameehan N S",
    "addressOne": "Conray",
    "addressTwo": "Rossinver",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "14339S",
    "schoolName": "Aughavas Ns",
    "addressOne": "Aughavas",
    "addressTwo": "Carrigallen",
    "addressThree": "Co.Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "14898I",
    "schoolName": "Drumeela N S",
    "addressOne": "Drumeela",
    "addressTwo": "Carrigallen PO",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "15116D",
    "schoolName": "Ardvarney Mxd N S",
    "addressOne": "Cashel",
    "addressTwo": "Dromahair",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "15194A",
    "schoolName": "Naomh Caillin",
    "addressOne": "Commons",
    "addressTwo": "Fenagh",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "15382B",
    "schoolName": "Drumlease N S",
    "addressOne": "Drumlease",
    "addressTwo": "Dromahair",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "15960P",
    "schoolName": "Gortletteragh Central",
    "addressOne": "FORNOCHT",
    "addressTwo": "CARA DROMA RUISC",
    "addressThree": "CO LEITRIM",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16130C",
    "schoolName": "St Josephs N S",
    "addressOne": "Killenummery",
    "addressTwo": "Dromahair",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16474L",
    "schoolName": "Carrigallen N S",
    "addressOne": "Longford Road",
    "addressTwo": "Carrigallen",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "16932P",
    "schoolName": "Aughawillan Ns",
    "addressOne": "Lisgrudy",
    "addressTwo": "Garadice",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17125O",
    "schoolName": "Differeen N S",
    "addressOne": "Diffreen",
    "addressTwo": "Manorhamilton",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17206O",
    "schoolName": "Glebe N S",
    "addressOne": "Kinlough",
    "addressTwo": "Co. Leitrim",
    "addressThree": "",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "17233R",
    "schoolName": "St Clare's National School",
    "addressOne": "Enniskillen Road",
    "addressTwo": "Manorhamilton",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18139H",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "TEARMON",
    "addressTwo": "Spencer Harbour",
    "addressThree": "Drumkeerin",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18181G",
    "schoolName": "St. Hugh's N.s.",
    "addressOne": "Dowra",
    "addressTwo": "via Carrick-on-Shannon",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18329M",
    "schoolName": "S N Naomh Mhuire",
    "addressOne": "Leckaun",
    "addressTwo": "VIA SLIGO",
    "addressThree": "",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18492A",
    "schoolName": "S N Naomh Brighid",
    "addressOne": "COILL NA GCROS",
    "addressTwo": "CARRICK ON SHANNON",
    "addressThree": "CO LEITRIM",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18589P",
    "schoolName": "S N Mhic Diarmada",
    "addressOne": "Bóthar Ghleann Fearna",
    "addressTwo": "Coillte Clochair",
    "addressThree": "Co. Liatroma",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "18741S",
    "schoolName": "Faitima N S",
    "addressOne": "CLOONE",
    "addressTwo": "CARRICK ON SHANNON",
    "addressThree": "CO LEITRIM",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19255O",
    "schoolName": "St Michaels Ns",
    "addressOne": "West Barrs",
    "addressTwo": "Glenfarne",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19360L",
    "schoolName": "Drumkeerin Central Ns",
    "addressOne": "Drumkeerin",
    "addressTwo": "Co. Leitrim",
    "addressThree": "",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19365V",
    "schoolName": "Achadh Na Sileann",
    "addressOne": "Aughnasheelin",
    "addressTwo": "Ballinamore",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19423J",
    "schoolName": "St Patricks Ns",
    "addressOne": "DRUMSHANBO",
    "addressTwo": "CO LEITRIM",
    "addressThree": "",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19519W",
    "schoolName": "St Brids",
    "addressOne": "Drumcong",
    "addressTwo": "Carrick-on-Shannon",
    "addressThree": "Co. Leitrim",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19554B",
    "schoolName": "Scoil Mhuire",
    "addressOne": "BORNACOOLA",
    "addressTwo": "CARRICK ON SHANNON",
    "addressThree": "CO LEITRIM",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "19600F",
    "schoolName": "S N Naomh Mhuire",
    "addressOne": "Drumlea",
    "addressTwo": "Carrigallen",
    "addressThree": "via Cavan",
    "addressFour": "Co. Leitrim",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "20079E",
    "schoolName": "Four Masters Ns",
    "addressOne": "Kinlough",
    "addressTwo": "Co. Leitrim",
    "addressThree": "",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "20203C",
    "schoolName": "Mohill N S",
    "addressOne": "Mohill",
    "addressTwo": "Co. Leitrim",
    "addressThree": "",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "20212D",
    "schoolName": "Gaelscoil Liatroma",
    "addressOne": "Cora Droma Rúisc",
    "addressTwo": "Co. Liatroma",
    "addressThree": "",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "20260O",
    "schoolName": "Gaelscoil Chluainín",
    "addressOne": "Sráid an Chaisleáin",
    "addressTwo": "Cluainín Uí Ruairc",
    "addressThree": "Co. Liatroma",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "20432R",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Carrick-on-Shannon",
    "addressTwo": "Co. Leitrim",
    "addressThree": "",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "20483L",
    "schoolName": "Scoil Chlann Naofa",
    "addressOne": "Ballinamore",
    "addressTwo": "Co Leitrim",
    "addressThree": "",
    "addressFour": "",
    "county": "Leitrim",
    "cecDistrict": "3"
  },
  {
    "rollNumber": "01676P",
    "schoolName": "Ballindine B N S",
    "addressOne": "Ballindine",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "02912G",
    "schoolName": "Scoil Na Gcoillini",
    "addressOne": "Meelick",
    "addressTwo": "Swinford",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "04796R",
    "schoolName": "Brackloon N S",
    "addressOne": "Brackloon",
    "addressTwo": "Westport",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "05120L",
    "schoolName": "Lehinch N S",
    "addressOne": "Lissatava",
    "addressTwo": "Hollymount",
    "addressThree": "Claremorris",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "05756K",
    "schoolName": "Burriscarra N S",
    "addressOne": "Carrownacon",
    "addressTwo": "Ballyglass",
    "addressThree": "Claremorris",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "06852L",
    "schoolName": "Garracloon N S",
    "addressOne": "Garracloon",
    "addressTwo": "Cloghans",
    "addressThree": "Ballina",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "07054L",
    "schoolName": "Cullens N S",
    "addressOne": "Faranoo",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "07075T",
    "schoolName": "S N Naomh Feichin",
    "addressOne": "Cross",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "07374G",
    "schoolName": "Ballintubber N S",
    "addressOne": "Ballintubber",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "08302J",
    "schoolName": "Holy Trinity National School",
    "addressOne": "Newport Road",
    "addressTwo": "Westport",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "09040K",
    "schoolName": "Newtownwhite Educate Together Ns",
    "addressOne": "Ballysakeery",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "11725I",
    "schoolName": "Beheymore N S",
    "addressOne": "Behymore",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "11834N",
    "schoolName": "Cloondaff N S",
    "addressOne": "Cloondaff",
    "addressTwo": "Glenhest",
    "addressThree": "Newport",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12173A",
    "schoolName": "Meelickmore N S",
    "addressOne": "Meelick More",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12206M",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "Currabaggan West",
    "addressTwo": "Knockmore",
    "addressThree": "Ballina",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12350T",
    "schoolName": "S N Na Haille",
    "addressOne": "The Neale",
    "addressTwo": "Ballinrobe",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12373I",
    "schoolName": "S N Eachleime",
    "addressOne": "Eachléim",
    "addressTwo": "Fód Dubh",
    "addressThree": "Béal an Átha",
    "addressFour": "Co. Mhaigh Eo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12467R",
    "schoolName": "Craggagh N S",
    "addressOne": "Craggagh",
    "addressTwo": "Balla",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12568A",
    "schoolName": "Sn Inbhear",
    "addressOne": "Inver",
    "addressTwo": "Barnatra",
    "addressThree": "Ballina",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12569C",
    "schoolName": "S N Ros Dumhach",
    "addressOne": "Ross Dumhach",
    "addressTwo": "Béal an Átha",
    "addressThree": "Co. Mhaigh Eo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12792F",
    "schoolName": "Saint Michaels N S",
    "addressOne": "Church Road",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12816Q",
    "schoolName": "S N Ceathru An Chlochar",
    "addressOne": "Cloonee",
    "addressTwo": "Ballinrobe",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12936D",
    "schoolName": "Cloonlyon N S",
    "addressOne": "Cloonlyon",
    "addressTwo": "Charlestown",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12938H",
    "schoolName": "S N Tamhnighan Fheadha",
    "addressOne": "Tavneena",
    "addressTwo": "Charlestown",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13080V",
    "schoolName": "Kilmovee In S",
    "addressOne": "Ballyglass",
    "addressTwo": "Kilmovee",
    "addressThree": "Ballaghadereen",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13145A",
    "schoolName": "S N Naomh Colm Cille",
    "addressOne": "The Quay",
    "addressTwo": "Westport",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13152U",
    "schoolName": "St Josephs N S",
    "addressOne": "Derrydorragh",
    "addressTwo": "Clonken",
    "addressThree": "Castlebar",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13174H",
    "schoolName": "St Columbas N.s.",
    "addressOne": "Ballyvaum",
    "addressTwo": "Inishturk",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13222P",
    "schoolName": "Sn Gleann A Chaisil",
    "addressOne": "Bunnahowen",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13225V",
    "schoolName": "Cormaic Nfa",
    "addressOne": "Killeennashask",
    "addressTwo": "Garranard",
    "addressThree": "Ballina",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13311O",
    "schoolName": "St Patricks Ns",
    "addressOne": "Clare Island",
    "addressTwo": "Westport",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13383Q",
    "schoolName": "S N An Tsraith",
    "addressOne": "Bunnahowen",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13389F",
    "schoolName": "Sn An Trian Lair",
    "addressOne": "Midfield",
    "addressTwo": "Swinford",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13444K",
    "schoolName": "S N Beal Atha Na Hein",
    "addressOne": "Ballyhean",
    "addressTwo": "Castlebar",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13500R",
    "schoolName": "Mount Pleasant N S",
    "addressOne": "Ballyglass",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13555T",
    "schoolName": "S N Faitche",
    "addressOne": "Toorgarve",
    "addressTwo": "Fahy",
    "addressThree": "Westport",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13659I",
    "schoolName": "Beacan Mixed N S",
    "addressOne": "Bekan",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13667H",
    "schoolName": "Sn Muine Chonallain",
    "addressOne": "Bonniconlon",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13684H",
    "schoolName": "Scoil Chroí Ró Naofa Beannchor Iorrais",
    "addressOne": "Srahanarry",
    "addressTwo": "Bangor Erris",
    "addressThree": "Ballina",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13758K",
    "schoolName": "Templemary N S",
    "addressOne": "Carbad Beg",
    "addressTwo": "Killala",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13773G",
    "schoolName": "S N Gort An Eadain",
    "addressOne": "Facefield",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13781F",
    "schoolName": "Breaffy N S",
    "addressOne": "Breaffy",
    "addressTwo": "Castlebar",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13797U",
    "schoolName": "Lecanvey N S",
    "addressOne": "Lecanvey",
    "addressTwo": "Westport",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13882L",
    "schoolName": "S N Gleann Na Muaidhe",
    "addressOne": "Béal an Átha",
    "addressTwo": "Co. Mhaigh Eo",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "14064F",
    "schoolName": "S N Coill An Bhaile",
    "addressOne": "Killawalla East",
    "addressTwo": "Westport",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "14188A",
    "schoolName": "Barnatra N S",
    "addressOne": "Barnatra",
    "addressTwo": "Ballina",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "14193Q",
    "schoolName": "S N Dhumha Thuama",
    "addressOne": "Dumha Thuama",
    "addressTwo": "Béal an Átha",
    "addressThree": "Co. Mhaigh Eo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "14205U",
    "schoolName": "S N Mainistir Muigheo",
    "addressOne": "Lehanagh",
    "addressTwo": "Mayo Abbey",
    "addressThree": "Claremorris",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "14258S",
    "schoolName": "Cill Mhor Iorrais",
    "addressOne": "Binghamstown",
    "addressTwo": "Belmullet",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "14290O",
    "schoolName": "Scoil Naomh Brid",
    "addressOne": "Ballycastle",
    "addressTwo": "Co. Mayo",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "14497N",
    "schoolName": "S N B Curnanool",
    "addressOne": "Cornanool",
    "addressTwo": "Castlebar",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "14534Q",
    "schoolName": "Gortjordan N S",
    "addressOne": "Kilmaine",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "14808E",
    "schoolName": "Irishtown N S",
    "addressOne": "Claremorris",
    "addressTwo": "Co. Mayo",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "14863M",
    "schoolName": "Achill Sound Convent Ns",
    "addressOne": "The Points",
    "addressTwo": "Achill Sound",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "14873P",
    "schoolName": "Dookinella N.s.",
    "addressOne": "Low River",
    "addressTwo": "Dookinella",
    "addressThree": "Keel",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15014S",
    "schoolName": "Corclough Ns",
    "addressOne": "Corclogh West",
    "addressTwo": "Belmullet",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15030Q",
    "schoolName": "St Marys N S",
    "addressOne": "Aghamore",
    "addressTwo": "Ballyhaunis",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15032U",
    "schoolName": "S N Muire Gan Smal",
    "addressOne": "Ceathrú Thaidhg",
    "addressTwo": "Béal Átha an Fheadha",
    "addressThree": "Co. Mhaigh Eo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15113U",
    "schoolName": "S N Sheamais",
    "addressOne": "Barnacogue",
    "addressTwo": "Swinford",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15257V",
    "schoolName": "Quignamanger N S",
    "addressOne": "Creggs Road",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15539I",
    "schoolName": "St Johns Ns",
    "addressOne": "Logboy",
    "addressTwo": "Tulrahan",
    "addressThree": "Claremorris",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15555G",
    "schoolName": "Scoil Cholmcille",
    "addressOne": "Breaffy",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15866A",
    "schoolName": "Carrakennedy N S",
    "addressOne": "Liscarney",
    "addressTwo": "Westport",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15967G",
    "schoolName": "Crimlin N S",
    "addressOne": "Crimlin",
    "addressTwo": "Ross",
    "addressThree": "Castlebar",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15996N",
    "schoolName": "Rathbane N S",
    "addressOne": "Curraghmore",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16021U",
    "schoolName": "Lisaniska N S",
    "addressOne": "Lisaniska East",
    "addressTwo": "Foxford",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16052I",
    "schoolName": "S N Naomh Padraig Saile",
    "addressOne": "Sáile",
    "addressTwo": "Gob a Choire",
    "addressThree": "Acaill",
    "addressFour": "Co. Mhaigh Eo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16113C",
    "schoolName": "Sn Toin Na Gaoithe",
    "addressOne": "TOIN Ré Gaoith, Acaill,",
    "addressTwo": "CATHAIR NA MART",
    "addressThree": "CO MHAIGH EO",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16122D",
    "schoolName": "Knock N S",
    "addressOne": "Knock",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16170O",
    "schoolName": "Cloghans N S",
    "addressOne": "Cloghans",
    "addressTwo": "Knockmore",
    "addressThree": "Ballina",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16173U",
    "schoolName": "Kinaffe N S",
    "addressOne": "Kinaffe",
    "addressTwo": "Swinford",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16269K",
    "schoolName": "Killasser Ns",
    "addressOne": "KILLASSER",
    "addressTwo": "SWINFORD",
    "addressThree": "CO MAYO",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16283E",
    "schoolName": "S N Pol A Tsomais",
    "addressOne": "Pol a tSomais",
    "addressTwo": "Béal an Átha",
    "addressThree": "Co. Mhaigh Eo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16289Q",
    "schoolName": "St Johns N S",
    "addressOne": "CARROWMORE",
    "addressTwo": "SWINFORD",
    "addressThree": "CO MAYO",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16379R",
    "schoolName": "Valley N S",
    "addressOne": "Dugort",
    "addressTwo": "Achill",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16562I",
    "schoolName": "Knockanillo N S",
    "addressOne": "BALLINA",
    "addressTwo": "CO MAYO",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16618J",
    "schoolName": "Myna N S",
    "addressOne": "Kilmeena",
    "addressTwo": "Westport",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16756V",
    "schoolName": "S N Brighde",
    "addressOne": "Tooreen",
    "addressTwo": "Ballyhaunis",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16780S",
    "schoolName": "Culmore N S",
    "addressOne": "SWINFORD",
    "addressTwo": "CO MAYO",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16811D",
    "schoolName": "Killala N S",
    "addressOne": "Killala",
    "addressTwo": "Co. Mayo",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16832L",
    "schoolName": "Muirisc Ns",
    "addressOne": "Murrisk",
    "addressTwo": "Westport",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16904K",
    "schoolName": "S N Lainn Cille",
    "addressOne": "CATHAIR NA MART",
    "addressTwo": "CO MAYO",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16911H",
    "schoolName": "S N Leath Ardan",
    "addressOne": "BEAL ATHA NA FHEADHA",
    "addressTwo": "CO MAYO",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16984L",
    "schoolName": "S N Naomh Sheosamh",
    "addressOne": "Shrule",
    "addressTwo": "Co. Mayo",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17039V",
    "schoolName": "S N Coill Mor",
    "addressOne": "Drummin",
    "addressTwo": "Westport",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17082W",
    "schoolName": "Scoil Chomain Naofa",
    "addressOne": "Roundfort",
    "addressTwo": "Hollymount",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17098O",
    "schoolName": "S N Tighearnain Naofa",
    "addressOne": "VIA CROSSMOLINA P.O.",
    "addressTwo": "BALLINA",
    "addressThree": "CO MAYO",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17119T",
    "schoolName": "Scoil Naisiunta Balla Aluinn",
    "addressOne": "Station Road",
    "addressTwo": "Balla",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17129W",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "RATH NA MBEACH",
    "addressTwo": "CROSSMOLINA",
    "addressThree": "CO MAYO",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17176I",
    "schoolName": "S N Realt Na Mara",
    "addressOne": "MULRANNY",
    "addressTwo": "CO MAYO",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17209U",
    "schoolName": "Cooneal N.s.",
    "addressOne": "Cooneal",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17301I",
    "schoolName": "S N Teach Caoin",
    "addressOne": "CLAR CLOINNE MHUIRIS",
    "addressTwo": "CO MAYO",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17321O",
    "schoolName": "S N An Coill Mhor",
    "addressOne": "Newport",
    "addressTwo": "Co. Mayo",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17482P",
    "schoolName": "Clogher Ns",
    "addressOne": "Clogher",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17483R",
    "schoolName": "Carraholly N S",
    "addressOne": "Rusheen",
    "addressTwo": "Carrowholly",
    "addressThree": "Westport",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17532E",
    "schoolName": "S N Druim Slaod",
    "addressOne": "Cross Hill",
    "addressTwo": "Ballycroy",
    "addressThree": "Westport",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17562N",
    "schoolName": "St Paul's National School",
    "addressOne": "ISLANDEADY",
    "addressTwo": "CASTLEBAR",
    "addressThree": "CO MAYO",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17585C",
    "schoolName": "S N Beal Caradh",
    "addressOne": "BELCARRA",
    "addressTwo": "CASTLEBAR",
    "addressThree": "CO MAYO",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17615I",
    "schoolName": "Sn Naomh Proinnsias",
    "addressOne": "Kilroe",
    "addressTwo": "Ower",
    "addressThree": "Headford",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17678J",
    "schoolName": "S N Fiondalbha",
    "addressOne": "Manulla",
    "addressTwo": "Castlebar",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17682A",
    "schoolName": "S N Colm Naofa",
    "addressOne": "COGGALE",
    "addressTwo": "WESTPORT",
    "addressThree": "CO MAYO",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17727T",
    "schoolName": "Sn Croi Muire",
    "addressOne": "Belmullet",
    "addressTwo": "Co. Mayo",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17874J",
    "schoolName": "Glencorrib N S",
    "addressOne": "Ballisnahyny",
    "addressTwo": "Shrule",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17922R",
    "schoolName": "Cloghans Hill N S",
    "addressOne": "Cloghan's Hill",
    "addressTwo": "Tuam",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17923T",
    "schoolName": "S N Beal An Mhuirthead",
    "addressOne": "Pearse Street",
    "addressTwo": "Belmullet",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18002D",
    "schoolName": "Drumgallagh N S",
    "addressOne": "Ballycroy",
    "addressTwo": "Westport",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18003F",
    "schoolName": "S N Athracht Nfa",
    "addressOne": "Charlestown",
    "addressTwo": "Co. Mayo",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18070U",
    "schoolName": "Scoil Mhuire Gan Smál",
    "addressOne": "Claremount",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18082E",
    "schoolName": "S N Dumhach",
    "addressOne": "Dooagh",
    "addressTwo": "Achill",
    "addressThree": "Westport",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18145C",
    "schoolName": "Sn Nmh Treasa",
    "addressOne": "Swinford Road",
    "addressTwo": "Kilkelly",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18503C",
    "schoolName": "S N Naomh Brid C",
    "addressOne": "OIRREAMH",
    "addressTwo": "CASTLEBAR",
    "addressThree": "CO MAYO",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18542M",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "Upper Chapel Street",
    "addressTwo": "Castlebar",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18561Q",
    "schoolName": "St Joseph's Ns",
    "addressOne": "Rehins",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18562S",
    "schoolName": "S N An Teaghlaigh",
    "addressOne": "AN PHAIRC",
    "addressTwo": "TURLOCH",
    "addressThree": "CAISLEAN AN BHARRAIGH",
    "addressFour": "CO MAYO",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18594I",
    "schoolName": "S N Achaidh An Ghlaisin",
    "addressOne": "BEAL AN MHUIRTHEAD",
    "addressTwo": "CO MHAIGH EO",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18694M",
    "schoolName": "Convent Of Mercy N S",
    "addressOne": "The Lawn",
    "addressTwo": "Castlebar",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18712L",
    "schoolName": "S N Cnoc Ruscaighe",
    "addressOne": "Knockrooskey",
    "addressTwo": "Westport",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18754E",
    "schoolName": "Sn Naomh Seosamh",
    "addressOne": "Bun an Chorraigh",
    "addressTwo": "Acaill",
    "addressThree": "Co. Mhaigh Eo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18848N",
    "schoolName": "S N Peadar Agus Pol",
    "addressOne": "Knockafall",
    "addressTwo": "Straide",
    "addressThree": "Foxford",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18876S",
    "schoolName": "Sn Baile Cuisin",
    "addressOne": "Ballycushion",
    "addressTwo": "Kilconly",
    "addressThree": "Tuam",
    "addressFour": "Co. Galway",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18880J",
    "schoolName": "S N Gort Sceiche",
    "addressOne": "Gortskehy",
    "addressTwo": "Hollymount",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18922W",
    "schoolName": "S N Chluain Luifin",
    "addressOne": "Cloonliffen",
    "addressTwo": "Ballinrobe",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19324H",
    "schoolName": "S N Teaghlaigh Naofa",
    "addressOne": "SN TEAGHLAIGH NAOFA",
    "addressTwo": "KILLEEN",
    "addressThree": "LOUISBURGH CO MAYO",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19394F",
    "schoolName": "Robeen Central N S",
    "addressOne": "Robeen",
    "addressTwo": "Hollymount",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19402B",
    "schoolName": "Ballyvary Central N S",
    "addressOne": "BALLYVARY CENTRAL N S",
    "addressTwo": "CASTLEBAR",
    "addressThree": "CO MAYO",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19451O",
    "schoolName": "Newport Central",
    "addressOne": "BAILE UI BHFIACHAIN",
    "addressTwo": "CO MHAIGH EO",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19488O",
    "schoolName": "Scoil Naomh Feichin",
    "addressOne": "Kilgellia",
    "addressTwo": "Attymass",
    "addressThree": "Ballina",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19651W",
    "schoolName": "Carracastle Central Ns",
    "addressOne": "Carracatsle",
    "addressTwo": "Ballaghadereen",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19710M",
    "schoolName": "Barnacarroll Central Ns",
    "addressOne": "Barnacarroll",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19776T",
    "schoolName": "Geesala Central School",
    "addressOne": "Geesala",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19798G",
    "schoolName": "St Colmans Ns",
    "addressOne": "Derrinabroc",
    "addressTwo": "Cloontia",
    "addressThree": "Ballymote",
    "addressFour": "Co. Sligo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19808G",
    "schoolName": "Tavrane Central Ns",
    "addressOne": "KILKELLY",
    "addressTwo": "BALLYHAUNIS",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19812U",
    "schoolName": "Foxford Central Ns",
    "addressOne": "Foxford",
    "addressTwo": "Co. Mayo",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19832D",
    "schoolName": "Scoil Raifteiri",
    "addressOne": "Faiche an Aonaigh",
    "addressTwo": "Castlebar",
    "addressThree": "CAISLEAN AN BHARRAIGH",
    "addressFour": "CO MHAIGH EO",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19903A",
    "schoolName": "Kiltimagh Central",
    "addressOne": "KILTIMAGH",
    "addressTwo": "CO MAYO",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19911W",
    "schoolName": "St Patricks Central Ns",
    "addressOne": "Kilmaine",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19914F",
    "schoolName": "Scoil Naisiunta Thola",
    "addressOne": "Bohola",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19915H",
    "schoolName": "Claremorris Boys Ns",
    "addressOne": "Kilcolman Road",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19916J",
    "schoolName": "St Peters Ns",
    "addressOne": "Snugboro",
    "addressTwo": "Castlebar",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19951L",
    "schoolName": "Swinford Ns",
    "addressOne": "Station Road",
    "addressTwo": "Swinford",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19972T",
    "schoolName": "S N Uileog De Burca",
    "addressOne": "LOCHÁN NA MBAN",
    "addressTwo": "CLÁR CHLAINNE MHUIRIS",
    "addressThree": "CO MAIGH EO",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20037L",
    "schoolName": "S N Padraig Naofa",
    "addressOne": "Louisburgh",
    "addressTwo": "Co. Mayo",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20046M",
    "schoolName": "Gaelscoil Na Cruaiche",
    "addressOne": "Bóthar an Chúrsa Gailf",
    "addressTwo": "Cathair na Mart",
    "addressThree": "Co. Mhaigh Eo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20084U",
    "schoolName": "Gaelscoil Na Gceithre Maol",
    "addressOne": "Cearnóg an Mhargaidh",
    "addressTwo": "Béal an Átha",
    "addressThree": "Co. Mhaigh Eo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20089H",
    "schoolName": "St. Josephs N. S.",
    "addressOne": "Convent Road",
    "addressTwo": "Ballinrobe",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20125I",
    "schoolName": "Crossmolina N.s.",
    "addressOne": "Crossmolina",
    "addressTwo": "Co. Mayo",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20142I",
    "schoolName": "Scoil Iosa",
    "addressOne": "Abbeyquarter",
    "addressTwo": "Ballyhaunis",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20217N",
    "schoolName": "Mount Palmer National School",
    "addressOne": "Kincon",
    "addressTwo": "Kilfian",
    "addressThree": "Ballina",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20230F",
    "schoolName": "Scoil Phadraig",
    "addressOne": "Newport Road",
    "addressTwo": "Westport",
    "addressThree": "Co Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20256A",
    "schoolName": "Scoil Náisiúnta Thuar Mhic Éadaigh",
    "addressOne": "Trianláir",
    "addressTwo": "Tuarmhicéadaigh",
    "addressThree": "Co. Mhaigh Eo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20275E",
    "schoolName": "Scoil Íosa",
    "addressOne": "Convent Hill",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20431P",
    "schoolName": "Scoil Néifinn",
    "addressOne": "Keenagh",
    "addressTwo": "Ballina",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20457K",
    "schoolName": "Castlebar Educate Together National School",
    "addressOne": "Marsh House",
    "addressTwo": "Newtown",
    "addressThree": "Castlebar",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20499D",
    "schoolName": "St Mary's National School",
    "addressOne": "Partry",
    "addressTwo": "Claremorris",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20510L",
    "schoolName": "Westport Educate Together National School",
    "addressOne": "C/O Sharkey Hill Community Centre",
    "addressTwo": "Páirc na Coille",
    "addressThree": "Westport",
    "addressFour": "Co. Mayo",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20512P",
    "schoolName": "Lacken National School",
    "addressOne": "Carrowomre-Lacken",
    "addressTwo": "Ballina",
    "addressThree": "Co Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20513R",
    "schoolName": "Kilmurry Ns",
    "addressOne": "Crossmolina",
    "addressTwo": "Co. Mayo",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19248R",
    "schoolName": "St Anthonys Special Sc",
    "addressOne": "Humbert Way",
    "addressTwo": "Castlebar",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19375B",
    "schoolName": "St Brids Special Sch",
    "addressOne": "Pavilion Road",
    "addressTwo": "Castlebar",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19387I",
    "schoolName": "St Dympnas Spec School",
    "addressOne": "Convent Hill",
    "addressTwo": "Ballina",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19773N",
    "schoolName": "St Nicholas Spec Sch",
    "addressOne": "BALLINA",
    "addressTwo": "CO MAYO",
    "addressThree": "",
    "addressFour": "",
    "county": "Mayo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "02013S",
    "schoolName": "Ballintogher N S",
    "addressOne": "Kingsfort",
    "addressTwo": "Ballintogher",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "03924S",
    "schoolName": "Owenbeg N S",
    "addressOne": "Owenbeg",
    "addressTwo": "Culleens",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "04487E",
    "schoolName": "S N Naithi Naofa",
    "addressOne": "Achonry",
    "addressTwo": "Tubbercurry",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "04802J",
    "schoolName": "Cloonacool N S",
    "addressOne": "Cloonacool",
    "addressTwo": "Tubbercurry",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "09691F",
    "schoolName": "Killeenduff N S",
    "addressOne": "Killeenduff",
    "addressTwo": "Easkey",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12140I",
    "schoolName": "St Joseph's National School",
    "addressOne": "Ballure",
    "addressTwo": "Culleens",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12171T",
    "schoolName": "Corballa N S",
    "addressOne": "Corballa",
    "addressTwo": "Via Ballina",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12537M",
    "schoolName": "Carn N S",
    "addressOne": "Gurteen",
    "addressTwo": "Ballymote",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "12767G",
    "schoolName": "S N Ronain Naofa",
    "addressOne": "Cloonloo",
    "addressTwo": "Via Boyle",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13196R",
    "schoolName": "Taunagh National School",
    "addressOne": "Riverstown",
    "addressTwo": "Co. Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13242V",
    "schoolName": "Castlerock N S",
    "addressOne": "Drummartin",
    "addressTwo": "Aclare",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13831R",
    "schoolName": "Moylough N S",
    "addressOne": "Moylough",
    "addressTwo": "Tubbercurry",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "13940W",
    "schoolName": "Enniscrone N S",
    "addressOne": "Enniscrone",
    "addressTwo": "Co. Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "14051T",
    "schoolName": "Stokane Ns",
    "addressOne": "Stokane",
    "addressTwo": "Enniscrone",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "14636B",
    "schoolName": "Carraroe N S",
    "addressOne": "Tonafortes",
    "addressTwo": "Carraroe",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15004P",
    "schoolName": "Scoil Asicus",
    "addressOne": "Golf Course Road",
    "addressTwo": "Strandhill",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15217J",
    "schoolName": "Ardkeerin N S",
    "addressOne": "Ardeeran",
    "addressTwo": "Riverstown",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15337T",
    "schoolName": "S N Mhuire",
    "addressOne": "Castlegal",
    "addressTwo": "Cliffoney",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15342M",
    "schoolName": "S N Naomh Mhuire",
    "addressOne": "Knockconor",
    "addressTwo": "Keash",
    "addressThree": "Ballymote",
    "addressFour": "Co. Sligo",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15378K",
    "schoolName": "Rockfield N S",
    "addressOne": "Coolney",
    "addressTwo": "Co. Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15431L",
    "schoolName": "Killaville N S",
    "addressOne": "Killavil",
    "addressTwo": "Ballymote",
    "addressThree": "CO. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15496Q",
    "schoolName": "Leaffoney N S",
    "addressOne": "Leaffoney",
    "addressTwo": "Kilglass",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "15571E",
    "schoolName": "Kilglass N S",
    "addressOne": "Ballyglass",
    "addressTwo": "Enniscrone",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16044J",
    "schoolName": "Kilross N S",
    "addressOne": "BALLINTOGHER",
    "addressTwo": "CO SLIGO",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16136O",
    "schoolName": "Cliffoney N S",
    "addressOne": "Ballinphull",
    "addressTwo": "Cliffoney",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16492N",
    "schoolName": "Rathlee N S",
    "addressOne": "Easkey",
    "addressTwo": "Co. Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16781U",
    "schoolName": "Coolbock N S",
    "addressOne": "RIVERSTOWN",
    "addressTwo": "VIA BOYLE",
    "addressThree": "CO SLIGO",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "16927W",
    "schoolName": "S N Naomh Sheosaimh",
    "addressOne": "Kilmactranny",
    "addressTwo": "via Boyle",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17021C",
    "schoolName": "S N Padraig Naofa",
    "addressOne": "Kilmacannon",
    "addressTwo": "Ballinfull",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17277O",
    "schoolName": "St Edwards N S",
    "addressOne": "Ballytivnan",
    "addressTwo": "Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17283J",
    "schoolName": "S N Padraig Naofa",
    "addressOne": "Geevagh",
    "addressTwo": "via Boyle",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17599N",
    "schoolName": "S N Baile An Luig",
    "addressOne": "BEAL TRA",
    "addressTwo": "SLIGEACH",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17641J",
    "schoolName": "S N Realt Na Mara",
    "addressOne": "ROSSES POINT",
    "addressTwo": "SLIGO",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17725P",
    "schoolName": "S N Bhride",
    "addressOne": "Carn",
    "addressTwo": "Moneygold",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17774F",
    "schoolName": "S N Lissara Naofa",
    "addressOne": "Ballinacarrow",
    "addressTwo": "Ballymote",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17838F",
    "schoolName": "S N Eoin Naofa",
    "addressOne": "BAILE EASA DARA",
    "addressTwo": "CO SLIGEACH",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "17967Q",
    "schoolName": "S N Mullach Rua",
    "addressOne": "Mullaghroe",
    "addressTwo": "Gurteen",
    "addressThree": "Via Boyle",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18029A",
    "schoolName": "S N Muire Gan Smal",
    "addressOne": "DRUIMEANNA",
    "addressTwo": "TUBBERCURRY",
    "addressThree": "CO SLIGO",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18053U",
    "schoolName": "Sooey N S",
    "addressOne": "Sooey",
    "addressTwo": "Boyle",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18298E",
    "schoolName": "S N Cul Fada",
    "addressOne": "Culfadda",
    "addressTwo": "Ballymote",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18334F",
    "schoolName": "S N Cnoc Mionna",
    "addressOne": "Turlaghraun",
    "addressTwo": "Ballymote",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18366S",
    "schoolName": "S N Aodain",
    "addressOne": "Hollyfield",
    "addressTwo": "Ballintrillick",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18543O",
    "schoolName": "S N Clochog",
    "addressOne": "Castlebaldwin",
    "addressTwo": "Boyle",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18575E",
    "schoolName": "S N Molaoise",
    "addressOne": "Grange",
    "addressTwo": "Co. Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18580U",
    "schoolName": "S N Muire Gan Smal",
    "addressOne": "Kilrusheighter",
    "addressTwo": "Templeboy",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18592E",
    "schoolName": "S N Naomh Iosef",
    "addressOne": "Ardkill",
    "addressTwo": "Templeboy",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18715R",
    "schoolName": "S N Pairc Ard",
    "addressOne": "DROMARD",
    "addressTwo": "CO SLIGO",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "18979F",
    "schoolName": "S N Ursula",
    "addressOne": "Strandhill Road",
    "addressTwo": "Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19389M",
    "schoolName": "Sn Muire Gan Smal",
    "addressOne": "Drumbaun",
    "addressTwo": "Curry",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19392B",
    "schoolName": "Scoil Naomh Aodain",
    "addressOne": "Monasteraden",
    "addressTwo": "via Ballaghadereen",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19408N",
    "schoolName": "Rathcormack N S",
    "addressOne": "Drumcliffe South",
    "addressTwo": "Co. Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19416M",
    "schoolName": "St Pauls Ns Collooney",
    "addressOne": "The Square",
    "addressTwo": "Collooey",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19495L",
    "schoolName": "Carbury Nat Sch",
    "addressOne": "The Mall",
    "addressTwo": "Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19635B",
    "schoolName": "Ransboro New Central",
    "addressOne": "Ransboro",
    "addressTwo": "Knocknahur",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19688W",
    "schoolName": "Dromore West Central",
    "addressOne": "Dromore West",
    "addressTwo": "Co. Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19826I",
    "schoolName": "St Brendans Ns",
    "addressOne": "Cartron",
    "addressTwo": "Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19910U",
    "schoolName": "Sligo Project School",
    "addressOne": "Abbey Quarter",
    "addressTwo": "Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19942K",
    "schoolName": "St Patricks Ns",
    "addressOne": "Colgagh",
    "addressTwo": "Calry",
    "addressThree": "Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19964U",
    "schoolName": "Scoil Mhuire Gan Smal",
    "addressOne": "Ballymote",
    "addressTwo": "Co. Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19974A",
    "schoolName": "Scoil Eoin Naofa",
    "addressOne": "Temple Street",
    "addressTwo": "Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19985F",
    "schoolName": "Our Lady Of Mercy N S",
    "addressOne": "Pearse Road",
    "addressTwo": "Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20019J",
    "schoolName": "Holy Family School",
    "addressOne": "Ballymote Road",
    "addressTwo": "Tubbercurry",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20044I",
    "schoolName": "Gaelscoil Chnoc Na Re",
    "addressOne": "BOTHAR BAILE UI DHUGAIN",
    "addressTwo": "SLIGEACH",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20113B",
    "schoolName": "Scoil Croi Naofa",
    "addressOne": "Bunnanadden",
    "addressTwo": "Co. Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20122C",
    "schoolName": "Scoil Mhuire Agus Iosaf",
    "addressOne": "Collooney",
    "addressTwo": "Co. Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "20385L",
    "schoolName": "Our Lady's National School",
    "addressOne": "Banada",
    "addressTwo": "Tourlestrane",
    "addressThree": "Co. Sligo",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19206B",
    "schoolName": "St. Cecilia's School",
    "addressOne": "Cregg",
    "addressTwo": "Sligo",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "19340F",
    "schoolName": "St Josephs Special Sch",
    "addressOne": "BALLYTIVAN",
    "addressTwo": "SLIGO",
    "addressThree": "",
    "addressFour": "",
    "county": "Sligo",
    "cecDistrict": "4"
  },
  {
    "rollNumber": "01356U",
    "schoolName": "Kilnaleck Mixed N S",
    "addressOne": "Barrack Road",
    "addressTwo": "Kilnaleck",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "05627W",
    "schoolName": "Bailieboro Model N S",
    "addressOne": "Bailieborough",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "06998Q",
    "schoolName": "St Patrick's Ns",
    "addressOne": "Corlough",
    "addressTwo": "Belturbet",
    "addressThree": "C/O Belturbet Post Office",
    "addressFour": "Co. Cavan",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "08143P",
    "schoolName": "S N Mhuire",
    "addressOne": "Bawn Road",
    "addressTwo": "Swanlibar",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "08453H",
    "schoolName": "S N Cruabanai",
    "addressOne": "Crubany",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "08490N",
    "schoolName": "St Clares Primary School",
    "addressOne": "Ard Mhuire",
    "addressTwo": "Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "08948L",
    "schoolName": "Milltown N S",
    "addressOne": "Monea",
    "addressTwo": "Milltown",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "10146K",
    "schoolName": "Corliss N S",
    "addressOne": "Corliss",
    "addressTwo": "Killeshandra",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "11205F",
    "schoolName": "Killeshandra National School ( C Of I)",
    "addressOne": "Main Street",
    "addressTwo": "Killeshandra",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "11409V",
    "schoolName": "Ballyconell Central N S",
    "addressOne": "Church Street",
    "addressTwo": "Ballyconnell",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "11517B",
    "schoolName": "Cavan 1 N S",
    "addressOne": "Farnham Street",
    "addressTwo": "Cavan",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "11541V",
    "schoolName": "Dromaili S N",
    "addressOne": "Drumelis",
    "addressTwo": "Cavan",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "12099O",
    "schoolName": "Billis N S",
    "addressOne": "New Inns",
    "addressTwo": "Ballyjamesduff",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "12312L",
    "schoolName": "Darley N S",
    "addressOne": "Monaghan Road",
    "addressTwo": "Cootehill",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "13203L",
    "schoolName": "St Patricks Mxd N S",
    "addressOne": "Loch Gowna",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "13271F",
    "schoolName": "Fairgreen N S",
    "addressOne": "Railway Road",
    "addressTwo": "Belturbet",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "14073G",
    "schoolName": "Castletara N S",
    "addressOne": "Castletara",
    "addressTwo": "Ballyhaise",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "14211P",
    "schoolName": "S N Lathrach 2",
    "addressOne": "Muff",
    "addressTwo": "Kingscourt",
    "addressThree": "Co Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "14320U",
    "schoolName": "Corlea N S",
    "addressOne": "Corlea",
    "addressTwo": "Kingscourt",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "14336M",
    "schoolName": "Searcog I N S",
    "addressOne": "Kingscourt Road",
    "addressTwo": "Shercock",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "14399N",
    "schoolName": "Killygarry N S",
    "addressOne": "Killygarry",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "14732U",
    "schoolName": "Scoil Bhride",
    "addressOne": "Mountnugent",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "15120R",
    "schoolName": "Killyconnan N S",
    "addressOne": "Stradone",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "15452T",
    "schoolName": "Kildallon N S",
    "addressOne": "Kildallan",
    "addressTwo": "Ballyconnell",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "15502I",
    "schoolName": "Killinkere N S",
    "addressOne": "Corratinner",
    "addressTwo": "Virginia",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16057S",
    "schoolName": "Convent Of Mercy N S",
    "addressOne": "Upper Bridge Street",
    "addressTwo": "Belturbet",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16083T",
    "schoolName": "Virginia Mxd N S",
    "addressOne": "Virginia",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16093W",
    "schoolName": "Ballyconnell Mxd N S",
    "addressOne": "Ballyconnell",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16316Q",
    "schoolName": "St Marys N S",
    "addressOne": "Drumalt",
    "addressTwo": "Arva",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16390F",
    "schoolName": "Scoil Bhride",
    "addressOne": "Killeshandra",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16520P",
    "schoolName": "St Mary's N.s.",
    "addressOne": "Drung",
    "addressTwo": "Ballyhaise",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16767D",
    "schoolName": "Coronea N S",
    "addressOne": "Arva",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16921K",
    "schoolName": "S N Beal Atha Na Neach",
    "addressOne": "Ballinagh",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16959M",
    "schoolName": "S N Corrabha",
    "addressOne": "Curraghvagh",
    "addressTwo": "Glangevlin",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17230L",
    "schoolName": "Scoil Naomh Brid",
    "addressOne": "Killoughter",
    "addressTwo": "Redhills",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17240O",
    "schoolName": "S N Tamhnach Dhuibh",
    "addressOne": "Tunnyduff",
    "addressTwo": "Bailieborough",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17326B",
    "schoolName": "Sn Naomh Feidhlim Boys Snr Sch",
    "addressOne": "Farnham Street",
    "addressTwo": "Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17440W",
    "schoolName": "S N Naomh Maodhog",
    "addressOne": "Currin",
    "addressTwo": "Ballyconnell",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17479D",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Lacken",
    "addressTwo": "Ballinagh",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17531C",
    "schoolName": "Baile Na Mona",
    "addressOne": "Bailieboro",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17601U",
    "schoolName": "S N Doire Na Ceise",
    "addressOne": "Maudabawn",
    "addressTwo": "Cootehill",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17625L",
    "schoolName": "Cnoc An Teampaill",
    "addressOne": "Knocktemple",
    "addressTwo": "Virginia",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17630E",
    "schoolName": "S N Maodhog",
    "addressOne": "Knockbride East",
    "addressTwo": "Bailieborough",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17780A",
    "schoolName": "Ballyhaise N S",
    "addressOne": "Cavan Road",
    "addressTwo": "Ballyhaise",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17990L",
    "schoolName": "S N Drom Cnamh",
    "addressOne": "Drumcrave",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18059J",
    "schoolName": "Bailieboro N S",
    "addressOne": "Tanderagee",
    "addressTwo": "Bailieborough",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18127A",
    "schoolName": "Sn Mhuire Boys Senior School",
    "addressOne": "Railway Road",
    "addressTwo": "Belturbet",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18340A",
    "schoolName": "S N Padraig",
    "addressOne": "Lisboduff",
    "addressTwo": "Cootehill",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18346M",
    "schoolName": "S N Greach Rathain",
    "addressOne": "Greaghrahan",
    "addressTwo": "Belturbet",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18355N",
    "schoolName": "Aughadreena National School",
    "addressOne": "Aughadreena",
    "addressTwo": "Stradone",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18393V",
    "schoolName": "S N Cillin",
    "addressOne": "Crossreagh",
    "addressTwo": "Mullagh",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18518P",
    "schoolName": "S N Corr Lorgan",
    "addressOne": "Corlurgan",
    "addressTwo": "Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18564W",
    "schoolName": "S N Leitir",
    "addressOne": "Leiter",
    "addressTwo": "Bailieborough",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18833A",
    "schoolName": "S N Lathrach",
    "addressOne": "Laragh",
    "addressTwo": "Stradone",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18857O",
    "schoolName": "S N Carraig A Bruis",
    "addressOne": "Virginia",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19202Q",
    "schoolName": "Drumkilly Ns",
    "addressOne": "Kilnaleck",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19285A",
    "schoolName": "Kill N S",
    "addressOne": "Cootehill",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19322D",
    "schoolName": "Kilmore Central N S",
    "addressOne": "Kilmore",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19326L",
    "schoolName": "Butlersbridge Ns",
    "addressOne": "Butlersbridge",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19363R",
    "schoolName": "Mullahoran Central N S",
    "addressOne": "Mullaghoran",
    "addressTwo": "Kilcogy",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19378H",
    "schoolName": "Ballynarry N S",
    "addressOne": "Ballyheelan",
    "addressTwo": "Kilnaleck",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19418Q",
    "schoolName": "Castlerahan Central Ns",
    "addressOne": "Castlerahan",
    "addressTwo": "Ballyjamesduff",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19432K",
    "schoolName": "Crosskeys Central N S",
    "addressOne": "Carrickatober",
    "addressTwo": "Crosskeys",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19527V",
    "schoolName": "Cabra Central N S",
    "addressOne": "Cabra",
    "addressTwo": "Kingscourt",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19568M",
    "schoolName": "St Patricks",
    "addressOne": "Bruskey",
    "addressTwo": "Ballinagh",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19596R",
    "schoolName": "Crosserlough N S",
    "addressOne": "Crosserlough",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19608V",
    "schoolName": "St Killians N S",
    "addressOne": "Mullagh",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19679V",
    "schoolName": "S N Aodhain Naofa",
    "addressOne": "Bawnboy",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19871N",
    "schoolName": "St Patricks N S",
    "addressOne": "Loughan",
    "addressTwo": "Blacklion",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19921C",
    "schoolName": "Drung No 2 Ns",
    "addressOne": "Doohassan",
    "addressTwo": "Drung",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19983B",
    "schoolName": "St Clares Ns",
    "addressOne": "Virginia Road",
    "addressTwo": "Ballyjamesduff",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20026G",
    "schoolName": "Gaelscoil Bhreifne",
    "addressOne": "Cnoc an Choiligh",
    "addressTwo": "An Cabhán",
    "addressThree": "Co an Chabháin",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20134J",
    "schoolName": "St Michaels N. S.",
    "addressOne": "Clifferna",
    "addressTwo": "Stradon",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20172R",
    "schoolName": "St Josephs N S",
    "addressOne": "Kingscourt",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20277I",
    "schoolName": "St Michaels National School",
    "addressOne": "Cootehill",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19439B",
    "schoolName": "Holy Family S S",
    "addressOne": "Cootehill",
    "addressTwo": "Co. Cavan",
    "addressThree": "",
    "addressFour": "",
    "county": "Cavan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "00851C",
    "schoolName": "Presentation Convent",
    "addressOne": "Ballymakenny Road",
    "addressTwo": "Drogheda",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "01434O",
    "schoolName": "S N Columcille",
    "addressOne": "Tullydonnell",
    "addressTwo": "Togher",
    "addressThree": "Drogheda",
    "addressFour": "Co. Louth",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "01553W",
    "schoolName": "St. Mochta's National School",
    "addressOne": "Louth Village",
    "addressTwo": "Co. Louth",
    "addressThree": "",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "01554B",
    "schoolName": "Baile An Phusta N S",
    "addressOne": "Ballapousta",
    "addressTwo": "Ardee",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "02322I",
    "schoolName": "St Olivers Ns",
    "addressOne": "Dundalk Street",
    "addressTwo": "Carlingford",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "02745N",
    "schoolName": "Tallonstown N S",
    "addressOne": "Tallanstown",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "02793B",
    "schoolName": "S N Mullach Bui",
    "addressOne": "Castletowncooley",
    "addressTwo": "Riverstown",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "03787L",
    "schoolName": "Walshestown N S",
    "addressOne": "Walshestown",
    "addressTwo": "Clogherhead",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "06576N",
    "schoolName": "Dromin N S",
    "addressOne": "Dromin",
    "addressTwo": "Dunleer",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "11072M",
    "schoolName": "S N Naomh Peadar",
    "addressOne": "Bolton Street",
    "addressTwo": "Drogheda",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "13670T",
    "schoolName": "Dulargy Mixed N S",
    "addressOne": "Dulargy",
    "addressTwo": "Ravensdale",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "14069P",
    "schoolName": "Dun Dealgan N S",
    "addressOne": "Jocelyn Street",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "14207B",
    "schoolName": "Sn Chill Sarain",
    "addressOne": "Kilsaran",
    "addressTwo": "Castlebellingham",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "14252G",
    "schoolName": "Callystown Mixed N S",
    "addressOne": "Callystown",
    "addressTwo": "Clogherhead",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "14578N",
    "schoolName": "Scoil Naomh Fainche",
    "addressOne": "School Lane",
    "addressTwo": "Collon",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "14651U",
    "schoolName": "Castletown Rd Convent",
    "addressOne": "Castletown Road",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "15101N",
    "schoolName": "Knockbridge Mixed N S",
    "addressOne": "Knockbridge",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "15259C",
    "schoolName": "S N N Maolmhaodhagh C",
    "addressOne": "Anne Stret",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "15260K",
    "schoolName": "S N N Maolmhaodhagh N",
    "addressOne": "Anne Street",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "15285D",
    "schoolName": "Sc Na Gcreagacha Dubha",
    "addressOne": "Sandy Lane",
    "addressTwo": "Blackrock",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16208N",
    "schoolName": "Scoil Náisíunta Naomh Feichín",
    "addressOne": "Termonfeckin",
    "addressTwo": "Drogheda",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16249E",
    "schoolName": "Bellurgan N S",
    "addressOne": "New Road",
    "addressTwo": "Bellurgan",
    "addressThree": "Dundalk",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16431Q",
    "schoolName": "S N Oilibear Beannaithe",
    "addressOne": "Stonetown",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16469S",
    "schoolName": "St Nicholas Monastery Ns",
    "addressOne": "PHILIP STREET",
    "addressTwo": "DUNDALK",
    "addressThree": "CO LOUTH",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16749B",
    "schoolName": "Scoil Mhuire Gan Smál",
    "addressOne": "Kilkerley",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16760M",
    "schoolName": "Dromiskin Mixed N S",
    "addressOne": "Dromiskin",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17059E",
    "schoolName": "Scoil Na Mbraithre Sn",
    "addressOne": "Sunday's Gate",
    "addressTwo": "Drogheda",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17124M",
    "schoolName": "Ardee Monastery",
    "addressOne": "Drogheda Road",
    "addressTwo": "Ardee",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17195M",
    "schoolName": "C.b.s. Primary",
    "addressOne": "Chapel Street",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17315T",
    "schoolName": "Scoil Dairbhre Mixed",
    "addressOne": "Readypenny",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17550G",
    "schoolName": "Scoil Fhursa",
    "addressOne": "Marlbog Road",
    "addressTwo": "Haggardstown",
    "addressThree": "Dundalk",
    "addressFour": "Co. Louth",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17726R",
    "schoolName": "S N Tulach Aluinn",
    "addressOne": "Tullyallen",
    "addressTwo": "Co. Louth",
    "addressThree": "",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17952D",
    "schoolName": "S N Naomh Fhionain",
    "addressOne": "Adamstown",
    "addressTwo": "Dunleer",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17965M",
    "schoolName": "S N Bhride",
    "addressOne": "ARD ACHAIDH",
    "addressTwo": "OMEATH",
    "addressThree": "DUNDALK",
    "addressFour": "CO. LOUTH",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18001B",
    "schoolName": "S N Naomh Lorcan",
    "addressOne": "OMEATH",
    "addressTwo": "DUNDALK",
    "addressThree": "CO LOUTH",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18019U",
    "schoolName": "S N Chaoimhin Naofa",
    "addressOne": "Philipstown",
    "addressTwo": "Dunleer",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18034Q",
    "schoolName": "S N Mhuire",
    "addressOne": "Muchgrange",
    "addressTwo": "Greenore",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18045V",
    "schoolName": "S N Muire Gan Smal",
    "addressOne": "Cartown",
    "addressTwo": "Termonfeckin",
    "addressThree": "Drogheda",
    "addressFour": "Co. Louth",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18069M",
    "schoolName": "Naomh Seosamh",
    "addressOne": "Mell",
    "addressTwo": "Drogheda",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18099V",
    "schoolName": "S N Muire Na Trocaire",
    "addressOne": "Hale Street",
    "addressTwo": "Ardee",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18101F",
    "schoolName": "S N Muire",
    "addressOne": "Rampark",
    "addressTwo": "Jenkinstown",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18126V",
    "schoolName": "S N San Nioclas",
    "addressOne": "TIGH BANAN",
    "addressTwo": "CASTLEBELLINGHAM",
    "addressThree": "CO LOUTH",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18148I",
    "schoolName": "Scoil Bhride Mixed N S",
    "addressOne": "Lann Léire",
    "addressTwo": "Ardee Road",
    "addressThree": "Dunleer",
    "addressFour": "Co. Louth",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18186Q",
    "schoolName": "Scoil Phadraig Naofa",
    "addressOne": "Carrickedmond",
    "addressTwo": "Kilcurry",
    "addressThree": "Dundalk",
    "addressFour": "Co. Louth",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18312S",
    "schoolName": "S N Talamh Na Manach",
    "addressOne": "TALAMH NA MANACH",
    "addressTwo": "CARLINGFORD",
    "addressThree": "CO LOUTH",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18347O",
    "schoolName": "S N San Nioclas",
    "addressOne": "NICHOLAS ST",
    "addressTwo": "DUNDALK",
    "addressThree": "CO LOUTH",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18382Q",
    "schoolName": "Scoil Bhride",
    "addressOne": "Shelagh",
    "addressTwo": "Hackballscross",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18391R",
    "schoolName": "Faughart Community National School",
    "addressOne": "Faughart",
    "addressTwo": "Dundalk",
    "addressThree": "",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18471P",
    "schoolName": "S N Rath Corr",
    "addressOne": "Rathcor",
    "addressTwo": "Riverstown",
    "addressThree": "Dundalk",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18504E",
    "schoolName": "S N N Maolmhaodhagh B",
    "addressOne": "Anne Street",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18635T",
    "schoolName": "S N Tigh An Iubhair",
    "addressOne": "Tenure",
    "addressTwo": "Dunleer",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19215C",
    "schoolName": "S N Ard Mhuire C",
    "addressOne": "Donore Avenue",
    "addressTwo": "Drogheda",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19223B",
    "schoolName": "S N Padraig",
    "addressOne": "Harestown",
    "addressTwo": "Monasterboice",
    "addressThree": "Drogheda",
    "addressFour": "Co. Louth",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19246N",
    "schoolName": "S N An Tslanaitheora B",
    "addressOne": "Ard Easmuinn",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19247P",
    "schoolName": "S N An Tslanaitheora C",
    "addressOne": "Ard Easmuinn Road",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19479N",
    "schoolName": "Rathmullan N S",
    "addressOne": "Marley's Lane",
    "addressTwo": "Drogheda",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19598V",
    "schoolName": "Muire Na Ngael Ns",
    "addressOne": "Bay Estate",
    "addressTwo": "Dundalik",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19673J",
    "schoolName": "St Josephs N S",
    "addressOne": "Tom Bellew Avenue",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19678T",
    "schoolName": "St Pauls Senior Ns",
    "addressOne": "Marley's Lane",
    "addressTwo": "Drogheda",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19740V",
    "schoolName": "S N Aonghusa",
    "addressOne": "GEATA AN DOMHNAIGH",
    "addressTwo": "DROICHEAD ATHA",
    "addressThree": "CO LU",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19892V",
    "schoolName": "Gaelscoil Dhun Dealgan",
    "addressOne": "Muirtheimhne Mór",
    "addressTwo": "Dún Dealgan",
    "addressThree": "Co. Lú",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20146Q",
    "schoolName": "Le Cheile Educate Together",
    "addressOne": "Mornington Road",
    "addressTwo": "Drogheda",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20163Q",
    "schoolName": "S.n Eoin Baiste",
    "addressOne": "Fatima",
    "addressTwo": "Castletown",
    "addressThree": "Dundalk",
    "addressFour": "Co. Louth",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20171P",
    "schoolName": "Ardee Educate Together N.s",
    "addressOne": "Dundalk Road",
    "addressTwo": "Ardee",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20205G",
    "schoolName": "St Marys Parish Primary School",
    "addressOne": "Dublin Road",
    "addressTwo": "Drogheda",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20259G",
    "schoolName": "St. Francis National School",
    "addressOne": "Rock Road",
    "addressTwo": "Blackrock",
    "addressThree": "",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20294I",
    "schoolName": "Aston Village Educate Together National School",
    "addressOne": "Dunlin Street",
    "addressTwo": "Aston Village",
    "addressThree": "Drogheda",
    "addressFour": "Co. Louth",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20339E",
    "schoolName": "Réalt Na Mara School",
    "addressOne": "Mill Street",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20349H",
    "schoolName": "Scoil Oilibhéir Naofa",
    "addressOne": "Ballymakenny Road",
    "addressTwo": "Drogheda",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20508B",
    "schoolName": "St Brigid's And St Patrick's National School",
    "addressOne": "Bothar Brugha",
    "addressTwo": "Drogheda",
    "addressThree": "Co Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18772G",
    "schoolName": "St Brighids Special Sch",
    "addressOne": "Ard Easmuinn",
    "addressTwo": "Dundalk",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18936K",
    "schoolName": "St Itas Special School",
    "addressOne": "Crushrod Avenue",
    "addressTwo": "Drogheda",
    "addressThree": "Co. Louth",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19214A",
    "schoolName": "St Marys Special Sch",
    "addressOne": "Drumcar",
    "addressTwo": "Co. Louth",
    "addressThree": "",
    "addressFour": "",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20374G",
    "schoolName": "Abacas School",
    "addressOne": "Drogheda ABACAS Special School",
    "addressTwo": "Congress Avenue",
    "addressThree": "Drogheda",
    "addressFour": "Co. Louth",
    "county": "Louth",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "00359V",
    "schoolName": "St. Louis Girls National School",
    "addressOne": "Park Road",
    "addressTwo": "Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "00373P",
    "schoolName": "Deravoy National School",
    "addressOne": "Deravoy",
    "addressTwo": "Emyvale",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "05501A",
    "schoolName": "Scoil Naoimh Eanna",
    "addressOne": "Killanny",
    "addressTwo": "Carrickmacross",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "06028F",
    "schoolName": "Rockcorry N S",
    "addressOne": "Rockcorry",
    "addressTwo": "Co. Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "06117E",
    "schoolName": "Doohamlet N S",
    "addressOne": "Doohamlet",
    "addressTwo": "Castleblayney",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "07751K",
    "schoolName": "Monaghan Model School",
    "addressOne": "North Road",
    "addressTwo": "Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "09186P",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Magherarney",
    "addressTwo": "Smithborough",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "10282S",
    "schoolName": "Drumacruttin N S",
    "addressOne": "Drumacrutten",
    "addressTwo": "Dunraymond",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "10429W",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Boyher",
    "addressTwo": "Rockcorry",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "10751E",
    "schoolName": "Clontibret N S",
    "addressOne": "Aghnameal",
    "addressTwo": "Clontibret",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "13632L",
    "schoolName": "Annalitten N S",
    "addressOne": "Annalitten",
    "addressTwo": "Castleblayeny",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "13811L",
    "schoolName": "Corcreagh N S",
    "addressOne": "Corcreagh",
    "addressTwo": "Shercock",
    "addressThree": "Co. Cavan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "14071C",
    "schoolName": "Drumgossett N S",
    "addressOne": "Drumgossett",
    "addressTwo": "Carrickmacross",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "15142E",
    "schoolName": "Naomh Micheal",
    "addressOne": "Donaghmoyne",
    "addressTwo": "Carrickmacross",
    "addressThree": "Co Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "15143G",
    "schoolName": "Lisdoonan N S",
    "addressOne": "Lisdonnan",
    "addressTwo": "Carrickmacross",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "15329U",
    "schoolName": "Bun Scoil Louis Naofa",
    "addressOne": "Cloughvalley",
    "addressTwo": "Carrickmacross",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "15654I",
    "schoolName": "Knockconnon N S",
    "addressOne": "Knockconan",
    "addressTwo": "Emyvale",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16129R",
    "schoolName": "Corcaghan N S",
    "addressOne": "Corcaghan",
    "addressTwo": "Stranooden",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16202B",
    "schoolName": "Castleblayney Convent National School",
    "addressOne": "Laurel Hill",
    "addressTwo": "Castleblayney",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16319W",
    "schoolName": "Castleblayney Convent Infants National School",
    "addressOne": "Laurel Hill",
    "addressTwo": "Castleblayney",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16769H",
    "schoolName": "Latnamard N S",
    "addressOne": "Latnambard",
    "addressTwo": "Smithboro",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16842O",
    "schoolName": "Annyalla National School",
    "addressOne": "Annyalla",
    "addressTwo": "Co. Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16923O",
    "schoolName": "Urbleshanny N S",
    "addressOne": "Urbleshanny",
    "addressTwo": "Scotstown",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16954C",
    "schoolName": "Scoil Mhichil",
    "addressOne": "Rackwallace",
    "addressTwo": "Castleshane",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "16968N",
    "schoolName": "S N Blaithin Iosa",
    "addressOne": "Ballaghnagearn",
    "addressTwo": "Magheracloone",
    "addressThree": "Carrickmacross",
    "addressFour": "Co. Monaghan",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17099Q",
    "schoolName": "St Joseph's National School",
    "addressOne": "Farney Street",
    "addressTwo": "Carrickmacross",
    "addressThree": "Co Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17126Q",
    "schoolName": "Scoil Enda",
    "addressOne": "Scotshouse",
    "addressTwo": "Clones",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17150N",
    "schoolName": "St Marys Bns",
    "addressOne": "St Mary's Hill",
    "addressTwo": "Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17686I",
    "schoolName": "Scoil Mhuire Bns",
    "addressOne": "Castleblayney",
    "addressTwo": "Co. Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17735S",
    "schoolName": "St Patricks Ns",
    "addressOne": "Broomfield",
    "addressTwo": "Castleblayney",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "17776J",
    "schoolName": "S N Comhghall",
    "addressOne": "Drumsloe",
    "addressTwo": "Drummully",
    "addressThree": "Clones",
    "addressFour": "Co. Monaghan",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18023L",
    "schoolName": "Aughnafarcon National School",
    "addressOne": "Aughnafarcon",
    "addressTwo": "Broomfield",
    "addressThree": "Castleblayney",
    "addressFour": "Co. Monaghan",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18028V",
    "schoolName": "Corr A Chrainn National School",
    "addressOne": "Corracrin",
    "addressTwo": "Emyvale",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18234B",
    "schoolName": "Scoil Naomh Padraig",
    "addressOne": "Oram",
    "addressTwo": "Castleblayney",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18256L",
    "schoolName": "Scoil Bhrighde",
    "addressOne": "Tyholland",
    "addressTwo": "Co. Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18401R",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Glaslough",
    "addressTwo": "Co. Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18482U",
    "schoolName": "Mhuire Gransla",
    "addressOne": "Latgallan",
    "addressTwo": "Clones",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18494E",
    "schoolName": "St Louis Infant School",
    "addressOne": "Park Road",
    "addressTwo": "Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18539A",
    "schoolName": "Scoil Naomh Mhuire",
    "addressOne": "Tullybuck",
    "addressTwo": "Clontibret",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18820O",
    "schoolName": "S N Cholmcille",
    "addressOne": "BLACKSTAFF",
    "addressTwo": "CARRAIG MHACHAIRE ROIS",
    "addressThree": "CO MHUINEACHAIN",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19036C",
    "schoolName": "Scoil Phadraig",
    "addressOne": "GARRON",
    "addressTwo": "TYHOLLAND",
    "addressThree": "CO MONAGHAN",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19162H",
    "schoolName": "Threemilehouse N S",
    "addressOne": "Threemilehouse",
    "addressTwo": "Co. Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19251G",
    "schoolName": "Scoil Naomh Deagha",
    "addressOne": "The Glebe",
    "addressTwo": "Inniskeen",
    "addressThree": "Dundalk",
    "addressFour": "Co Louth",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19362P",
    "schoolName": "St Patricks N S",
    "addressOne": "Clara",
    "addressTwo": "Killybrone",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19364T",
    "schoolName": "Killeevan Central N S",
    "addressOne": "Newbliss",
    "addressTwo": "Co. Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19434O",
    "schoolName": "Latton",
    "addressOne": "LATTON",
    "addressTwo": "CASTLEBLAYNEY",
    "addressThree": "CO. MONAGHAN",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19469K",
    "schoolName": "Naomh Oliver Plunkett",
    "addressOne": "Tossey",
    "addressTwo": "Loughmourne",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19703P",
    "schoolName": "Drumcorrin N S",
    "addressOne": "Cortober",
    "addressTwo": "Drum",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19736H",
    "schoolName": "Scoil Phadraig",
    "addressOne": "Corduff",
    "addressTwo": "Carrickmacross",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19737J",
    "schoolName": "Castleblayney Cent N S",
    "addressOne": "DRUMALISH",
    "addressTwo": "CASTLEBLAYNEY",
    "addressThree": "CO MONAGHAN",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19738L",
    "schoolName": "St Dympnas Ns",
    "addressOne": "Tedavnet",
    "addressTwo": "Co. Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19780K",
    "schoolName": "Scoil Bride",
    "addressOne": "Mullaghrafferty",
    "addressTwo": "Carrickmacross",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19857T",
    "schoolName": "Scoil Rois",
    "addressOne": "Carraig Mhachaire Rois",
    "addressTwo": "Co. Mhuineacháin",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19884W",
    "schoolName": "Ballybay Central Ns",
    "addressOne": "Ballybay",
    "addressTwo": "Co. Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "19936P",
    "schoolName": "Gaelscoil Ultain",
    "addressOne": "Cnoc an Chonnaidh",
    "addressTwo": "Muineacháin",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20055N",
    "schoolName": "Gaelscoil Éois",
    "addressOne": "Bóthar Ros Liath",
    "addressTwo": "Cluain Éois",
    "addressThree": "Co. Mhuineacháin",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20067U",
    "schoolName": "St Josephs N S",
    "addressOne": "KNOCKATALLON",
    "addressTwo": "SCOTSTOWN",
    "addressThree": "CO MONAGHAN",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20120V",
    "schoolName": "St Tiarnach's Primary School",
    "addressOne": "Roslea",
    "addressTwo": "Clones",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20174V",
    "schoolName": "Scoil Éanna",
    "addressOne": "Ballybay",
    "addressTwo": "Co. Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20246U",
    "schoolName": "The Billis National School",
    "addressOne": "Billis",
    "addressTwo": "Glaslough",
    "addressThree": "Co. Monaghan",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20327U",
    "schoolName": "Gaelscoil Lorgan",
    "addressOne": "Baile na Lorgan",
    "addressTwo": "Co. Mhuineacháin",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "20337A",
    "schoolName": "Edenmore N.s.",
    "addressOne": "Emyvale",
    "addressTwo": "Co Monaghan",
    "addressThree": "",
    "addressFour": "",
    "county": "Monaghan",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "01000E",
    "schoolName": "Esker N S",
    "addressOne": "Esker",
    "addressTwo": "Athenry",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "01013N",
    "schoolName": "Scoil Croi Iosa",
    "addressOne": "Newcastle Road",
    "addressTwo": "Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "01328P",
    "schoolName": "Kiltormer N S",
    "addressOne": "Kiltormer",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "03607G",
    "schoolName": "S N Aindreis Naofa",
    "addressOne": "Leitrim",
    "addressTwo": "Kylebrack East",
    "addressThree": "Loughrea",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "04506F",
    "schoolName": "Scoil Na Ngasur",
    "addressOne": "Maree Road",
    "addressTwo": "Oranmore",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "04515G",
    "schoolName": "Scoil An Linbh Iosa",
    "addressOne": "St Francis Street",
    "addressTwo": "Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "05754G",
    "schoolName": "S N Creachmhaoil",
    "addressOne": "Crinnage",
    "addressTwo": "Craughwell",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "06044D",
    "schoolName": "S N Cill Cuile",
    "addressOne": "Kilcooley",
    "addressTwo": "Loughrea",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "06489S",
    "schoolName": "S N An Tsaileain",
    "addressOne": "Sylane",
    "addressTwo": "Tuam",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "07455G",
    "schoolName": "Scoil Mhuire Gan Smal",
    "addressOne": "Ballygar",
    "addressTwo": "Co. Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "07551C",
    "schoolName": "Ballinderreen Mxd N S",
    "addressOne": "Ballinderreen",
    "addressTwo": "Kilcolgan",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "08379V",
    "schoolName": "Gortanumera N S",
    "addressOne": "Ballyshrule",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "08446K",
    "schoolName": "Sn Tullach Ui Chadhain",
    "addressOne": "Tulach Uí Chadhain",
    "addressTwo": "Maigh Cuilinn",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "08512U",
    "schoolName": "Iomair N S",
    "addressOne": "Killimor",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "09069L",
    "schoolName": "S N An Bhain Mhoir",
    "addressOne": "Cloghaun",
    "addressTwo": "Claregalway",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "09833W",
    "schoolName": "S N Leitirgeis",
    "addressOne": "Lettergesh West",
    "addressTwo": "Renvyle",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "10095T",
    "schoolName": "S N Naomh Treasa",
    "addressOne": "Killure",
    "addressTwo": "Ahascragh",
    "addressThree": "Ballinasloe",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "10591I",
    "schoolName": "S N An Ard Mhoir",
    "addressOne": "Aird Mhoir",
    "addressTwo": "Cill Chiarain",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "10675O",
    "schoolName": "Ballymana N S",
    "addressOne": "Ballymana",
    "addressTwo": "Craughwell",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "10863P",
    "schoolName": "S N Na Coille Glaise",
    "addressOne": "Kilglass",
    "addressTwo": "Ahascragh",
    "addressThree": "Ballinasloe",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "11261P",
    "schoolName": "Scoil Mhuire",
    "addressOne": "An Tuairín",
    "addressTwo": "Béal an Daingin",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "11290W",
    "schoolName": "S N Muire Gan Smal",
    "addressOne": "Camus",
    "addressTwo": "Co. na Gaillimhe",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "11373D",
    "schoolName": "S N Mhuire",
    "addressOne": "Turlach Beag",
    "addressTwo": "Rosmuc",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "11669B",
    "schoolName": "S N Naomh Brid",
    "addressOne": "Cloonluane",
    "addressTwo": "Renvyle",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "12095G",
    "schoolName": "S N Naomh Antoine",
    "addressOne": "Kill",
    "addressTwo": "Kingstown",
    "addressThree": "Clifden",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "12106I",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "An Chloc Breac",
    "addressTwo": "An Fhairche",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "12138V",
    "schoolName": "S N Ceathru Na Laithigh",
    "addressOne": "Brownsgrove",
    "addressTwo": "Tuam",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "12339I",
    "schoolName": "S N Inis Meadhoin",
    "addressOne": "Inis Meáin",
    "addressTwo": "Oileann Árainn",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "12502Q",
    "schoolName": "S N Eanna",
    "addressOne": "Roundstone",
    "addressTwo": "Co. Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "12574S",
    "schoolName": "Lurga N S",
    "addressOne": "Lurga",
    "addressTwo": "Gort",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "12606F",
    "schoolName": "Crumlin N S",
    "addressOne": "Crumlin",
    "addressTwo": "Ballyglunin",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "12706J",
    "schoolName": "Sn Sailearna",
    "addressOne": "Indreabhán",
    "addressTwo": "Co. na Gaillimhe",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "12782C",
    "schoolName": "Bunscoil Naomh Chuana",
    "addressOne": "Kilcoona",
    "addressTwo": "Headford",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "12946G",
    "schoolName": "S N Coilm Cille",
    "addressOne": "Ros a'Mhíl",
    "addressTwo": "Baile na hAbhann",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "12954F",
    "schoolName": "S N Bhride",
    "addressOne": "Lackagh Beg",
    "addressTwo": "Turloughmore",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13365O",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Castle Road",
    "addressTwo": "Oranmore",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13415D",
    "schoolName": "Sn Tuairini",
    "addressOne": "Tuairíní",
    "addressTwo": "Maigh Cuilinn",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13621G",
    "schoolName": "S N Muire",
    "addressOne": "Letterfrack",
    "addressTwo": "Co. Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13665D",
    "schoolName": "S N An Cillin",
    "addressOne": "Ballyshrule",
    "addressTwo": "Portumna",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13686L",
    "schoolName": "S N Naomh Iosef",
    "addressOne": "Nymphsfield",
    "addressTwo": "Cong",
    "addressThree": "Co. Mayo",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13699U",
    "schoolName": "S N Colmcille",
    "addressOne": "Leitir Móir",
    "addressTwo": "Co. na Gaillimhe",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13821O",
    "schoolName": "S N Na Naomh Uile",
    "addressOne": "Knockbrack",
    "addressTwo": "Cleggan",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13856K",
    "schoolName": "Bushy Park N S",
    "addressOne": "Circular Road",
    "addressTwo": "Bushy Park",
    "addressThree": "Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13914V",
    "schoolName": "Scoil Naomh Iosef",
    "addressOne": "Ráthún",
    "addressTwo": "Bearna",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13927H",
    "schoolName": "Inishbofin N S",
    "addressOne": "Middlequarter",
    "addressTwo": "Inishbofin",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13952G",
    "schoolName": "S N Bhride",
    "addressOne": "Leitir Caladh",
    "addressTwo": "Lettermore",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14031N",
    "schoolName": "Carnain N.s.",
    "addressOne": "Carnaun",
    "addressTwo": "Athenry",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14273O",
    "schoolName": "S N Padraig Naofa",
    "addressOne": "Lisheen",
    "addressTwo": "Williamstown",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14278B",
    "schoolName": "Scoil Naomh Padraig",
    "addressOne": "Knockroon",
    "addressTwo": "Headford",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14294W",
    "schoolName": "Brierfield N School",
    "addressOne": "Brierfield",
    "addressTwo": "Tuam",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14377D",
    "schoolName": "S N Cill Conaill",
    "addressOne": "Corraneena",
    "addressTwo": "Kilconnell",
    "addressThree": "Ballinasloe",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14383V",
    "schoolName": "S N Padraig Naofa",
    "addressOne": "Ballaghlea",
    "addressTwo": "Ballygar",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14394D",
    "schoolName": "S N Cill Fheicin",
    "addressOne": "Rakerin",
    "addressTwo": "Gort",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14420B",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "Gorteennaglogh",
    "addressTwo": "Renvyle",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14421D",
    "schoolName": "S N Ard",
    "addressOne": "Aird Thiar",
    "addressTwo": "Carna",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14448A",
    "schoolName": "Cloughanower N S",
    "addressOne": "Cloughanover",
    "addressTwo": "Headford",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14590D",
    "schoolName": "Ainbhthin Naofa",
    "addressOne": "Rosscahill East",
    "addressTwo": "Rosscahill",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14642T",
    "schoolName": "S N Mhuire",
    "addressOne": "Fiddaun",
    "addressTwo": "Ardrahan",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14712O",
    "schoolName": "Sn An Fhairche",
    "addressOne": "An Fháirche",
    "addressTwo": "via Clár Chlainne Mhuiris",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14724V",
    "schoolName": "Scoil Ronain",
    "addressOne": "Oileán Trá Bhán",
    "addressTwo": "Leitir Móir",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15027E",
    "schoolName": "S N Na Heaglaise",
    "addressOne": "Ahascragh",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15071H",
    "schoolName": "S N Cillini Dioma",
    "addressOne": "Killeendeema East",
    "addressTwo": "Loughrea",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15331H",
    "schoolName": "S N Baile Nua",
    "addressOne": "Baile Nua",
    "addressTwo": "Maigh Cuilinn",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15475I",
    "schoolName": "Kilconly N S",
    "addressOne": "Kilconly",
    "addressTwo": "Tuam",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15523Q",
    "schoolName": "S N Naomh Iosef",
    "addressOne": "Convent Road",
    "addressTwo": "Kinvara",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15796F",
    "schoolName": "S N Cor An Droma",
    "addressOne": "CLAREGALWAY",
    "addressTwo": "CO. GALWAY",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15835M",
    "schoolName": "St Brendans N S",
    "addressOne": "Mount Pleasant",
    "addressTwo": "Loughrea",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15900U",
    "schoolName": "The Glebe N.s.",
    "addressOne": "AUGHRIM",
    "addressTwo": "BALLINASLOE",
    "addressThree": "CO GALWAY",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15958F",
    "schoolName": "St. Josephs N.s.",
    "addressOne": "WOODFORD",
    "addressTwo": "LOUGHREA",
    "addressThree": "COUNTY GALWAY",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15997P",
    "schoolName": "St Brendan's N.s.",
    "addressOne": "The Square",
    "addressTwo": "Eyrecourt",
    "addressThree": "Ballinasloe",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16071M",
    "schoolName": "Scoil Chroi Naofa",
    "addressOne": "Tuam Road",
    "addressTwo": "Athenry",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16121B",
    "schoolName": "Attymon N S",
    "addressOne": "Attymon",
    "addressTwo": "Athenry",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16293H",
    "schoolName": "S N Cill Richill",
    "addressOne": "BAILE LOCHA RIABHACH",
    "addressTwo": "CO NA GAILLIMHE",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16410I",
    "schoolName": "Ballinderry N S",
    "addressOne": "Cummer",
    "addressTwo": "Tuam",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16464I",
    "schoolName": "Castleblakeney N S",
    "addressOne": "Castleblakeney",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16596C",
    "schoolName": "St Feichins N School",
    "addressOne": "ABBEY",
    "addressTwo": "LOUGHREA",
    "addressThree": "CO GALWAY",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16750J",
    "schoolName": "Parochial N S",
    "addressOne": "Waterside",
    "addressTwo": "Woodquay",
    "addressThree": "Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16762Q",
    "schoolName": "S N Ide Naofa",
    "addressOne": "Cross Street",
    "addressTwo": "Loughrea",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16804G",
    "schoolName": "Sn Chlair Na Gaillimhe",
    "addressOne": "Lakeview",
    "addressTwo": "Claregalway",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16857E",
    "schoolName": "S N Naomh Uinseann",
    "addressOne": "Coolarne",
    "addressTwo": "Turloughmore",
    "addressThree": "Athenry",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16936A",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "Cregmore",
    "addressTwo": "Claregalway",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16937C",
    "schoolName": "S N Fhursa",
    "addressOne": "Nile Lodge",
    "addressTwo": "Gaillimh",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16943U",
    "schoolName": "Niochlas N S",
    "addressOne": "Saint Dominick's Road",
    "addressTwo": "The Claddagh",
    "addressThree": "Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16982H",
    "schoolName": "S N Ath Eascrach Chuain",
    "addressOne": "Ahascragh",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17007I",
    "schoolName": "S N Ard Raithin",
    "addressOne": "Ardrahan",
    "addressTwo": "Co. Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17051L",
    "schoolName": "S N Na Fuar Coilleadh",
    "addressOne": "CRAUGHWELL",
    "addressTwo": "CO GALWAY",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17071R",
    "schoolName": "S N Baile A Mhoinin",
    "addressOne": "Castle Ffrench",
    "addressTwo": "Callinamore Bridge",
    "addressThree": "Ballinsloe",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17095I",
    "schoolName": "S N Na Cealltraighe",
    "addressOne": "Kilclare",
    "addressTwo": "Caltra",
    "addressThree": "Ballinasloe",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17118R",
    "schoolName": "S N Naomh Fhursa",
    "addressOne": "CLARAN",
    "addressTwo": "Headford",
    "addressThree": "CO NA GAILLIMHE",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17154V",
    "schoolName": "S N Breandain Naofa",
    "addressOne": "Mullagh More",
    "addressTwo": "Gurtymadden",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17170T",
    "schoolName": "Baile Mor Siol Anmcadha",
    "addressOne": "Oghil More",
    "addressTwo": "Lawrencetown",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17197Q",
    "schoolName": "S N An Leath Bhaile",
    "addressOne": "Tuam",
    "addressTwo": "Co. Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17198S",
    "schoolName": "S N Muire Gan Smal",
    "addressOne": "Shannonbridge",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17207Q",
    "schoolName": "S N Domhnach Padraig",
    "addressOne": "Beagh Beg",
    "addressTwo": "Caherlistrane",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17221K",
    "schoolName": "Sn Colmcille",
    "addressOne": "School Road",
    "addressTwo": "Castlegar",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17242S",
    "schoolName": "S N Pairc Na Slinne",
    "addressOne": "MAGH GLAS",
    "addressTwo": "BAILE LOCHA RIACH",
    "addressThree": "CO NA GAILLIMHE",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17273G",
    "schoolName": "S N Baile Na Cille",
    "addressOne": "BAILE LOCHA RIACH",
    "addressTwo": "CO NA GAILLIMHE",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17280D",
    "schoolName": "S N Baile Atha N Riogh",
    "addressOne": "Knockaunglass",
    "addressTwo": "Athenry",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17289V",
    "schoolName": "S N Caomhain",
    "addressOne": "An Trá",
    "addressTwo": "Inis Oirr",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17331R",
    "schoolName": "S N An Droma",
    "addressOne": "Ballinakill",
    "addressTwo": "Loughrea",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17368R",
    "schoolName": "Croi Ronaofa Meascaithe",
    "addressOne": "Corralough",
    "addressTwo": "Williamstown",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17444H",
    "schoolName": "S N Seosamh Naofa",
    "addressOne": "Carrabane",
    "addressTwo": "Athenry",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17456O",
    "schoolName": "S N Ronain",
    "addressOne": "Cill Ronáin",
    "addressTwo": "Inis Mór",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17463L",
    "schoolName": "S N Briocain",
    "addressOne": "AN GORT MOR",
    "addressTwo": "ROSMUC",
    "addressThree": "CO NA GAILLIMHE",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17475S",
    "schoolName": "S N Aine Naofa",
    "addressOne": "Loughcutra",
    "addressTwo": "Gort",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17485V",
    "schoolName": "S N Padraig Naofa",
    "addressOne": "Fohenagh",
    "addressTwo": "Ahascragh",
    "addressThree": "Ballinasloe",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17488E",
    "schoolName": "Sn An Aill Bhreach",
    "addressOne": "Aillebrack",
    "addressTwo": "Ballyconneely",
    "addressThree": "Co Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17490O",
    "schoolName": "S N Lorcain Naofa",
    "addressOne": "Tynagh",
    "addressTwo": "Loughrea",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17502S",
    "schoolName": "S N Naomh Thomais",
    "addressOne": "TOBAR PHEADAR",
    "addressTwo": "GAILLIMH",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17506D",
    "schoolName": "S N Breandan Naofa",
    "addressOne": "Duniry",
    "addressTwo": "Kylebrack East",
    "addressThree": "Loughrea",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17529P",
    "schoolName": "S N Iomair Naofa",
    "addressOne": "Brackloon",
    "addressTwo": "Kiltullagh",
    "addressThree": "Athenry",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17539S",
    "schoolName": "S N Cill Cruain",
    "addressOne": "SCOIL CHILL CHRUAIN",
    "addressTwo": "WILLIAMSTOWN",
    "addressThree": "CASTLEREA",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17547R",
    "schoolName": "S N Breandan Naofa",
    "addressOne": "Derryoober East",
    "addressTwo": "Woodford",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17574U",
    "schoolName": "S N Naomh Ciarain",
    "addressOne": "Cill Chiaráin",
    "addressTwo": "Conamara",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17613E",
    "schoolName": "S N Caitriona Naofa",
    "addressOne": "EACHDRUIM",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17631G",
    "schoolName": "Ceathru Na Ngarrdhanta",
    "addressOne": "Ballytrasna",
    "addressTwo": "Tuam",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17645R",
    "schoolName": "S N Cronain",
    "addressOne": "Kiltiernan",
    "addressTwo": "Kilcolgan",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17647V",
    "schoolName": "Muire Na Dea Comhairle",
    "addressOne": "Church Road",
    "addressTwo": "Headford",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17648A",
    "schoolName": "S N Breandain Naofa",
    "addressOne": "Church Road",
    "addressTwo": "Headford",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17655U",
    "schoolName": "S N Caladh Na Muc",
    "addressOne": "Carrowmoreknock",
    "addressTwo": "Rosscahill",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17660N",
    "schoolName": "S N Naomh Treasa",
    "addressOne": "Cashel",
    "addressTwo": "Co. Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17668G",
    "schoolName": "S N Na Bhforbacha",
    "addressOne": "AN SPIDEAL",
    "addressTwo": "CO NA GAILLIMHE",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17689O",
    "schoolName": "S N Tir An Fhiaidh",
    "addressOne": "Tír an Fhia",
    "addressTwo": "Leitir Móir,",
    "addressThree": "Co. Na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17759J",
    "schoolName": "S N Brighde",
    "addressOne": "Mionlach",
    "addressTwo": "An Caisleán Gearr",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17760R",
    "schoolName": "S N Baile An Leasa",
    "addressOne": "DUNMORE",
    "addressTwo": "TUAM",
    "addressThree": "CO GALWAY",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17770U",
    "schoolName": "S N Naomh Colmain",
    "addressOne": "Scoil Cholmain",
    "addressTwo": "Muigh-Inis",
    "addressThree": "Cárna",
    "addressFour": "Co. na Gaillimhe",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17771W",
    "schoolName": "S N Mhuire",
    "addressOne": "Lisheenkyle",
    "addressTwo": "Oranmore",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17772B",
    "schoolName": "S N Brighdhe Naofa",
    "addressOne": "CINN MARA",
    "addressTwo": "CO NA GAILLIMHE",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17782E",
    "schoolName": "S N Bride Naofa",
    "addressOne": "Shantalla Road",
    "addressTwo": "Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17784I",
    "schoolName": "S N Padraic Naofa",
    "addressOne": "Lombard Street",
    "addressTwo": "Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17789S",
    "schoolName": "S N Cill Tartain",
    "addressOne": "GORT INSE GUAIRE",
    "addressTwo": "CO NA GAILLIMHE",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17793J",
    "schoolName": "S N Padraig Naofa",
    "addressOne": "CLOIDEACH",
    "addressTwo": "ATH CINN",
    "addressThree": "CO NA GAILLIMHE",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17807R",
    "schoolName": "S N Cathair Geal",
    "addressOne": "Cahergal",
    "addressTwo": "Tuam",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17845C",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Brierhill School",
    "addressTwo": "Brierhill",
    "addressThree": "Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17863E",
    "schoolName": "Scoil Iarlatha Naofa",
    "addressOne": "Garbally",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17869Q",
    "schoolName": "S N Ciarain Naofa",
    "addressOne": "Cloonsh",
    "addressTwo": "Kinvara",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17870B",
    "schoolName": "Scoil Bhreandain Naofa",
    "addressOne": "CLUAIN FHEARTA",
    "addressTwo": "BEAL ATHA NA SLUAIGH",
    "addressThree": "CO NA GAILLIMHE",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17877P",
    "schoolName": "Scoil Muire Naofa",
    "addressOne": "Menlough",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17919F",
    "schoolName": "Aibhistin Naofa",
    "addressOne": "Clontuskert",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17934B",
    "schoolName": "Scoil Bhride",
    "addressOne": "AN CNOC BHREAC",
    "addressTwo": "BEAL ATHA NA SLUAIGH",
    "addressThree": "CO NA GAILLIMHE",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17980I",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Ardeevin",
    "addressTwo": "Williamstown",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18021H",
    "schoolName": "Sn An Croi Ro Naofa",
    "addressOne": "Belclare",
    "addressTwo": "Tuam",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18043R",
    "schoolName": "S N Muire Gan Smal",
    "addressOne": "Tiernascragh",
    "addressTwo": "Ballycrissane",
    "addressThree": "Ballinasloe",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18089S",
    "schoolName": "Scoil Naomh Mhuire",
    "addressOne": "Maree",
    "addressTwo": "Oranmore",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18097R",
    "schoolName": "Togala Mhuire",
    "addressOne": "Kiltullagh",
    "addressTwo": "Co. Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18111I",
    "schoolName": "S N Gort Na Gaoithe",
    "addressOne": "GORT NA GAOITHE",
    "addressTwo": "Newbridge",
    "addressThree": "Ballinasloe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18112K",
    "schoolName": "Scoil Naomh Eanna",
    "addressOne": "BULLAUN",
    "addressTwo": "LOUGHREA",
    "addressThree": "CO GALWAY",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18113M",
    "schoolName": "S N Cill Solain",
    "addressOne": "Killasolan",
    "addressTwo": "Caltra",
    "addressThree": "Ballinasloe",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18121L",
    "schoolName": "S N Mhuire",
    "addressOne": "Carna",
    "addressTwo": "Co. na Gaillimhe",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18125T",
    "schoolName": "Scoil Naomh Mhuire",
    "addressOne": "Mountbellew",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18163E",
    "schoolName": "Sn N Breandain",
    "addressOne": "EANACH DHUIN",
    "addressTwo": "COR AN DOLA",
    "addressThree": "CO NA GAILLIMHE",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18252D",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Doire Glinne",
    "addressTwo": "Sraith Salach",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18268S",
    "schoolName": "S N Cillinin",
    "addressOne": "Killeeneen",
    "addressTwo": "Craughwell",
    "addressThree": "Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18289D",
    "schoolName": "Scoil Naomh Iosef",
    "addressOne": "Castlehackett",
    "addressTwo": "Belclare",
    "addressThree": "Tuam",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18309G",
    "schoolName": "Scoil Bride",
    "addressOne": "Cooloo",
    "addressTwo": "Moylough",
    "addressThree": "Ballinasloe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18332B",
    "schoolName": "Scoil Naomh Padraig",
    "addressOne": "Moylough",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18441G",
    "schoolName": "Scoil Naomh Chuan",
    "addressOne": "Killimor",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18460K",
    "schoolName": "Sn Baile An Mhuilinn",
    "addressOne": "Milltown",
    "addressTwo": "Tuam",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18490T",
    "schoolName": "Sn M An Croi Gan Smal",
    "addressOne": "LEENANE",
    "addressTwo": "CO GALWAY",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18514H",
    "schoolName": "S N Choilm Chille",
    "addressOne": "An Tulach",
    "addressTwo": "Baile na hAbhann O.P.,",
    "addressThree": "Co na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18572V",
    "schoolName": "Glenamaddy Ns",
    "addressOne": "Kilkerrin Road",
    "addressTwo": "Glenamaddy",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18581W",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Corr na Móna",
    "addressTwo": "Co. na Gaillimhe",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18608Q",
    "schoolName": "S N Muire Gan Smal",
    "addressOne": "Claddaghduff",
    "addressTwo": "Co. Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18634R",
    "schoolName": "Scoil Ide",
    "addressOne": "Ardnamara",
    "addressTwo": "Salthill",
    "addressThree": "Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18636V",
    "schoolName": "S N Bheanain",
    "addressOne": "Tuam",
    "addressTwo": "Co. Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18686N",
    "schoolName": "S N Gort Na Leime",
    "addressOne": "DUNMORE",
    "addressTwo": "TUAM",
    "addressThree": "CO GALWAY",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18746F",
    "schoolName": "Sn Muine Mhea",
    "addressOne": "Monivea",
    "addressTwo": "Athenry",
    "addressThree": "Co Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18929N",
    "schoolName": "Scoil Naomh Einde",
    "addressOne": "Dr Mannix Road",
    "addressTwo": "Salthill",
    "addressThree": "Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19225F",
    "schoolName": "Scoil Michil Naofa",
    "addressOne": "Walter Macken Road",
    "addressTwo": "Ballybane",
    "addressThree": "Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19226H",
    "schoolName": "Scoil Na Trionoide Naofa",
    "addressOne": "Mervue",
    "addressTwo": "Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19241D",
    "schoolName": "Scoil Náisiúnta Róis",
    "addressOne": "Rosary Lane",
    "addressTwo": "Taylor's Hill",
    "addressThree": "Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19275U",
    "schoolName": "Barnaderg Central Sch",
    "addressOne": "Barnaderg",
    "addressTwo": "Tuam",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19276W",
    "schoolName": "St Colmans Mxd N S",
    "addressOne": "Cummer",
    "addressTwo": "Tuam",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19283T",
    "schoolName": "Ballymacward Central Sc",
    "addressOne": "BALLYMACWARD CENTRAL SC",
    "addressTwo": "BALLINASLOE",
    "addressThree": "CO GALWAY",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19290Q",
    "schoolName": "Ballyconeely N S",
    "addressOne": "Clifden",
    "addressTwo": "Co. Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19357W",
    "schoolName": "Sn Tir Na Cille",
    "addressOne": "Tír na Cille",
    "addressTwo": "An Mam",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19371Q",
    "schoolName": "S N Iognaid",
    "addressOne": "Bóithrín na Sliogán",
    "addressTwo": "Gaillimh",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19377F",
    "schoolName": "Naomh Colman Mac Duaigh",
    "addressOne": "Tierneevin",
    "addressTwo": "Gort",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19380R",
    "schoolName": "St Oliver Plunkett National School",
    "addressOne": "Kilkerrin",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19388K",
    "schoolName": "Clonberne Central Sch",
    "addressOne": "Clonberne",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19391W",
    "schoolName": "Garrafrauns Central Sch",
    "addressOne": "Dunmore",
    "addressTwo": "Tuam",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19401W",
    "schoolName": "S N Caitriona Sois",
    "addressOne": "Renmore Avenue",
    "addressTwo": "Renmore",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19449E",
    "schoolName": "St Oliver Plunketts Ns",
    "addressOne": "Newcastle",
    "addressTwo": "Athenry",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19468I",
    "schoolName": "Sn Caitriona Sinsear",
    "addressOne": "Renmore Avenue",
    "addressTwo": "Renmore",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19506N",
    "schoolName": "Cappatagle Central Sch",
    "addressOne": "Cappataggle",
    "addressTwo": "Ballinasloe",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19529C",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Maigh Cuilinn",
    "addressTwo": "Co. na Gaillimhe",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19544V",
    "schoolName": "Kilchreest Central Sch",
    "addressOne": "Lackabaun",
    "addressTwo": "Kilchreest",
    "addressThree": "Loughrea",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19744G",
    "schoolName": "Nioclas Naofa",
    "addressOne": "Sion Hill",
    "addressTwo": "Dunmore",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19770H",
    "schoolName": "Gurteen Central Ns",
    "addressOne": "Gurteen",
    "addressTwo": "Ballinasloe",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19795A",
    "schoolName": "Tirellan Heights N S",
    "addressOne": "Tirellan Heights",
    "addressTwo": "Headford Road",
    "addressThree": "Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19803T",
    "schoolName": "Sn Seamus Naofa",
    "addressOne": "Bearna",
    "addressTwo": "Co. na Gaillimhe",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19818J",
    "schoolName": "Creggs Central N S",
    "addressOne": "CREEGS",
    "addressTwo": "VIA ROSCOMMON",
    "addressThree": "CO GALWAY",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19828M",
    "schoolName": "Sn Cearn Mor",
    "addressOne": "Bóthar na Scoile",
    "addressTwo": "An Carn Mór",
    "addressThree": "Órán Mór",
    "addressFour": "Co. na Gaillimhe",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19841E",
    "schoolName": "Glinsk N S",
    "addressOne": "Via Castlerea",
    "addressTwo": "Co Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19858V",
    "schoolName": "Gaelscoil Dara",
    "addressOne": "Bóthar Bhaile an Locháin",
    "addressTwo": "An Rinn Mhór",
    "addressThree": "Gaillimh",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19932H",
    "schoolName": "Sn Mhic Dara",
    "addressOne": "An Ceathrú Rua",
    "addressTwo": "Co. na Gaillimhe",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19965W",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Clarinbridge",
    "addressTwo": "Co. Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19969H",
    "schoolName": "Sn Ui Cheithearnaigh",
    "addressOne": "Céide Ghearrbhaile",
    "addressTwo": "Béal Átha na Slua",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19973V",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Racecourse Road",
    "addressTwo": "Clifden",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19994G",
    "schoolName": "Gaelscoil Mhic Amhlaigh",
    "addressOne": "Lána an Mhuillearoa",
    "addressTwo": "Cnoc na Cathrach",
    "addressThree": "Gaillimh",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19996K",
    "schoolName": "St Brendans Ns",
    "addressOne": "St Brigid's Road",
    "addressTwo": "Portumna",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19997M",
    "schoolName": "Scoil Bhrige Agus Bhreandain Naofa",
    "addressOne": "Corrandulla",
    "addressTwo": "Co. Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19998O",
    "schoolName": "Gaelscoil De Hide",
    "addressOne": "Ard na Mara",
    "addressTwo": "Uarán Mór",
    "addressThree": "CO NA GAILLIMHE",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20000L",
    "schoolName": "Galway Educate Together N.s.",
    "addressOne": "Thomas Hynes Road",
    "addressTwo": "Newcastle",
    "addressThree": "Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20040A",
    "schoolName": "St Brendans N S",
    "addressOne": "BELMONT",
    "addressTwo": "CLOGHANS HILL",
    "addressThree": "TUAM",
    "addressFour": "CO GALWAY",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20042E",
    "schoolName": "Scoil An Chroi Naofa",
    "addressOne": "Society Street",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20061I",
    "schoolName": "Gaelscoil Iarfhlatha",
    "addressOne": "Tír an Chóir",
    "addressTwo": "Tuaim",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20108I",
    "schoolName": "St. John The Apostle, Knocknacarra Ns",
    "addressOne": "Western Distributor Road",
    "addressTwo": "Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20115F",
    "schoolName": "Scoil Einne",
    "addressOne": "An Spidéal",
    "addressTwo": "Co. na Gaillimhe",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20123E",
    "schoolName": "Gaelscoil Riabhach",
    "addressOne": "Cois Móna",
    "addressTwo": "Baile Locha Riach",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20199O",
    "schoolName": "Scoil Chuimín & Caitríona",
    "addressOne": "Carrowmanagh",
    "addressTwo": "Oughterard",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20210W",
    "schoolName": "S N Eoin Pol Ii",
    "addressOne": "Eoghanacht",
    "addressTwo": "Inis Mor",
    "addressThree": "Oileain Arann",
    "addressFour": "Co na Gaillimhe",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20211B",
    "schoolName": "Claregalway Educate Together N S",
    "addressOne": "Lakeview",
    "addressTwo": "Claregalway",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20237T",
    "schoolName": "Gaelscoil Riada",
    "addressOne": "Bóthar Ráithin",
    "addressTwo": "Baile Átha an Rí",
    "addressThree": "Co. na Gaillimhe",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20264W",
    "schoolName": "Gaelscoil Na Bhfili",
    "addressOne": "Gort Inse Guaire",
    "addressTwo": "Co. na Gaillimhe",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20280U",
    "schoolName": "Newtown Ns",
    "addressOne": "Newtown",
    "addressTwo": "Abbeyknockmoy",
    "addressThree": "Tuam",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20326S",
    "schoolName": "Kilcolgan Educate Together Ns",
    "addressOne": "Kilcornan",
    "addressTwo": "Clarinbridge",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20345W",
    "schoolName": "S N Leitir Meallain",
    "addressOne": "Leitir Meallain",
    "addressTwo": "Co na Gaillimhe",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20350P",
    "schoolName": "Merlin Woods Primary School",
    "addressOne": "Doughiska Road",
    "addressTwo": "Doughiska",
    "addressThree": "Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20410H",
    "schoolName": "Knocknacarra Educate Together Ns",
    "addressOne": "An Coimín Mór",
    "addressTwo": "Cappagh Road",
    "addressThree": "Knocknacarra",
    "addressFour": "Co. Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20448J",
    "schoolName": "Annagh Hill Ns",
    "addressOne": "Ballyglunin",
    "addressTwo": "Tuam",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20456I",
    "schoolName": "Tuam Educate Together National School",
    "addressOne": "Dublin Road",
    "addressTwo": "Tuam",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20462D",
    "schoolName": "Cuan Na Gaillimhe Community National School",
    "addressOne": "An Cimin Mor",
    "addressTwo": "Cappagh Road",
    "addressThree": "Knocknacarra",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20465J",
    "schoolName": "Gort National School",
    "addressOne": "Tubber Road",
    "addressTwo": "Gort",
    "addressThree": "Co Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20491K",
    "schoolName": "St Columba's National School",
    "addressOne": "Ballyturn",
    "addressTwo": "Gort",
    "addressThree": "Co Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20547L",
    "schoolName": "Trinity Primary School",
    "addressOne": "Tuam",
    "addressTwo": "Co Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19047H",
    "schoolName": "St Josephs Special Sch",
    "addressOne": "Thomas Hynes Road",
    "addressTwo": "Newcastle",
    "addressThree": "Co. Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19201O",
    "schoolName": "Lake View School",
    "addressOne": "RENMORE",
    "addressTwo": "GALWAY",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19567K",
    "schoolName": "Scoil Aine",
    "addressOne": "Merlin Park",
    "addressTwo": "Galway",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20070J",
    "schoolName": "Rosedale School",
    "addressOne": "WOODLANDS CENTRE",
    "addressTwo": "RENMORE",
    "addressThree": "GALWAY",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20328W",
    "schoolName": "St. Teresa's Special School",
    "addressOne": "c/o Ballinasloe Enterprise Centre",
    "addressTwo": "Creagh Road",
    "addressThree": "Ballinalsoe",
    "addressFour": "Co Galway",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20329B",
    "schoolName": "Tígh Nan Dooley Child Education And Development Centre",
    "addressOne": "An Cheathrú Rua",
    "addressTwo": "Co na Gaillimhe",
    "addressThree": "",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20330J",
    "schoolName": "St. Oliver's Child Education And Development Centre",
    "addressOne": "The Glebe",
    "addressTwo": "Tuam",
    "addressThree": "Co Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20371A",
    "schoolName": "Ábalta Special School",
    "addressOne": "Parkmore East Business Park",
    "addressTwo": "Parkmore",
    "addressThree": "Galway",
    "addressFour": "",
    "county": "Galway",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "01086R",
    "schoolName": "S N Pol Naofa",
    "addressOne": "Church Road",
    "addressTwo": "Castlerea",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "01607T",
    "schoolName": "S N Cor Na Fola B",
    "addressOne": "Cornafulla",
    "addressTwo": "Athlone",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "01866U",
    "schoolName": "Ballyforan Mixed N S",
    "addressOne": "Ballyforan",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "04800F",
    "schoolName": "Rooskey N S",
    "addressOne": "Rooskey",
    "addressTwo": "Carrick-on-Shannon",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "05220P",
    "schoolName": "S N Michil Naofa",
    "addressOne": "Woodbrook",
    "addressTwo": "Carrick-on-Shannon",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "06100K",
    "schoolName": "S N Cill Ronain",
    "addressOne": "Keadue West",
    "addressTwo": "Boyle",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "10967E",
    "schoolName": "Threen N S",
    "addressOne": "Trien",
    "addressTwo": "Castlerea",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "11201U",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "Scrabbagh",
    "addressTwo": "Kilmore",
    "addressThree": "Carrick-on-Shannon",
    "addressFour": "Co. Roscommon",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "11242L",
    "schoolName": "Cloonbonnif N S",
    "addressOne": "Cloonbonniffe",
    "addressTwo": "Castlerea",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "11466K",
    "schoolName": "Lismoil N S",
    "addressOne": "Ballyline",
    "addressTwo": "Curraghboy",
    "addressThree": "Athlone",
    "addressFour": "Co. Roscommon",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "11943S",
    "schoolName": "Ballinlough N S",
    "addressOne": "Ballinlough",
    "addressTwo": "Co. Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "12003V",
    "schoolName": "S N Ronain",
    "addressOne": "Castlesampson",
    "addressTwo": "Bealnamulla",
    "addressThree": "Athlone",
    "addressFour": "Co. Roscommon",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "12754U",
    "schoolName": "S N Cnoc An Samhraidh",
    "addressOne": "Crannagh",
    "addressTwo": "Summerhill",
    "addressThree": "Athlone",
    "addressFour": "Co. Roscommon",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "12964I",
    "schoolName": "S N Naomh Seosamh",
    "addressOne": "Corroy",
    "addressTwo": "Ballymurray",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13047A",
    "schoolName": "S N Lios A Cuill M",
    "addressOne": "Corracoggil North",
    "addressTwo": "Lisacul",
    "addressThree": "Castlerea",
    "addressFour": "Co. Roscommon",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13198V",
    "schoolName": "St Annes Con N S",
    "addressOne": "The Square",
    "addressTwo": "Castlerea",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13262E",
    "schoolName": "Clover Hill N S",
    "addressOne": "Lisagallan",
    "addressTwo": "Cloverhill",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13498K",
    "schoolName": "Cloonfour N S",
    "addressOne": "Cloonfour",
    "addressTwo": "Rooskey",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13757I",
    "schoolName": "Tarmon N S",
    "addressOne": "Tarmon",
    "addressTwo": "Castlerea",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13839K",
    "schoolName": "Carrick N S",
    "addressOne": "Carrick",
    "addressTwo": "Curraghboy",
    "addressThree": "Athlone",
    "addressFour": "Co. Westmeath",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13879W",
    "schoolName": "Slatta N S",
    "addressOne": "Moher",
    "addressTwo": "Kilglass",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "13978B",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "Ballyfarnon",
    "addressTwo": "Boyle",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14049J",
    "schoolName": "Whitehall N S",
    "addressOne": "Whitehall",
    "addressTwo": "Tarmonbarry",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14056G",
    "schoolName": "Mount Talbot N S",
    "addressOne": "Mount Talbot",
    "addressTwo": "Co. Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14356S",
    "schoolName": "Lisaniskey N S",
    "addressOne": "Ballymacfarrane",
    "addressTwo": "Donamon",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14684M",
    "schoolName": "Aughrim N S",
    "addressOne": "Hillstreet",
    "addressTwo": "Carrick-on-Shannon",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14925I",
    "schoolName": "S N Naomh Eoin",
    "addressOne": "Ballinameen",
    "addressTwo": "Boyle",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "14966W",
    "schoolName": "Kilteevan N S",
    "addressOne": "Kilteevan",
    "addressTwo": "Co. Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15043C",
    "schoolName": "Scoil Na Naingeal Naofa",
    "addressOne": "Carrick Road",
    "addressTwo": "Boyle",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15045G",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "Elphin Street",
    "addressTwo": "Strokestown",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15255R",
    "schoolName": "Don N S",
    "addressOne": "Ballaghadereen",
    "addressTwo": "Co. Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15308M",
    "schoolName": "Athleague N S",
    "addressOne": "Athleague",
    "addressTwo": "Co. Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15413J",
    "schoolName": "Brideswell N S",
    "addressOne": "Brideswell",
    "addressTwo": "Athlone",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15425Q",
    "schoolName": "Fairymount N S",
    "addressOne": "Fairymount",
    "addressTwo": "Castlerea",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15531P",
    "schoolName": "Scoil Mhuire,",
    "addressOne": "Newtown",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15543W",
    "schoolName": "Tibohine N S",
    "addressOne": "Teevnacreeva",
    "addressTwo": "Tibohine",
    "addressThree": "Castlerea",
    "addressFour": "Co. Roscommon",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15545D",
    "schoolName": "Castleplunkett N S",
    "addressOne": "Castleplunkett",
    "addressTwo": "Castlerea",
    "addressThree": "Co Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15557K",
    "schoolName": "Cloonfad N S",
    "addressOne": "Cloonfad",
    "addressTwo": "Ballyhaunis",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15584N",
    "schoolName": "Grange N S",
    "addressOne": "Boyle",
    "addressTwo": "Co. Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15614T",
    "schoolName": "Taughmaconnell N S",
    "addressOne": "Taughmaconnell",
    "addressTwo": "Ballinasloe",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15628H",
    "schoolName": "St Josephs B N S",
    "addressOne": "Abbeytown",
    "addressTwo": "Boyle",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15664L",
    "schoolName": "Granlahan G N S",
    "addressOne": "Granlahan",
    "addressTwo": "Ballinlough",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "15980V",
    "schoolName": "Camcloon N S",
    "addressOne": "Ballydangan",
    "addressTwo": "Athlone",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16009H",
    "schoolName": "Carrick Mixed N S",
    "addressOne": "Ballinlough",
    "addressTwo": "Co. Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16034G",
    "schoolName": "Tulsk N S",
    "addressOne": "TULSK",
    "addressTwo": "CASTLEREA",
    "addressThree": "CO ROSCOMMON",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16127N",
    "schoolName": "Gorthaganny N S",
    "addressOne": "Gortaganny",
    "addressTwo": "Loughglynn",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16291D",
    "schoolName": "Clooncagh N S",
    "addressOne": "Clooncagh",
    "addressTwo": "Stokestown",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16334S",
    "schoolName": "St. Bride's N.s.",
    "addressOne": "Ballintubber",
    "addressTwo": "Castlerea",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16427C",
    "schoolName": "Clonown N S",
    "addressOne": "Athlone",
    "addressTwo": "Co. Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "16815L",
    "schoolName": "S N Naomh Ceitheach",
    "addressOne": "Runnamoat",
    "addressTwo": "Ballymacurley",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17054R",
    "schoolName": "S N Ard Cianain",
    "addressOne": "Drum",
    "addressTwo": "Athlone",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17100V",
    "schoolName": "S N Rath Aradh",
    "addressOne": "Athleague Road",
    "addressTwo": "Rahara",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17266J",
    "schoolName": "Ballanagare N S",
    "addressOne": "Ballinagare",
    "addressTwo": "Castlerea",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17329H",
    "schoolName": "S N Paroisteach",
    "addressOne": "Boyle",
    "addressTwo": "Co. Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17353E",
    "schoolName": "S N Cill Trostain",
    "addressOne": "STROKESTOWN",
    "addressTwo": "CO ROSCOMMON",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17571O",
    "schoolName": "Dangan Ns",
    "addressOne": "Dangan",
    "addressTwo": "Kilmore",
    "addressThree": "Carrick-on-Shannon",
    "addressFour": "Co. Roscommon",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17622F",
    "schoolName": "S N Lasair Naofa",
    "addressOne": "Greaghnafarna",
    "addressTwo": "Arigna",
    "addressThree": "Carrick on Shannon",
    "addressFour": "Co. Roscommon",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17709R",
    "schoolName": "S N Naomh Mhuire",
    "addressOne": "Drumboylan",
    "addressTwo": "Leitrim P.O.",
    "addressThree": "Carrick-on-Shannon",
    "addressFour": "Co. Roscommon",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17748E",
    "schoolName": "S N Padraig Naofa",
    "addressOne": "Dúngar (Frenchpark)",
    "addressTwo": "An Caisleáin Riabhach",
    "addressThree": "Co. Ros Comáin",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17808T",
    "schoolName": "St Mary's Primary",
    "addressOne": "Lisroyne",
    "addressTwo": "Strokestown",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17849K",
    "schoolName": "S N O Dubhlain",
    "addressOne": "BALLAGH",
    "addressTwo": "KILROOSKEY",
    "addressThree": "CO ROSCOMMON",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "17904P",
    "schoolName": "S N Brusna",
    "addressOne": "Brosna",
    "addressTwo": "Ballaghadereen",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18005J",
    "schoolName": "Scoil Mhuire Gan Smal",
    "addressOne": "Feevagh",
    "addressTwo": "Dysart",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18012G",
    "schoolName": "S N Cruachain",
    "addressOne": "Carrowmore",
    "addressTwo": "Croghan",
    "addressThree": "via Boyle",
    "addressFour": "Co. Roscommon",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18027T",
    "schoolName": "Clooniquin N S",
    "addressOne": "Clooneyquinn",
    "addressTwo": "Elphin",
    "addressThree": "Co Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18061T",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Ballyleague",
    "addressTwo": "Lanesborough",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18116S",
    "schoolName": "Cloontuskert National School",
    "addressOne": "Lanesboro",
    "addressTwo": "Co. Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18165I",
    "schoolName": "Tisrara National School",
    "addressOne": "Tisrara",
    "addressTwo": "Four Roads",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18182I",
    "schoolName": "Naomh Atrachta",
    "addressOne": "KINGSLAND",
    "addressTwo": "MAINISTIR NA BUILLE",
    "addressThree": "CO ROSCOMMON",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18194P",
    "schoolName": "Ciaran Naofa",
    "addressOne": "Cooly",
    "addressTwo": "Fuerty",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18272J",
    "schoolName": "Lecarrow Community National School",
    "addressOne": "Lecarrow",
    "addressTwo": "Co. Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18395C",
    "schoolName": "S N Cluain Na Cille",
    "addressOne": "Cloonakille",
    "addressTwo": "Bealnamulla",
    "addressThree": "Athlone",
    "addressFour": "Co. Roscommon",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18432F",
    "schoolName": "S N Baile Ui Fhidhne",
    "addressOne": "Scramogue",
    "addressTwo": "CO ROSCOMMON",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18454P",
    "schoolName": "S N Naomh Treasa",
    "addressOne": "CARRAIGIN RUA",
    "addressTwo": "MAINISTIR NA BUILLE",
    "addressThree": "CO ROSCOMMON",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18536R",
    "schoolName": "S N Mhuire Lourdes",
    "addressOne": "Loughglynn",
    "addressTwo": "Castlerea",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18571T",
    "schoolName": "S N Cnoc An Chrocaire",
    "addressOne": "Knockcroghery",
    "addressTwo": "Co. Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18626S",
    "schoolName": "S N An Gleann Duibh",
    "addressOne": "Kiltoom",
    "addressTwo": "Athlone",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18729F",
    "schoolName": "Ballintleva N S",
    "addressOne": "Curraghboy",
    "addressTwo": "Athlone",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "18742U",
    "schoolName": "Roxboro N S",
    "addressOne": "Derrane",
    "addressTwo": "Co. Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19257S",
    "schoolName": "Strabaggan N S",
    "addressOne": "LOCH AILLINNE",
    "addressTwo": "CARA DROMA RUISG",
    "addressThree": "CO ROSCOMMON",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19368E",
    "schoolName": "S N Naomh Ciaran",
    "addressOne": "SCOIL NAOMH CIARAN",
    "addressTwo": "ROOTY CROSS OLDTOWN",
    "addressThree": "ATHLONE",
    "addressFour": "CO ROSCOMMON",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19534S",
    "schoolName": "Ballybay National School",
    "addressOne": "Kiltoom",
    "addressTwo": "Athlone",
    "addressThree": "Co Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19720P",
    "schoolName": "N Micheal Agus Padraig",
    "addressOne": "Clegna",
    "addressTwo": "Cootehall",
    "addressThree": "Boyle",
    "addressFour": "Co. Roscommon",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19809I",
    "schoolName": "Abbeycarton Ns",
    "addressOne": "Bishop Street",
    "addressTwo": "Elphin",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19815D",
    "schoolName": "Scoil Bhríde",
    "addressOne": "Four Mile House",
    "addressTwo": "Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19980S",
    "schoolName": "St Attractas N S",
    "addressOne": "Ballaghaderreen",
    "addressTwo": "Co. Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20126K",
    "schoolName": "Gaelscoil De Hide",
    "addressOne": "Cnoc na Crúibe",
    "addressTwo": "Bóthar na Gaillimhe",
    "addressThree": "Co. Ros Comáin",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "20498B",
    "schoolName": "St Cóman's Wood Primary School",
    "addressOne": "Roscommon Town",
    "addressTwo": "Roscommon",
    "addressThree": "",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "19789F",
    "schoolName": "Scoil Micheal Naofa",
    "addressOne": "Roselawn Drive",
    "addressTwo": "Castlerea",
    "addressThree": "Co. Roscommon",
    "addressFour": "",
    "county": "Roscommon",
    "cecDistrict": "6"
  },
  {
    "rollNumber": "01821V",
    "schoolName": "S N Rath Mor",
    "addressOne": "Rathmore",
    "addressTwo": "Naas",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "06209J",
    "schoolName": "Athy Model School",
    "addressOne": "Tomard",
    "addressTwo": "Athy",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "07790U",
    "schoolName": "Churchtown N S",
    "addressOne": "Churchtown",
    "addressTwo": "Athy",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "08099P",
    "schoolName": "St Laurences National School",
    "addressOne": "Sallins",
    "addressTwo": "Co. Kildare",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "09414C",
    "schoolName": "St Laurences N S",
    "addressOne": "Crookstown",
    "addressTwo": "Ballytore",
    "addressThree": "Athy",
    "addressFour": "Co. Kildare",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "11893G",
    "schoolName": "St Davids Ns",
    "addressOne": "Pipers Hill Educational Campus",
    "addressTwo": "Kilcullen Road",
    "addressThree": "Naas",
    "addressFour": "Co. Kildare",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "11976K",
    "schoolName": "Scoil Chóca Naofa",
    "addressOne": "Church Street",
    "addressTwo": "Kilcock",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "12182B",
    "schoolName": "St. John's National School",
    "addressOne": "Drogheda Street",
    "addressTwo": "Monasterevin",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "12998C",
    "schoolName": "S N Cianog Naofa",
    "addressOne": "Timahoe",
    "addressTwo": "Coill Dubh",
    "addressThree": "Naas",
    "addressFour": "Co. Kildare",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "13165G",
    "schoolName": "Kilberry N S",
    "addressOne": "Kilberry",
    "addressTwo": "Athy",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "13328I",
    "schoolName": "St Patricks N S",
    "addressOne": "Morristown",
    "addressTwo": "Newbridge",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "13350B",
    "schoolName": "Scoil Bride",
    "addressOne": "Athgarvan",
    "addressTwo": "The Curragh",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "13902O",
    "schoolName": "Hewetsons N S",
    "addressOne": "Millicent",
    "addressTwo": "Clane",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "14643V",
    "schoolName": "S N Na Cloiche Moire",
    "addressOne": "Ballyraggan",
    "addressTwo": "Rathvilly",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15040T",
    "schoolName": "Mercy Convent Primary School",
    "addressOne": "Sallins Road",
    "addressTwo": "Naas",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15456E",
    "schoolName": "Ballyshannon N S",
    "addressOne": "Ballyshannon",
    "addressTwo": "Kilcullen",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15769C",
    "schoolName": "Monasterevan Convent",
    "addressOne": "Drogheda Street",
    "addressTwo": "Monasterevin",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15870O",
    "schoolName": "Scoil Chonnla Phadraig",
    "addressOne": "Chapel Lane",
    "addressTwo": "Newbridge",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15957D",
    "schoolName": "Rathangan B N S",
    "addressOne": "Rathangan",
    "addressTwo": "Co. Kildare",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16302F",
    "schoolName": "St Brigids N S",
    "addressOne": "Ballysax Little",
    "addressTwo": "Curragh",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16345A",
    "schoolName": "Scoil Bhríde",
    "addressOne": "Nurney",
    "addressTwo": "Co. Kildare",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16635J",
    "schoolName": "Scoil Náisiúnta Naomh Pádraig",
    "addressOne": "CURRAGH CAMP",
    "addressTwo": "CO KILDARE",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16636L",
    "schoolName": "Curragh Camp G N S",
    "addressOne": "Curragh Camp",
    "addressTwo": "Co. Kildare",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16654N",
    "schoolName": "S N Brighde",
    "addressOne": "Milltown",
    "addressTwo": "Newbridge",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16705E",
    "schoolName": "Scoil Phadraig Naofa",
    "addressOne": "An Tom Ard",
    "addressTwo": "Baile Átha Í",
    "addressThree": "Co. Chill Dara",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16706G",
    "schoolName": "St Josephs Ns",
    "addressOne": "Highfield Park",
    "addressTwo": "Kilcock",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16707I",
    "schoolName": "Scoil Naisiunta Naomh Pheadar",
    "addressOne": "Monasterevin",
    "addressTwo": "Co. Kildare",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16777G",
    "schoolName": "S N Nmh Mhuire",
    "addressOne": "Staplestown",
    "addressTwo": "Donadea",
    "addressThree": "Naas",
    "addressFour": "Co Kildare",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16817P",
    "schoolName": "Brannoxtown Community N S",
    "addressOne": "Brannockstown",
    "addressTwo": "Naas",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16845U",
    "schoolName": "Rathcoffey N S",
    "addressOne": "School Road",
    "addressTwo": "Rathcoffey South",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17254C",
    "schoolName": "Scoil Chorbain",
    "addressOne": "Fairgreen",
    "addressTwo": "Naas",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17567A",
    "schoolName": "Almhaine N S",
    "addressOne": "Kilmeague",
    "addressTwo": "Naas",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17650K",
    "schoolName": "S N Ide",
    "addressOne": "Kilmead",
    "addressTwo": "Athy",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17662R",
    "schoolName": "S N Brighde",
    "addressOne": "Kill",
    "addressTwo": "Co. Kildare",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17674B",
    "schoolName": "St Annes National School",
    "addressOne": "Tipperstown",
    "addressTwo": "Straffan",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17714K",
    "schoolName": "S N Cill Cae",
    "addressOne": "Castledermot",
    "addressTwo": "Athy",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17787O",
    "schoolName": "S N Colmcill Naofa",
    "addressOne": "MOONE",
    "addressTwo": "ATHY",
    "addressThree": "CO KILDARE",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17872F",
    "schoolName": "St Conleths And Marys N S",
    "addressOne": "Newbridge",
    "addressTwo": "Co. Kildare",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17873H",
    "schoolName": "S N Connlaodh Naofa N",
    "addressOne": "Naas Road",
    "addressTwo": "Newbridge",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17931S",
    "schoolName": "S N Brighde",
    "addressOne": "Ticknevin",
    "addressTwo": "Carbury",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17968S",
    "schoolName": "Ursaille Naofa",
    "addressOne": "Stephenstown South",
    "addressTwo": "Two Mile House",
    "addressThree": "Naas",
    "addressFour": "Co. Kildare",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17995V",
    "schoolName": "S N Oilibhear Plunglead",
    "addressOne": "Killina",
    "addressTwo": "Carbury",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18018S",
    "schoolName": "Scoil Bhride N S",
    "addressOne": "Rathangan",
    "addressTwo": "Co. Kildare",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18031K",
    "schoolName": "S N Bride",
    "addressOne": "Suncroft",
    "addressTwo": "Curragh",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18063A",
    "schoolName": "S N Naomh Lorcain",
    "addressOne": "Levitstown",
    "addressTwo": "Maganey",
    "addressThree": "Athy",
    "addressFour": "Co. Kildare",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18092H",
    "schoolName": "S N Baile Mhic Adaim",
    "addressOne": "BAILE MHIC ADAIM",
    "addressTwo": "MAGH BHEALAIGH",
    "addressThree": "CO CILL DARA",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18093J",
    "schoolName": "S N Cloch Rinnce",
    "addressOne": "Broadford",
    "addressTwo": "Co. Kildare",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18096P",
    "schoolName": "S N Coill Dubh",
    "addressOne": "Coill Dubh",
    "addressTwo": "Naas",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18130M",
    "schoolName": "St Patricks Ns",
    "addressOne": "Johnstown Bridge",
    "addressTwo": "Enfield",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18179T",
    "schoolName": "S N Bride",
    "addressOne": "Lackagh",
    "addressTwo": "Monasterevin",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18287W",
    "schoolName": "S N Na Maighdine Mhuire",
    "addressOne": "Garrisker",
    "addressTwo": "Broadford",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18288B",
    "schoolName": "Scoil Mhichil Naofa",
    "addressOne": "Mount Hawkins",
    "addressTwo": "Athy",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18378C",
    "schoolName": "S N Naomh Ioseph",
    "addressOne": "Halverstown",
    "addressTwo": "Kilcullen",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18430B",
    "schoolName": "S N Baile Roibeaird",
    "addressOne": "Robertstown",
    "addressTwo": "Co. Kildare",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18445O",
    "schoolName": "S N Scoil Treasa",
    "addressOne": "Kilshanroe",
    "addressTwo": "Enfield",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18449W",
    "schoolName": "St Conleths N S",
    "addressOne": "Derrinturn",
    "addressTwo": "Carbury",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18515J",
    "schoolName": "Prosperous N S",
    "addressOne": "Prosperous",
    "addressTwo": "Naas",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18650P",
    "schoolName": "Newtown Ns",
    "addressOne": "Newtown",
    "addressTwo": "Enfield",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18654A",
    "schoolName": "Caragh N S",
    "addressOne": "Caragh",
    "addressTwo": "Naas",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18666H",
    "schoolName": "S N Tir Mochain",
    "addressOne": "Donadea",
    "addressTwo": "Naas",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18675I",
    "schoolName": "S N Cill Daingin",
    "addressOne": "KILDANGAN",
    "addressTwo": "MONASTEREVAN",
    "addressThree": "CO KILDARE",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19425N",
    "schoolName": "Ballyroe Central N S",
    "addressOne": "ATHY",
    "addressTwo": "CO KILDARE",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19452Q",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Ballymany",
    "addressTwo": "Newbridge",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19550Q",
    "schoolName": "Ballymany Junior Ns",
    "addressOne": "Standhouse Rd.",
    "addressTwo": "Newbridge",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19597T",
    "schoolName": "An Linbh Iosa",
    "addressOne": "Ballycane",
    "addressTwo": "Naas",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19675N",
    "schoolName": "St Brigids N S",
    "addressOne": "Kilcullen",
    "addressTwo": "Co. Kildare",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19786W",
    "schoolName": "Castledermot Ns Mxd",
    "addressOne": "Athy Road",
    "addressTwo": "Castledermot",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19796C",
    "schoolName": "St Patricks Bns",
    "addressOne": "Clane",
    "addressTwo": "Co. Kildare",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19797E",
    "schoolName": "Scoil Naisiunta Bhride",
    "addressOne": "Prosperous Road",
    "addressTwo": "Clane",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19897I",
    "schoolName": "Scoil Uí Riada",
    "addressOne": "An Bhanóg",
    "addressTwo": "Cill Choca",
    "addressThree": "Co Chill Dara",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20023A",
    "schoolName": "Gaelscoil Chill Dara",
    "addressOne": "Green Road",
    "addressTwo": "Newbridge",
    "addressThree": "Co. Chill Dara",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20087D",
    "schoolName": "Killashee Multi-denominational Ns",
    "addressOne": "KILCULLEN ROAD",
    "addressTwo": "NAAS",
    "addressThree": "CO KILDARE",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20114D",
    "schoolName": "Scoil Bríd",
    "addressOne": "Oldtown",
    "addressTwo": "Naas",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20159C",
    "schoolName": "Gaelscoil Nas Na Riogh",
    "addressOne": "Cnoc an Phíobaire",
    "addressTwo": "An Nás",
    "addressThree": "Co. Chill Dara",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20177E",
    "schoolName": "Newbridge Educate Together",
    "addressOne": "Green Road",
    "addressTwo": "The Curragh",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20192A",
    "schoolName": "Gaelscoil Átha Í",
    "addressOne": "An Tom Ard",
    "addressTwo": "Baile Átha Í",
    "addressThree": "Co. Chill Dara",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20271T",
    "schoolName": "Scoil Na Naomh Uilig",
    "addressOne": "Rickardstown",
    "addressTwo": "Newbridge",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20351R",
    "schoolName": "Naas Community National School",
    "addressOne": "Craddockstown Road",
    "addressTwo": "Naas",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20403K",
    "schoolName": "Kildare Town Educate Together",
    "addressOne": "Melitta Road",
    "addressTwo": "Kildare Town",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20427B",
    "schoolName": "St Brigid's Kildare Town Primary School",
    "addressOne": "Grey Abbey Road",
    "addressTwo": "Kildare Town",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20428D",
    "schoolName": "Gaelscoil Mhic Aodha",
    "addressOne": "Bóthar Mhelitta",
    "addressTwo": "Baile Chill Dara",
    "addressThree": "Co Chill Dara",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20505S",
    "schoolName": "Scoil Mhuire Allenwood National School",
    "addressOne": "Allenwood",
    "addressTwo": "Robertstown",
    "addressThree": "Naas",
    "addressFour": "Co Kildare",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19277B",
    "schoolName": "St Annes Special School",
    "addressOne": "Ballymany Cross",
    "addressTwo": "The Curragh",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19455W",
    "schoolName": "St Marks Special School",
    "addressOne": "PIERCETOWN",
    "addressTwo": "NEWBRIDGE",
    "addressThree": "CO KILDARE",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20376K",
    "schoolName": "Saplings Special School",
    "addressOne": "Main Street",
    "addressTwo": "Kill",
    "addressThree": "Co Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "00856M",
    "schoolName": "Scoil Naomh Micheal",
    "addressOne": "St. Mel's Road",
    "addressTwo": "Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "00860D",
    "schoolName": "Forgney N S",
    "addressOne": "Cloncallow",
    "addressTwo": "Ballymahon",
    "addressThree": "Co. Longford",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "05115S",
    "schoolName": "S N An Leana Mor",
    "addressOne": "Lenamore",
    "addressTwo": "Co. Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "07518E",
    "schoolName": "S N Cnoc An Mharcaigh",
    "addressOne": "Melview",
    "addressTwo": "Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "10223C",
    "schoolName": "Tashinny N S",
    "addressOne": "Tashinny",
    "addressTwo": "Ballymahon",
    "addressThree": "Co. Longford",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "12136R",
    "schoolName": "Scoil Bhríde",
    "addressOne": "Glen",
    "addressTwo": "Edgeworthstown",
    "addressThree": "Co. Longford",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "12683A",
    "schoolName": "St. Johns National School",
    "addressOne": "Battery Road",
    "addressTwo": "Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "12736S",
    "schoolName": "Naomh Guasachta N S",
    "addressOne": "Bunlahy",
    "addressTwo": "Ballinalee",
    "addressThree": "Co. Longford",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "12813K",
    "schoolName": "St Columbas Mxd N S",
    "addressOne": "Cloonagh",
    "addressTwo": "Dring",
    "addressThree": "Co. Longford",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "13313S",
    "schoolName": "St. John's National School,",
    "addressOne": "Ballinalee Road",
    "addressTwo": "Edgeworthstown",
    "addressThree": "Co. Longford",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "13320P",
    "schoolName": "Fermoyle Mixed N S",
    "addressOne": "Fermoyle",
    "addressTwo": "Lanesboro",
    "addressThree": "Co. Longford",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "13752V",
    "schoolName": "Naomh Padraig N S",
    "addressOne": "Muckerstaff",
    "addressTwo": "Coolarty",
    "addressThree": "Co. Longford",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "14300O",
    "schoolName": "Killasonna Mixed N S",
    "addressOne": "Killasonna",
    "addressTwo": "Granard",
    "addressThree": "Co. Longford",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "14386E",
    "schoolName": "Stonepark N S",
    "addressOne": "Stonepark",
    "addressTwo": "Co. Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "14650S",
    "schoolName": "Cloontagh Mixed N S",
    "addressOne": "Cloontagh",
    "addressTwo": "Killashee",
    "addressThree": "Co. Longford",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "14672F",
    "schoolName": "Colehill Mixed N S",
    "addressOne": "Colehill",
    "addressTwo": "Co. Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15122V",
    "schoolName": "St Bernards Mixed N S",
    "addressOne": "Abbeylara",
    "addressTwo": "Co. Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15154L",
    "schoolName": "Naomh Dominic N S",
    "addressOne": "Kenagh",
    "addressTwo": "Co. Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16665S",
    "schoolName": "St Marys Mixed N S",
    "addressOne": "Hill Street",
    "addressTwo": "Drumlish",
    "addressThree": "Co. Longford",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17561L",
    "schoolName": "Samhthann N S",
    "addressOne": "Ballinalee",
    "addressTwo": "Co. Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17644P",
    "schoolName": "Scoil Mhuire",
    "addressOne": "AUGHNAGARRON",
    "addressTwo": "GRANARD",
    "addressThree": "CO LONGFORD",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18178R",
    "schoolName": "St Josephs Convent",
    "addressOne": "Dublin Road",
    "addressTwo": "Longford Town",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18258P",
    "schoolName": "Naomh Earnain N S",
    "addressOne": "KILLASHEE",
    "addressTwo": "LONGFORD",
    "addressThree": "CO LONGFORD",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18259R",
    "schoolName": "Lanesborough Primary School",
    "addressOne": "Lanesborough",
    "addressTwo": "Co. Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18733T",
    "schoolName": "S N Mhuire",
    "addressOne": "Cloondara",
    "addressTwo": "Co. Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19171I",
    "schoolName": "S N Naomh Mhuire",
    "addressOne": "BAILE NUA AN CHAISIL",
    "addressTwo": "CO LONGPHORT",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19279F",
    "schoolName": "S N Naomh Treasa",
    "addressOne": "Clontumpher",
    "addressTwo": "Ballinalee",
    "addressThree": "Co. Longford",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19296F",
    "schoolName": "S N Naomh Colmcille",
    "addressOne": "AUGHNACLIFFE",
    "addressTwo": "CO LONGFORD",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19353O",
    "schoolName": "St Patricks Ns Longford",
    "addressOne": "Dromard",
    "addressTwo": "Moyne",
    "addressThree": "Co. Longford",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19467G",
    "schoolName": "Saint Mels",
    "addressOne": "Ardagh Village",
    "addressTwo": "Co. Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19753H",
    "schoolName": "St Emer's National School",
    "addressOne": "ST EMERS",
    "addressTwo": "TEMPLEMICHAEL",
    "addressThree": "CO LONGFORD",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19975C",
    "schoolName": "St Patricks",
    "addressOne": "BALLINAMUCK",
    "addressTwo": "CO LONGFORD",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19987J",
    "schoolName": "Scoil Mhuire N S",
    "addressOne": "Newtownforbes",
    "addressTwo": "Co. Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20083S",
    "schoolName": "Gaelscoil An Longfoirt",
    "addressOne": "Fearann Uí Dhugáin",
    "addressTwo": "An Longfort",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20101R",
    "schoolName": "The Sacred Heart Primary N.s.",
    "addressOne": "Granard",
    "addressTwo": "Co. Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20124G",
    "schoolName": "St Marys N.s.",
    "addressOne": "Edgeworthstown",
    "addressTwo": "Co. Longford",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20128O",
    "schoolName": "St Matthews Mixed N.s",
    "addressOne": "Drinan",
    "addressTwo": "Ballymahon",
    "addressThree": "Co. Longford",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19429V",
    "schoolName": "St Christophers S S",
    "addressOne": "BATTERY ROAD",
    "addressTwo": "LONGFORD",
    "addressThree": "",
    "addressFour": "",
    "county": "Longford",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "00883P",
    "schoolName": "Aine Naofa N S",
    "addressOne": "Fairgreen",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "00885T",
    "schoolName": "Ratoath Junior N S",
    "addressOne": "Fairyhouse Road",
    "addressTwo": "Ratoath",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "01309L",
    "schoolName": "Stackallen N S",
    "addressOne": "Stackallen",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "01421F",
    "schoolName": "Kilskyre Mixed N S",
    "addressOne": "Kilskyre",
    "addressTwo": "Kells",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "02905J",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "Baconstown",
    "addressTwo": "Enfield",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "03275H",
    "schoolName": "Newtown N S",
    "addressOne": "Newtown",
    "addressTwo": "Ardee",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "04210H",
    "schoolName": "Kilmessan Mxd N S",
    "addressOne": "Kilmessan",
    "addressTwo": "Co. Meath",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "05062A",
    "schoolName": "Kells Parochial N S",
    "addressOne": "Navan Road",
    "addressTwo": "Kells",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "05630L",
    "schoolName": "Scoil Mhichil Na Buachailli",
    "addressOne": "Patrick Street",
    "addressTwo": "Trim",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "07120V",
    "schoolName": "Killyon N S",
    "addressOne": "Hill of Down",
    "addressTwo": "Co. Meath",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "09238I",
    "schoolName": "Ballinlough N S",
    "addressOne": "Ballinlough",
    "addressTwo": "Kells",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "10801Q",
    "schoolName": "Drumbarragh N S",
    "addressOne": "Drumbaragh",
    "addressTwo": "Kells",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "11039O",
    "schoolName": "Kilbeg N S",
    "addressOne": "Kilbeg",
    "addressTwo": "Thomastown",
    "addressThree": "Carlanstown",
    "addressFour": "Co. Meath",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "11978O",
    "schoolName": "Scoil Mhuire Ns",
    "addressOne": "Moylagh",
    "addressTwo": "Oldcastle",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "12068D",
    "schoolName": "Our Lady Of Mercy Ns",
    "addressOne": "Jim Brunnock Road",
    "addressTwo": "Kells",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "12488C",
    "schoolName": "Gilson National School",
    "addressOne": "Chapel Street",
    "addressTwo": "Oldcastle",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "12788O",
    "schoolName": "Flowerfield N S",
    "addressOne": "Trim Road",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "12897T",
    "schoolName": "Ughtyneill N S",
    "addressOne": "Ughtyneill",
    "addressTwo": "Moynalty",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "13775K",
    "schoolName": "St Patricks N S",
    "addressOne": "St Loman's Street",
    "addressTwo": "Trim",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "14348T",
    "schoolName": "Carrickleck N S",
    "addressOne": "Carrickleck",
    "addressTwo": "Kingscourt",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15104T",
    "schoolName": "Scoil Bhríde",
    "addressOne": "Cannistown",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15483H",
    "schoolName": "St Louis N S",
    "addressOne": "Rathkenny",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16067V",
    "schoolName": "Scoil Nais Finin Naofa",
    "addressOne": "CLONARD",
    "addressTwo": "ENFIELD",
    "addressThree": "CO MEATH",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16100Q",
    "schoolName": "Mercy Convent N S",
    "addressOne": "Railway Street",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16143L",
    "schoolName": "S N Pheadair Agus Phoil",
    "addressOne": "Drumconrath",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16549Q",
    "schoolName": "Scoil Naoimh Pio",
    "addressOne": "KNOCKCOMMON",
    "addressTwo": "BEAUPARC",
    "addressThree": "CO MEATH",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16646O",
    "schoolName": "St Marys Convent N S",
    "addressOne": "Patrick Street",
    "addressTwo": "Trim",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16655P",
    "schoolName": "St. Nicholas Primary School",
    "addressOne": "Longwood",
    "addressTwo": "Co. Meath",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16722E",
    "schoolName": "Scoil Cholmcille",
    "addressOne": "Jim Brunnock Road",
    "addressTwo": "Kells",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16774A",
    "schoolName": "Scoil Eoin Báiste",
    "addressOne": "Nobber",
    "addressTwo": "Kells",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16790V",
    "schoolName": "S N Naomh Seosamh",
    "addressOne": "St. Joseph's N.S.",
    "addressTwo": "Dunsany",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16825O",
    "schoolName": "Kilbride National School",
    "addressOne": "Kilbride",
    "addressTwo": "Clonee",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16991I",
    "schoolName": "Carnaross N S",
    "addressOne": "Carnaross",
    "addressTwo": "Co. Meath",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17029S",
    "schoolName": "S N Naomh Cianain",
    "addressOne": "Cushenstown",
    "addressTwo": "Ashbourne",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17088L",
    "schoolName": "Scoil Uí Ghramhnaigh",
    "addressOne": "Ráth Chairn",
    "addressTwo": "Áth Buí",
    "addressThree": "Co.na Mí",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17123K",
    "schoolName": "Kilmainham Wood N S",
    "addressOne": "Kilmainham Wood",
    "addressTwo": "Kells",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17162U",
    "schoolName": "S N Bhrighde",
    "addressOne": "Cortown",
    "addressTwo": "Kells",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17175G",
    "schoolName": "Bohermeen N S",
    "addressOne": "Bohermeen NS",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17203I",
    "schoolName": "S N Ultain Naofa",
    "addressOne": "Baile Ghib",
    "addressTwo": "An Uaimh",
    "addressThree": "Co. na Mí",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17213L",
    "schoolName": "S N Mhuire",
    "addressOne": "MA NEALTA",
    "addressTwo": "CEANNANUS MOR",
    "addressThree": "CO NA MIDHE",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17220I",
    "schoolName": "S N Bhrighde",
    "addressOne": "Meath Hill",
    "addressTwo": "Drumconrath",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17312N",
    "schoolName": "S N Cul An Mhuilinn",
    "addressOne": "CULMULLEN",
    "addressTwo": "DRUMREE",
    "addressThree": "CO MEATH",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17480L",
    "schoolName": "S N Baile Cheant",
    "addressOne": "Kentstown",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17494W",
    "schoolName": "S N An Rath Mhor",
    "addressOne": "RATHMORE",
    "addressTwo": "ATHBOY",
    "addressThree": "CO MEATH",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17513A",
    "schoolName": "S N Cill Bhrighde",
    "addressOne": "Kilbride",
    "addressTwo": "Trim",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17520U",
    "schoolName": "S N Mhuire",
    "addressOne": "ROBINSTOWN",
    "addressTwo": "NAVAN",
    "addressThree": "CO MEATH",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17521W",
    "schoolName": "S N Colmcille",
    "addressOne": "Skryne",
    "addressTwo": "Tara",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17560J",
    "schoolName": "S N Seosamh Naomtha",
    "addressOne": "Dunderry",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17623H",
    "schoolName": "O'growney National School",
    "addressOne": "Town Parks",
    "addressTwo": "Athboy",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17629T",
    "schoolName": "S N Padraig Naofa",
    "addressOne": "Ardcath",
    "addressTwo": "Garristown",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17652O",
    "schoolName": "Scoil Oilibheir Naofa",
    "addressOne": "Kilcloon",
    "addressTwo": "Co. Meath",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17705J",
    "schoolName": "S N Cros Ban",
    "addressOne": "Whitecross",
    "addressTwo": "Julianstown",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17741N",
    "schoolName": "Scoil Nais Aitinn Bhui",
    "addressOne": "Beauparc",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17742P",
    "schoolName": "Scoil Nais Mhuire Naofa",
    "addressOne": "KILTALE",
    "addressTwo": "DUNSANY",
    "addressThree": "CO MEATH",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17821L",
    "schoolName": "St. Mary's Primary School",
    "addressOne": "Johnstown Road",
    "addressTwo": "Enfield",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17839H",
    "schoolName": "S N Bheinn Naofa B",
    "addressOne": "Duleek",
    "addressTwo": "Co. Meath",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17840P",
    "schoolName": "S N Bheinin Naofa C",
    "addressOne": "Duleek",
    "addressTwo": "Co. Meath",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17856H",
    "schoolName": "S N Dun Uabhair",
    "addressOne": "Donore",
    "addressTwo": "Drogheda",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17857J",
    "schoolName": "S N Aindreis Naofa",
    "addressOne": "Curragha",
    "addressTwo": "Ashbourne",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17930Q",
    "schoolName": "S N Seachnaill Naofa",
    "addressOne": "Main Street",
    "addressTwo": "Dunshaughlin",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17946I",
    "schoolName": "Scoil Nais Naomh Aine",
    "addressOne": "Maio",
    "addressTwo": "Tierworker",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17947K",
    "schoolName": "Scoil Naomh Bride",
    "addressOne": "Batterstown",
    "addressTwo": "Trim",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17964K",
    "schoolName": "S N Mhuire Naofa",
    "addressOne": "Rathfeigh",
    "addressTwo": "Tara",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17969U",
    "schoolName": "S N Mhuire",
    "addressOne": "Abbey Road",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17986U",
    "schoolName": "S N Nmh Sheosamh",
    "addressOne": "Boyerstown",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18016O",
    "schoolName": "S N Columbain",
    "addressOne": "Mullingar Road",
    "addressTwo": "Ballivor",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18037W",
    "schoolName": "S N Mhuire",
    "addressOne": "Heronstown",
    "addressTwo": "Lobinstown",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18040L",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "Collon Road",
    "addressTwo": "Slane",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18044T",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "Cockhill Road",
    "addressTwo": "Stamullen",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18051Q",
    "schoolName": "Coole N.s,",
    "addressOne": "GARRADICE,",
    "addressTwo": "KILCOCK,",
    "addressThree": "CO. MEATH",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18106P",
    "schoolName": "S N Na Trionoide Naofa",
    "addressOne": "Lismullen",
    "addressTwo": "Garlow Cross",
    "addressThree": "Navan",
    "addressFour": "Co. Meath",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18132Q",
    "schoolName": "S N Muire",
    "addressOne": "Carlanstown",
    "addressTwo": "Kells",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18169Q",
    "schoolName": "S N Rath Riagain",
    "addressOne": "Rathregan",
    "addressTwo": "Batterstown",
    "addressThree": "Dunboyne",
    "addressFour": "Co. Meath",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18174J",
    "schoolName": "S N Caitriona Naofa",
    "addressOne": "Oristown",
    "addressTwo": "Kells",
    "addressThree": "Co. Meath.",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18344I",
    "schoolName": "S N Mhuire",
    "addressOne": "Moynalvey",
    "addressTwo": "Summerhill",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18358T",
    "schoolName": "S N Fiach",
    "addressOne": "Ballinacree",
    "addressTwo": "Oldcastle",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18413B",
    "schoolName": "S N Naomh Treasa",
    "addressOne": "BELLEWSTOWN",
    "addressTwo": "DROGHEDA",
    "addressThree": "CO Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18429Q",
    "schoolName": "S N Mhuire",
    "addressOne": "CUL RONAIN",
    "addressTwo": "BAILE IOMHAIR",
    "addressThree": "CO MEATH",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18448U",
    "schoolName": "S N Rath Beagain",
    "addressOne": "Rathbeggan",
    "addressTwo": "Dunshaughlin",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18453N",
    "schoolName": "S N Mhichil Naofa",
    "addressOne": "CILL",
    "addressTwo": "RATH MOLLADHAIN",
    "addressThree": "CO NA MI",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18500T",
    "schoolName": "S N Naomh Colmcille",
    "addressOne": "Mount Hanover",
    "addressTwo": "Duleek",
    "addressThree": "Co Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18762D",
    "schoolName": "S N Realt Na Mara (b)",
    "addressOne": "Donacarney",
    "addressTwo": "Mornington",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "5"
  },
  {
    "rollNumber": "18767N",
    "schoolName": "S N Realt Na Mara (c)",
    "addressOne": "Donacarney",
    "addressTwo": "Co. Meath",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18887A",
    "schoolName": "St. Joseph's National School",
    "addressOne": "Mulhussey",
    "addressTwo": "Kilcock",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19018A",
    "schoolName": "Dangan Mixed N S",
    "addressOne": "Dangan",
    "addressTwo": "Summerhill",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19253K",
    "schoolName": "Scoil Naomh Barra",
    "addressOne": "Wilkinstown",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19292U",
    "schoolName": "Castletown N S",
    "addressOne": "NAVAN",
    "addressTwo": "CO MEATH",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19476H",
    "schoolName": "St Oliver Plunkett Ns",
    "addressOne": "Blackcastle",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19486K",
    "schoolName": "Scoil Nais Deaglain",
    "addressOne": "Ashbourne",
    "addressTwo": "Co. Meath",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19671F",
    "schoolName": "St Pauls N S",
    "addressOne": "Abbeylands",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19725C",
    "schoolName": "Gaelscoil Eanna",
    "addressOne": "Bóthar Áth Troim",
    "addressTwo": "An Uaimh",
    "addressThree": "Co. na Mí",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19768U",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Tudor Grove",
    "addressTwo": "Ashbourne",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19779C",
    "schoolName": "Gaelscoil Na Cille",
    "addressOne": "CILL DHEAGLAIN",
    "addressTwo": "ASHBOURNE",
    "addressThree": "CO MEATH",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19813W",
    "schoolName": "St. Dympna's National School",
    "addressOne": "Kildalkey",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19867W",
    "schoolName": "Scoil Na Rithe",
    "addressOne": "Dún Seachlainn",
    "addressTwo": "Co. na Mí",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20017F",
    "schoolName": "Scoil An Spioraid Naoimh",
    "addressOne": "Laytown",
    "addressTwo": "Co. Meath",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20032B",
    "schoolName": "Dunboyne Junior N S",
    "addressOne": "Station Road",
    "addressTwo": "Dunboyne",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20033D",
    "schoolName": "Dunboyne Senior N S",
    "addressOne": "Station Road",
    "addressTwo": "Dunboyne",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20051F",
    "schoolName": "Gaelscoil Na Boinne",
    "addressOne": "DUBLIN ROAD",
    "addressTwo": "TRIM",
    "addressThree": "CO. MEATH.",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20082Q",
    "schoolName": "Gaelscoil Thulach Na Nóg",
    "addressOne": "Bóthar Rúisc",
    "addressTwo": "Dún Búinne",
    "addressThree": "Co. na Mí",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20164S",
    "schoolName": "Navan Educate Together Ns",
    "addressOne": "Commons Road",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20179I",
    "schoolName": "St Stephens Ns",
    "addressOne": "Johnstown",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20180Q",
    "schoolName": "Scoil Naomh Eoin",
    "addressOne": "Clonmagadden Valley",
    "addressTwo": "Windtown",
    "addressThree": "Navan",
    "addressFour": "Co. Meath",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20191V",
    "schoolName": "St Peters National School",
    "addressOne": "St Peters NS, COI",
    "addressTwo": "Maynooth Rd",
    "addressThree": "Dunboyne",
    "addressFour": "Meath",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20200T",
    "schoolName": "Ratoath Senior Ns",
    "addressOne": "Fairyhouse Road",
    "addressTwo": "Ratoath",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20215J",
    "schoolName": "St Pauls Ns",
    "addressOne": "Jamestown",
    "addressTwo": "Ratoath",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20216L",
    "schoolName": "Scoil Oilibheir Naofa",
    "addressOne": "Coast Road",
    "addressTwo": "Bettystown",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20258E",
    "schoolName": "Gaelscoil An Bhradáin Feasa",
    "addressOne": "Bóthar an Mhuilinn",
    "addressTwo": "Droichead Átha",
    "addressThree": "Co. na Mí",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20352T",
    "schoolName": "Ard Rí Community National School",
    "addressOne": "Balreask Old",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20382F",
    "schoolName": "Gaelscoil Na Mí",
    "addressOne": "Campas Oideachais Chill Dhéagláin",
    "addressTwo": "Cill Dhéagláin",
    "addressThree": "Co. na Mí",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20396Q",
    "schoolName": "Ashbourne Educate Together National School",
    "addressOne": "Ashbourne Educate Together",
    "addressTwo": "Killegland",
    "addressThree": "Ashbourne",
    "addressFour": "Co. Meath",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20444B",
    "schoolName": "Trim Educate Together National School",
    "addressOne": "The Maudlins",
    "addressTwo": "Trim",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20546J",
    "schoolName": "Ashbourne Community National School",
    "addressOne": "Ashbourne",
    "addressTwo": "Co Meath",
    "addressThree": "",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19216E",
    "schoolName": "St Ultans Special Sch",
    "addressOne": "Flower Hill",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19560T",
    "schoolName": "St Marys Special School",
    "addressOne": "Johnstown",
    "addressTwo": "Navan",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20379Q",
    "schoolName": "Stepping Stones Special School",
    "addressOne": "Harristown",
    "addressTwo": "Kilcloon",
    "addressThree": "Co. Meath",
    "addressFour": "",
    "county": "Meath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "02413L",
    "schoolName": "S N Naomh Eoin",
    "addressOne": "Rath",
    "addressTwo": "Fivealley",
    "addressThree": "Birr",
    "addressFour": "Co. Offaly",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "03220F",
    "schoolName": "Mercy Primary School",
    "addressOne": "Chapel Lane",
    "addressTwo": "Birr",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "07191V",
    "schoolName": "S N Seosamh",
    "addressOne": "Blueball",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "07949I",
    "schoolName": "S N Osmann",
    "addressOne": "Model School Road",
    "addressTwo": "Birr",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "09191I",
    "schoolName": "Geashill 1 N S",
    "addressOne": "Geashill",
    "addressTwo": "Co. Offaly",
    "addressThree": "",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "10353P",
    "schoolName": "Charleville N S",
    "addressOne": "Church View",
    "addressTwo": "Tullamore",
    "addressThree": "Co Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "11203B",
    "schoolName": "Seir Kierans N S",
    "addressOne": "Clareen",
    "addressTwo": "Birr",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "12294M",
    "schoolName": "St Cronans Mixed N S",
    "addressOne": "Lusmagh",
    "addressTwo": "Banagher",
    "addressThree": "Birr",
    "addressFour": "Co. Offaly",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "12343W",
    "schoolName": "Shinrone Mixed N S",
    "addressOne": "Shinrone",
    "addressTwo": "Birr",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "12370C",
    "schoolName": "St Brendans Monastery",
    "addressOne": "Moorpark Street",
    "addressTwo": "Birr",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "13118U",
    "schoolName": "Clara Convent N S",
    "addressOne": "Kilbeggan Road",
    "addressTwo": "Clara",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "13191H",
    "schoolName": "High St Mixed N S",
    "addressOne": "Highstreet",
    "addressTwo": "Belmont",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15325M",
    "schoolName": "Clonbullogue N S",
    "addressOne": "Clonbullogue",
    "addressTwo": "Co. Offaly",
    "addressThree": "",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15395K",
    "schoolName": "Mount Bolus N S",
    "addressOne": "Mount Bolus",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15638K",
    "schoolName": "Edenderry 2 N S",
    "addressOne": "EDENDERRY",
    "addressTwo": "CO OFFALY",
    "addressThree": "",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15656M",
    "schoolName": "Ballykilmurry N S",
    "addressOne": "Ballinamere",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15923J",
    "schoolName": "Cloneyhurke N S",
    "addressOne": "Cloneyhurke",
    "addressTwo": "Portarlington",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15983E",
    "schoolName": "S N Naomh Brogain",
    "addressOne": "Bracknagh",
    "addressTwo": "Rathanagan",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16013V",
    "schoolName": "Edenderry Convent N S",
    "addressOne": "School Lane",
    "addressTwo": "Edenderry",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16160L",
    "schoolName": "Clonaghadoo N S",
    "addressOne": "Clonaghdoo",
    "addressTwo": "Geashill",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16435B",
    "schoolName": "St Ciarans Mixed N S",
    "addressOne": "Boher",
    "addressTwo": "Ballycumber",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16620T",
    "schoolName": "Daingean N S",
    "addressOne": "Church Road",
    "addressTwo": "Daingean",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16802C",
    "schoolName": "Ceann Eitigh N S",
    "addressOne": "Kinnitty",
    "addressTwo": "Birr",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16928B",
    "schoolName": "S N Naomh Philomena",
    "addressOne": "Convent Road",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16990G",
    "schoolName": "S N Naomh Callin",
    "addressOne": "Rashinagh",
    "addressTwo": "Ballinahowen",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17003A",
    "schoolName": "S N Seosamh",
    "addressOne": "Moneygall",
    "addressTwo": "Co. Offaly",
    "addressThree": "",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17031F",
    "schoolName": "Shinchill N S",
    "addressOne": "Shinchill",
    "addressTwo": "Killeigh",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17069H",
    "schoolName": "S N Muire Naofa",
    "addressOne": "Pullough",
    "addressTwo": "Co. Offaly",
    "addressThree": "",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17188P",
    "schoolName": "S N Chiarain Naofa",
    "addressOne": "Clonmacnoise",
    "addressTwo": "Co. Offaly",
    "addressThree": "",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17336E",
    "schoolName": "S N Mhuire Bainrioghan",
    "addressOne": "Frankfort",
    "addressTwo": "Dunkerrin",
    "addressThree": "Birr",
    "addressFour": "Co. Offaly",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17508H",
    "schoolName": "S N Naomh Cholumchille",
    "addressOne": "DURROW",
    "addressTwo": "TULLAMORE",
    "addressThree": "CO OFFALY",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17523D",
    "schoolName": "S N Cronain Naofa",
    "addressOne": "DRUM UI CIANAIN",
    "addressTwo": "ROSCREA",
    "addressThree": "CO TIPPERARY",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17540D",
    "schoolName": "S N Mhanachain",
    "addressOne": "Tubber",
    "addressTwo": "Moate",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17616K",
    "schoolName": "Naomh Mhuire N S",
    "addressOne": "Walsh Island",
    "addressTwo": "Geashill",
    "addressThree": "Co Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17637S",
    "schoolName": "S N Sheosaimh Naofa",
    "addressOne": "Ballinagar",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17746A",
    "schoolName": "Scoil Colmain Naofa",
    "addressOne": "Mucklagh",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17753U",
    "schoolName": "S N Chiarain Naofa",
    "addressOne": "Broughall",
    "addressTwo": "Kilcormac",
    "addressThree": "Birr",
    "addressFour": "Co. Offaly",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18057F",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Kilcruttin",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18068K",
    "schoolName": "S N Peadar Agus Pol",
    "addressOne": "ATH AN URCHAIR",
    "addressTwo": "MOATE",
    "addressThree": "CO OFFALY",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18115Q",
    "schoolName": "S N Mhuire",
    "addressOne": "Cloneygowan",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18157J",
    "schoolName": "St Rynaghs N S",
    "addressOne": "BANAGHER",
    "addressTwo": "BIRR",
    "addressThree": "",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18267Q",
    "schoolName": "Croinchoill N S",
    "addressOne": "Crinkill",
    "addressTwo": "BIRR",
    "addressThree": "CO OFFALY",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18328K",
    "schoolName": "S N Phadraig",
    "addressOne": "Ballybryan",
    "addressTwo": "Fahy",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18331W",
    "schoolName": "S N Caoimhin Naofa",
    "addressOne": "Clonlisk National School",
    "addressTwo": "SHINRONE",
    "addressThree": "Birr",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18335H",
    "schoolName": "S N Mhuire Naofa",
    "addressOne": "Rathcobican",
    "addressTwo": "Rhode",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18364O",
    "schoolName": "St Patrick's National School",
    "addressOne": "Gilroy Avenue",
    "addressTwo": "Edenderry",
    "addressThree": "Co.Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18399K",
    "schoolName": "S N Bhride",
    "addressOne": "Ballyboy",
    "addressTwo": "Kilcormac",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18406E",
    "schoolName": "S N Phroinsiais Naofa",
    "addressOne": "Kilbeggan Road",
    "addressTwo": "Clara",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18524K",
    "schoolName": "S N Naomh Brighde Buach",
    "addressOne": "Kilcruttin",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18537T",
    "schoolName": "S N Ros Com Rua",
    "addressOne": "Roscomroe",
    "addressTwo": "Roscrea",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18690E",
    "schoolName": "Scoil Bhride",
    "addressOne": "An Cruachán",
    "addressTwo": "An Tulach Mhór",
    "addressThree": "Co. Uíbh Fhailí",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18777Q",
    "schoolName": "S N Lomchluana",
    "addressOne": "Lumcloon",
    "addressTwo": "Cloghan",
    "addressThree": "Birr",
    "addressFour": "Co. Offaly",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18795S",
    "schoolName": "S N Mhuire",
    "addressOne": "Coolanarney",
    "addressTwo": "Blueball",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18797W",
    "schoolName": "St. Joseph's N.s.",
    "addressOne": "Ardan View",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19300Q",
    "schoolName": "Castlejordan Central Ns",
    "addressOne": "Castlejordan",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19354Q",
    "schoolName": "St Colmans N S",
    "addressOne": "Cappagh",
    "addressTwo": "Croghan",
    "addressThree": "Tullamore",
    "addressFour": "Co. Offaly",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19638H",
    "schoolName": "Coolderry Central Ns",
    "addressOne": "BROSNA",
    "addressTwo": "BIRR",
    "addressThree": "CO OFFALY",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19713S",
    "schoolName": "Arden Boys Ns",
    "addressOne": "Arden View",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19990V",
    "schoolName": "Gaelscoil An Eiscir Riada",
    "addressOne": "CLUAIN CALGA",
    "addressTwo": "AN TULACH MHOR",
    "addressThree": "CO. UIBH FHAILI",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20068W",
    "schoolName": "St Marys National School",
    "addressOne": "CLOGHAN",
    "addressTwo": "BIRR",
    "addressThree": "CO OFFALY",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20189L",
    "schoolName": "Tullamore Educate Together Ns",
    "addressOne": "Collins Lane",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20267F",
    "schoolName": "Scoil Bhríde Primary School",
    "addressOne": "Killane",
    "addressTwo": "Edenderry",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20273A",
    "schoolName": "S N Chartaigh Naofa",
    "addressOne": "Rahan",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20289P",
    "schoolName": "St. Cynoc's National School",
    "addressOne": "Ferbane",
    "addressTwo": "Co. Offaly",
    "addressThree": "",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20332N",
    "schoolName": "Gaelscoil Éadan Doire",
    "addressOne": "Cill Anna",
    "addressTwo": "Éadan Doire",
    "addressThree": "Co Uíbh Fhailí",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20387P",
    "schoolName": "Scoil Mhuire Agus Chormaic",
    "addressOne": "Kilcormac",
    "addressTwo": "Co. Offaly",
    "addressThree": "",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20461B",
    "schoolName": "Gaelscoil Na Laochra",
    "addressOne": "Sandymount Haven",
    "addressTwo": "Birr",
    "addressThree": "",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20099K",
    "schoolName": "Offaly School Of Special Education",
    "addressOne": "Kilcruttin Business Park",
    "addressTwo": "Tullamore",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Offaly",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "00934G",
    "schoolName": "Presentation Convent (jnr)",
    "addressOne": "Harbour Street",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "00941D",
    "schoolName": "S N Bhride",
    "addressOne": "Emper",
    "addressTwo": "Ballynacargy",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "01731U",
    "schoolName": "Ballynacargy Mixed N S",
    "addressOne": "Ballynacargy",
    "addressTwo": "Co. Westmeath",
    "addressThree": "",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "02263S",
    "schoolName": "Crowenstown N S",
    "addressOne": "Crowenstown",
    "addressTwo": "Delvin",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "03936C",
    "schoolName": "S N An Chuil",
    "addressOne": "Coole",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "05513H",
    "schoolName": "Castlepollard Mixed N S",
    "addressOne": "Water Street",
    "addressTwo": "Castlepollard",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "05916G",
    "schoolName": "St. Anne's National School",
    "addressOne": "Main Street",
    "addressTwo": "Tyrellspass",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "07722D",
    "schoolName": "St Peters N S Snr",
    "addressOne": "Excise Street",
    "addressTwo": "Athlone",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "08037Q",
    "schoolName": "Tang N S",
    "addressOne": "Clogher",
    "addressTwo": "Tang",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "08100U",
    "schoolName": "S N Phadraig",
    "addressOne": "Edmonstown",
    "addressTwo": "Killucan",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "10857U",
    "schoolName": "Castlepollard Paroc.n S",
    "addressOne": "Pakenhamhall Road",
    "addressTwo": "Castlepollard",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "13571R",
    "schoolName": "Drumraney Mixed N S",
    "addressOne": "Drumraney",
    "addressTwo": "Athlone",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "14450K",
    "schoolName": "St Feighans Mxd N S",
    "addressOne": "Fore",
    "addressTwo": "Castlepollard",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15291V",
    "schoolName": "Streamstown Mixed N S",
    "addressOne": "Streamstown",
    "addressTwo": "Co. Westmeath",
    "addressThree": "",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15307K",
    "schoolName": "Dalystown N S",
    "addressOne": "Dalystown",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "15512L",
    "schoolName": "St Brigid's Primary School",
    "addressOne": "Station Road",
    "addressTwo": "Moate",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16092U",
    "schoolName": "Athlone N S",
    "addressOne": "Arcadia",
    "addressTwo": "Athlone",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16304J",
    "schoolName": "Milltownpass N S",
    "addressOne": "MILLTOWNPASS",
    "addressTwo": "CO WESTMEATH",
    "addressThree": "",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16340N",
    "schoolName": "Ballinagore Mixed N S",
    "addressOne": "Ballinagore",
    "addressTwo": "Kilbeggan",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16639R",
    "schoolName": "Sn Deaghan O Ceallaigh",
    "addressOne": "The Batteries",
    "addressTwo": "Athlone",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16838A",
    "schoolName": "St. Colmcille's N.s.",
    "addressOne": "Gainstown",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16868J",
    "schoolName": "Baile Na Gceallach N S",
    "addressOne": "COLLINSTOWN",
    "addressTwo": "MULLINGAR",
    "addressThree": "CO WESTMEATH",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16914N",
    "schoolName": "Baile Coireil N S",
    "addressOne": "Coralstown",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "16961W",
    "schoolName": "Kilcumeragh N S",
    "addressOne": "Kilcrumreragh",
    "addressTwo": "Rosemount",
    "addressThree": "Moate",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17025K",
    "schoolName": "S N Na Ndun",
    "addressOne": "Mullingar",
    "addressTwo": "Co. Westmeath",
    "addressThree": "",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17089N",
    "schoolName": "Cluain Maolain N S",
    "addressOne": "Main Street",
    "addressTwo": "Clonmellon",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17102C",
    "schoolName": "Cluain Buinne N S",
    "addressOne": "Clonbonny",
    "addressTwo": "Athlone",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17167H",
    "schoolName": "Bl Oliver Plunkett N S",
    "addressOne": "Lake Road",
    "addressTwo": "Moate",
    "addressThree": "",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17182D",
    "schoolName": "Mhichil Naofa N S",
    "addressOne": "School Road",
    "addressTwo": "Castletown Geoghegan",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17189R",
    "schoolName": "Rath Eoghan N S",
    "addressOne": "RATHOWEN",
    "addressTwo": "MULLINGAR",
    "addressThree": "",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17236A",
    "schoolName": "Boher N S",
    "addressOne": "Boher",
    "addressTwo": "Streamstown",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17290G",
    "schoolName": "Dysart Ns",
    "addressOne": "Dysart",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17327D",
    "schoolName": "Holy Family Primary School",
    "addressOne": "Curraghmore",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17432A",
    "schoolName": "Magh Mora N S",
    "addressOne": "MOYVORE",
    "addressTwo": "CO WESTMEATH",
    "addressThree": "",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17606H",
    "schoolName": "Naomh Micheal N S",
    "addressOne": "Castletown-Finea",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17708P",
    "schoolName": "Sc Ciarain",
    "addressOne": "Twyford",
    "addressTwo": "Baylin",
    "addressThree": "Athlone",
    "addressFour": "Co. Westmeath",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17743R",
    "schoolName": "Naomh Muire N S",
    "addressOne": "Finea",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17806P",
    "schoolName": "Cill Cleithe N S",
    "addressOne": "Kilcleagh",
    "addressTwo": "Castledaly",
    "addressThree": "Moate",
    "addressFour": "Co. Westmeath",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17882I",
    "schoolName": "S N Aodha Naofa",
    "addressOne": "Rahugh",
    "addressTwo": "Kilbeggan",
    "addressThree": "",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17903N",
    "schoolName": "Corr Na Madadh N S",
    "addressOne": "Cornamaddy",
    "addressTwo": "Athlone",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17932U",
    "schoolName": "Odhran Naofa N S",
    "addressOne": "SONNA",
    "addressTwo": "SLANEMORE",
    "addressThree": "MULLINGAR",
    "addressFour": "CO WESTMEATH",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "17991N",
    "schoolName": "Eoin Naofa N S",
    "addressOne": "High Street",
    "addressTwo": "Ballymore",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18036U",
    "schoolName": "Diarmada N S",
    "addressOne": "Castlepollard",
    "addressTwo": "Co. Westmeath",
    "addressThree": "",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18193N",
    "schoolName": "S N Naomh Fiontan",
    "addressOne": "Lismacaffrey",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18212O",
    "schoolName": "Scoil Na Maighdine Mhuire",
    "addressOne": "Harbour Street",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18262G",
    "schoolName": "Lochan An Bhealaigh N S",
    "addressOne": "Loughanavally",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18381O",
    "schoolName": "Scoil Na Mbraithre",
    "addressOne": "College Street",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18405C",
    "schoolName": "S N Phoil Naofa",
    "addressOne": "Lyster Street",
    "addressTwo": "Athlone",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18484B",
    "schoolName": "Mhuire N S",
    "addressOne": "RATH AIRNE",
    "addressTwo": "MULLINGAR",
    "addressThree": "CO WESTMEATH",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18505G",
    "schoolName": "Naomh Clar N S",
    "addressOne": "Tubberclare",
    "addressTwo": "Athlone",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18533L",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Taghmon",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18591C",
    "schoolName": "Naomh Tomas N S",
    "addressOne": "Ballydorey",
    "addressTwo": "Rathowen",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18598Q",
    "schoolName": "Earnain Mxd N S",
    "addressOne": "Castletown",
    "addressTwo": "Delvin",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18640M",
    "schoolName": "Naomh Iosef N S",
    "addressOne": "Rathwire",
    "addressTwo": "Killucan",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18679Q",
    "schoolName": "An Ghrianan N S",
    "addressOne": "Mount Temple",
    "addressTwo": "Moate",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18744B",
    "schoolName": "S N Na Naomh Uile",
    "addressOne": "Church Avenue",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18764H",
    "schoolName": "Ard Na Gcraith N S",
    "addressOne": "Ardnagrath",
    "addressTwo": "Walderstown",
    "addressThree": "Co. Westmeath",
    "addressFour": "CO WESTMEATH",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18812P",
    "schoolName": "Loch An Ghair N S",
    "addressOne": "Loughegar",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18864L",
    "schoolName": "S N An Cusan",
    "addressOne": "Coosan",
    "addressTwo": "Athlone",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19008U",
    "schoolName": "Scoil Phadraig",
    "addressOne": "Milltown",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19205W",
    "schoolName": "Naomh Tola N S",
    "addressOne": "Hiskinstown",
    "addressTwo": "Delvin",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19350I",
    "schoolName": "S N Chruimin Naofa",
    "addressOne": "Rathyganny",
    "addressTwo": "Multyfarnham",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19607T",
    "schoolName": "Kilpatrick Ns",
    "addressOne": "KILPATRICK",
    "addressTwo": "MULLINGAR",
    "addressThree": "CO WESTMEATH",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19632S",
    "schoolName": "Ballinahowen Ns",
    "addressOne": "ATHLONE",
    "addressTwo": "CO WESTMEATH",
    "addressThree": "",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19650U",
    "schoolName": "Scoil Cholmain Naofa",
    "addressOne": "Bellview",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19848S",
    "schoolName": "St Etchens National School",
    "addressOne": "Mullingar Road",
    "addressTwo": "Kinnegad",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19948W",
    "schoolName": "Sc Na Gceithre Maistri",
    "addressOne": "Lios Uí Mhulláin",
    "addressTwo": "Áth Luain",
    "addressThree": "Co. na hIarmhí",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20002P",
    "schoolName": "Gaelscoil An Mhuilinn",
    "addressOne": "Bóthar an Ághasaigh",
    "addressTwo": "An Muileann gCearr",
    "addressThree": "Co. na hIarmhí",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20063M",
    "schoolName": "Scoil An Chlochair Cill Bheagan",
    "addressOne": "Dublin Road",
    "addressTwo": "Kilbeggan",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20073P",
    "schoolName": "St Marys Ns",
    "addressOne": "Gracepark Road",
    "addressTwo": "Athlone",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20188J",
    "schoolName": "Mullingar Educate Together",
    "addressOne": "Rathgowan",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20238V",
    "schoolName": "Gaelscoil An Choillín",
    "addressOne": "An Coillín Mór",
    "addressTwo": "An Muileann gCearr",
    "addressThree": "Co. na hIarmhí",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20439I",
    "schoolName": "Scoil Chroi Naofa",
    "addressOne": "Rochfortbridge",
    "addressTwo": "Co. Westmeath",
    "addressThree": "",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18534N",
    "schoolName": "Naomh Mhuire",
    "addressOne": "South Hill",
    "addressTwo": "Delvin",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19261J",
    "schoolName": "St Hildas Sp Sch",
    "addressOne": "GRACE PARK ROAD",
    "addressTwo": "ATHLONE",
    "addressThree": "CO WESTMEATH",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "19792R",
    "schoolName": "St Brigids Spec Sch",
    "addressOne": "Harbour Street",
    "addressTwo": "Mullingar",
    "addressThree": "Co. Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "20373E",
    "schoolName": "Saplings Special School",
    "addressOne": "Lynn Road",
    "addressTwo": "Mullingar",
    "addressThree": "Co Westmeath",
    "addressFour": "",
    "county": "Westmeath",
    "cecDistrict": "7"
  },
  {
    "rollNumber": "18055B",
    "schoolName": "S N Mhuire",
    "addressOne": "Ballymore Eustace",
    "addressTwo": "Naas",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "00973Q",
    "schoolName": "Grange Con N S",
    "addressOne": "Grangecon",
    "addressTwo": "Dunlavin",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "00984V",
    "schoolName": "Glenealy 1 N S",
    "addressOne": "Glenealy",
    "addressTwo": "Co. Wicklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "01782O",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "Barnacleagh",
    "addressTwo": "Arklow",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "06176U",
    "schoolName": "Blessington 1 N S",
    "addressOne": "Blessington Demesne",
    "addressTwo": "Blessington",
    "addressThree": "Co Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "07246U",
    "schoolName": "Ravenswell Primary School",
    "addressOne": "Ravenswell",
    "addressTwo": "Bray",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "09760V",
    "schoolName": "Powerscourt N S",
    "addressOne": "Cookstown Road",
    "addressTwo": "Enniskerry",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "10111O",
    "schoolName": "Lacken Mxd N S",
    "addressOne": "Lacken",
    "addressTwo": "Blessington",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "10131U",
    "schoolName": "Moin An Bhealaigh N S",
    "addressOne": "Valleymount",
    "addressTwo": "Blessington",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "10683N",
    "schoolName": "Kilbride N S",
    "addressOne": "Manor Kilbride",
    "addressTwo": "Blessington",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "11372B",
    "schoolName": "Brittas Bay Mxd N S",
    "addressOne": "Ballynacarrig",
    "addressTwo": "Britta's Bay",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "11649S",
    "schoolName": "Nuns Cross N S",
    "addressOne": "Nun's Cross",
    "addressTwo": "Ashford",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "12413R",
    "schoolName": "Donaghmore N S",
    "addressOne": "Donoughmore",
    "addressTwo": "Donard",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "12529N",
    "schoolName": "St Saviours N S",
    "addressOne": "Station Road",
    "addressTwo": "Rathdrum",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "12554M",
    "schoolName": "St Patrick's National School",
    "addressOne": "Church Road",
    "addressTwo": "Greystones",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "12688K",
    "schoolName": "Kiltegan N S",
    "addressOne": "Kiltegan",
    "addressTwo": "Co. Wicklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "13224T",
    "schoolName": "St Kevin's Ballycoog Ns",
    "addressOne": "Ballycoog",
    "addressTwo": "Avoca",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "13246G",
    "schoolName": "Moneystown N S",
    "addressOne": "Moneystown",
    "addressTwo": "Roundwood Bray",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "13597M",
    "schoolName": "St Andrews N S",
    "addressOne": "Newcourt Road",
    "addressTwo": "Bray",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "13679O",
    "schoolName": "Delgany N S",
    "addressOne": "Delgany",
    "addressTwo": "Co. Wicklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "14045B",
    "schoolName": "Carysfort Mxd N S",
    "addressOne": "Knockanrahan",
    "addressTwo": "Arklow",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "14269A",
    "schoolName": "Jonathan Swift Ns",
    "addressOne": "Dunlavin",
    "addressTwo": "Co. Wicklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "14398L",
    "schoolName": "Glebe Ns",
    "addressOne": "Church Hill",
    "addressTwo": "Wicklow Town",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "14829M",
    "schoolName": "Scoil San Eoin",
    "addressOne": "Redcross",
    "addressTwo": "Co. Wicklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "14972R",
    "schoolName": "All Saints National School",
    "addressOne": "Carnew",
    "addressTwo": "Co. Wicklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "15359G",
    "schoolName": "Shillelagh No 1 N S",
    "addressOne": "Ballard",
    "addressTwo": "Shillelagh",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "15676S",
    "schoolName": "Padraig Naofa N S",
    "addressOne": "St Patricks Road",
    "addressTwo": "Wicklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "16027J",
    "schoolName": "S N Muire",
    "addressOne": "Stratford-on-Slaney",
    "addressTwo": "Baltinglass",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "16573N",
    "schoolName": "St Brigids School",
    "addressOne": "Trafalgar Road",
    "addressTwo": "Greystones",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "16634H",
    "schoolName": "St Laurence O'toole's Ns",
    "addressOne": "Roundwood",
    "addressTwo": "Bray",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "16872A",
    "schoolName": "Cronan Naofa N S",
    "addressOne": "Vevay Crescent",
    "addressTwo": "Vevay Road",
    "addressThree": "Bray",
    "addressFour": "Co. Wicklow",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "16924Q",
    "schoolName": "Caoimhin Naofa N S",
    "addressOne": "Glendalough",
    "addressTwo": "Co. Wicklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17091A",
    "schoolName": "S N Muire",
    "addressOne": "Main Street",
    "addressTwo": "Blessington",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17181B",
    "schoolName": "St Josephs N S",
    "addressOne": "Templerainey",
    "addressTwo": "Arklow",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17223O",
    "schoolName": "S N Muire Is Gearard",
    "addressOne": "St. Mary's & St. Gerard's NS",
    "addressTwo": "Enniskerry",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17228B",
    "schoolName": "Clochar Muire N S",
    "addressOne": "Rathdrum",
    "addressTwo": "Co. Wicklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17265H",
    "schoolName": "Rathdrum Boys N S",
    "addressOne": "Rathdrum",
    "addressTwo": "Co. Wicklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17304O",
    "schoolName": "Annacurra N S",
    "addressOne": "Annacurra",
    "addressTwo": "Tinahely",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17500O",
    "schoolName": "S N Muire Mxd",
    "addressOne": "Barndarrig",
    "addressTwo": "Co. Wicklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17537O",
    "schoolName": "Scoil Padraig Naofa",
    "addressOne": "Vevay Road",
    "addressTwo": "Bray",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17576B",
    "schoolName": "Scoil An Choroin Mhuire",
    "addressOne": "Dominican Campus",
    "addressTwo": "Wicklow",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17720F",
    "schoolName": "Scoil Naomh Caoimhghin",
    "addressOne": "Rathdown Road",
    "addressTwo": "Greystones",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17810G",
    "schoolName": "Sn Nicolais Naofa",
    "addressOne": "DUNLUAIN",
    "addressTwo": "CO CILL MHANTAIN",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17826V",
    "schoolName": "Scoil Na Coróine Mhuire",
    "addressOne": "Ballinahinch",
    "addressTwo": "Ashford",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17907V",
    "schoolName": "Crossbridge N S",
    "addressOne": "CROSSBRIDGE",
    "addressTwo": "TINAHELY",
    "addressThree": "CO WICKLOW",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17920N",
    "schoolName": "Donard N S",
    "addressOne": "Barrack Street",
    "addressTwo": "Donard",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18033O",
    "schoolName": "Kilcommon N S",
    "addressOne": "Kilcommon",
    "addressTwo": "Churchlands",
    "addressThree": "Tinahely",
    "addressFour": "Co. Wicklow",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18054W",
    "schoolName": "Hollywood N S",
    "addressOne": "Hollywood Village",
    "addressTwo": "Hollywood",
    "addressThree": "Co Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18118W",
    "schoolName": "Coolfancy N S",
    "addressOne": "CUL FHASAIGH",
    "addressTwo": "TINAHELY",
    "addressThree": "CO WICKLOW",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18198A",
    "schoolName": "Padraig Naofa N S",
    "addressOne": "AVOCA",
    "addressTwo": "CO WICKLOW",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18242A",
    "schoolName": "Carnew N S",
    "addressOne": "Carnew",
    "addressTwo": "Arklow",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18357R",
    "schoolName": "S N Padraig Naofa",
    "addressOne": "Curtlestown",
    "addressTwo": "Enniskerry",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18365Q",
    "schoolName": "Kilmacanogue N S",
    "addressOne": "Kilmacanogue",
    "addressTwo": "Bray",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18434J",
    "schoolName": "Rathcoyle N S",
    "addressOne": "Rathcoyle Upper",
    "addressTwo": "Rathdangan",
    "addressThree": "Kiltegan",
    "addressFour": "Co. Wicklow",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18464S",
    "schoolName": "St. Peter's Primary School",
    "addressOne": "Hawthorn Road",
    "addressTwo": "Bray",
    "addressThree": "Co Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18470N",
    "schoolName": "Naomh Brid N S",
    "addressOne": "Knockananna",
    "addressTwo": "Arklow",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18473T",
    "schoolName": "An Chroi Ro Naofa N S",
    "addressOne": "Main Street",
    "addressTwo": "Aughrim",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18489L",
    "schoolName": "Tinahely N S",
    "addressOne": "TINAHELY",
    "addressTwo": "ARKLOW",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18502A",
    "schoolName": "S N Naomh Brid",
    "addressOne": "Talbotstown",
    "addressTwo": "Kiltegan",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19339U",
    "schoolName": "Stratford Lodge Ns",
    "addressOne": "BALTINGLASS",
    "addressTwo": "CO WICKLOW",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19573F",
    "schoolName": "St Laurences N S",
    "addressOne": "Kindlestown",
    "addressTwo": "Greystones",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19584K",
    "schoolName": "Scoil Chualann",
    "addressOne": "Bóthar Vevay",
    "addressTwo": "Bré",
    "addressThree": "Co. Chill Mhantáin",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19734D",
    "schoolName": "St Francis N S",
    "addressOne": "Church Lane",
    "addressTwo": "Newcastle",
    "addressThree": "Greystones",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19748O",
    "schoolName": "Scoil Mhuire Na Naird",
    "addressOne": "SHILLELAGH",
    "addressTwo": "CO WICKLOW",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19754J",
    "schoolName": "Bray School Project N S",
    "addressOne": "KILLARNEY RD",
    "addressTwo": "BALLYWALTRIM",
    "addressThree": "BRAY",
    "addressFour": "CO WICKLOW",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20016D",
    "schoolName": "Gaelscoil Ui Cheadaigh",
    "addressOne": "Bóthar Vevay",
    "addressTwo": "Bré",
    "addressThree": "Co.Chill Mhantáin",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20039P",
    "schoolName": "Scoil Naomh Iosaf",
    "addressOne": "Lathaleer",
    "addressTwo": "Baltinglass",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20045K",
    "schoolName": "Gaelscoil Chill Mhantáin",
    "addressOne": "Former Abbey Community College",
    "addressTwo": "Wicklow",
    "addressThree": "Co Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20080M",
    "schoolName": "Gaelscoil An Inbhir Mhoir",
    "addressOne": "Bóthar Emoclew",
    "addressTwo": "An tInbhear Mór",
    "addressThree": "Co. Chill Mhantáin",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20110S",
    "schoolName": "S N Mhuire Senior School",
    "addressOne": "Blessington",
    "addressTwo": "Co. Wicklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20178G",
    "schoolName": "Wicklow Educate Together Ns",
    "addressOne": "Hawkstown Road",
    "addressTwo": "Wicklow Town",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20208M",
    "schoolName": "St Coen's National School",
    "addressOne": "Merrymeeting",
    "addressTwo": "Rathnew",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20242M",
    "schoolName": "Blessington Educate Together",
    "addressOne": "Red Lane",
    "addressTwo": "Blessington",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20243O",
    "schoolName": "Gael Scoil Na Lochanna",
    "addressOne": "Cill Moloma",
    "addressTwo": "Baile Coimín",
    "addressThree": "Co. Chill Mhantáin",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20278K",
    "schoolName": "Newtownmountkennedy Primary School",
    "addressOne": "Newtownmountkennedy",
    "addressTwo": "Co. Wicklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20300A",
    "schoolName": "Greystones Educate Together National School",
    "addressOne": "Blacklion",
    "addressTwo": "Greystones",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20301C",
    "schoolName": "Gaelscoil Na Gcloch Liath",
    "addressOne": "Blacklion",
    "addressTwo": "Greystones",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20346B",
    "schoolName": "Kilcoole Primary School",
    "addressOne": "Main Street",
    "addressTwo": "Kilcoole",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20466L",
    "schoolName": "St Fergal's National School",
    "addressOne": "Ballywaltrim",
    "addressTwo": "Bray",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20469R",
    "schoolName": "St John's Senior School",
    "addressOne": "Coolgreaney Road",
    "addressTwo": "Arklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20470C",
    "schoolName": "St Michael's And St Peter's Junior School",
    "addressOne": "Hickey's Hill;",
    "addressTwo": "Arklow",
    "addressThree": "",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20473I",
    "schoolName": "Greystones Community Ns",
    "addressOne": "Greystones Rugby Club",
    "addressTwo": "Mill Road",
    "addressThree": "Greystones",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20524W",
    "schoolName": "Newtownmountkennedy Educate Together National School",
    "addressOne": "NTMK Educate Together NS",
    "addressTwo": "Newtownmountkennedy",
    "addressThree": "Co Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18281K",
    "schoolName": "Marino School N.s.",
    "addressOne": "Church Road",
    "addressTwo": "Bray",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18408I",
    "schoolName": "New Court School",
    "addressOne": "Newcourt Road",
    "addressTwo": "Bray",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19522L",
    "schoolName": "St Catherines Special School",
    "addressOne": "Kilmullen Lane",
    "addressTwo": "Newcastle",
    "addressThree": "Greystones",
    "addressFour": "Co. Wicklow",
    "county": "Wicklow",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "02872U",
    "schoolName": "St. Mary's National School",
    "addressOne": "Lamb's Cross",
    "addressTwo": "Sandyford",
    "addressThree": "Dublin 18",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "03359N",
    "schoolName": "Ballyroan B N S",
    "addressOne": "Ballyroan Road",
    "addressTwo": "Rathfarnham",
    "addressThree": "Dublin 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "05600C",
    "schoolName": "Clochar San Dominic",
    "addressOne": "Convent Road",
    "addressTwo": "Dun Laoghaire",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "06200O",
    "schoolName": "Booterstown Boys",
    "addressOne": "Grotto Place",
    "addressTwo": "Booterstown",
    "addressThree": "Blackrock",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "10494K",
    "schoolName": "All Saints N S",
    "addressOne": "Carysfort Avenue",
    "addressTwo": "Blackrock",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "11638N",
    "schoolName": "Whitechurch Nat School",
    "addressOne": "Whitechurch Road",
    "addressTwo": "Rathfarnham",
    "addressThree": "Dublin 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "11873A",
    "schoolName": "Rathmichael N S",
    "addressOne": "Rathmichael",
    "addressTwo": "Shankill",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "14586M",
    "schoolName": "Carysfort Ns",
    "addressOne": "Convent Road",
    "addressTwo": "Blackrock",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "14647G",
    "schoolName": "Dalkey N S (2)",
    "addressOne": "Harbour Road",
    "addressTwo": "Dalkey",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "15132B",
    "schoolName": "Harold Boys N S",
    "addressOne": "St. Patrick's Road",
    "addressTwo": "Dalkey",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "15284B",
    "schoolName": "Taney N S",
    "addressOne": "Sydenham Villas",
    "addressTwo": "Dundrum",
    "addressThree": "Dublin 14",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "16352U",
    "schoolName": "St Brigids Boys N S",
    "addressOne": "Mart Lane",
    "addressTwo": "Foxrock",
    "addressThree": "Dublin 18",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "16353W",
    "schoolName": "St Brigids Girls N S",
    "addressOne": "The Park",
    "addressTwo": "Cabinteely",
    "addressThree": "Dublin 18",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "16629O",
    "schoolName": "Kilternan Church Of Ireland Ns",
    "addressOne": "Glebe Road",
    "addressTwo": "Enniskerry Road",
    "addressThree": "Kilternan",
    "addressFour": "Dublin 18",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "16794G",
    "schoolName": "St Brigids N S",
    "addressOne": "Merville Road",
    "addressTwo": "Stillorgan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "16893I",
    "schoolName": "S N Naomh Lorcan",
    "addressOne": "Upper Kilmacud Road",
    "addressTwo": "Stillorgan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17055T",
    "schoolName": "S N Naomh Mhuire",
    "addressOne": "Garter's Lane",
    "addressTwo": "Saggart",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17470I",
    "schoolName": "St Raphaelas N S",
    "addressOne": "Saint Raphaela's Road",
    "addressTwo": "Stillorgan",
    "addressThree": "Blackrock",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17507F",
    "schoolName": "S N Padraig Naofa",
    "addressOne": "Ballybetagh Road",
    "addressTwo": "Kiltiernan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17899C",
    "schoolName": "Scoil Carmel",
    "addressOne": "Firhouse Road",
    "addressTwo": "Dublin 24",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17953F",
    "schoolName": "S N Bhaile Eamonn",
    "addressOne": "Edmondstown Road",
    "addressTwo": "Rathfarnham",
    "addressThree": "Dublin 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17954H",
    "schoolName": "Scoil Caoimhin Naofa",
    "addressOne": "Mount Merrion",
    "addressTwo": "Blackrock",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17970F",
    "schoolName": "Our Lady Of Mercy Convent School",
    "addressOne": "Rosemount Terrace",
    "addressTwo": "Booterstown",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17979A",
    "schoolName": "S N Cnoc Ainbhil",
    "addressOne": "Lower Kilmacud Road",
    "addressTwo": "Stillorgan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "17996A",
    "schoolName": "Glen Na Smol N S",
    "addressOne": "Glenasmole",
    "addressTwo": "Bohernabreena",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18173H",
    "schoolName": "S N Briotas",
    "addressOne": "Brittas",
    "addressTwo": "Co. Dublin",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18411U",
    "schoolName": "St Marys School",
    "addressOne": "Greenhills Road",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18451J",
    "schoolName": "Scoil Lorcáin",
    "addressOne": "Cearnóg Eaton",
    "addressTwo": "Baile na Manach",
    "addressThree": "Co. Átha Cliath",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18647D",
    "schoolName": "S N San Treasa",
    "addressOne": "The Rise",
    "addressTwo": "Mount Merrion",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18806U",
    "schoolName": "Kilternan N S 1",
    "addressOne": "Kilternan",
    "addressTwo": "Dublin 18",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18886V",
    "schoolName": "Kill-o'-the-grange Ns",
    "addressOne": "Deansgrange",
    "addressTwo": "Blackrock",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19066L",
    "schoolName": "Loreto National School",
    "addressOne": "Loreto Avenue",
    "addressTwo": "Dalkey",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19114T",
    "schoolName": "St Patrick Gns",
    "addressOne": "Ballyroan",
    "addressTwo": "Rathfarnham",
    "addressThree": "Dublin 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19258U",
    "schoolName": "Scoil Padraig Naofa B",
    "addressOne": "Hollypark",
    "addressTwo": "Blackrock",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19259W",
    "schoolName": "St Patricks Gns",
    "addressOne": "Foxrock Avenue",
    "addressTwo": "Foxrock",
    "addressThree": "Dublin 18",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19314E",
    "schoolName": "Scoil Na Maighdine Mhuire Boy",
    "addressOne": "Broadford Rise",
    "addressTwo": "Ballinteer",
    "addressThree": "Dublin 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19319O",
    "schoolName": "St Olaf's National School",
    "addressOne": "Balally Drive",
    "addressTwo": "Dundrum",
    "addressThree": "Dublin 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19320W",
    "schoolName": "Our Lady Of Good Counsel Boys N S",
    "addressOne": "Johnstown",
    "addressTwo": "Dún Laoghaire",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19321B",
    "schoolName": "Our Lady Good Counsel Gns",
    "addressOne": "Woodley Road",
    "addressTwo": "Johnstown",
    "addressThree": "Dun Laoghaire",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19335M",
    "schoolName": "Scoil Na Aingeal",
    "addressOne": "Newtownpark AVenue",
    "addressTwo": "Blackrock",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19390U",
    "schoolName": "St Marks Sen Ns",
    "addressOne": "Springfield",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19396J",
    "schoolName": "Na Maighdine Muire Girl",
    "addressOne": "Ballinteer",
    "addressTwo": "Dublin 16",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19400U",
    "schoolName": "Sn Gleann Na Gcaorach Iníon Léinín",
    "addressOne": "Wyvern",
    "addressTwo": "Killiney",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19437U",
    "schoolName": "Scoil Naithi",
    "addressOne": "Bóthar Átha Leathan",
    "addressTwo": "Baile an tSaoir",
    "addressThree": "Baile Átha Cliath 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19438W",
    "schoolName": "Scoil Colmcille Senior",
    "addressOne": "Coolevin",
    "addressTwo": "Ballybrack",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19462T",
    "schoolName": "Scoil Maelruain Junior",
    "addressOne": "Old Bawn",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19463V",
    "schoolName": "Scoil Maelruain Senior",
    "addressOne": "Old Bawn",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19465C",
    "schoolName": "St Kevins Boys",
    "addressOne": "Kilnamanagh",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19466E",
    "schoolName": "St Kevins Girls",
    "addressOne": "Kilnamanagh",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19472W",
    "schoolName": "St Marks Junior Mixed",
    "addressOne": "Springfield",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19474D",
    "schoolName": "Scoil Colmcille Naofa",
    "addressOne": "Idrone Avenue",
    "addressTwo": "Knocklyon",
    "addressThree": "Dublin 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19490B",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Glenmore Court",
    "addressTwo": "Ballyboden",
    "addressThree": "Dublin 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19497P",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Rathsallagh",
    "addressTwo": "Shankill",
    "addressThree": "Dublin 18",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19502F",
    "schoolName": "Scoil Aenghusa Jun Ns",
    "addressOne": "Balrothery",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19503H",
    "schoolName": "Scoil Chronain",
    "addressOne": "An tSráid Mhór",
    "addressTwo": "Ráth Cúil",
    "addressThree": "Co. Átha Cliath",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19537B",
    "schoolName": "St Attractas Junior N S",
    "addressOne": "Meadowbrook",
    "addressTwo": "Dundrum",
    "addressThree": "Dublin 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19541P",
    "schoolName": "Belgard Heights N S",
    "addressOne": "Cookstown Road",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19543T",
    "schoolName": "Scoil N An Croi Ro Naofa",
    "addressOne": "Killinarden",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19556F",
    "schoolName": "St Kilians Junior School",
    "addressOne": "Kingswood Heights",
    "addressTwo": "Castleview",
    "addressThree": "Tallaght",
    "addressFour": "Dublin 24",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19565G",
    "schoolName": "Scoil Treasa Firhouse",
    "addressOne": "Ballycullen Avenue",
    "addressTwo": "Firhouse",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19574H",
    "schoolName": "Marley Grange Ns",
    "addressOne": "Grange Manor Close",
    "addressTwo": "Marley Grange",
    "addressThree": "Rathfarnham",
    "addressFour": "Dublin 16",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19576L",
    "schoolName": "S N Aenghusa",
    "addressOne": "SCOIL N AENGHUSA SIN",
    "addressTwo": "BALROTHERY",
    "addressThree": "TALLAGHT",
    "addressFour": "DUBLIN 24",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19577N",
    "schoolName": "Scoil Iosa",
    "addressOne": "Tymon Road",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19582G",
    "schoolName": "St Maelruains N S",
    "addressOne": "Kilclare Avenue",
    "addressTwo": "Jobstown",
    "addressThree": "Tallaght",
    "addressFour": "Dublin 24",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19612M",
    "schoolName": "Dalkey School Project",
    "addressOne": "GLENAGEARY LODGE",
    "addressTwo": "GLENAGEARY",
    "addressThree": "CO DUBLIN",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19613O",
    "schoolName": "Scoil Cnoc Mhuire Sin",
    "addressOne": "Knockmore Avenue",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19617W",
    "schoolName": "St Martin De Porres N S",
    "addressOne": "Heatherview Lawn",
    "addressTwo": "Aylesbury",
    "addressThree": "Tallaght",
    "addressFour": "Dublin 24",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19641T",
    "schoolName": "Scoil Cholmcille Junior Ns",
    "addressOne": "Coolevin",
    "addressTwo": "Ballybrack",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19646G",
    "schoolName": "Scoil Santain",
    "addressOne": "Bóthar na hAbhann Móire",
    "addressTwo": "Tamhlacht",
    "addressThree": "Baile Átha Cliath 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19652B",
    "schoolName": "An Chroi Ro Naofa Sois",
    "addressOne": "Killinarden",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19702N",
    "schoolName": "St Thomas Junior N S",
    "addressOne": "Fortunestown Road",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19716B",
    "schoolName": "St Attractas Senior N S",
    "addressOne": "Meadowbrook",
    "addressTwo": "Dundrum",
    "addressThree": "Dublin 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19723V",
    "schoolName": "Queen Of Angels Primary School",
    "addressOne": "Wedgewood",
    "addressTwo": "Sandyford",
    "addressThree": "Dublin 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19742C",
    "schoolName": "St Colmcille Senior N S",
    "addressOne": "Idrone Avenue",
    "addressTwo": "Knocklyon",
    "addressThree": "Dublin 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19765O",
    "schoolName": "St Thomas Senior N S",
    "addressOne": "Jobstown",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19775R",
    "schoolName": "Scoil Cnoc Mhuire Junior",
    "addressOne": "Knockmore Avenue",
    "addressTwo": "Killinarden",
    "addressThree": "Tallaght",
    "addressFour": "Dublin 24",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19782O",
    "schoolName": "St Brigids N S",
    "addressOne": "Brookfield",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19834H",
    "schoolName": "St Aidans Ns",
    "addressOne": "Brookfield",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19840C",
    "schoolName": "Holy Family School",
    "addressOne": "Dunedin Park",
    "addressTwo": "Monkstown Farm",
    "addressThree": "Glenageary",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19872P",
    "schoolName": "Scoil Chaitlin Maude",
    "addressOne": "Garrán an Choill",
    "addressTwo": "Tamhlacht",
    "addressThree": "Baile Átha Cliath 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19878E",
    "schoolName": "Holy Rosary Primary School",
    "addressOne": "Old Court Avenue",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19888H",
    "schoolName": "St Annes Mxd N S",
    "addressOne": "Stonebridge Road",
    "addressTwo": "Shankill",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19893A",
    "schoolName": "St Kilians Senior N S",
    "addressOne": "Kingswood Heights",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19901T",
    "schoolName": "Booterstown N S",
    "addressOne": "CROSS AVE",
    "addressTwo": "BLACKROCK",
    "addressThree": "CO DUBLIN",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19904C",
    "schoolName": "Holy Cross N S",
    "addressOne": "Kilmacud Road Upper",
    "addressTwo": "Dundrum",
    "addressThree": "Dublin 14",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19938T",
    "schoolName": "St Josephs",
    "addressOne": "Tivoli Road",
    "addressTwo": "Dun Laoghaire",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19960M",
    "schoolName": "St Johns N S",
    "addressOne": "Church Road",
    "addressTwo": "Ballybrack",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19979K",
    "schoolName": "St Kevins N S",
    "addressOne": "Pearse Street",
    "addressTwo": "Sallynoggin",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20020R",
    "schoolName": "Gaelscoil Thaobh Na Coille",
    "addressOne": "Beallairmín",
    "addressTwo": "An Chéim",
    "addressThree": "Baile Átha Cliath 18",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20043G",
    "schoolName": "Gaelscoil Chnoc Liamhna",
    "addressOne": "SEAN BHÓTHAR CHNOC LIAMHNA",
    "addressTwo": "BOTHAR CNOC LIAMHNA",
    "addressThree": "BAILE ÁTHA CLIATH 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20056P",
    "schoolName": "Gaelscoil Phadraig",
    "addressOne": "Ascaill Shíleann",
    "addressTwo": "Baile Breac",
    "addressThree": "Co. Átha Cliath",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20060G",
    "schoolName": "Monkstown Educate Together Ns",
    "addressOne": "Kill Avenue",
    "addressTwo": "Dun Laoghaire",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20141G",
    "schoolName": "The Harold School",
    "addressOne": "Eden Road",
    "addressTwo": "Glasthule",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20173T",
    "schoolName": "St Annes Primary School",
    "addressOne": "Kilcarrig Avenue",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20190T",
    "schoolName": "Holy Trinity National School",
    "addressOne": "Glencairn",
    "addressTwo": "Leopardstown",
    "addressThree": "Dublin 18",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20218P",
    "schoolName": "St Columbanus National School",
    "addressOne": "Loughlinstown Drive",
    "addressTwo": "Dun Laoghaire",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20398U",
    "schoolName": "Scoil Niamh Community National School",
    "addressOne": "Fortunestown Lane",
    "addressTwo": "Citywest",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20400E",
    "schoolName": "Ballinteer Educate Together National School",
    "addressOne": "Notre Dame Campus",
    "addressTwo": "Upper Churchtown Road",
    "addressThree": "Churchtown",
    "addressFour": "Dublin 14",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20401G",
    "schoolName": "Stepaside Educate Together National School",
    "addressOne": "Belarmine Vale",
    "addressTwo": "Stepaside",
    "addressThree": "Dublin 18",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20402I",
    "schoolName": "City West Educate Together National School",
    "addressOne": "Fortunestown Lane",
    "addressTwo": "Citywest",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20411J",
    "schoolName": "Firhouse Educate Together National School",
    "addressOne": "Ballycullen Drive",
    "addressTwo": "Firhouse",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20412L",
    "schoolName": "Gaelscoil Na Giúise",
    "addressOne": "Céide Bhaile Uí Chuilinn",
    "addressTwo": "Teach na Giúise",
    "addressThree": "Baile Átha Cliath",
    "addressFour": "D24 W682",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20422O",
    "schoolName": "Scoil Aoife Cns",
    "addressOne": "Citywest Drive",
    "addressTwo": "Citywest",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20425U",
    "schoolName": "Gaelscoil Shliabh Rua",
    "addressOne": "Bóthar Bhaile Uí Ógáin",
    "addressTwo": "BÁC 18",
    "addressThree": "Baile Átha Cliath 18",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20468P",
    "schoolName": "St Dominic's National School",
    "addressOne": "Mountain Park",
    "addressTwo": "Tallaght",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20487T",
    "schoolName": "Gaelscoil Na Fuinseoige",
    "addressOne": "Páirc Finsbury",
    "addressTwo": "Baile an Teampaill",
    "addressThree": "BÁC 14",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20503O",
    "schoolName": "Dun Laoghaire Etns",
    "addressOne": "C/O The Red Door School",
    "addressTwo": "Monkstown Grove",
    "addressThree": "Monkstown Avenue",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18499O",
    "schoolName": "St Augustines School",
    "addressOne": "Obelisk Park",
    "addressTwo": "Carysfort Avenue",
    "addressThree": "Blackrock",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18815V",
    "schoolName": "Our Lady Of Lourdes School",
    "addressOne": "Rochestown Avenue",
    "addressTwo": "Dun Laoghaire",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "18863J",
    "schoolName": "Benincasa Special School",
    "addressOne": "1 MOUNT MERRION AVE",
    "addressTwo": "BLACKROCK",
    "addressThree": "CO DUBLIN",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19355S",
    "schoolName": "Ballyowen Meadows",
    "addressOne": "Loughlinstown Drive",
    "addressTwo": "Dun Laoghaire",
    "addressThree": "",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19499T",
    "schoolName": "St Oliver Plunkett Sp Sc",
    "addressOne": "ALMA PLACE",
    "addressTwo": "CARRIGBREANNAN",
    "addressThree": "BLACKROCK",
    "addressFour": "CO DUBLIN",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "19520H",
    "schoolName": "St Josephs Special Sch",
    "addressOne": "BALROTHERY",
    "addressTwo": "TALLAGHT",
    "addressThree": "DUBLIN 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20010O",
    "schoolName": "St Roses Special School",
    "addressOne": "C/O Scoil Aonghusa SNS",
    "addressTwo": "Castle Park",
    "addressThree": "Tallaght",
    "addressFour": "Dublin 24",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20028K",
    "schoolName": "Setanta Special School",
    "addressOne": "Beechpark",
    "addressTwo": "Stillorgan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20121A",
    "schoolName": "Carmona Special National School",
    "addressOne": "111 Upper Glenageary Road",
    "addressTwo": "Dun Laoghaire",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20378O",
    "schoolName": "Abacas Special School For Children With Autism",
    "addressOne": "Treepark Road",
    "addressTwo": "Kilnamanagh",
    "addressThree": "Dublin 24",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "20381D",
    "schoolName": "Red Door Special School",
    "addressOne": "Monkstown Grove",
    "addressTwo": "Monkstown Avenue",
    "addressThree": "Monkstown",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "8"
  },
  {
    "rollNumber": "00714P",
    "schoolName": "Lucan B N S",
    "addressOne": "Chapel Hill",
    "addressTwo": "Lucan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "00729F",
    "schoolName": "Clochar Loreto N S",
    "addressOne": "Grange Road",
    "addressTwo": "Lucan",
    "addressThree": "Dublin 14",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "12014D",
    "schoolName": "St Andrews N S",
    "addressOne": "St Edmundsbury",
    "addressTwo": "Lucan Road",
    "addressThree": "Lucan",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "13447Q",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Lucan Road",
    "addressTwo": "Lucan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19542R",
    "schoolName": "St Thomas Junior National School",
    "addressOne": "Newcastle Road",
    "addressTwo": "Lucan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19817H",
    "schoolName": "St Marys N S",
    "addressOne": "Airlie Heights",
    "addressTwo": "Lucan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19865S",
    "schoolName": "Divine Mercy Junior National School",
    "addressOne": "Lynch's Lane",
    "addressTwo": "Lucan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20066S",
    "schoolName": "Lucan Educate Together Ns",
    "addressOne": "Mount Bellew Way",
    "addressTwo": "Willsbrook",
    "addressThree": "Lucan",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20166W",
    "schoolName": "Griffeen Valley Educate Together Ns",
    "addressOne": "Griffeen Glen Boulevard",
    "addressTwo": "Lucan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20268H",
    "schoolName": "Adamstown Castle Educate Together National School",
    "addressOne": "Station Road",
    "addressTwo": "Adamstown",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20274C",
    "schoolName": "Esker Educate Together N.s.",
    "addressOne": "The Glebe",
    "addressTwo": "Esker Lane",
    "addressThree": "Lucan",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19676P",
    "schoolName": "Scoil Aine Naofa",
    "addressOne": "Esker",
    "addressTwo": "Lucan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20187H",
    "schoolName": "Divine Mercy Senior National School",
    "addressOne": "Balgaddy",
    "addressTwo": "Lucan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20194E",
    "schoolName": "St. John The Evangelist National School",
    "addressOne": "Station Road",
    "addressTwo": "Adamstown Castle",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20303G",
    "schoolName": "Lucan East Etns",
    "addressOne": "Kishogue Cross",
    "addressTwo": "Off Griffeen Avenue",
    "addressThree": "Lucan",
    "addressFour": "County Dublin",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20426W",
    "schoolName": "Lucan Community National School",
    "addressOne": "Balgaddy Road",
    "addressTwo": "Lucan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "03917V",
    "schoolName": "Naomh Padraig Boys",
    "addressOne": "Cambridge Road",
    "addressTwo": "Ringsend",
    "addressThree": "Dublin 4",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "07546J",
    "schoolName": "Goldenbridge Convent",
    "addressOne": "Saint Vincent Street West",
    "addressTwo": "Goldenbridge",
    "addressThree": "Inchicore",
    "addressFour": "Dublin 8",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "09750S",
    "schoolName": "St Josephs Boys N S",
    "addressOne": "Terenure Road East",
    "addressTwo": "Dublin 6",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "11578V",
    "schoolName": "City Quay Ns",
    "addressOne": "City Quay",
    "addressTwo": "Gloucester Street South",
    "addressThree": "Dublin 2",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "11894I",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Gilford Road",
    "addressTwo": "Sandymount",
    "addressThree": "Dublin 4",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "12755W",
    "schoolName": "Kildare Place N S",
    "addressOne": "92/96 Rathmines Road Upper",
    "addressTwo": "Dublin 6",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "13611D",
    "schoolName": "Presentation Primary School",
    "addressOne": "Warrenmount",
    "addressTwo": "Blackpitts",
    "addressThree": "Dublin 8",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "13612F",
    "schoolName": "Presentation Primary School",
    "addressOne": "Terenure Road West",
    "addressTwo": "Dublin 6W",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "14556D",
    "schoolName": "St Endas Primary School",
    "addressOne": "Whitefriar Street",
    "addressTwo": "Dublin 8",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "14717B",
    "schoolName": "Rathgar N S",
    "addressOne": "Rathgar Avenue",
    "addressTwo": "Dublin 6",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "14917J",
    "schoolName": "Zion Parish Primary School",
    "addressOne": "Bushy Park Road",
    "addressTwo": "Rathgar",
    "addressThree": "Dublin 6",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "14939T",
    "schoolName": "Rathfarnham Parish N S",
    "addressOne": "Washington Lane",
    "addressTwo": "Rathfarnham",
    "addressThree": "Dublin 14",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "15253N",
    "schoolName": "St Patricks Girls Ns",
    "addressOne": "Cambridge Road",
    "addressTwo": "Ringsend",
    "addressThree": "Dublin 4",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "15618E",
    "schoolName": "Sandford Parish National School",
    "addressOne": "Sandford Close",
    "addressTwo": "Ranelagh",
    "addressThree": "Dublin 6",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "15622S",
    "schoolName": "St Patricks Ns",
    "addressOne": "Chapelizod Village",
    "addressTwo": "Dublin 20",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "15625B",
    "schoolName": "St Catherines West N S",
    "addressOne": "Donore Avenue",
    "addressTwo": "South Circular Road",
    "addressThree": "Dublin 8",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "15995L",
    "schoolName": "Star Of The Sea",
    "addressOne": "Leahy's Terrace",
    "addressTwo": "Sandymount",
    "addressThree": "Dublin 4",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "16461C",
    "schoolName": "Caisleain Nua Liamhna",
    "addressOne": "Main Street",
    "addressTwo": "Newcastle",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "16651H",
    "schoolName": "St Clares Convent N S",
    "addressOne": "Harold's Cross Road",
    "addressTwo": "Dublin 6W",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "16786H",
    "schoolName": "St Brigids Primary School",
    "addressOne": "The Coombe",
    "addressTwo": "Dublin 8",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "16964F",
    "schoolName": "Scoil Mhuire Ogh 1",
    "addressOne": "Crumlin Road",
    "addressTwo": "Dublin 12",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "16966J",
    "schoolName": "Scoil Naisiunta Stratford",
    "addressOne": "1 Zion Road",
    "addressTwo": "Rathgar",
    "addressThree": "Dublin 6",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "16983J",
    "schoolName": "S N Naomh Cillin",
    "addressOne": "Robinhood Road",
    "addressTwo": "Bluebell",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "17083B",
    "schoolName": "S N Muire Gan Smal B",
    "addressOne": "Tyrconnell Road",
    "addressTwo": "Inchicore",
    "addressThree": "Dublin 8",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "17210F",
    "schoolName": "Clochar Lughaidh Cailin",
    "addressOne": "Williams Park",
    "addressTwo": "Rathmines",
    "addressThree": "Dublin 6",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "17211H",
    "schoolName": "Clochar Lughaidh Naoidh",
    "addressOne": "Williams Park",
    "addressTwo": "Rathmines",
    "addressThree": "Dublin 6",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "17355I",
    "schoolName": "Muire Na Dea Coirle Girls Senior",
    "addressOne": "Mourne Road",
    "addressTwo": "Drimnagh",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "17356K",
    "schoolName": "Muire Na Dea Coirle Inf",
    "addressOne": "Mourne Road",
    "addressTwo": "Drimnagh",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "17603B",
    "schoolName": "Scoil Iosagain Boys Senior",
    "addressOne": "Aughavannagh Road",
    "addressTwo": "Crumlin",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "17683C",
    "schoolName": "Muire Og 2 Loreto Con",
    "addressOne": "Crumlin Road",
    "addressTwo": "Dublin 12",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "17891J",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Tower Road",
    "addressTwo": "Chapelizod",
    "addressThree": "Dublin 20",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "17893N",
    "schoolName": "Sancta Maria C B S",
    "addressOne": "Synge Strete",
    "addressTwo": "Dublin 8",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18170B",
    "schoolName": "Sn Muire Na Freastogala Girls Sen",
    "addressOne": "Assumption Senior Girls NS",
    "addressTwo": "Long Mile Rd",
    "addressThree": "Walkinstown",
    "addressFour": "Dublin 12",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18282M",
    "schoolName": "Sn Paroiste Maitiu Nfa",
    "addressOne": "Cranfield Place",
    "addressTwo": "Sandymount",
    "addressThree": "Dublin 4",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18323A",
    "schoolName": "Scoil Lorcain B",
    "addressOne": "The Oval",
    "addressTwo": "Palmerstown",
    "addressThree": "Dublin 20",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18324C",
    "schoolName": "Scoil Bride C",
    "addressOne": "Turret Road",
    "addressTwo": "Palmerstown",
    "addressThree": "Dublin 20",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18386B",
    "schoolName": "Marist National School",
    "addressOne": "Clogher Road",
    "addressTwo": "Crumlin",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18477E",
    "schoolName": "Scoil Na Mbrathar",
    "addressOne": "John Dillon Street",
    "addressTwo": "Dublin 8",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18585H",
    "schoolName": "Sn Banrion Na Naingeal1",
    "addressOne": "GURTEEN ROAD",
    "addressTwo": "BALLYFERMOT",
    "addressThree": "DUBLIN 10",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18602E",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Convent Road",
    "addressTwo": "Clondalkin",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18642Q",
    "schoolName": "S N Naomh Eoin",
    "addressOne": "Tower Road",
    "addressTwo": "Clondalkin",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18655C",
    "schoolName": "Scoil Naomh Seosamh",
    "addressOne": "Boot Road",
    "addressTwo": "Clondalkin",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18817C",
    "schoolName": "S N Brighde",
    "addressOne": "Cullenswood House",
    "addressTwo": "Bóthar Feadha Cuilinn",
    "addressThree": "Raghnallach",
    "addressFour": "Baile Átha Cliath 6",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18843D",
    "schoolName": "Bainrion Na N-aingal 2",
    "addressOne": "Gurteen Road",
    "addressTwo": "Ballyfermot",
    "addressThree": "Dublin 10",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19177U",
    "schoolName": "St Pius X N S Boys",
    "addressOne": "Fortfield Park",
    "addressTwo": "Terenure",
    "addressThree": "Dublin 6",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19178W",
    "schoolName": "St Pius X G N S",
    "addressOne": "Fortfield Park",
    "addressTwo": "Terenure",
    "addressThree": "Dublin 6W",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19220S",
    "schoolName": "Scoil Naomh Ide",
    "addressOne": "New Road",
    "addressTwo": "Clondalkin",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19221U",
    "schoolName": "Scoil Naomh Aine",
    "addressOne": "New Road",
    "addressTwo": "Clondalkin Village",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19374W",
    "schoolName": "Our Lady's Grove Primary School",
    "addressOne": "Goatstown Road,",
    "addressTwo": "Dublin 14",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19446V",
    "schoolName": "Scoil Mhuire Boys",
    "addressOne": "Grange Road",
    "addressTwo": "Rathfarnham",
    "addressThree": "Dublin 14",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19480V",
    "schoolName": "St Patricks",
    "addressOne": "St Patrick's Close",
    "addressTwo": "Dublin 8",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19494J",
    "schoolName": "Bishop Galvin Ns",
    "addressOne": "Orwell Park",
    "addressTwo": "Templeogue",
    "addressThree": "Dublin 6w",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19509T",
    "schoolName": "Scoil Nano Nagle",
    "addressOne": "Bawnogue Road",
    "addressTwo": "Clondalkin",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19510E",
    "schoolName": "Talbot Senior Ns",
    "addressOne": "Bawnoge",
    "addressTwo": "Clondalkin",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19566I",
    "schoolName": "Our Lady Queen Of Apostles",
    "addressOne": "Dunawley Avenue",
    "addressTwo": "Clondalkin",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19569O",
    "schoolName": "Neillstown N S",
    "addressOne": "Neillstown Road",
    "addressTwo": "Clondalkin",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19575J",
    "schoolName": "St Marys Junior N S",
    "addressOne": "Rowlagh",
    "addressTwo": "Clondalkin",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19589U",
    "schoolName": "Gaelscoil Inse Chor",
    "addressOne": "Droichead ns hInse",
    "addressTwo": "Baile Átha Cliath 8",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19647I",
    "schoolName": "St Marys Sen N S",
    "addressOne": "ROWLAGH",
    "addressTwo": "CLONDALKIN",
    "addressThree": "DUBLIN 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19658N",
    "schoolName": "Bishop Shanahan Ns",
    "addressOne": "Orwell Park",
    "addressTwo": "Templeogue",
    "addressThree": "Dublin 6W",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19661C",
    "schoolName": "St Gabriels Ns",
    "addressOne": "Ballyfermot Road",
    "addressTwo": "Ballyfermot",
    "addressThree": "Dublin 10",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19662E",
    "schoolName": "St Michaels Ns",
    "addressOne": "Kylemore Road",
    "addressTwo": "Ballyfermot",
    "addressThree": "Dublin 10",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19663G",
    "schoolName": "St Raphaels Ns",
    "addressOne": "Ballyfermot Road",
    "addressTwo": "Dublin 10",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19669S",
    "schoolName": "Lady Of Good Counsel Boys Senior Ns",
    "addressOne": "Mourne Road",
    "addressTwo": "Drimnagh",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19707A",
    "schoolName": "St Ronans N S",
    "addressOne": "St Cuthbert's Road",
    "addressTwo": "Deansrath",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19727G",
    "schoolName": "St Marys Central N S",
    "addressOne": "Belmont Avenue",
    "addressTwo": "Donnybrook",
    "addressThree": "Dublin 4",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19743E",
    "schoolName": "St Bernadettes Junior N S",
    "addressOne": "Quarryvale",
    "addressTwo": "Clondalkin",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19764M",
    "schoolName": "Our Lady Of Wayside N S",
    "addressOne": "Bluebell Road",
    "addressTwo": "Bluebell",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19785U",
    "schoolName": "St Bernadettes Senior N S",
    "addressOne": "Quarryvale",
    "addressTwo": "Clondalkin",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19799I",
    "schoolName": "Sacred Heart N S",
    "addressOne": "Sruleen",
    "addressTwo": "Clondalkin",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19837N",
    "schoolName": "Drimnagh Castle Cbs N S",
    "addressOne": "Drimnagh Castle",
    "addressTwo": "Long Mile Road",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19855P",
    "schoolName": "Gaelscoil Chluain Dolcain",
    "addressOne": "Seanbhóthar Nangair",
    "addressTwo": "Cluain Dolcáin",
    "addressThree": "Baile Átha Cliath 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19896G",
    "schoolName": "Scoil Caitriona Na Mbraithre",
    "addressOne": "59 Lower Baggot Street",
    "addressTwo": "Dublin 2",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19907I",
    "schoolName": "Gaelscoil Mologa",
    "addressOne": "Bóthar Thigh Chláir,",
    "addressTwo": "Cros Araild",
    "addressThree": "BAILE ATHA CLIATH 6W",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19922E",
    "schoolName": "Our Ladys N S",
    "addressOne": "St Columbanus",
    "addressTwo": "Milltown",
    "addressThree": "Dublin 14",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19924I",
    "schoolName": "Harolds Cross N S",
    "addressOne": "Clareville Road",
    "addressTwo": "Harold's Cross",
    "addressThree": "Dublin 6W",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19928Q",
    "schoolName": "Ranelagh Multi Denom Ns",
    "addressOne": "Ranelagh Road",
    "addressTwo": "Ranelagh",
    "addressThree": "Dublin 6",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19933J",
    "schoolName": "Scoil Treasa Naofa",
    "addressOne": "Petrie Road",
    "addressTwo": "Donore Avenue",
    "addressThree": "Dublin 8",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19939V",
    "schoolName": "Scoil Naisiunta An Dea Aoire",
    "addressOne": "Whitehall Road",
    "addressTwo": "Churchtown",
    "addressThree": "Dublin 14",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19940G",
    "schoolName": "Gaelscoil Naomh Padraig",
    "addressOne": "Bóthar an Chaisleán",
    "addressTwo": "Leamhcán",
    "addressThree": "Co. Átha Cliath",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19943M",
    "schoolName": "St Damiens Ns",
    "addressOne": "Quarry Drive",
    "addressTwo": "Perrystown",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19945Q",
    "schoolName": "Rathfarnham Educate Together",
    "addressOne": "Loreto Avenue",
    "addressTwo": "Rathfarnham",
    "addressThree": "Dublin 14",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19949B",
    "schoolName": "Islamic National School",
    "addressOne": "19 Roebuck Road",
    "addressTwo": "Clonskeagh",
    "addressThree": "Dublin 14",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19991A",
    "schoolName": "Gaelscoil Na Camoige",
    "addressOne": "Bóthar an Úlloird",
    "addressTwo": "Cluain Dolcáin",
    "addressThree": "Baile Átha Cliath 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20012S",
    "schoolName": "Griffith Barracks Multi D School",
    "addressOne": "The Old Guardhouse",
    "addressTwo": "South Circular Road",
    "addressThree": "Dublin 8",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20048Q",
    "schoolName": "Gaelscoil Lios Na Nog",
    "addressOne": "Teach Feadha Chuileann",
    "addressTwo": "Bóthar Oakley",
    "addressThree": "Raghnallach",
    "addressFour": "Baile Átha Cliath 6",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20092T",
    "schoolName": "St Ultans Ns",
    "addressOne": "Cherry Orchard Avenue",
    "addressTwo": "Ballyfermot",
    "addressThree": "Dublin 10",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20103V",
    "schoolName": "John Scottus Ns",
    "addressOne": "47/49 Northumberland Road",
    "addressTwo": "Ballsbridge",
    "addressThree": "Dublin 4",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20104A",
    "schoolName": "St Audoens Ns",
    "addressOne": "Cook Street",
    "addressTwo": "Merchant's Quay",
    "addressThree": "Dublin 8",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20139T",
    "schoolName": "Inchicore Ns",
    "addressOne": "Sarsfield Road",
    "addressTwo": "Inchicore",
    "addressThree": "Dublin 10",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20223I",
    "schoolName": "Gaelscoil Eiscir Riada",
    "addressOne": "Bóthar an Ghrifín",
    "addressTwo": "Leamhcán",
    "addressThree": "Co. Átha Cliath",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20397S",
    "schoolName": "St Louise De Marillac Primary School",
    "addressOne": "Drumfinn Road",
    "addressTwo": "Ballyfermot",
    "addressThree": "Dublin 10",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20429F",
    "schoolName": "St. James's Primary School",
    "addressOne": "Basin Lane",
    "addressTwo": "James' Street",
    "addressThree": "Dublin 8",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20430N",
    "schoolName": "Canal Way Educate Together National School",
    "addressOne": "Basin Street Upper",
    "addressTwo": "Dublin 8",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20441S",
    "schoolName": "Shellybanks Educate Together National School",
    "addressOne": "Roslyn Park",
    "addressTwo": "Seafort Avenue",
    "addressThree": "Sandymount",
    "addressFour": "Dublin 4",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20450T",
    "schoolName": "Assumption Junior School",
    "addressOne": "Sisters of Charity",
    "addressTwo": "Walkinstown",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20463F",
    "schoolName": "Holy Spirit Junior Primary School",
    "addressOne": "Limekiln Lane",
    "addressTwo": "Greenhills",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20464H",
    "schoolName": "Holy Spirit Senior Primary School",
    "addressOne": "Limekiln Lane",
    "addressTwo": "Greenhills",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20488V",
    "schoolName": "Scoil Úna Naofa",
    "addressOne": "Armagh Road",
    "addressTwo": "Crumlin",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20494Q",
    "schoolName": "Riverview Educate Together National School",
    "addressOne": "Limekiln Road",
    "addressTwo": "Greenhills",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20501K",
    "schoolName": "Harcourt Terrace Educate Together National School",
    "addressOne": "151-153 Harold's Cross Road",
    "addressTwo": "Harold's Cross",
    "addressThree": "Dublin 6W",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20545H",
    "schoolName": "St Christopher's Ns",
    "addressOne": "Haddington Road",
    "addressTwo": "Ballsbridge",
    "addressThree": "Dublin 4",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "17971H",
    "schoolName": "St Michaels Spec School",
    "addressOne": "Glenmaroon",
    "addressTwo": "Chapelizod",
    "addressThree": "Dublin 20",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18370J",
    "schoolName": "Enable Ireland Sandymount School",
    "addressOne": "SANDYMOUNT AVENUE",
    "addressTwo": "DUBLIN 4",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18569J",
    "schoolName": "St Declans Special Sch",
    "addressOne": "35 NORTHUMBERLAND ROAD",
    "addressTwo": "BALLSBRIDGE",
    "addressThree": "DUBLIN 4",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18671A",
    "schoolName": "St Michaels Hse Spec Sc",
    "addressOne": "St. Michael's House",
    "addressTwo": "Grosvenor School",
    "addressThree": "Leopardstown Road",
    "addressFour": "Dublin 18",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18788V",
    "schoolName": "Our Ladys Hospital Sp S",
    "addressOne": "CRUMLIN",
    "addressTwo": "DUBLIN 12",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "18904U",
    "schoolName": "St Peters Special Sch",
    "addressOne": "St Peters School",
    "addressTwo": "59 Orwell Road",
    "addressThree": "Rathgar",
    "addressFour": "Dublin 6",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19032R",
    "schoolName": "Stewarts School",
    "addressOne": "Waterstown Ave",
    "addressTwo": "Palmerstown",
    "addressThree": "Dublin 20",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19151C",
    "schoolName": "St John Of God Sp Sch",
    "addressOne": "Islandbridge",
    "addressTwo": "Dublin 8",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19382V",
    "schoolName": "Scoil Eoin",
    "addressOne": "Armagh Road",
    "addressTwo": "Crumlin",
    "addressThree": "Dublin 12",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19590F",
    "schoolName": "Scoil Mochua",
    "addressOne": "Old Nangor Road",
    "addressTwo": "Deansrath",
    "addressThree": "Clondalkin",
    "addressFour": "Dublin 22",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19630O",
    "schoolName": "Linn Dara Child And Adolescent (camhs) Inpatient Special School",
    "addressOne": "Cherry Orchard Hospital",
    "addressTwo": "Ballyfermot",
    "addressThree": "Dublin 10",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19705T",
    "schoolName": "Catherine Mc Auley N Sc",
    "addressOne": "59 Baggot Street Lower",
    "addressTwo": "Dublin 2",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20053J",
    "schoolName": "Cheeverstown Sp Sch",
    "addressOne": "Templeogue",
    "addressTwo": "Dublin 6w",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20372C",
    "schoolName": "Saplings Special School",
    "addressOne": "Ballyroan Crescent",
    "addressTwo": "Rathfarnham",
    "addressThree": "Dublin 16",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "20390E",
    "schoolName": "Ballydowd High Support Special School",
    "addressOne": "Ballyowen",
    "addressTwo": "Palmerstown",
    "addressThree": "Dublin 20",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19642V",
    "schoolName": "St Peter Apostle Sen Ns",
    "addressOne": "Neilstown",
    "addressTwo": "Clondalkin",
    "addressThree": "Dublin 22",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "10653E",
    "schoolName": "Chapelizod N S",
    "addressOne": "Martin's Row",
    "addressTwo": "Chapelizod",
    "addressThree": "Dublin 20",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "19481A",
    "schoolName": "Visiting Teacher Service",
    "addressOne": "",
    "addressTwo": "",
    "addressThree": "",
    "addressFour": "",
    "county": "",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "U00050",
    "schoolName": "Alexandra College Junior School",
    "addressOne": "Dublin City South East",
    "addressTwo": "",
    "addressThree": "",
    "addressFour": "",
    "county": "",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "U00044",
    "schoolName": "Catholic University School",
    "addressOne": "Dublin City South East",
    "addressTwo": "",
    "addressThree": "",
    "addressFour": "",
    "county": "",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "U00038",
    "schoolName": "THE TERESIAN SCHOOL",
    "addressOne": "Dublin City South East",
    "addressTwo": "",
    "addressThree": "",
    "addressFour": "",
    "county": "",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "U00030",
    "schoolName": "LORETO COLLEGE JUNIOR SCH",
    "addressOne": "Dublin City South East",
    "addressTwo": "",
    "addressThree": "",
    "addressFour": "",
    "county": "",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "00358T",
    "schoolName": "ST MICHAEL'S COLLEGE JUNIOR NS",
    "addressOne": "Dublin City South East",
    "addressTwo": "",
    "addressThree": "",
    "addressFour": "",
    "county": "",
    "cecDistrict": "9"
  },
  {
    "rollNumber": "00651R",
    "schoolName": "Borris Mxd N S",
    "addressOne": "Lower Main Street",
    "addressTwo": "Borris",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "00977B",
    "schoolName": "Ballyconnell N S",
    "addressOne": "Ballyconnell",
    "addressTwo": "Tullow",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "01116A",
    "schoolName": "Baile An Chuilinn N S",
    "addressOne": "Ballinkillen",
    "addressTwo": "Muine Bheag",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "01215C",
    "schoolName": "Newtown Dunleckney Mxd",
    "addressOne": "Newtown",
    "addressTwo": "Muinebheag",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "01415K",
    "schoolName": "Rathoe Ns",
    "addressOne": "Rathoe",
    "addressTwo": "Co. Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "02124E",
    "schoolName": "Scoil Nais Molaise",
    "addressOne": "Parknakyle",
    "addressTwo": "Old Leighlin",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "04077I",
    "schoolName": "Scoil Nais Bhride",
    "addressOne": "Grange",
    "addressTwo": "Tullow",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "09320Q",
    "schoolName": "Scoil Nais Mhuire",
    "addressOne": "Drumphea",
    "addressTwo": "Bagenalstown",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "11135K",
    "schoolName": "St Marys N S",
    "addressOne": "Dunleckney",
    "addressTwo": "Muine Bheag",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "13105L",
    "schoolName": "St Bridgets Monastery Boys Senior School",
    "addressOne": "Muinebheag",
    "addressTwo": "Co. Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "13507I",
    "schoolName": "S N Muire Lourdes",
    "addressOne": "Mill Street",
    "addressTwo": "Tullow",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "13607M",
    "schoolName": "St Columbas N S",
    "addressOne": "Dublin Road",
    "addressTwo": "Tullow",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "14837L",
    "schoolName": "S N Peadar Agus Pol",
    "addressOne": "Ballon",
    "addressTwo": "Co. Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16080N",
    "schoolName": "S N Phadraig Naofa",
    "addressOne": "The Course",
    "addressTwo": "Tullow",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16938E",
    "schoolName": "Fr Cullen Memorial N S",
    "addressOne": "Tinryland",
    "addressTwo": "Co. Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17053P",
    "schoolName": "Bishop Foley Memorial School Boys Snr Sch",
    "addressOne": "Station Road",
    "addressTwo": "Co. Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17096K",
    "schoolName": "S N Nmh Fhingin",
    "addressOne": "Garryhill",
    "addressTwo": "Muinebheag",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17127S",
    "schoolName": "St Josephs Ns",
    "addressOne": "Hacketstown",
    "addressTwo": "Co. Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17330P",
    "schoolName": "Scoil N Moling-glynn",
    "addressOne": "Glynn",
    "addressTwo": "Saint Mulllins",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17462J",
    "schoolName": "Scoil Nais Mhichil",
    "addressOne": "Newtown",
    "addressTwo": "Borris",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17481N",
    "schoolName": "St Brendans N S",
    "addressOne": "Drummond",
    "addressTwo": "Saint Mullins",
    "addressThree": "Co. Carlow",
    "addressFour": "via Kilkenny",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17501Q",
    "schoolName": "Bennekerry National School",
    "addressOne": "Bennekerry",
    "addressTwo": "Co. Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17514C",
    "schoolName": "S N Cluain Na Gall",
    "addressOne": "Main Street",
    "addressTwo": "Clonegal",
    "addressThree": "Co Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17663T",
    "schoolName": "St Patricks N S",
    "addressOne": "Rathvilly",
    "addressTwo": "Co. Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17669I",
    "schoolName": "S N Treasa Naomha",
    "addressOne": "Tynock",
    "addressTwo": "Kiltegan",
    "addressThree": "Co. Wicklow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17756D",
    "schoolName": "Ballinabranna Mxd N S",
    "addressOne": "Milford",
    "addressTwo": "Co. Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17796P",
    "schoolName": "Our Lady's National School",
    "addressOne": "Nurney",
    "addressTwo": "Carlow",
    "addressThree": "Co Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17994T",
    "schoolName": "S N Fhoirtcheirn/fhinin",
    "addressOne": "Myshall",
    "addressTwo": "Bagenalstown",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18024N",
    "schoolName": "Scoil Muire Gan Smal",
    "addressOne": "Ardattin",
    "addressTwo": "Co. Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18090D",
    "schoolName": "S N Cill Damhain",
    "addressOne": "Kildavin",
    "addressTwo": "Co. Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18183K",
    "schoolName": "Queen Of Universe N S",
    "addressOne": "Long Range",
    "addressTwo": "Muinebeagh",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18363M",
    "schoolName": "S N Muire Gan Smal",
    "addressOne": "Green Lane",
    "addressTwo": "Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18424G",
    "schoolName": "Scoil Nais Iosef Naofa",
    "addressOne": "St. Joseph's Road",
    "addressTwo": "Co. Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18510W",
    "schoolName": "St Patrick's National School",
    "addressOne": "Ballymurphy",
    "addressTwo": "Borris",
    "addressThree": "Co Carlow (via Kilkenny)",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18609S",
    "schoolName": "S N Fiontain Naofa",
    "addressOne": "Rathmore",
    "addressTwo": "Tullow",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18615N",
    "schoolName": "S N Ceatharlach",
    "addressOne": "Green Road",
    "addressTwo": "Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19477J",
    "schoolName": "Holy Family B N S",
    "addressOne": "Askea",
    "addressTwo": "Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19478L",
    "schoolName": "Holy Family Girls N S",
    "addressOne": "Askea",
    "addressTwo": "Co. Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19784S",
    "schoolName": "St Laserians Mxd Ns",
    "addressOne": "Leighlinbridge",
    "addressTwo": "Co. Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19811S",
    "schoolName": "Gaelscoil Eoghain Uí Thuairisc",
    "addressOne": "Garrán na Fuinseoige",
    "addressTwo": "Ceatharlach",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19958C",
    "schoolName": "S.n. Naomh Fhiach",
    "addressOne": "Killeshin Road",
    "addressTwo": "Graiguecullen",
    "addressThree": "Co. Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20295K",
    "schoolName": "Carlow Educate Together Ns",
    "addressOne": "Bestfield",
    "addressTwo": "Athy Road",
    "addressThree": "Carlow",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19315G",
    "schoolName": "St Laserians Special Sc",
    "addressOne": "Dublin Road",
    "addressTwo": "Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20370V",
    "schoolName": "Saplings Carlow Special School",
    "addressOne": "Killeshin",
    "addressTwo": "Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Carlow",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "00788V",
    "schoolName": "Lisdowney N S",
    "addressOne": "Lisdowney",
    "addressTwo": "Ballyragget",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "00796U",
    "schoolName": "Scoil Lachtain",
    "addressOne": "Freshford",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "01300Q",
    "schoolName": "St Michaels National School",
    "addressOne": "Danesfort",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "05927L",
    "schoolName": "Church Hill Mixed N S",
    "addressOne": "Church Hill",
    "addressTwo": "Cuffesgrange",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "06621P",
    "schoolName": "Ringville National School",
    "addressOne": "Ballinlaw",
    "addressTwo": "Slieverue",
    "addressThree": "Co Kilkenny",
    "addressFour": "via Waterford",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "07481H",
    "schoolName": "Kilmoganny Mixed N S",
    "addressOne": "Cottresllstown",
    "addressTwo": "Kilmoganny",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "10835K",
    "schoolName": "Presentation Convent National School",
    "addressOne": "Kilkenny Street",
    "addressTwo": "Castlecomer",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "12476S",
    "schoolName": "Slieverue Mixed N S",
    "addressOne": "Slieverue",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "14476F",
    "schoolName": "Wandesforde Mixed N S",
    "addressOne": "Castlecomer",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15160G",
    "schoolName": "The Rower Mixed N S",
    "addressOne": "The Rower",
    "addressTwo": "Thomastown",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15340I",
    "schoolName": "Carigeen N S",
    "addressOne": "Carrigeen",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16116I",
    "schoolName": "S N Naomh Colmain",
    "addressOne": "Clarabricken",
    "addressTwo": "Clara",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16140F",
    "schoolName": "Skeaghvastheen N S",
    "addressOne": "Gorteen",
    "addressTwo": "Skeaghvasteen",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16204F",
    "schoolName": "S N An Moinin Rua",
    "addressOne": "The Glen",
    "addressTwo": "Moneenroe",
    "addressThree": "Castlecomer",
    "addressFour": "Co. Kilkenny",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16230G",
    "schoolName": "S N Lisnafunchin",
    "addressOne": "Castlecomer",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16311G",
    "schoolName": "Graig Na Manach Buac",
    "addressOne": "GRAIGNAMANAGH",
    "addressTwo": "CO KILKENNY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16406R",
    "schoolName": "Bennettsbridge National School",
    "addressOne": "BENNETTSBRIDGE",
    "addressTwo": "KILKENNY",
    "addressThree": "CO KILKENNY",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16430O",
    "schoolName": "Owning National School",
    "addressOne": "PILTOWN",
    "addressTwo": "CO KILKENNY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16445E",
    "schoolName": "St Patricks Ns",
    "addressOne": "Boneyarrow",
    "addressTwo": "Clogh",
    "addressThree": "Castlecomer",
    "addressFour": "Co. Kilkenny",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16485Q",
    "schoolName": "St Brendans Mixed N S",
    "addressOne": "Huggsintown",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16827S",
    "schoolName": "Scoil San Lionard",
    "addressOne": "Dunnamaggin",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16865D",
    "schoolName": "Clontubrid Mixed N S",
    "addressOne": "Clontubrid",
    "addressTwo": "Freshford",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16875G",
    "schoolName": "S N Naomh Padraigh",
    "addressOne": "Strangsmills",
    "addressTwo": "Kilmacow",
    "addressThree": "Co. Kilkenny",
    "addressFour": "via Waterford",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17073V",
    "schoolName": "S N Cholmcille",
    "addressOne": "Inistioge",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17093E",
    "schoolName": "S N Bhreandain Naofa",
    "addressOne": "Clorinka",
    "addressTwo": "Ballyfoyle",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17108O",
    "schoolName": "St Johns Infants N S",
    "addressOne": "Michael Street",
    "addressTwo": "Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17174E",
    "schoolName": "S N Bhrighde",
    "addressOne": "Coon West",
    "addressTwo": "Carlow",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17200C",
    "schoolName": "S N Colmain",
    "addressOne": "CONAHY",
    "addressTwo": "JENKINSTOWN",
    "addressThree": "CO KILKENNY",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17218V",
    "schoolName": "Kilkenny Mixed N S",
    "addressOne": "Castlecomer Road",
    "addressTwo": "Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17222M",
    "schoolName": "S N Mhichil Naofa",
    "addressOne": "Galmoy Parochial School",
    "addressTwo": "Moneynamuck",
    "addressThree": "Stopford",
    "addressFour": "Galmoy (via Thurles)",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17224Q",
    "schoolName": "S N Muire Gan Smal",
    "addressOne": "Graigenamanagh",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17253A",
    "schoolName": "S N Caislean An Cumair",
    "addressOne": "Donaguile",
    "addressTwo": "Castlecomer",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17314R",
    "schoolName": "Scoil Naomh Eoin Dea",
    "addressOne": "Upper New Street",
    "addressTwo": "Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17339K",
    "schoolName": "S N Naomh Chiarain",
    "addressOne": "Rathdowney Road",
    "addressTwo": "Johnstown",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17357M",
    "schoolName": "S N Baile An Phiull",
    "addressOne": "Kildalton",
    "addressTwo": "Piltown",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17374M",
    "schoolName": "S N Chrion Choill",
    "addressOne": "Gathabawn",
    "addressTwo": "via Thurles",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17566V",
    "schoolName": "S N Bhridhe",
    "addressOne": "Kells",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17589K",
    "schoolName": "S N Chiaran Naofa",
    "addressOne": "STONEYFORD",
    "addressTwo": "CO KILKENNY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17590S",
    "schoolName": "S N Moin Ruadh Mixed",
    "addressOne": "KNOCKTOPHER",
    "addressTwo": "CO KILKENNY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17628R",
    "schoolName": "S N Naomh Padraig Mxd",
    "addressOne": "BAILE HAOL",
    "addressTwo": "CO KILKENNY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17675D",
    "schoolName": "S N Teampall Loiscithe",
    "addressOne": "BURNCHURCH",
    "addressTwo": "CUFFESGRANGE KILKENNY",
    "addressThree": "CO KILKENNY",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17758H",
    "schoolName": "Scoil Mhichil Naofa",
    "addressOne": "CROSSPATRICK",
    "addressTwo": "CO KILKENNY (VIA THURLES)",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17854D",
    "schoolName": "S N Padraig Naofa",
    "addressOne": "St. Patrick's B.N.S.",
    "addressTwo": "Coote's Lane",
    "addressThree": "Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17867M",
    "schoolName": "Scoil Iognaid De Ris",
    "addressOne": "Stephen Street",
    "addressTwo": "Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17905R",
    "schoolName": "S N Tobair Eoin Baisde",
    "addressOne": "JOHNSWELL",
    "addressTwo": "CO KILKENNY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17911M",
    "schoolName": "Colmcille Mixed N S",
    "addressOne": "Bigwood",
    "addressTwo": "Mullinavat",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18064C",
    "schoolName": "S N Muire",
    "addressOne": "Gowran",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18078N",
    "schoolName": "Scoil Bhride B7c",
    "addressOne": "Paulstown",
    "addressTwo": "Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18158L",
    "schoolName": "S N Seamus Naofa",
    "addressOne": "Robinstown",
    "addressTwo": "Glenmore",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18257N",
    "schoolName": "S N Baile An Fhasaigh",
    "addressOne": "Ballyfacey",
    "addressTwo": "Glenmore",
    "addressThree": "via New Ross",
    "addressFour": "Co. Kilkenny",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18607O",
    "schoolName": "Scoil Naomh Ioseph",
    "addressOne": "Clinstown",
    "addressTwo": "Jenkinstown",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18643S",
    "schoolName": "Holycross N.s.",
    "addressOne": "FIRODA",
    "addressTwo": "CASTLECOMER",
    "addressThree": "CO KILKENNY",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18660S",
    "schoolName": "S N Shan Nioclas",
    "addressOne": "Windgap",
    "addressTwo": "CO KILKENNY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18670V",
    "schoolName": "S N Tulach Ruain",
    "addressOne": "Tullarone",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18748J",
    "schoolName": "Scoil Mholainge Listerlin",
    "addressOne": "Listerlin",
    "addressTwo": "Mullinavat",
    "addressThree": "via Waterford",
    "addressFour": "Co. Kilkenny",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19267V",
    "schoolName": "Templeorum N S",
    "addressOne": "Templeorum",
    "addressTwo": "Piltown",
    "addressThree": "via Carrick-on-Suir",
    "addressFour": "Co. Kilkenny",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19344N",
    "schoolName": "St Aidans N S",
    "addressOne": "KILMANAGH",
    "addressTwo": "CO KILKENNY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19626A",
    "schoolName": "St Canices Central N S",
    "addressOne": "Granges Road",
    "addressTwo": "Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19856R",
    "schoolName": "Gaelscoil Osrai",
    "addressOne": "Loch Buí",
    "addressTwo": "Cill Chainnigh",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19905E",
    "schoolName": "Kilkenny School Project",
    "addressOne": "Springfields",
    "addressTwo": "Waterford Road",
    "addressThree": "Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19925K",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Parnell Street",
    "addressTwo": "Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19930D",
    "schoolName": "Scoil An Chroí Ró-naofa",
    "addressOne": "Urlingford",
    "addressTwo": "Co Kilkenny",
    "addressThree": "via Thurles",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19944O",
    "schoolName": "Goresbridge N.s.",
    "addressOne": "GORESBRIDGE",
    "addressTwo": "CO KILKENNY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19976E",
    "schoolName": "St Beacons N S",
    "addressOne": "MULLINAVAT",
    "addressTwo": "CO KILKENNY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20011Q",
    "schoolName": "St Johns Senior Ns",
    "addressOne": "Ballybought Street",
    "addressTwo": "Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20255V",
    "schoolName": "Bunscoil Mcauley Rice",
    "addressOne": "Kilkenny Road",
    "addressTwo": "Callan",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20272V",
    "schoolName": "St Mary's National School",
    "addressOne": "Thomastown",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20443W",
    "schoolName": "St. Senan's National School",
    "addressOne": "Kilmacow",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "Via Waterford",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20492M",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Mooncoin",
    "addressTwo": "Co. Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20506U",
    "schoolName": "Ballyragget National School",
    "addressOne": "Kilkenny Road",
    "addressTwo": "Ballyragget",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19210P",
    "schoolName": "Mother Of Fair Love Spec School",
    "addressOne": "James's Street",
    "addressTwo": "Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19383A",
    "schoolName": "St Patricks Spec Sch",
    "addressOne": "SISTERS OF CHARITY",
    "addressTwo": "KELLS ROAD",
    "addressThree": "KILKENNY",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19523N",
    "schoolName": "School Of The Holy Spirit Special School",
    "addressOne": "Callan Road",
    "addressTwo": "Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20377M",
    "schoolName": "Saplings Special School",
    "addressOne": "Goresbridge",
    "addressTwo": "Co Kilkenny",
    "addressThree": "",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20380B",
    "schoolName": "The Jonah Special School",
    "addressOne": "Old V.E.C. Building",
    "addressTwo": "Slieverue",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Kilkenny",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "00892Q",
    "schoolName": "Shanganamore N S",
    "addressOne": "Shanganamore",
    "addressTwo": "Athy",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "00895W",
    "schoolName": "Ballinakill Mixed N S",
    "addressOne": "Ballinakill",
    "addressTwo": "Portlaoise",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "07183W",
    "schoolName": "St Josephs Girls N.s.",
    "addressOne": "Davitt Road",
    "addressTwo": "Mountmellick",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "07442U",
    "schoolName": "St Josephs National School",
    "addressOne": "Borrin-in-Ossory",
    "addressTwo": "Portlaoise",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "07636K",
    "schoolName": "St Fintans N S",
    "addressOne": "New Line Road",
    "addressTwo": "Mountrath",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "10544W",
    "schoolName": "Cosby N S",
    "addressOne": "Main Street",
    "addressTwo": "Stradbally",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "12231L",
    "schoolName": "Rush Hall Mixed N S",
    "addressOne": "Pike-of-Rushall",
    "addressTwo": "Portloaise",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "12692B",
    "schoolName": "Kiladooley Mixed N S",
    "addressOne": "Killadooley",
    "addressTwo": "Borris-in-Ossory",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "13173F",
    "schoolName": "Paddock N S",
    "addressOne": "Paddock",
    "addressTwo": "Mountrath",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "13343E",
    "schoolName": "Scoil Bhride",
    "addressOne": "Stillbrook",
    "addressTwo": "Mountrath",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "13643Q",
    "schoolName": "Emo Mixed N S",
    "addressOne": "Emo",
    "addressTwo": "Portlaoise",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "13741Q",
    "schoolName": "Rath Mixed N S",
    "addressOne": "Rath",
    "addressTwo": "Ballybrittas",
    "addressThree": "Portlaoise",
    "addressFour": "Co. Laois",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "14260F",
    "schoolName": "Abbeyleix Sth N S",
    "addressOne": "Ballacolla Road",
    "addressTwo": "Abbeyleix",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "14838N",
    "schoolName": "Maryboro N S",
    "addressOne": "Summerhill",
    "addressTwo": "The Downs",
    "addressThree": "Portlaoise",
    "addressFour": "Co. Laois",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15446B",
    "schoolName": "Trummera N S",
    "addressOne": "Tromaire",
    "addressTwo": "Maighean Rátha",
    "addressThree": "Co. Laoise",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15537E",
    "schoolName": "S N Baile Finn",
    "addressOne": "Ballyfin",
    "addressTwo": "Portlaoise",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15556I",
    "schoolName": "Presentation Primary School",
    "addressOne": "Station Road",
    "addressTwo": "Portarlington",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15562D",
    "schoolName": "Cloch An Tsionnaigh N S",
    "addressOne": "Foxrock",
    "addressTwo": "Ballacolla",
    "addressThree": "Abbeyleix",
    "addressFour": "Co. Laois",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15933M",
    "schoolName": "Camross N S",
    "addressOne": "CAMROSS",
    "addressTwo": "PORTLAOISE",
    "addressThree": "CO LAOISE",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16070K",
    "schoolName": "Mountmellick Boys N S",
    "addressOne": "Davitt Road",
    "addressTwo": "Mountmellick",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16617H",
    "schoolName": "Ballyadams N S",
    "addressOne": "BALLYADAMS",
    "addressTwo": "Via ATHY",
    "addressThree": "CO Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16667W",
    "schoolName": "Tobar An Leinn",
    "addressOne": "Raheen",
    "addressTwo": "Abbeyleix",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17064U",
    "schoolName": "Scoil Padraig",
    "addressOne": "Ballylinan",
    "addressTwo": "Co. Laois",
    "addressThree": "",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17084D",
    "schoolName": "Cill An Iubhair N S",
    "addressOne": "KILLANURE",
    "addressTwo": "MOUNTRATH",
    "addressThree": "CO LAOIS",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17271C",
    "schoolName": "Scoil Mhuire Muigheo",
    "addressOne": "Mayo",
    "addressTwo": "Crettyard",
    "addressThree": "Carlow",
    "addressFour": "Co. Laois",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17441B",
    "schoolName": "Scoil Mhuire",
    "addressOne": "WOLFHILL",
    "addressTwo": "ATHY",
    "addressThree": "CO KILDARE",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17489G",
    "schoolName": "S N Fionntan Naofa",
    "addressOne": "Abbeyleix",
    "addressTwo": "Portlaoise",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17493U",
    "schoolName": "Rosenallis N S",
    "addressOne": "ROSENALLIS",
    "addressTwo": "CO LAOIS",
    "addressThree": "",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17555Q",
    "schoolName": "Scoil Naomh Abban",
    "addressOne": "Newtown",
    "addressTwo": "Crettyard",
    "addressThree": "Co. Laois.",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17557U",
    "schoolName": "Scoil Abbain Cillin",
    "addressOne": "Killeen",
    "addressTwo": "Maganey",
    "addressThree": "Co Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17604D",
    "schoolName": "Raithin An Uisce N S",
    "addressOne": "Ratheniska",
    "addressTwo": "Stradbally",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17617M",
    "schoolName": "Scoil Chomhgain Naofa",
    "addressOne": "Killeshin",
    "addressTwo": "Carlow",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17695J",
    "schoolName": "S N Molaise",
    "addressOne": "Spink",
    "addressTwo": "Abbeyleix",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17827A",
    "schoolName": "Scoil Phadraig",
    "addressOne": "Canal Road",
    "addressTwo": "Portarlington",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17883K",
    "schoolName": "Muire Naofa Castlecuffe",
    "addressOne": "CASTLECUFFE NS",
    "addressTwo": "CLONASLEE",
    "addressThree": "CO LAOIS",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18014K",
    "schoolName": "Scoil An Chroi Ro Naofa",
    "addressOne": "The Swan",
    "addressTwo": "via Athy",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18075H",
    "schoolName": "Rathdomhnaigh N S",
    "addressOne": "CHURCH STREET",
    "addressTwo": "RATHDOWNEY",
    "addressThree": "CO LAOIS.",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18150S",
    "schoolName": "Fraoch Mor N S",
    "addressOne": "Scoil an Fhraoich Mhóir",
    "addressTwo": "The Heath",
    "addressThree": "Portlaoise",
    "addressFour": "Laois",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18166K",
    "schoolName": "Cluain Eidhneach N S",
    "addressOne": "CLUAIN EIDHNEACH",
    "addressTwo": "MOUNTRATH",
    "addressThree": "CO LAOIS",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18203N",
    "schoolName": "Scoil Náisiúnta Cúil An Tsúdaire 2",
    "addressOne": "Sandy Lane",
    "addressTwo": "Portarlington",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18206T",
    "schoolName": "Na Carraige N S",
    "addressOne": "Rock",
    "addressTwo": "Mountmellick",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18265M",
    "schoolName": "Bhride N S",
    "addressOne": "Ardough",
    "addressTwo": "Bilboa",
    "addressThree": "Carlow",
    "addressFour": "Co. Laois",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18294T",
    "schoolName": "Barr Na Sruthan N S",
    "addressOne": "BARR NA SRUTHAN",
    "addressTwo": "MOUNTMELLICK",
    "addressThree": "CO LAOIS",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18368W",
    "schoolName": "Mhuire Fatima N S",
    "addressOne": "Timahoe",
    "addressTwo": "Portlaoise",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18433H",
    "schoolName": "Naomh Pius X N S",
    "addressOne": "Ballacolla",
    "addressTwo": "Portlaoise",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18513F",
    "schoolName": "Naomh Padraig N S",
    "addressOne": "DOIRE LIAIM OIG",
    "addressTwo": "ROSENALLIS",
    "addressThree": "CO LAOIS",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18532J",
    "schoolName": "Ardlios N S",
    "addressOne": "Arles",
    "addressTwo": "Ballickmoyler",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18641O",
    "schoolName": "S N Naomh Eoin",
    "addressOne": "Killenard",
    "addressTwo": "Co. Laois",
    "addressThree": "",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18780F",
    "schoolName": "S N Naomh Colmcille",
    "addressOne": "Errill",
    "addressTwo": "Portlaoise",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19369G",
    "schoolName": "St Pauls N S",
    "addressOne": "Irishtown",
    "addressTwo": "Mountmellick",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19747M",
    "schoolName": "Scoil Bhride Ns",
    "addressOne": "Knockmay",
    "addressTwo": "Portlaoise",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19750B",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Ballyroan Road",
    "addressTwo": "Abbeyleix",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19791P",
    "schoolName": "Scoil Tíghearnach Naofa",
    "addressOne": "Cullohill",
    "addressTwo": "Rathdowney",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20065Q",
    "schoolName": "Our Ladys Meadow National Schoo",
    "addressOne": "Durrow",
    "addressTwo": "Co. Laois",
    "addressThree": "",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20071L",
    "schoolName": "Scoil Bhride",
    "addressOne": "Rathdowney",
    "addressTwo": "Co. Laois",
    "addressThree": "",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20081O",
    "schoolName": "Gaelscoil Phort Laoise",
    "addressOne": "Cnoc an tSamhraidh",
    "addressTwo": "Portlaoise",
    "addressThree": "Co. Laoise",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20118L",
    "schoolName": "Scoil Bhride",
    "addressOne": "Chapel St",
    "addressTwo": "Clonaslee",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20169F",
    "schoolName": "Castletown N.s",
    "addressOne": "Elderfield Building",
    "addressTwo": "Castletown",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20224K",
    "schoolName": "Gaelscoil An Tsli Dala",
    "addressOne": "An Bealach Mór",
    "addressTwo": "Co. Laoise",
    "addressThree": "",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20270R",
    "schoolName": "Holy Family Junior School",
    "addressOne": "Aughnaharna",
    "addressTwo": "Summerhill",
    "addressThree": "Portlaoise",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20347D",
    "schoolName": "Portlaoise Educate Together Ns",
    "addressOne": "Summerhill Downs",
    "addressTwo": "Stradbally Road",
    "addressThree": "Portlaoise",
    "addressFour": "Co. Laois",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20353V",
    "schoolName": "St Coleman's National School",
    "addressOne": "Stradbally",
    "addressTwo": "Co. Laois",
    "addressThree": "",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20480F",
    "schoolName": "Holy Family Senior School",
    "addressOne": "Aughnaharna",
    "addressTwo": "Portlaoise",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20485P",
    "schoolName": "Ballyroan Primary School",
    "addressOne": "Ballyroan",
    "addressTwo": "Co. Laois",
    "addressThree": "",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19337Q",
    "schoolName": "St Francis S S",
    "addressOne": "New Road",
    "addressTwo": "Portlaoise",
    "addressThree": "Co Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20100P",
    "schoolName": "The Kolbe Special School",
    "addressOne": "Block Road",
    "addressTwo": "Portlaoise",
    "addressThree": "Co. Laois",
    "addressFour": "",
    "county": "Laois",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "01840C",
    "schoolName": "St Canice S Convent",
    "addressOne": "Rosbercon",
    "addressTwo": "New Ross",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "03633H",
    "schoolName": "Scoil Náisiúnta Bhantiarna Lourdes",
    "addressOne": "Hospital Hill",
    "addressTwo": "Bunclody",
    "addressThree": "Enniscorthy",
    "addressFour": "Co. Wexford",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "05070W",
    "schoolName": "S N Baile Muirne",
    "addressOne": "Ballymurn",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "06959G",
    "schoolName": "Clonroche N S",
    "addressOne": "Clonroche",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "08221J",
    "schoolName": "St Senans National Sch",
    "addressOne": "Templeshannon",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "09184L",
    "schoolName": "Shielbeggan Convent",
    "addressOne": "Shielbaggan",
    "addressTwo": "New Ross",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "10780L",
    "schoolName": "Ballyoughter N S",
    "addressOne": "Ballyoughter",
    "addressTwo": "Gorey",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "11361T",
    "schoolName": "Faythe Convent",
    "addressOne": "99 The Faythe",
    "addressTwo": "Wexford",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "11380A",
    "schoolName": "Sacred Heart National School, Caim",
    "addressOne": "Caim",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "11986N",
    "schoolName": "Convent Of Mercy",
    "addressOne": "Whitemill Road",
    "addressTwo": "Wexford",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "12741L",
    "schoolName": "Marshalstown N S",
    "addressOne": "Marshalstown",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "12841P",
    "schoolName": "St Patricks N S",
    "addressOne": "Ballyroebuck",
    "addressTwo": "Bunclody",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "13299E",
    "schoolName": "Glanbrian N S",
    "addressOne": "Gallinastraw",
    "addressTwo": "Glenbrien",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "13335F",
    "schoolName": "Court N S",
    "addressOne": "Court",
    "addressTwo": "Gorey",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "13999J",
    "schoolName": "Kilnamanagh Community National School",
    "addressOne": "Knockskimolin",
    "addressTwo": "Outlart",
    "addressThree": "Gorey",
    "addressFour": "Co. Wexford",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "14254K",
    "schoolName": "S N Mhuire",
    "addressOne": "Danescastle",
    "addressTwo": "Carrig-on-Bannow",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "14492D",
    "schoolName": "Curracloe N S",
    "addressOne": "Curracloe",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "14668O",
    "schoolName": "Ballaghkeene N S",
    "addressOne": "Ballaghkeane",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "14777T",
    "schoolName": "Kilmyshall N S",
    "addressOne": "Kilmyshall",
    "addressTwo": "Bunclody",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "14900P",
    "schoolName": "Gusserane N S",
    "addressOne": "Gusserane",
    "addressTwo": "New Ross",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "14903V",
    "schoolName": "St Josephs N S",
    "addressOne": "Kilmuckridge",
    "addressTwo": "Gorey",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "14909K",
    "schoolName": "St Leonards N S",
    "addressOne": "St. Leonards",
    "addressTwo": "Ballycullane",
    "addressThree": "New Ross",
    "addressFour": "Co. Wexford",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15177A",
    "schoolName": "Carrigduff Nat School",
    "addressOne": "Carrigduff",
    "addressTwo": "Bunclody",
    "addressThree": "Enniscorthy",
    "addressFour": "Co. Wexford",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15354T",
    "schoolName": "Camolin N S",
    "addressOne": "Camolin",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15367F",
    "schoolName": "Riverchapel N S",
    "addressOne": "Riverchapel",
    "addressTwo": "Gorey",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15407O",
    "schoolName": "Sn Baile Thomais",
    "addressOne": "Ballythomas",
    "addressTwo": "Gorey",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15419V",
    "schoolName": "Oulart N S",
    "addressOne": "Oulart",
    "addressTwo": "Gorey",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15420G",
    "schoolName": "Ballycanew N S",
    "addressOne": "Ballycanew",
    "addressTwo": "Gorey",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15937U",
    "schoolName": "Monaseed N S",
    "addressOne": "Monaseed",
    "addressTwo": "Gorey",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15940J",
    "schoolName": "Tombrack N S",
    "addressOne": "Tombrack",
    "addressTwo": "Ferns",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "15962T",
    "schoolName": "Ballindaggin N S",
    "addressOne": "Ballindaggin",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16023B",
    "schoolName": "Rathgarogue N S",
    "addressOne": "Rathgarogue",
    "addressTwo": "New Ross",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16072O",
    "schoolName": "Newbawn N S",
    "addressOne": "Newbawn",
    "addressTwo": "Co. Wexford",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16145P",
    "schoolName": "Bunscoil Loreto",
    "addressOne": "St Michael's Road",
    "addressTwo": "Gorey",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16409A",
    "schoolName": "St Marys N S",
    "addressOne": "BALLYGARRETT",
    "addressTwo": "GOREY",
    "addressThree": "CO WEXFORD",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16605A",
    "schoolName": "Kilrane N S",
    "addressOne": "Kilrane",
    "addressTwo": "Rosslare Harbour",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16673R",
    "schoolName": "S N Baile Cuisin",
    "addressOne": "Cushinstown",
    "addressTwo": "Foulksmills",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16828U",
    "schoolName": "S N Bearna Na H-aille",
    "addressOne": "The Oil",
    "addressTwo": "Oylegate",
    "addressThree": "Enniscorthy",
    "addressFour": "Co Wexford",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16833N",
    "schoolName": "Kiltealy N S",
    "addressOne": "Kiltealy",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16841M",
    "schoolName": "S N Nmh Brighde",
    "addressOne": "Blackwater",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16879O",
    "schoolName": "S N Clochar Mhuire",
    "addressOne": "Rosslare",
    "addressTwo": "Co. Wexford",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "16992K",
    "schoolName": "Clongeen Mxd N S",
    "addressOne": "Clongeen",
    "addressTwo": "Foulksmills",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17005E",
    "schoolName": "S N An Ghleanna",
    "addressOne": "Glynn",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17017L",
    "schoolName": "S N Phadraig",
    "addressOne": "CROSSABEG",
    "addressTwo": "CO WEXFORD",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17117P",
    "schoolName": "S N Cul Greine",
    "addressOne": "Coolgreany",
    "addressTwo": "Gorey",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17194K",
    "schoolName": "S N Baile Ui Coileain",
    "addressOne": "BALLYCULLANE",
    "addressTwo": "NEW ROSS",
    "addressThree": "CO WEXFORD",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17217T",
    "schoolName": "Scoil Na Mbraithre",
    "addressOne": "Green Street",
    "addressTwo": "Wexford",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17235V",
    "schoolName": "S N Chaomhain Torrchoill",
    "addressOne": "GOREY",
    "addressTwo": "CO WEXFORD",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17354G",
    "schoolName": "S N Padraig",
    "addressOne": "Island Upper",
    "addressTwo": "Craanford",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17373K",
    "schoolName": "S N Clochar Mhuire",
    "addressOne": "St John's Road",
    "addressTwo": "Wexford",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17443F",
    "schoolName": "S N Fionntain",
    "addressOne": "Chapel Street",
    "addressTwo": "Taghmon",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17450C",
    "schoolName": "S N Mhaodhoig",
    "addressOne": "Poulfur",
    "addressTwo": "Fethard-On-Sea",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17471K",
    "schoolName": "S N Shean Bhoth",
    "addressOne": "Rosbercon",
    "addressTwo": "New Ross",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17510R",
    "schoolName": "S N Treasa Nfa",
    "addressOne": "BALLYELLIS",
    "addressTwo": "GOREY",
    "addressThree": "CO WEXFORD",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17519M",
    "schoolName": "S N Caislean Dochraill",
    "addressOne": "Ballycarney",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17526J",
    "schoolName": "S N Abbain",
    "addressOne": "Adamstown",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17554O",
    "schoolName": "S N Nmh Seosaimh",
    "addressOne": "Hilltown",
    "addressTwo": "Ballymitty",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17610V",
    "schoolName": "S N Baile Fada",
    "addressOne": "Ballyfad",
    "addressTwo": "Gorey",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17612C",
    "schoolName": "St Garvans Ns",
    "addressOne": "Carrowreigh",
    "addressTwo": "Taghmon",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17638U",
    "schoolName": "St Joseph's N.s.",
    "addressOne": "DONARD",
    "addressTwo": "POULPEASTY",
    "addressThree": "CLONROCHE, ENNISCORTHY",
    "addressFour": "CO WEXFORD",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17656W",
    "schoolName": "S N Olibheir Beannuithe",
    "addressOne": "Duncannon",
    "addressTwo": "Co. Wexford",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17707N",
    "schoolName": "S N Rath An Iubhair",
    "addressOne": "Rathnure",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17734Q",
    "schoolName": "Scoil Eoin Baiste",
    "addressOne": "Galbally",
    "addressTwo": "Ballyhogue",
    "addressThree": "Enniscorthy",
    "addressFour": "Co. Wexford",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17768K",
    "schoolName": "Scoil Ghormáin Naofa",
    "addressOne": "Castletown",
    "addressTwo": "Inch",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17769M",
    "schoolName": "S N Mhuire",
    "addressOne": "Grahormack",
    "addressTwo": "Tagoat",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17800D",
    "schoolName": "S N Seosamh Nfa",
    "addressOne": "BREE",
    "addressTwo": "ENNISCORTY",
    "addressThree": "CO WEXFORD",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17812K",
    "schoolName": "St Marys N S",
    "addressOne": "Parnell Avenue",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17841R",
    "schoolName": "Sn Mhuire",
    "addressOne": "Ballyhogue",
    "addressTwo": "Bree",
    "addressThree": "Enniscorthy",
    "addressFour": "Co. Wexford",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17858L",
    "schoolName": "S N Coill An Iarainn",
    "addressOne": "Kilanerin",
    "addressTwo": "Gorey",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "17913Q",
    "schoolName": "S N Mhuire",
    "addressOne": "Barntown",
    "addressTwo": "Co. Wexford",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18010C",
    "schoolName": "Davidstown Primary School",
    "addressOne": "Davidstown Primary School",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18133S",
    "schoolName": "Fionntain Nfa N S",
    "addressOne": "Mayglass",
    "addressTwo": "Bridgetown",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18191J",
    "schoolName": "S N Raithin",
    "addressOne": "RAHEEN",
    "addressTwo": "ENNISCORTHY",
    "addressThree": "CO WEXFORD",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18280I",
    "schoolName": "Scoil Naomh Ioseph",
    "addressOne": "Gorey",
    "addressTwo": "Co. Wexford",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18308E",
    "schoolName": "S N Moin Na Gcaor",
    "addressOne": "Monageer",
    "addressTwo": "Ballysimon",
    "addressThree": "Enniscorthy",
    "addressFour": "Co. Wexford",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18315B",
    "schoolName": "S N Padraig Nfa",
    "addressOne": "Courtnacuddy",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18336J",
    "schoolName": "Boolavogue N S",
    "addressOne": "Boolavogue",
    "addressTwo": "Ferns",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18387D",
    "schoolName": "S N Catriona Nfa",
    "addressOne": "Ballyhack",
    "addressTwo": "Arthurstown",
    "addressThree": "New Ross",
    "addressFour": "Co. Wexford",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18558E",
    "schoolName": "S N Baile An Phiarsaigh",
    "addressOne": "Piercestown",
    "addressTwo": "Drinagh",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18684J",
    "schoolName": "S N Bhaile Mhuirne",
    "addressOne": "MURRINTOWN",
    "addressTwo": "CO WEXFORD",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18687P",
    "schoolName": "Ballyduff N S",
    "addressOne": "Ballyduff",
    "addressTwo": "Camolin",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18707S",
    "schoolName": "Castlebridge N S",
    "addressOne": "Castlebridge",
    "addressTwo": "Co. Wexford",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18714P",
    "schoolName": "St Edans N S",
    "addressOne": "Clone Road",
    "addressTwo": "Ferns",
    "addressThree": "Enniscorthy",
    "addressFour": "Co. Wexford",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18824W",
    "schoolName": "St Iberius N S",
    "addressOne": "Davitt Road South",
    "addressTwo": "Wexford",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "18839M",
    "schoolName": "S N Na Scrine",
    "addressOne": "Screen",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19317K",
    "schoolName": "Rathangan N S",
    "addressOne": "Rathangan",
    "addressTwo": "Duncormick",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19352M",
    "schoolName": "Sc Nais Realta Na Mara",
    "addressOne": "Ballask",
    "addressTwo": "Kilmore",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19419S",
    "schoolName": "Gorey Central School",
    "addressOne": "Charlotte Row",
    "addressTwo": "Gorey",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19528A",
    "schoolName": "Ramsgrange Central N S",
    "addressOne": "Ramsgrange",
    "addressTwo": "Co. Wexford",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19604N",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Ballinamona",
    "addressTwo": "Campile",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19739N",
    "schoolName": "Scoil Mhuire Coolcotts",
    "addressOne": "Coolcotts",
    "addressTwo": "Wexford",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19741A",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Broadway",
    "addressTwo": "Co. Wexford",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19890R",
    "schoolName": "Naomh Maodhog N.s.",
    "addressOne": "Main Street",
    "addressTwo": "Ferns",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20003R",
    "schoolName": "St Aidans Parish School",
    "addressOne": "Convent Road",
    "addressTwo": "Enniscorthy",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20008E",
    "schoolName": "Gaelscoil Loch Garman",
    "addressOne": "An Charraig Bhán Thuaidh",
    "addressTwo": "Loch Garman",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20057R",
    "schoolName": "Gaelscoil Inis Corthaidh",
    "addressOne": "Drom Guail",
    "addressTwo": "Inis Corthaidh",
    "addressThree": "Co. Loch Garman",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20165U",
    "schoolName": "Gaelscoil Mhoshiolog",
    "addressOne": "An Chraobach",
    "addressTwo": "Bóthar Charn an Bhua",
    "addressThree": "Guaire",
    "addressFour": "Co. Loch Garman",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20214H",
    "schoolName": "Gorey Educate Together Ns",
    "addressOne": "Kilnahue Lane",
    "addressTwo": "Gorey",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20333P",
    "schoolName": "Wexford Educate Together National School",
    "addressOne": "Whitemill",
    "addressTwo": "Clonard",
    "addressThree": "Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20458M",
    "schoolName": "New Ross Educate Together National School",
    "addressOne": "Barrett's Park",
    "addressTwo": "New Ross",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20481H",
    "schoolName": "Bunscoil Nic Amhlaidh",
    "addressOne": "Castlemoyle",
    "addressTwo": "New Ross",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "20482J",
    "schoolName": "Bunscoil Ris",
    "addressOne": "Castlemoyle",
    "addressTwo": "New Ross",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19240B",
    "schoolName": "St Patricks Spec School",
    "addressOne": "Bohreen Hill",
    "addressTwo": "Enniscourty",
    "addressThree": "Co. Wexford",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "19266T",
    "schoolName": "Our Lady Of Fatima Sp S",
    "addressOne": "CARRIGEEN STREET",
    "addressTwo": "WEXFORD",
    "addressThree": "",
    "addressFour": "",
    "county": "Wexford",
    "cecDistrict": "10"
  },
  {
    "rollNumber": "02439G",
    "schoolName": "S N Mhuire",
    "addressOne": "Bauroe",
    "addressTwo": "Feakle",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "03898U",
    "schoolName": "Toonagh N S",
    "addressOne": "Fountain",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "03928D",
    "schoolName": "Mullach N S",
    "addressOne": "Mullagh",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "04548V",
    "schoolName": "Scoil Seanain Naofa",
    "addressOne": "Clonlara",
    "addressTwo": "Co. Clare",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "04919H",
    "schoolName": "Cratloe N S",
    "addressOne": "Cratloe",
    "addressTwo": "Co. Clare",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "05253H",
    "schoolName": "O Callaghans Mills N S",
    "addressOne": "Iragh",
    "addressTwo": "O'Callaghan's Mills",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "07315N",
    "schoolName": "Holy Family Snr",
    "addressOne": "Station Road",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "09390O",
    "schoolName": "Rockmount Mixed N S",
    "addressOne": "Rockmount",
    "addressTwo": "Miltown Malbay",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "09425H",
    "schoolName": "Rineen N S",
    "addressOne": "Rineen",
    "addressTwo": "Miltown Malbay",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "10763L",
    "schoolName": "Boston N S",
    "addressOne": "Boston",
    "addressTwo": "Tubber",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "10886E",
    "schoolName": "Tubber N S",
    "addressOne": "Tubber",
    "addressTwo": "Co. Clare",
    "addressThree": "Via Galway",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "11234M",
    "schoolName": "Clohanbeg N S",
    "addressOne": "Cree",
    "addressTwo": "Kilrush",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "11714D",
    "schoolName": "Bansha N S",
    "addressOne": "Bansha",
    "addressTwo": "Kilkee",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "11765U",
    "schoolName": "Doonbeg N S",
    "addressOne": "Killard Road",
    "addressTwo": "Doonbeg",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "11990E",
    "schoolName": "Bodyke N S",
    "addressOne": "Coolreagh",
    "addressTwo": "Bodyke",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "12633I",
    "schoolName": "Coolmeen N S",
    "addressOne": "Coolmeen",
    "addressTwo": "Kilmurray McMahon",
    "addressThree": "Kilrush",
    "addressFour": "Co. Clare",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "12848G",
    "schoolName": "Doonaha N S",
    "addressOne": "Doonaha West",
    "addressTwo": "Kilkee",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13379C",
    "schoolName": "S N Padraig Nfa Fanoir",
    "addressOne": "Fanore",
    "addressTwo": "Ballyvaughan",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13418J",
    "schoolName": "Ballyea Mixed N S",
    "addressOne": "Ballyea",
    "addressTwo": "Darragh",
    "addressThree": "Ennis",
    "addressFour": "Co. Clare",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13625O",
    "schoolName": "Kilnamona N S",
    "addressOne": "Kilnamona",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13730L",
    "schoolName": "Clohanes N S",
    "addressOne": "Mullagh",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13738E",
    "schoolName": "Burrane N S",
    "addressOne": "Burrane Upper",
    "addressTwo": "Killimer",
    "addressThree": "Kilrush",
    "addressFour": "Co. Clare",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13804O",
    "schoolName": "S N Na Crannaighe",
    "addressOne": "Carrowreagh East",
    "addressTwo": "Cranny",
    "addressThree": "Kilrush",
    "addressFour": "Co. Clare",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13826B",
    "schoolName": "Kilmihil N.s.",
    "addressOne": "Kilmihil",
    "addressTwo": "Co. Clare",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13870E",
    "schoolName": "Kilkishen N S",
    "addressOne": "Kilkishen",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13876Q",
    "schoolName": "Moveen N S",
    "addressOne": "Kilkee",
    "addressTwo": "Co. Clare",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13942D",
    "schoolName": "S N Cill Muire",
    "addressOne": "Kilmurry",
    "addressTwo": "Sixmilebridge",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "14111L",
    "schoolName": "Cross N S",
    "addressOne": "Kilrush",
    "addressTwo": "Co. Clare",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "14468G",
    "schoolName": "Kilmaley N S",
    "addressOne": "Kilmaley",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "14571W",
    "schoolName": "Tomgraney N S",
    "addressOne": "Tuamgraney",
    "addressTwo": "Co. Clare",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "14622N",
    "schoolName": "Eidhneach N S",
    "addressOne": "Carrowkeel",
    "addressTwo": "Inagh",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "14830U",
    "schoolName": "Barefield Mixed N S",
    "addressOne": "Barefield",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15042A",
    "schoolName": "Ennis N S",
    "addressOne": "Ashline",
    "addressTwo": "Kilrush Road",
    "addressThree": "Ennis",
    "addressFour": "Co. Clare",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15221A",
    "schoolName": "Annagh N S",
    "addressOne": "Annagh",
    "addressTwo": "Miltown Malbay",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15279I",
    "schoolName": "Clooney N S",
    "addressOne": "Carrahan",
    "addressTwo": "Tulla",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15301V",
    "schoolName": "Kildysart N S",
    "addressOne": "Ennis Road",
    "addressTwo": "Kildysart",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15350L",
    "schoolName": "Stonehall N S",
    "addressOne": "Stonehall",
    "addressTwo": "Newmarket-on-Fergus",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15370R",
    "schoolName": "Killaloe Boys N S",
    "addressOne": "Convent Hill",
    "addressTwo": "Killaloe",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15408Q",
    "schoolName": "Connolly N S",
    "addressOne": "Connolly",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15981A",
    "schoolName": "Lakyle N S",
    "addressOne": "Lakyle",
    "addressTwo": "Whitegate",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16186G",
    "schoolName": "Inch N S",
    "addressOne": "ENNIS",
    "addressTwo": "CO CLARE",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16677C",
    "schoolName": "Bunscoil Na Mbraithre",
    "addressOne": "New Road",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16908S",
    "schoolName": "Sixmilebridge N S",
    "addressOne": "School Road",
    "addressTwo": "Sixmilebridge",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16930L",
    "schoolName": "Scoil An Sraith S N",
    "addressOne": "Shragh",
    "addressTwo": "Doonbeg",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16946D",
    "schoolName": "Sn An Phairtin Mixed",
    "addressOne": "Parteen",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17020A",
    "schoolName": "Quilty Ns",
    "addressOne": "Quilty",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17026M",
    "schoolName": "Clarecastle National School",
    "addressOne": "Ennis Road",
    "addressTwo": "Clarecastle",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17246D",
    "schoolName": "S N Cluain An Atha",
    "addressOne": "EIDHNEACH",
    "addressTwo": "INIS",
    "addressThree": "CO CLARE",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17270A",
    "schoolName": "S N Colm Cille",
    "addressOne": "Scoil Cholmcille",
    "addressTwo": "Clouna National School",
    "addressThree": "Ennistymon",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17517I",
    "schoolName": "Doolin Mixed N S",
    "addressOne": "Ennis",
    "addressTwo": "Co. Clare",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17563P",
    "schoolName": "Ogonnelloe National School",
    "addressOne": "OGONNELLOE",
    "addressTwo": "SCARIFF",
    "addressThree": "CO CLARE",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17583V",
    "schoolName": "S N Cnoc An Ein",
    "addressOne": "Knockanean",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17633K",
    "schoolName": "S N Eoin Baiste",
    "addressOne": "Ballyvaughan",
    "addressTwo": "Via Galway",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17816S",
    "schoolName": "Cooraclare B N S",
    "addressOne": "Cooraclare",
    "addressTwo": "Kilrush",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17832Q",
    "schoolName": "Kilnaboy N S",
    "addressOne": "kILNABOY,",
    "addressTwo": "ENNIS,",
    "addressThree": "COUNTY CLARE",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17847G",
    "schoolName": "S N Aibhistin Naofa",
    "addressOne": "Fanaleen",
    "addressTwo": "Kilshanny",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17957N",
    "schoolName": "Holy Family Junior School",
    "addressOne": "Station Road",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18032M",
    "schoolName": "S N Cluain Muinge",
    "addressOne": "Clonmoney",
    "addressTwo": "Newmarket-on-Fergus",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18109V",
    "schoolName": "S N Inis Cealtrach",
    "addressOne": "Mountshannon",
    "addressTwo": "Co. Clare",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18168O",
    "schoolName": "S N An Chrioch",
    "addressOne": "AN CHRIOCH",
    "addressTwo": "Kilrush",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18190H",
    "schoolName": "S N Cronain Nfa An Carn",
    "addressOne": "Carron",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18225A",
    "schoolName": "S N Mhuire Miliuc",
    "addressOne": "LUIMNEACH",
    "addressTwo": "CO LUIMNI",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18227E",
    "schoolName": "Scoil Mhuire Naisiunta",
    "addressOne": "Newtown",
    "addressTwo": "Corofin",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18327I",
    "schoolName": "S N Cnoc Doire",
    "addressOne": "KNOCKERRA",
    "addressTwo": "KILRUSH",
    "addressThree": "CO CLARE",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18339P",
    "schoolName": "Sn Iosef Naofa",
    "addressOne": "Maigh Mór",
    "addressTwo": "An Leacht",
    "addressThree": "Co. an Chláir",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18359V",
    "schoolName": "S N Cuan",
    "addressOne": "Kilbaha",
    "addressTwo": "Kilrush",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18410S",
    "schoolName": "S N Liosceanuir",
    "addressOne": "Liscannor",
    "addressTwo": "Co. Clare",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18467B",
    "schoolName": "S N Baile An Droichid",
    "addressOne": "Bridgetown",
    "addressTwo": "O'Brien's Bridge",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18526O",
    "schoolName": "Ballycar N S",
    "addressOne": "Ballycar",
    "addressTwo": "Newmarket-on-Fergus",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18540I",
    "schoolName": "Cluain Draigneach",
    "addressOne": "CLUAIN DRAIGHNEACH",
    "addressTwo": "LIOS UI CHATASAIGH",
    "addressThree": "INIS",
    "addressFour": "CO AN CHLAIR",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18541K",
    "schoolName": "S N Chathair Aodha",
    "addressOne": "Caherea",
    "addressTwo": "Lissycasey",
    "addressThree": "Ennis",
    "addressFour": "Co. Clare",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18555V",
    "schoolName": "Lissycasey N S",
    "addressOne": "Lissycasey",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18565B",
    "schoolName": "S N Ma Sheasta",
    "addressOne": "MOYASTA",
    "addressTwo": "CO CLARE",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18624O",
    "schoolName": "Mercy Convent Killaloe",
    "addressOne": "KILLALOE",
    "addressTwo": "CO CLARE",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18630J",
    "schoolName": "Carrigaholt Mixed N S",
    "addressOne": "CARRIGAHOLT",
    "addressTwo": "Kilrush",
    "addressThree": "CO CLARE",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18639E",
    "schoolName": "S N Baile Na Cailli",
    "addressOne": "Ballynacally",
    "addressTwo": "Ennis",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18696Q",
    "schoolName": "S N Dubh Rath",
    "addressOne": "Ballyvonnavaun",
    "addressTwo": "Clarecastle",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18720K",
    "schoolName": "Miltown Malbay B N S",
    "addressOne": "Mullagh Road",
    "addressTwo": "Miltown Malbay",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18739I",
    "schoolName": "Shannon Airport 1 N S",
    "addressOne": "Corrib Drive",
    "addressTwo": "Shannon",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18740Q",
    "schoolName": "Shannon Airport No 2 Ns",
    "addressOne": "SHANNON AIRPORT NO 2",
    "addressTwo": "DRUMGGELY AVE",
    "addressThree": "SHANNON",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18753C",
    "schoolName": "Saint Marys N S Lahinch",
    "addressOne": "Lahinch",
    "addressTwo": "Co. Clare",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18768P",
    "schoolName": "Forgleann N S",
    "addressOne": "Furraglaun",
    "addressTwo": "Lahinch",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18950E",
    "schoolName": "Dromindoora N.s.",
    "addressOne": "Dromindoora",
    "addressTwo": "Caher",
    "addressThree": "Feakle",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19043W",
    "schoolName": "New Quay Ns",
    "addressOne": "NEW QUAY",
    "addressTwo": "BURRIN",
    "addressThree": "COUNTY CLARE",
    "addressFour": "H91 HY40",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19274S",
    "schoolName": "St Conaires Ns",
    "addressOne": "Tullyvarraga",
    "addressTwo": "Shannon",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19338S",
    "schoolName": "Flagmount Central Ns",
    "addressOne": "Flagmount",
    "addressTwo": "Caher",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19366A",
    "schoolName": "Scariff Central N S",
    "addressOne": "Scariff",
    "addressTwo": "Co. Clare",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19386G",
    "schoolName": "Labasheeda Central N S",
    "addressOne": "Labasheeda",
    "addressTwo": "Kilrush",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19442N",
    "schoolName": "Ruan Central Ns",
    "addressOne": "Dromore",
    "addressTwo": "Ruan",
    "addressThree": "Ennis",
    "addressFour": "Co. Clare",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19551S",
    "schoolName": "Inchicronan Central Ns",
    "addressOne": "Clonmooney",
    "addressTwo": "Crusheen",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19552U",
    "schoolName": "Naomh Tola",
    "addressOne": "Tullyglass",
    "addressTwo": "Shannon",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19559L",
    "schoolName": "Chriost Ri",
    "addressOne": "Cloughleigh",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19698C",
    "schoolName": "St Aidans Ns",
    "addressOne": "Smithstown",
    "addressTwo": "Shannon",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19700J",
    "schoolName": "Lisdoonvarna N S",
    "addressOne": "Main Street",
    "addressTwo": "Lisdoonvarna",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19838P",
    "schoolName": "Gael Sc Mhichil Chiosog",
    "addressOne": "Bóthar an Ghoirt",
    "addressTwo": "Inis",
    "addressThree": "Co. an Chláir",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19849U",
    "schoolName": "Gaelscoil Donncha Rua",
    "addressOne": "Bóthar Linne",
    "addressTwo": "Sionna",
    "addressThree": "Co An Chláir",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19999Q",
    "schoolName": "Gaelscoil Ui Choimin",
    "addressOne": "Pound Street",
    "addressTwo": "CILL ROIS",
    "addressThree": "CO AN CHLAIR",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20041C",
    "schoolName": "Convent Of Mercy National School",
    "addressOne": "Kilrush",
    "addressTwo": "Co. Clare",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20075T",
    "schoolName": "St Mochullas N.s.",
    "addressOne": "Ennis Road",
    "addressTwo": "Tulla",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20078C",
    "schoolName": "Sn Realt Na Mara",
    "addressOne": "Chapel Street",
    "addressTwo": "Kilkee",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20086B",
    "schoolName": "Ennis Educate Together Ns",
    "addressOne": "Gort Road",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20127M",
    "schoolName": "Scoil Na Maighdine Mhuire",
    "addressOne": "Ennis Road",
    "addressTwo": "Newmarket-on-Fergus",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20149W",
    "schoolName": "St Fachnan & St Attractas N S",
    "addressOne": "Kilcarragh",
    "addressTwo": "Kilfenora",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20245S",
    "schoolName": "Ennistymon National School",
    "addressOne": "Ennistymon",
    "addressTwo": "Co. Clare",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20312H",
    "schoolName": "Raheen Wood Community National School",
    "addressOne": "Raheen Road",
    "addressTwo": "Tuamgraney",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20313J",
    "schoolName": "Mol An Óige Community National School",
    "addressOne": "Glencree",
    "addressTwo": "Ennistymon",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20404M",
    "schoolName": "Coore National School",
    "addressOne": "Coore",
    "addressTwo": "Mullagh",
    "addressThree": "Ennis",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20434V",
    "schoolName": "Scoil Mhichil",
    "addressOne": "Cahermurphy",
    "addressTwo": "Kilmihil",
    "addressThree": "Kilrush",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20447H",
    "schoolName": "Kilmurry Mcmahon National School",
    "addressOne": "Kilmurry McMahon",
    "addressTwo": "Kilrush",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20452A",
    "schoolName": "Broadford & Kilbane National School",
    "addressOne": "Broadford",
    "addressTwo": "Co. Clare",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20476O",
    "schoolName": "Scoil Na Mainistreach Quin Dangan",
    "addressOne": "Newline Road",
    "addressTwo": "Quin",
    "addressThree": "Co Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19233E",
    "schoolName": "St Clares Special Sch",
    "addressOne": "Gort Road",
    "addressTwo": "Ennis",
    "addressThree": "",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19414I",
    "schoolName": "St Annes S S",
    "addressOne": "Saint Senan's Road",
    "addressTwo": "Ennis",
    "addressThree": "Co. Clare",
    "addressFour": "",
    "county": "Clare",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "00606M",
    "schoolName": "Monard N S",
    "addressOne": "Monard",
    "addressTwo": "Solohead",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "01285A",
    "schoolName": "Tipperary Jnr Bn S",
    "addressOne": "St Michael's Street",
    "addressTwo": "Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "01569O",
    "schoolName": "Ballycahill N S",
    "addressOne": "Ballycahill",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "01594N",
    "schoolName": "St Johns",
    "addressOne": "Goldengrove Road",
    "addressTwo": "Roscrea",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "01862M",
    "schoolName": "Tipperary G N S",
    "addressOne": "St Michael's Street",
    "addressTwo": "Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "02237R",
    "schoolName": "Dualla N S",
    "addressOne": "Dualla",
    "addressTwo": "Cashel",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "02428B",
    "schoolName": "Lackamore N S",
    "addressOne": "Lackamore",
    "addressTwo": "Newport",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "02670I",
    "schoolName": "San Isadoir",
    "addressOne": "Nodstown South",
    "addressTwo": "Boherlahan",
    "addressThree": "Cashel",
    "addressFour": "Co. Tipperary",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "04005G",
    "schoolName": "S N Naomh Peadar",
    "addressOne": "Pouldine",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "04067F",
    "schoolName": "Convent Of Mercy",
    "addressOne": "Church Road",
    "addressTwo": "Newport",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "04075E",
    "schoolName": "Moyglass N S",
    "addressOne": "Moyglass",
    "addressTwo": "Fethard",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "04620D",
    "schoolName": "Thomastown N S",
    "addressOne": "Ardobireen",
    "addressTwo": "Thomastown",
    "addressThree": "Golden",
    "addressFour": "Co. Tipperary",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "05144C",
    "schoolName": "New Inn B N S",
    "addressOne": "New Inn",
    "addressTwo": "Cashel",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "06658P",
    "schoolName": "Kiladangan N S",
    "addressOne": "Kiladangan National School",
    "addressTwo": "Puckane",
    "addressThree": "Nenagh",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "06789H",
    "schoolName": "Lisronagh N S",
    "addressOne": "Lisronagh",
    "addressTwo": "Clonmel",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "07245S",
    "schoolName": "Cullen N S",
    "addressOne": "Cullen",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "07358I",
    "schoolName": "S N Sceichin A Rince",
    "addressOne": "Skeheenarinky",
    "addressTwo": "Burncourt",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "09190G",
    "schoolName": "Boher N S",
    "addressOne": "Boher",
    "addressTwo": "Ballina",
    "addressThree": "Killaloe PO",
    "addressFour": "Co. Tipperary",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "09432E",
    "schoolName": "St Josephs Primary School",
    "addressOne": "Murgasty Road",
    "addressTwo": "Tipperary Town",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "09967U",
    "schoolName": "Upper Newtown N S",
    "addressOne": "Newtown Upper",
    "addressTwo": "Faugheen",
    "addressThree": "Carrick-on-Suir",
    "addressFour": "Co. Tipperary",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "10120P",
    "schoolName": "S N Mhuire Na Trocaire",
    "addressOne": "Convent Road",
    "addressTwo": "Cahir",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "10533R",
    "schoolName": "Ballydrehid N S",
    "addressOne": "Ballydrehid",
    "addressTwo": "Cahir",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "11367I",
    "schoolName": "Portroe N S",
    "addressOne": "Nenagh",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "11384I",
    "schoolName": "Gurtagarry N S",
    "addressOne": "Gortagarry",
    "addressTwo": "Toomevara",
    "addressThree": "Nenagh",
    "addressFour": "Co. Tipperary",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "11453B",
    "schoolName": "Lisnamrock N S",
    "addressOne": "Coalbrook",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "11470B",
    "schoolName": "Slieveardagh N S",
    "addressOne": "Kyleballygalvan",
    "addressTwo": "The Commons",
    "addressThree": "Thurles",
    "addressFour": "Co. Tipperary",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "11605V",
    "schoolName": "Rosegreen N S",
    "addressOne": "Rosegreen",
    "addressTwo": "Cashel",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "11872V",
    "schoolName": "Clochar Na Toirbhirte",
    "addressOne": "Greenside South",
    "addressTwo": "Carrick-on-Suir",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "12124K",
    "schoolName": "Rear N S",
    "addressOne": "Rearcross",
    "addressTwo": "Newport",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "12180U",
    "schoolName": "Presentation Primary School",
    "addressOne": "Convent Road",
    "addressTwo": "Clonmel",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "12349L",
    "schoolName": "S N Muire Na Naingeal",
    "addressOne": "Sisters of Charity",
    "addressTwo": "Mary Street",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "12540B",
    "schoolName": "Clonmore N S",
    "addressOne": "Clonmore",
    "addressTwo": "Templemore",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13404V",
    "schoolName": "Scoil Mhuire Gan Smál",
    "addressOne": "New Inn",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13678M",
    "schoolName": "Killurney N S",
    "addressOne": "Killurney",
    "addressTwo": "Clonmel",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13847J",
    "schoolName": "Hollyford N S",
    "addressOne": "Curraheen",
    "addressTwo": "Hollyford",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13867P",
    "schoolName": "Roscrea N S No 2",
    "addressOne": "Rosemount",
    "addressTwo": "Roscrea",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13991Q",
    "schoolName": "Birdhill N S",
    "addressOne": "Birdhill",
    "addressTwo": "Killaloe",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "14008S",
    "schoolName": "S N Bhride",
    "addressOne": "Donaskeagh",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "14181J",
    "schoolName": "Poulacapple N S",
    "addressOne": "Mullinahone",
    "addressTwo": "via Callan",
    "addressThree": "Co. Kilkenny",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "14426N",
    "schoolName": "Knockavilla N S",
    "addressOne": "Killenure",
    "addressTwo": "Dundrum",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "14460N",
    "schoolName": "Killea N S",
    "addressOne": "Killea",
    "addressTwo": "Templemore",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "14791N",
    "schoolName": "Cappawhite N S",
    "addressOne": "Cappagh",
    "addressTwo": "Cappawhite",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15008A",
    "schoolName": "Shronell N S",
    "addressOne": "Lattin",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15157R",
    "schoolName": "Mount Bruis N S",
    "addressOne": "Mount Bruis",
    "addressTwo": "Tipperary Town",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15299O",
    "schoolName": "Gaile N S",
    "addressOne": "Holycross",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15362S",
    "schoolName": "S N Michil Naofa",
    "addressOne": "Mullinahone",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15560W",
    "schoolName": "Bishop Harty Ns",
    "addressOne": "Ballinree",
    "addressTwo": "Nenagh",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15696B",
    "schoolName": "Silvermines N S",
    "addressOne": "Silvermines",
    "addressTwo": "Nenagh",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15778D",
    "schoolName": "St Marys N S",
    "addressOne": "Church Road",
    "addressTwo": "Nenagh",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15795D",
    "schoolName": "St Marys N S",
    "addressOne": "CHURCH STREET",
    "addressTwo": "TEMPLEMORE",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15970S",
    "schoolName": "Ballytarsna N S",
    "addressOne": "Ballytarsna",
    "addressTwo": "Cashel",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16077B",
    "schoolName": "Ardfinnan N S",
    "addressOne": "Clonmel",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16111V",
    "schoolName": "Killusty N S",
    "addressOne": "Killusty",
    "addressTwo": "Fethard",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16112A",
    "schoolName": "St Marys Convent",
    "addressOne": "O'Rahilly Street",
    "addressTwo": "Nenagh",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16166A",
    "schoolName": "Carrig N S",
    "addressOne": "Carrig",
    "addressTwo": "Birr",
    "addressThree": "Co. Offaly",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16211C",
    "schoolName": "Two Mile Borris N S",
    "addressOne": "TWO MILE BORRIS",
    "addressTwo": "THURLES",
    "addressThree": "CO. TIPPERARY",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16250M",
    "schoolName": "Templetuohy N S",
    "addressOne": "Main Street",
    "addressTwo": "Templetuohy",
    "addressThree": "Thurles",
    "addressFour": "Co. Tipperary",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16276H",
    "schoolName": "Carrig N S",
    "addressOne": "Ballycommon",
    "addressTwo": "Nenagh",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16344V",
    "schoolName": "St Marys Jnr B N S",
    "addressOne": "St Flannan Street",
    "addressTwo": "Nenagh",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16725K",
    "schoolName": "S N Mhuire Na Mbraithre",
    "addressOne": "John Street",
    "addressTwo": "Carrick-on-Suir",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16727O",
    "schoolName": "Scoil Na Mbraithre",
    "addressOne": "Nenagh",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16728Q",
    "schoolName": "St Marys N S",
    "addressOne": "IRISHTOWN",
    "addressTwo": "CLONMEL",
    "addressThree": "CO TIPPERARY",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16729S",
    "schoolName": "St Peter And Paul",
    "addressOne": "Kickham Street",
    "addressTwo": "Clonmel",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16810B",
    "schoolName": "Eochaille Ara N S",
    "addressOne": "NEWTOWN",
    "addressTwo": "NENAGH",
    "addressThree": "CO TIPPERARY",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16933R",
    "schoolName": "Loch Mor Maigh N S",
    "addressOne": "Loughmore",
    "addressTwo": "Templemore",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16979S",
    "schoolName": "St Colmcilles Primary School",
    "addressOne": "Church Avenue",
    "addressTwo": "Templemore",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17114J",
    "schoolName": "S N An Ghabhailin",
    "addressOne": "Golden",
    "addressTwo": "Cashel",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17140K",
    "schoolName": "S N Baile Sluagh",
    "addressOne": "Ballysloe",
    "addressTwo": "Gortnahoe",
    "addressThree": "Thurles",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17179O",
    "schoolName": "S N An Cillin",
    "addressOne": "BIRR",
    "addressTwo": "CO OFFALY",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17237C",
    "schoolName": "S N Cill Barfhionn",
    "addressOne": "Coolbawn",
    "addressTwo": "Nenagh",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17244W",
    "schoolName": "S N Naomh Ruadhain",
    "addressOne": "Redwood",
    "addressTwo": "Lorrha",
    "addressThree": "Nenagh",
    "addressFour": "Co. Tipperary",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17276M",
    "schoolName": "Scoil Na Mbraithre",
    "addressOne": "Murgasty Road",
    "addressTwo": "Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17296S",
    "schoolName": "S N Baile Na Hinse",
    "addressOne": "Ballinahinch",
    "addressTwo": "Birdhill",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17332T",
    "schoolName": "St Marys Parochial School",
    "addressOne": "Western Road",
    "addressTwo": "Clonmel",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17486A",
    "schoolName": "S N Cleireachain",
    "addressOne": "CLERIHAN",
    "addressTwo": "CLONMEL",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17498H",
    "schoolName": "S N Naomh Sheosamh",
    "addressOne": "TOOMEVARA",
    "addressTwo": "NENAGH",
    "addressThree": "CO TIPPERARY",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17511T",
    "schoolName": "S N Baile An Iubhair",
    "addressOne": "Thurles",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17512V",
    "schoolName": "S N Flannain Naofa",
    "addressOne": "Rathcabbin",
    "addressTwo": "Roscrea",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17542H",
    "schoolName": "S N Rath Chaomhghin",
    "addressOne": "Rathkeevin",
    "addressTwo": "Clonmel",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17543J",
    "schoolName": "Cloughjordan N S",
    "addressOne": "Cloughjordan",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17580P",
    "schoolName": "S N Gort Na Huaighe",
    "addressOne": "Gortnahoe",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17608L",
    "schoolName": "S N Lua Naofa",
    "addressOne": "Lorrha",
    "addressTwo": "Nenagh",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17634M",
    "schoolName": "Scoil Ailbhe",
    "addressOne": "Parnell Street",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17640H",
    "schoolName": "S N Colmain Naofa",
    "addressOne": "Terryglass",
    "addressTwo": "Nenagh",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17653Q",
    "schoolName": "Castle Iny N S",
    "addressOne": "Castleiney",
    "addressTwo": "Templemore",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17665A",
    "schoolName": "S N Gleann Guail",
    "addressOne": "Glengoole North",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17681V",
    "schoolName": "S N Na Maighne",
    "addressOne": "MOYNE",
    "addressTwo": "THURLES",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17694H",
    "schoolName": "S N Chluainin",
    "addressOne": "Cloneen",
    "addressTwo": "Fethard",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17703F",
    "schoolName": "S N Ard Croine",
    "addressOne": "Ardcroney",
    "addressTwo": "Nenagh",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17712G",
    "schoolName": "S N Chiarda Naofa",
    "addressOne": "Kilkeary",
    "addressTwo": "Nenagh",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17731K",
    "schoolName": "S N Iosef Naofa",
    "addressOne": "Templemore",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17779P",
    "schoolName": "Powerstown N S",
    "addressOne": "Clonmel",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17783G",
    "schoolName": "S N Chuirt Doighte",
    "addressOne": "Burncourt",
    "addressTwo": "Cahir",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17799V",
    "schoolName": "S N Na Haille",
    "addressOne": "AILL,",
    "addressTwo": "AN MHOIN ARD",
    "addressThree": "CO THIOBRAD ARANN",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17850S",
    "schoolName": "S N Phadraig Naofa",
    "addressOne": "Kylepark",
    "addressTwo": "Borrisokane",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17940T",
    "schoolName": "S N Na Mbuachailli",
    "addressOne": "Clonbealy",
    "addressTwo": "Newport",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18062V",
    "schoolName": "S N An Grainseach",
    "addressOne": "Clonmel",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18085K",
    "schoolName": "Ballyneale N S",
    "addressOne": "Ballyneale",
    "addressTwo": "Carrick-on-Suir",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18087O",
    "schoolName": "S N Odhran Naofa",
    "addressOne": "Ballinaclough",
    "addressTwo": "Nenagh",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18135W",
    "schoolName": "Scoil Angela",
    "addressOne": "Ursuline Convent",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18164G",
    "schoolName": "S N Buirgheas",
    "addressOne": "NENAGH",
    "addressTwo": "CO TIPPERARY",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18207V",
    "schoolName": "S N Baile An Atha",
    "addressOne": "Grange Road",
    "addressTwo": "Ballina",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18213Q",
    "schoolName": "S N Leamhach",
    "addressOne": "Leugh",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18285S",
    "schoolName": "S N Cill Chuimin",
    "addressOne": "Thurles",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18322V",
    "schoolName": "S N An Droma",
    "addressOne": "Drom",
    "addressTwo": "Templemore",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18326G",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "CILL CHAIS",
    "addressTwo": "CLONMEL",
    "addressThree": "CO TIPPERARY",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18343G",
    "schoolName": "S N Chaoimhghin",
    "addressOne": "Littleton",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18345K",
    "schoolName": "S N Iosef Naofa",
    "addressOne": "Corville",
    "addressTwo": "Roscrea",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18350D",
    "schoolName": "S N Na Hinse",
    "addressOne": "Bouladuff",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18369B",
    "schoolName": "S N Cill Ruadhain",
    "addressOne": "Nenagh",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18379E",
    "schoolName": "Barnane N S",
    "addressOne": "Barnane",
    "addressTwo": "Templemore",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18396E",
    "schoolName": "S N Rath Eilte",
    "addressOne": "THURLES",
    "addressTwo": "CO TIPPERARY",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18435L",
    "schoolName": "Sacred Heart Primary School",
    "addressOne": "Newline",
    "addressTwo": "Roscrea",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18443K",
    "schoolName": "S N Iosef Naofa",
    "addressOne": "Aglish",
    "addressTwo": "Roscrea",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18459C",
    "schoolName": "Scoil Mhuire",
    "addressOne": "CILL MHEANMAN",
    "addressTwo": "MULLINAHONE THURLES",
    "addressThree": "CO TIPPERARY",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18465U",
    "schoolName": "S N Lios An Halla",
    "addressOne": "Lissenhall",
    "addressTwo": "Nenagh",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18486F",
    "schoolName": "S N Cill Siolain",
    "addressOne": "Chapel Road",
    "addressTwo": "Kilsheelan",
    "addressThree": "Clonmel",
    "addressFour": "Co. Tipperary",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18501V",
    "schoolName": "Grangemockler Ns",
    "addressOne": "Grangemockler",
    "addressTwo": "Carrick-On-Suir",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18512D",
    "schoolName": "St Francis National School",
    "addressOne": "GARRYSHANE",
    "addressTwo": "Donohill",
    "addressThree": "Co Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18528S",
    "schoolName": "Annacarty N S",
    "addressOne": "Shanaknock",
    "addressTwo": "Annacarty",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18538V",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Newcastle",
    "addressTwo": "Clonmel",
    "addressThree": "Co. Tippeary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18557C",
    "schoolName": "S N Teampall Doire",
    "addressOne": "Templederry",
    "addressTwo": "Nenagh",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18582B",
    "schoolName": "Ballylooby N S",
    "addressOne": "CAHIR",
    "addressTwo": "CO TIPPERARY",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18716T",
    "schoolName": "Cahir B N S",
    "addressOne": "Market Street",
    "addressTwo": "Cahir",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18775M",
    "schoolName": "S N Micheal Naofa",
    "addressOne": "Holycross",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19294B",
    "schoolName": "Lisvernane N S",
    "addressOne": "Lisvernane",
    "addressTwo": "The Glen of Aherlow",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19305D",
    "schoolName": "Tankerstown N S",
    "addressOne": "Tankerstown",
    "addressTwo": "Bansha",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19356U",
    "schoolName": "Killenaule N S",
    "addressOne": "Killenaule",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19422H",
    "schoolName": "Cloughjordan No 1 N S",
    "addressOne": "Townfields",
    "addressTwo": "Cloughjordan",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19540N",
    "schoolName": "Clogheen Central N S",
    "addressOne": "CLOGHEEN",
    "addressTwo": "CO TIPPERARY",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19640R",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Lismackin",
    "addressTwo": "Roscrea",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19645E",
    "schoolName": "St Oliver Plunketts Ns",
    "addressOne": "Mountain View",
    "addressTwo": "Elm Park",
    "addressThree": "Clonmel",
    "addressFour": "Co. Tipperary",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19677R",
    "schoolName": "Scoil Iosagain",
    "addressOne": "Upperchurch",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19689B",
    "schoolName": "Bansha N S",
    "addressOne": "BANSHA",
    "addressTwo": "CO TIPPERARY",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19690J",
    "schoolName": "Clonoulty Central N S",
    "addressOne": "Clonouty",
    "addressTwo": "Cashel",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19692N",
    "schoolName": "St John The Baptist",
    "addressOne": "Old Road",
    "addressTwo": "Cashel",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19696V",
    "schoolName": "Cashel Deanery",
    "addressOne": "CASHEL",
    "addressTwo": "CO TIPPERARY",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19874T",
    "schoolName": "Presentation Primary School",
    "addressOne": "Cathedral Street",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19879G",
    "schoolName": "Naomh Padraig Junior",
    "addressOne": "Drangan",
    "addressTwo": "Thurles",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19880O",
    "schoolName": "Naomh Padraig Senior",
    "addressOne": "Newtown",
    "addressTwo": "Drangan",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19937R",
    "schoolName": "Gaelscoil Aonach",
    "addressOne": "Bóthar Naomh Chonlain",
    "addressTwo": "an tAonach",
    "addressThree": "Co. Thiobraid Árann",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19968F",
    "schoolName": "Gaelscoil Durlas Eile",
    "addressOne": "Bóthar na Naomh",
    "addressTwo": "Durlas",
    "addressThree": "Co. Thiobraid Árann",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20007C",
    "schoolName": "Gaelscoil Chluain Meala",
    "addressOne": "Baile Gaelach",
    "addressTwo": "Cluain Meala",
    "addressThree": "Co. Thiobraid Árainn",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20027I",
    "schoolName": "Gaelscoil Thiobraid Arann",
    "addressOne": "Cnoc an Railéigh",
    "addressTwo": "Baile Thiobraid Árann",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20062K",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Nenagh Road",
    "addressTwo": "Borrisokane",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20085W",
    "schoolName": "Gaelscoil Charraig Na Siuire",
    "addressOne": "Coolnamuck Road",
    "addressTwo": "Carrick-on-Suir",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20129Q",
    "schoolName": "Scoil Teampall Toinne",
    "addressOne": "Ballyporeen",
    "addressTwo": "Cahir",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20155R",
    "schoolName": "St John The Baptist Boys School",
    "addressOne": "Old Road",
    "addressTwo": "Cashel",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20355C",
    "schoolName": "Holy Trinity National School",
    "addressOne": "Rocklow Road",
    "addressTwo": "Fethard",
    "addressThree": "Co Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20451V",
    "schoolName": "Scoil Naomh Cualán",
    "addressOne": "Borrisoleigh",
    "addressTwo": "Thurles",
    "addressThree": "Co Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20467N",
    "schoolName": "St Ailbe's National School",
    "addressOne": "Emly",
    "addressTwo": "Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20475M",
    "schoolName": "Our Lady's National School",
    "addressOne": "Thurles",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20504Q",
    "schoolName": "Rossmore National School",
    "addressOne": "Rossmore",
    "addressTwo": "Cashel",
    "addressThree": "Co Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19230V",
    "schoolName": "Cormaic Special School",
    "addressOne": "Monastery House",
    "addressTwo": "Golden Road",
    "addressThree": "Cashel",
    "addressFour": "Co. Tipperary",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19370O",
    "schoolName": "St Annes Special Sch",
    "addressOne": "Sean Ross Abbey",
    "addressTwo": "Roscrea",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19615S",
    "schoolName": "Scoil Aonghusa",
    "addressOne": "Cahir Road",
    "addressTwo": "Cashel",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19687U",
    "schoolName": "Coláiste Sliabh Na Mban",
    "addressOne": "Ferryhouse",
    "addressTwo": "Clonmel",
    "addressThree": "Co. Tipperary",
    "addressFour": "",
    "county": "Tipperary",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "01395H",
    "schoolName": "Aglish N S",
    "addressOne": "Aglish",
    "addressTwo": "Cappoquin",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "01711O",
    "schoolName": "S N Cill Rosanta",
    "addressOne": "Kilrossanty",
    "addressTwo": "Kilmacthomas",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "02889O",
    "schoolName": "S N An Chlais Mhor",
    "addressOne": "Clashmore",
    "addressTwo": "Co. Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "05548D",
    "schoolName": "Sn Baile Mhic Airt",
    "addressOne": "S.N. Bhaile Mhic Airt",
    "addressTwo": "Dún Garbhán",
    "addressThree": "Co. Phort Láirge",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "07441S",
    "schoolName": "Ballycurrane N S",
    "addressOne": "Clashmore",
    "addressTwo": "via Youghal",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "07737Q",
    "schoolName": "Villierstown N S",
    "addressOne": "Villierstown",
    "addressTwo": "Cappoquin",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "11969N",
    "schoolName": "Carrickbeg N S",
    "addressOne": "Crehana",
    "addressTwo": "Carrick-Beg",
    "addressThree": "Carick-on-Suir",
    "addressFour": "Co. Waterford",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "12007G",
    "schoolName": "Our Lady Of Good Counsel Gns",
    "addressOne": "Abbey Road",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "12535I",
    "schoolName": "S N Ursula Naofa",
    "addressOne": "Inner Ring Road",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13020D",
    "schoolName": "Our Lady Of Mercy N S",
    "addressOne": "Carrigahilla",
    "addressTwo": "Stradbally",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "13635R",
    "schoolName": "Ballyduff N S",
    "addressOne": "Ballyduff West",
    "addressTwo": "Kilmeaden",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "14164J",
    "schoolName": "S N Lios Mor Mochuda",
    "addressOne": "North Mall",
    "addressTwo": "Lismore",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "14568K",
    "schoolName": "Killea Boys N S",
    "addressOne": "Dunmore East",
    "addressTwo": "Co. Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "14679T",
    "schoolName": "S N Baile Builearaigh",
    "addressOne": "Butlerstown",
    "addressTwo": "Co. Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "14989L",
    "schoolName": "Passage East N S",
    "addressOne": "Crooke",
    "addressTwo": "Passage East",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15046I",
    "schoolName": "St Stephens N S",
    "addressOne": "25 Patrick Street",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15318P",
    "schoolName": "Glenbeg N S",
    "addressOne": "Glenbeg",
    "addressTwo": "Dungarvan",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15540Q",
    "schoolName": "Ballyduff B 2 N S",
    "addressOne": "Ballyduff",
    "addressTwo": "Co. Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "15963V",
    "schoolName": "Rathgormack B N S",
    "addressOne": "Carrick-on-Suir",
    "addressTwo": "Co. Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16732H",
    "schoolName": "Scoil Naomh Seosamh",
    "addressOne": "Mitchell Street",
    "addressTwo": "Dungarvan",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16748W",
    "schoolName": "S N Na Cille",
    "addressOne": "Kill",
    "addressTwo": "Kilmacthomas",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16818R",
    "schoolName": "Sn Na Leanai",
    "addressOne": "AN RINN",
    "addressTwo": "DUN GARBHAN",
    "addressThree": "CO PHORT LAIRGE",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16950R",
    "schoolName": "S N An Carraig Liath",
    "addressOne": "Ballyduff",
    "addressTwo": "Dungarvan",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "16976M",
    "schoolName": "S N Deaglan",
    "addressOne": "Water Street",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17133N",
    "schoolName": "S N An Baile Nua",
    "addressOne": "Newtown,",
    "addressTwo": "Kilmacthomas",
    "addressThree": "Co Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17159I",
    "schoolName": "S N An Garrain Bhain",
    "addressOne": "Garranbane",
    "addressTwo": "Dungarvan",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17295Q",
    "schoolName": "S N Na Rinne",
    "addressOne": "Maoil a'Chóirne",
    "addressTwo": "Rinn Ua gCuanach",
    "addressThree": "Dún Garbhán",
    "addressFour": "Co. Phortláirge",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17351A",
    "schoolName": "S N Naomh Mhuire",
    "addressOne": "Ballygunner",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17522B",
    "schoolName": "Knockanore N S",
    "addressOne": "Knockanore",
    "addressTwo": "Tallow",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17525H",
    "schoolName": "Light Of Christ National School",
    "addressOne": "Dunmore East",
    "addressTwo": "Co. Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17535K",
    "schoolName": "Fionnabhair N S",
    "addressOne": "Fenor",
    "addressTwo": "Tramore",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17536M",
    "schoolName": "S N Dun Aill",
    "addressOne": "Dunhill",
    "addressTwo": "Via Tramore",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17570M",
    "schoolName": "S N Na Bhfiodh",
    "addressOne": "Fews",
    "addressTwo": "Kilmacthomas",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17643N",
    "schoolName": "S N Cill Bhriain",
    "addressOne": "BALLINAMULT",
    "addressTwo": "CLONMEL",
    "addressThree": "CO WATERFORD",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17997C",
    "schoolName": "St Marys N S Grange",
    "addressOne": "Ballybrusa",
    "addressTwo": "Grange",
    "addressThree": "via Youghal",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18048E",
    "schoolName": "S N Naomh Deaglan",
    "addressOne": "College Road",
    "addressTwo": "Ardmore",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18077L",
    "schoolName": "S N Cnoc Machan",
    "addressOne": "Knockmahon",
    "addressTwo": "Bunmahon",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18094L",
    "schoolName": "S N Dun Na Mainistreach",
    "addressOne": "Sheares Street",
    "addressTwo": "Abbeyside",
    "addressThree": "Dungarvan",
    "addressFour": "Co. Waterford",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18108T",
    "schoolName": "Whitechurch N S Ceapach",
    "addressOne": "Clonkerdon",
    "addressTwo": "Cappagh",
    "addressThree": "Dungarvan",
    "addressFour": "Co. Waterford",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18167M",
    "schoolName": "S N Aine Nfa Seafield",
    "addressOne": "Seafield",
    "addressTwo": "Bonmahon",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18235D",
    "schoolName": "S N Muire An Port Mor",
    "addressOne": "Ross Road",
    "addressTwo": "Ferrybank",
    "addressThree": "Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18318H",
    "schoolName": "S N Naomh Parthalan",
    "addressOne": "CIONN SAILE BEAG",
    "addressTwo": "YOUGHAL",
    "addressThree": "CO CORK",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18321T",
    "schoolName": "S N Muire Magh Deilge",
    "addressOne": "Modeligo",
    "addressTwo": "Dungarvan",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18380M",
    "schoolName": "S N Faiche Liag",
    "addressOne": "Faithlegg",
    "addressTwo": "Co. Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18462O",
    "schoolName": "Scoil Lorcain Bns",
    "addressOne": "St. John's Park",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18488J",
    "schoolName": "Scoil Naomh Gobnait",
    "addressOne": "CUl NA SMEAR",
    "addressTwo": "DUNGARBHAN",
    "addressThree": "CO WATERFORD",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18509O",
    "schoolName": "An Teaghlaigh Naofa Girls Junior",
    "addressOne": "Military Road",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18681D",
    "schoolName": "Christ Church N S",
    "addressOne": "Lower Newtown",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18689T",
    "schoolName": "Our Lady Of Mercy Senior P.s.",
    "addressOne": "Military Road",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18779U",
    "schoolName": "St Marys Ns Touraneena",
    "addressOne": "BALLINAMULT",
    "addressTwo": "CLONMEL",
    "addressThree": "CO WATERFORD",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "18793O",
    "schoolName": "Sc Naomh Eoin Le Dia",
    "addressOne": "Passage Road",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19345P",
    "schoolName": "Ballymacarberry N S",
    "addressOne": "Ballymacarbry",
    "addressTwo": "Via Clonmel",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19443P",
    "schoolName": "Clonea Ns",
    "addressOne": "Clonea-Power",
    "addressTwo": "Carrick-on-Suir",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19511G",
    "schoolName": "St Saviours Ns",
    "addressOne": "Ballybeg Drive",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19616U",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Michael J Prendergast Road",
    "addressTwo": "Tallow",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19629G",
    "schoolName": "Holy Cross School",
    "addressOne": "Ballycarnane",
    "addressTwo": "Tramore",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19726E",
    "schoolName": "Stradbally C. Of Ire. Ns",
    "addressOne": "Church Lane",
    "addressTwo": "Stradbally",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19814B",
    "schoolName": "Scoil Gearbhain",
    "addressOne": "Clais na Lachan",
    "addressTwo": "Dún na Mainistreach",
    "addressThree": "Dún Garbhán",
    "addressFour": "Co. Phort Láirge",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19853L",
    "schoolName": "Gaelscoil Phort Lairge",
    "addressOne": "Baile Gunnair",
    "addressTwo": "Port Láirge",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19885B",
    "schoolName": "Gaelscoil Philib Barún",
    "addressOne": "Cruabhaile Uachtarach",
    "addressTwo": "An Trá Mhór",
    "addressThree": "Co. Phort Láirge",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19947U",
    "schoolName": "Mount Sion Cbs N S",
    "addressOne": "Barrack Street",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19953P",
    "schoolName": "St Marys Ns",
    "addressOne": "Youghal Road",
    "addressTwo": "Dungarvan",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19955T",
    "schoolName": "Presentation Primary School",
    "addressOne": "Slievekeale Road",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19970P",
    "schoolName": "Portlaw N.s.",
    "addressOne": "Convent Road",
    "addressTwo": "Portlaw",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20050D",
    "schoolName": "Gaelscoil Na Ndeise",
    "addressOne": "CAMPAS CHARRAIG PHIARAIS",
    "addressTwo": "BÓTHAR CHARRAIG PHIARAIS",
    "addressThree": "PORT LÁIRGE",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20076V",
    "schoolName": "Bun Scoil Bhothar Na Naomh",
    "addressOne": "Deerpark Road",
    "addressTwo": "Lismore",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20116H",
    "schoolName": "Glor Na Mara Ns",
    "addressOne": "Convent Hill",
    "addressTwo": "Tramore",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20143K",
    "schoolName": "Waterpark N S",
    "addressOne": "Park Road",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20157V",
    "schoolName": "Bunscoil Gleann Sidheain",
    "addressOne": "Cappoquin",
    "addressTwo": "Co. Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20160K",
    "schoolName": "Waterford Educate Together Ns",
    "addressOne": "Paráid na bPáisti",
    "addressTwo": "Carrickphierish Road",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20170N",
    "schoolName": "Scoil Choill Mhic Thomaisin",
    "addressOne": "Rossmore",
    "addressTwo": "Kilmacthomas",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20219R",
    "schoolName": "St Pauls B N S",
    "addressOne": "Church Road",
    "addressTwo": "Waterford",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20261Q",
    "schoolName": "Newtown Junior School",
    "addressOne": "Lower Newtown",
    "addressTwo": "Waterford City",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "20446F",
    "schoolName": "Tramore Educate Together National School",
    "addressOne": "Pond Road",
    "addressTwo": "Tramore",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19108B",
    "schoolName": "St Martins Special Sch",
    "addressOne": "St. John's Villas",
    "addressTwo": "Lower Grange",
    "addressThree": "Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19244J",
    "schoolName": "St Josephs Special Sch",
    "addressOne": "PARNELL STREET",
    "addressTwo": "WATERFORD",
    "addressThree": "",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "19282R",
    "schoolName": "St Johns Special Sch",
    "addressOne": "Youghal Road",
    "addressTwo": "Dungarvan",
    "addressThree": "Co. Waterford",
    "addressFour": "",
    "county": "Waterford",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "01687U",
    "schoolName": "Dromleigh N S",
    "addressOne": "Dromleigh",
    "addressTwo": "Kilmichael",
    "addressThree": "Macroom",
    "addressFour": "Co. Cork",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "04578H",
    "schoolName": "Ballyvongane Mixed N S",
    "addressOne": "Ballyvongane",
    "addressTwo": "Aghinagh,",
    "addressThree": "Coachford",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "05257P",
    "schoolName": "Presentation Convent",
    "addressOne": "Dunmanway Road",
    "addressTwo": "Bandon",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "05477G",
    "schoolName": "Laragh N S",
    "addressOne": "Laragh",
    "addressTwo": "Bandon",
    "addressThree": "Co Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "07651G",
    "schoolName": "St Joseph's Girls National School",
    "addressOne": "Convent Road",
    "addressTwo": "Clonakilty",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "08972I",
    "schoolName": "Castlealack N S",
    "addressOne": "Castlelack",
    "addressTwo": "Bandon",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "09161W",
    "schoolName": "Our Lady Of Mercy N S",
    "addressOne": "Summerhill",
    "addressTwo": "Bantry",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "09537S",
    "schoolName": "Ballinadee N S",
    "addressOne": "Ballinadee",
    "addressTwo": "Bandon",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "10047I",
    "schoolName": "Macroom Convent N S",
    "addressOne": "New Road",
    "addressTwo": "Macroom",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "10499U",
    "schoolName": "Kilgariffe N S",
    "addressOne": "Old Timoleague Road",
    "addressTwo": "Clonakilty",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "10548H",
    "schoolName": "St Brendans National School",
    "addressOne": "Wolfe Tone Square",
    "addressTwo": "Bantry",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "12147W",
    "schoolName": "S N An Aird",
    "addressOne": "Ardfield",
    "addressTwo": "Clonakilty",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "12292I",
    "schoolName": "Canovee Mixed N S",
    "addressOne": "Carrigadrohid",
    "addressTwo": "Macroom",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "12383L",
    "schoolName": "Union Hall Mixed N S",
    "addressOne": "Union Hall",
    "addressTwo": "Skibbereen",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "12395S",
    "schoolName": "Rusheen Ns",
    "addressOne": "Rusheen",
    "addressTwo": "Coachford",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "12505W",
    "schoolName": "Kilbarry N S",
    "addressOne": "Kilbarry",
    "addressTwo": "Macroom",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13095L",
    "schoolName": "S N An Droma Mhoir",
    "addressOne": "Dromore",
    "addressTwo": "Bantry",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13125R",
    "schoolName": "Scoil Dhairbhre",
    "addressOne": "Cruary",
    "addressTwo": "Clonakilty",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13286S",
    "schoolName": "Tirelton N S Mxd",
    "addressOne": "Tarelton",
    "addressTwo": "Macroom",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13375R",
    "schoolName": "Scoil Bhride",
    "addressOne": "Crossmahon",
    "addressTwo": "Bandon",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13728B",
    "schoolName": "Castletownsend Mxd N S",
    "addressOne": "Gurranes",
    "addressTwo": "Castletownshend",
    "addressThree": "Skibbereen",
    "addressFour": "Co. Cork",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "14022M",
    "schoolName": "Coachford N S",
    "addressOne": "Coachford",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "14227H",
    "schoolName": "Kilcoe Ns",
    "addressOne": "Kilcoe",
    "addressTwo": "Skibbereen",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "14433K",
    "schoolName": "Abbeystrewry N S",
    "addressOne": "Castletownsend Road",
    "addressTwo": "Skibbereen",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "14726C",
    "schoolName": "Kinsale N S",
    "addressOne": "Knocknabohilly",
    "addressTwo": "Kinsale",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "14839P",
    "schoolName": "Clondrohid N S",
    "addressOne": "Clondrohid",
    "addressTwo": "Macroom",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "15135H",
    "schoolName": "Bantry Boys Senior National School",
    "addressOne": "Seskin",
    "addressTwo": "Bantry",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "15410D",
    "schoolName": "Kilcrohane N S",
    "addressOne": "Kilcrohane",
    "addressTwo": "Bantry",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "15563F",
    "schoolName": "Lisheen Mixed N S",
    "addressOne": "Lisheen",
    "addressTwo": "Church Cross",
    "addressThree": "Skibbereen",
    "addressFour": "Co. Cork",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "15597W",
    "schoolName": "Macroom Boys Senior National School",
    "addressOne": "Sleaveen East",
    "addressTwo": "Macroom",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "15646J",
    "schoolName": "Coomhola N S",
    "addressOne": "Cooryleary",
    "addressTwo": "Bantry",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "16087E",
    "schoolName": "Kealkil N S",
    "addressOne": "BANTRY",
    "addressTwo": "CO CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "16246V",
    "schoolName": "Drumclugh N S",
    "addressOne": "Bantry",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "16286K",
    "schoolName": "Carrigboy N S",
    "addressOne": "DURRUS",
    "addressTwo": "BANTRY",
    "addressThree": "CO CORK",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "16885J",
    "schoolName": "Adrigole N S",
    "addressOne": "Adrigole",
    "addressTwo": "Bantry",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "16940O",
    "schoolName": "Dundar Mhuighe N S",
    "addressOne": "Dunderrow",
    "addressTwo": "Kinsale",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "16955E",
    "schoolName": "S N Na Mona Fliche",
    "addressOne": "MACROOM",
    "addressTwo": "CO CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17011W",
    "schoolName": "Mocomhog N S",
    "addressOne": "Cappabue",
    "addressTwo": "Kealkill",
    "addressThree": "Bantry",
    "addressFour": "Co. Cork",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17050J",
    "schoolName": "S N Naomh Sheamuis",
    "addressOne": "Durrus",
    "addressTwo": "Bantry",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17152R",
    "schoolName": "S N Cnoc Sceach",
    "addressOne": "Knockskeagh",
    "addressTwo": "Clonakilty",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17715M",
    "schoolName": "S N Rath A Bharraigh",
    "addressOne": "CLONAKILTY",
    "addressTwo": "CO CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18431D",
    "schoolName": "Sn Droichead Na Bandan",
    "addressOne": "Clancool More",
    "addressTwo": "Bandon",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18461M",
    "schoolName": "S N Muire Na Doirini",
    "addressOne": "Dreeny",
    "addressTwo": "Skibbereen",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18659K",
    "schoolName": "Lisavaird Mxd N S",
    "addressOne": "Lisavaird",
    "addressTwo": "Clonakilty",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19224D",
    "schoolName": "S N Cillmin",
    "addressOne": "Rossmore",
    "addressTwo": "Clonakilty",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19249T",
    "schoolName": "Barryroe Ns",
    "addressOne": "Lislevane",
    "addressTwo": "Bandon",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19348V",
    "schoolName": "Newcestown N S",
    "addressOne": "Newcestown",
    "addressTwo": "Bandon",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19381T",
    "schoolName": "Rathmore N S",
    "addressOne": "Baltimore",
    "addressTwo": "Skibbereen",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19501D",
    "schoolName": "Cahermore New Central S",
    "addressOne": "Cahermore",
    "addressTwo": "Allihies",
    "addressThree": "Beara",
    "addressFour": "Co Cork",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19507P",
    "schoolName": "Scoil Chaitigheirn",
    "addressOne": "Eyeries",
    "addressTwo": "Bantry",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19977G",
    "schoolName": "Bandon Boys Ns",
    "addressOne": "Convent Hill",
    "addressTwo": "Bandon",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19978I",
    "schoolName": "Maulatrahane Central Ns",
    "addressOne": "Leap",
    "addressTwo": "Skibbereen",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20022V",
    "schoolName": "Scoil Na Mbuachailli",
    "addressOne": "O'Rahilly Street",
    "addressTwo": "Clonakilty",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20088F",
    "schoolName": "Scoil Mhuire N.s.,",
    "addressOne": "SCHULL",
    "addressTwo": "CO CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20435A",
    "schoolName": "Scoil Naomh Eltin",
    "addressOne": "Kinsale",
    "addressTwo": "Co Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20477Q",
    "schoolName": "St Patrick's Boys' National School",
    "addressOne": "Skibbereen",
    "addressTwo": "Co Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "00467B",
    "schoolName": "Ballinspittle N S",
    "addressOne": "Ballycatten",
    "addressTwo": "Ballinspittle",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "04152T",
    "schoolName": "S N Cnoc An Bhile",
    "addressOne": "Knockavilla",
    "addressTwo": "Innishannon",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "05508O",
    "schoolName": "St Columbas N.s",
    "addressOne": "With Facility for Deaf Children",
    "addressTwo": "Douglas West",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "05565D",
    "schoolName": "Trafrask Mixed N S",
    "addressOne": "Trafrask",
    "addressTwo": "Adrigole",
    "addressThree": "Beara",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "05636A",
    "schoolName": "Dunmanway Model N S",
    "addressOne": "Bantry Road",
    "addressTwo": "Dunmanway",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "05940D",
    "schoolName": "Scoil Ursula",
    "addressOne": "Blackrock Road",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "08430S",
    "schoolName": "Scoil Naomh Seosamh",
    "addressOne": "Gortnaclohy",
    "addressTwo": "Skibberreen",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "10243I",
    "schoolName": "S N Achadh Eochaille",
    "addressOne": "Ahiohill",
    "addressTwo": "Enniskeane",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "10739O",
    "schoolName": "Ballincarriga Mxd N S",
    "addressOne": "Ballincarriga",
    "addressTwo": "Dunmanway",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "11245R",
    "schoolName": "Sn Cill Mhic Abhaidh",
    "addressOne": "Kilmacabea",
    "addressTwo": "Leap",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "12169J",
    "schoolName": "Templebrady N S",
    "addressOne": "Church Road",
    "addressTwo": "Crosshaven",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "12203G",
    "schoolName": "S N Naomh Antaine",
    "addressOne": "Beechwood Park",
    "addressTwo": "Ballinlough",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17804L",
    "schoolName": "S N Cnoc Na Manach",
    "addressOne": "Knocknamanagh",
    "addressTwo": "Minane Bridge",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17993R",
    "schoolName": "Scoil Mhuire Gan Smal B",
    "addressOne": "School Avenue",
    "addressTwo": "Glasheen Road",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18020F",
    "schoolName": "S N An Gharrain",
    "addressOne": "Gurranes",
    "addressTwo": "Innishannon",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18253F",
    "schoolName": "Scoil Naomh Caitriona",
    "addressOne": "Bishopstown Avenue",
    "addressTwo": "Model Farm Road",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18279A",
    "schoolName": "St Mary's Church Of Ireland N.s",
    "addressOne": "Waterpark",
    "addressTwo": "Carrigaline",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18292P",
    "schoolName": "Gaelscoil An Teaghlaigh Naofa",
    "addressOne": "Tory Top Road",
    "addressTwo": "Ballyphehane",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18296A",
    "schoolName": "Dromdhallagh N S",
    "addressOne": "Drimoleague",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18388F",
    "schoolName": "Scoil Naomh Micheal",
    "addressOne": "Church Road",
    "addressTwo": "Blackrock",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18428O",
    "schoolName": "S N Baile Nora",
    "addressOne": "Ballynora",
    "addressTwo": "Waterfall",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18444M",
    "schoolName": "S N Clogach",
    "addressOne": "Clogagh North",
    "addressTwo": "Timoleague",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18468D",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Castledonovan",
    "addressTwo": "Drimoleague",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18487H",
    "schoolName": "S N Naomh Mhuire",
    "addressOne": "Fearann",
    "addressTwo": "Ovens",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18491V",
    "schoolName": "S N Garran An Easaigh",
    "addressOne": "KILLBRITTAIN",
    "addressTwo": "CO CORK",
    "addressThree": "P72 YP78",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18535P",
    "schoolName": "St. Johns Girls N S",
    "addressOne": "Ballea Road",
    "addressTwo": "Carrigaline",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18563U",
    "schoolName": "Scoil Naomh Brid C",
    "addressOne": "Douglas Road",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18566D",
    "schoolName": "S N Athair Maitiu C",
    "addressOne": "Togher",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18713N",
    "schoolName": "S N Fionan Na Reanna",
    "addressOne": "Rennies",
    "addressTwo": "Nohoval",
    "addressThree": "Belgooly",
    "addressFour": "Co. Cork",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18734V",
    "schoolName": "Realt Na Maidine",
    "addressOne": "Ballyphehane",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18829J",
    "schoolName": "S N Chobh Chionn Tsaile",
    "addressOne": "Cionn tSáile",
    "addressTwo": "Co.Chorcaí.",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19000E",
    "schoolName": "S N An Spioraid Naomh C",
    "addressOne": "Curraheen Road",
    "addressTwo": "Bishopstown",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19231A",
    "schoolName": "S N Barra Naofa Bhuach",
    "addressOne": "Beaumont",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19232C",
    "schoolName": "S N Barra Naofa Cailini",
    "addressOne": "Woodvale Road",
    "addressTwo": "Beaumont",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19908K",
    "schoolName": "Gaelscoil Mhachan",
    "addressOne": "Ave de Rennes",
    "addressTwo": "Machan",
    "addressThree": "An Charraig Dhubh",
    "addressFour": "Corcaigh",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20009G",
    "schoolName": "Gaelscoil Dr M Ui Shuilleabhain",
    "addressOne": "Gort na Cloiche",
    "addressTwo": "An Sciobairín",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20025E",
    "schoolName": "Gaelscoil Droichead Na Banndan",
    "addressOne": "Cloch Mhic Shíomoin",
    "addressTwo": "Droichead na Banndan",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20049S",
    "schoolName": "Ringaskiddy Lower Harbour N S",
    "addressOne": "Ringaskiddy",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20077A",
    "schoolName": "Scoil An Spioraid Naomh (boys)",
    "addressOne": "Bishopstown",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20105C",
    "schoolName": "Star Of The Sea Primary School",
    "addressOne": "Maulbaun",
    "addressTwo": "Passage West",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20106E",
    "schoolName": "Scoil Nioclais",
    "addressOne": "Frankfield",
    "addressTwo": "Grange",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20182U",
    "schoolName": "St Endas Ns",
    "addressOne": "Kilnadur",
    "addressTwo": "Dunmanway",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20204E",
    "schoolName": "Scoil Mháirtin",
    "addressOne": "Kilworth",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20222G",
    "schoolName": "Scoil Chuil Aodha Barr D Inse",
    "addressOne": "Cúil-Aodha",
    "addressTwo": "Máighchromtha",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20265B",
    "schoolName": "Gaelscoil Chionn Tsáile",
    "addressOne": "An Ceapach",
    "addressTwo": "Cionn tSáile",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20310D",
    "schoolName": "Educate Together Carrigaline",
    "addressOne": "Kilnagleary",
    "addressTwo": "Carrigaline",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20335T",
    "schoolName": "Scoil Phadraig Naofa",
    "addressOne": "Foxwood",
    "addressTwo": "Rochestown",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20388R",
    "schoolName": "Bunscoil Chriost Ri",
    "addressOne": "Evergreen Road",
    "addressTwo": "Turners Cross",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20389T",
    "schoolName": "Scoil Maria Assumpta",
    "addressOne": "Ballyphehane",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20413N",
    "schoolName": "Douglas Rochestown Educate Together National School",
    "addressOne": "Garryduff Sports Centre",
    "addressTwo": "Rochestown",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20500I",
    "schoolName": "Gaelscoil An Chaisleáin",
    "addressOne": "Cumann Rugbaí Bhaile an Chollaigh",
    "addressTwo": "Tanner Park",
    "addressThree": "Cúl Rua,",
    "addressFour": "Baile an Chollaigh",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20522S",
    "schoolName": "Glasheen Pouladuff Cork City Primary School",
    "addressOne": "c/o Colaiste Stiofain Naofa",
    "addressTwo": "Tramore Rd, Cork City",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18208A",
    "schoolName": "Our Lady Of Good Counsel Ns",
    "addressOne": "Innishmore",
    "addressTwo": "Ballincollig",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18483W",
    "schoolName": "School Of The Divine Child (scoil An Linbh Íosa)",
    "addressOne": "Lavanagh Centre,",
    "addressTwo": "Ballintemple",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19759T",
    "schoolName": "St Marys Spec Sch",
    "addressOne": "Rochestown",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19788D",
    "schoolName": "St Kevins School",
    "addressOne": "The Rectory",
    "addressTwo": "Infirmary Road",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20074R",
    "schoolName": "St Gabriels Special School",
    "addressOne": "Murphy's Farm,",
    "addressTwo": "Curraheen Rd.,",
    "addressThree": "Bishopstown",
    "addressFour": "Cork.",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20119N",
    "schoolName": "Cork University Hos School",
    "addressOne": "Children's Ward",
    "addressTwo": "Wilton",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20162O",
    "schoolName": "Sonas Special Primary Junior School",
    "addressOne": "Cork Road",
    "addressTwo": "Carrigaline",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20331L",
    "schoolName": "Scoil Aisling",
    "addressOne": "Boreenmanna Road",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17338I",
    "schoolName": "S N Cill Bonain",
    "addressOne": "Knocknahilan",
    "addressTwo": "Aherla",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "05656G",
    "schoolName": "S N An Ghoilin",
    "addressOne": "Goleen",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "12259K",
    "schoolName": "S N Oir Cheann",
    "addressOne": "Urhan",
    "addressTwo": "Eyeries",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "12263B",
    "schoolName": "Gogginshill N S",
    "addressOne": "Goggins Hill",
    "addressTwo": "Ballinhassig",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "12456M",
    "schoolName": "Timoleague N S",
    "addressOne": "School Road",
    "addressTwo": "Timoleague",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "12473M",
    "schoolName": "Greenmount Monastery Ns",
    "addressOne": "Green Street",
    "addressTwo": "Greenmount",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "12685E",
    "schoolName": "Rushnacahara N S",
    "addressOne": "Rushnachara",
    "addressTwo": "Durrus",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "12967O",
    "schoolName": "Inchigeela N S",
    "addressOne": "Carrighleigh",
    "addressTwo": "Inchigeelagh",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13234W",
    "schoolName": "Cloughduv N S",
    "addressOne": "Cloughduv",
    "addressTwo": "Crookstown",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13512B",
    "schoolName": "Scoil Mhuire Lourdes",
    "addressOne": "Ballea Road",
    "addressTwo": "Carrigaline",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13543M",
    "schoolName": "Derrinacahara N S",
    "addressOne": "Derrinacahara",
    "addressTwo": "Dunmanway",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13648D",
    "schoolName": "St Lukes Mxd N S",
    "addressOne": "O' Mahony's Avenue",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13661S",
    "schoolName": "Dunmanway Convent Girls Senior National School",
    "addressOne": "Dunmanway",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13662U",
    "schoolName": "Dunmanway Convent Inf",
    "addressOne": "Dunmanway",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13828F",
    "schoolName": "Douglas B N S",
    "addressOne": "Douglas West",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13889C",
    "schoolName": "Shanbally N S",
    "addressOne": "Shanbally",
    "addressTwo": "Ringaskiddy",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13910N",
    "schoolName": "S N Bun An Tsabhairne",
    "addressOne": "Crosshaven",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13976U",
    "schoolName": "St Matthias N S",
    "addressOne": "Church Road",
    "addressTwo": "Ballydehob",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13980L",
    "schoolName": "S N B Togher Cork",
    "addressOne": "Togher Road",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "14116V",
    "schoolName": "Kilbrittain Mixed N S",
    "addressOne": "Kilbrittain",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "14225D",
    "schoolName": "Scoil Bhride",
    "addressOne": "Greenmount Road",
    "addressTwo": "Ballydehob",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "14303U",
    "schoolName": "S N Cleire",
    "addressOne": "Oileán Chléire",
    "addressTwo": "An Sciobairín",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "14430E",
    "schoolName": "Derrycreha N S",
    "addressOne": "Derrycreha",
    "addressTwo": "Glengarriff",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "14784Q",
    "schoolName": "Dunmanway Boys Senior National School",
    "addressOne": "Dunmanway",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "14816D",
    "schoolName": "Scoil Lachtain Naofa",
    "addressOne": "Cill na Martra",
    "addressTwo": "Maigh Chromtha",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "14993C",
    "schoolName": "S N Rae Na Ndoiri",
    "addressOne": "Rae na nDoirí",
    "addressTwo": "Maigh Chromtha",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "15346U",
    "schoolName": "Scoil Abán Naofa",
    "addressOne": "Baile Mhúirne",
    "addressTwo": "Maigh Chromtha",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "15550T",
    "schoolName": "Ballyheeda N S",
    "addressOne": "Ballinhassig",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "15781P",
    "schoolName": "Ballintemple N S",
    "addressOne": "BALLINTEMPLE N S",
    "addressTwo": "CRAB LANE",
    "addressThree": "CORK",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "16259H",
    "schoolName": "Kilcolman N S",
    "addressOne": "Kilcolman",
    "addressTwo": "Enniskeane",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "16746S",
    "schoolName": "Ballygarvan N S",
    "addressOne": "School Road",
    "addressTwo": "Ballygarvan",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "16863W",
    "schoolName": "Desertserges N S",
    "addressOne": "Desert",
    "addressTwo": "Enniskeane",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "16876I",
    "schoolName": "S N Caipin",
    "addressOne": "Coppeen",
    "addressTwo": "Enniskeane",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17024I",
    "schoolName": "Scoil Na Croise Naofa",
    "addressOne": "Avenue De Rennes",
    "addressTwo": "Mahon",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17105I",
    "schoolName": "Muire Gan Smal C",
    "addressOne": "School Avenue",
    "addressTwo": "Glasheen Road",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17168J",
    "schoolName": "S N Inis Eoghanain",
    "addressOne": "Scoil Eoin",
    "addressTwo": "Inis Eonáin",
    "addressThree": "Co.Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17245B",
    "schoolName": "Dripsey N S",
    "addressOne": "DRIPSEY",
    "addressTwo": "CO CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17251T",
    "schoolName": "Ovens N S",
    "addressOne": "Knockanemore",
    "addressTwo": "Ovens",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17281F",
    "schoolName": "Togher N S",
    "addressOne": "TOGHER",
    "addressTwo": "DUNMANWAY",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17313P",
    "schoolName": "Our Lady Of Lourdes",
    "addressOne": "Ballinlough Road",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17323S",
    "schoolName": "Monkstown N S",
    "addressOne": "Chapel Hill",
    "addressTwo": "Monkstown",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17363H",
    "schoolName": "Sn Cros Tseain",
    "addressOne": "Crosshaven Boy's NS",
    "addressTwo": "Crosshaven",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "17972J",
    "schoolName": "S N Cill Mhuire B",
    "addressOne": "Lisardagh",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18217B",
    "schoolName": "Scoil Padre Pio N S",
    "addressOne": "Churchfield Terrace West",
    "addressTwo": "Churchfield",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18246I",
    "schoolName": "S N Baile Muine",
    "addressOne": "Ballymoney",
    "addressTwo": "Ballineen",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "18356P",
    "schoolName": "S N Barra Naofa",
    "addressOne": "Gilabbey Terrace",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19420D",
    "schoolName": "Sn Fhiachna",
    "addressOne": "Kenmare Road",
    "addressTwo": "Glengarriff",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19525R",
    "schoolName": "Mhichil Naofa",
    "addressOne": "Derrycreeveen",
    "addressTwo": "Bere Island",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19557H",
    "schoolName": "Caheragh Ns",
    "addressOne": "Caheragh",
    "addressTwo": "Drimoleague",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19595P",
    "schoolName": "St Marys Central School",
    "addressOne": "ENNISKEANE",
    "addressTwo": "CO CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19637F",
    "schoolName": "Scoil Fionnbarra",
    "addressOne": "Béal Átha an Ghaorthaidh",
    "addressTwo": "Co. Chorcaí",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19672H",
    "schoolName": "Scoil Muire Na Ngrast",
    "addressOne": "Lacknacummeen",
    "addressTwo": "Belgooly",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19801P",
    "schoolName": "Drimoleague Junior Sch",
    "addressOne": "DRIMOLEAGUE",
    "addressTwo": "CO CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19839R",
    "schoolName": "Gael Scoil Ui Riordain",
    "addressOne": "Carraig an Earra",
    "addressTwo": "Baile an Chollaigh",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19852J",
    "schoolName": "Gaelscoil Ui Riada",
    "addressOne": "Bealach an Cháirdinéil",
    "addressTwo": "Wilton",
    "addressThree": "Corcaigh",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19882S",
    "schoolName": "Scoil An Athair Tadhg O Murchu",
    "addressOne": "Dúglas",
    "addressTwo": "Corcaigh",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "19918N",
    "schoolName": "Drinagh Mixed Ns",
    "addressOne": "DUNMANWAY",
    "addressTwo": "CO CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20001N",
    "schoolName": "Gaelscoil Bheanntrai",
    "addressOne": "Seisceann",
    "addressTwo": "Beanntraí",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20004T",
    "schoolName": "Scoil An Croi Ro Naofa",
    "addressOne": "Castletownbere",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "20006A",
    "schoolName": "Gaelscoil Mhichíl Uí Choileáin",
    "addressOne": "Bóthar Chnoc na Raithní",
    "addressTwo": "Cloch na gCoillte",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "01272O",
    "schoolName": "S N Chuan Doir",
    "addressOne": "Drombeg",
    "addressTwo": "Glandore",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "11931L",
    "schoolName": "S N Ioseph",
    "addressOne": "Derryclogh Lower",
    "addressTwo": "Drinagh",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "12012W",
    "schoolName": "St Lukes N S Douglas",
    "addressOne": "Churchyard Lane",
    "addressTwo": "Douglas",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "00538V",
    "schoolName": "Clochar Daingean",
    "addressOne": "An Phríomhsráid Uachtarach",
    "addressTwo": "Daingean Uí Chúis",
    "addressThree": "Co. Chiarraí",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "01396J",
    "schoolName": "Tulloha National School",
    "addressOne": "Bonane",
    "addressTwo": "Kenmare",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "01583I",
    "schoolName": "Glounaguillagh N S",
    "addressOne": "Glannagilliagh",
    "addressTwo": "Caragh Lake",
    "addressThree": "Killorglin",
    "addressFour": "Co. Kerry",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "02418V",
    "schoolName": "Knockaderry Farranfore National School",
    "addressOne": "Farranfore",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "03132I",
    "schoolName": "S N Sliabh A Mhadra",
    "addressOne": "Ballyduff",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "04062S",
    "schoolName": "Listowel Presentation Primary",
    "addressOne": "Ballybunion Road",
    "addressTwo": "Listowel",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "05348S",
    "schoolName": "Tahilla Community National School",
    "addressOne": "Tahilla",
    "addressTwo": "Sneem",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "05970M",
    "schoolName": "Scoil Barr Dubh",
    "addressOne": "Barraduff",
    "addressTwo": "Headford",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "06227L",
    "schoolName": "Sn Mhaolcheadair",
    "addressOne": "An Charraig",
    "addressTwo": "Baile na nGall",
    "addressThree": "Co. Chiarraí",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "07841L",
    "schoolName": "Kilgobnet N S",
    "addressOne": "Beaufort",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "07990F",
    "schoolName": "Scoil Naomh Carthach",
    "addressOne": "Ballyfinane",
    "addressTwo": "Firies",
    "addressThree": "Killarney",
    "addressFour": "Co Kerry",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "08147A",
    "schoolName": "Scoil An Ghleanna Pobal Scoil Náisi",
    "addressOne": "Aghort The Glen",
    "addressTwo": "Baile an Sceilg",
    "addressThree": "Co. Chiarraí",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "08251S",
    "schoolName": "Scoil Naomh Micheal",
    "addressOne": "West End",
    "addressTwo": "Sneem",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "08530W",
    "schoolName": "Lauragh National School",
    "addressOne": "Lauragh Lower",
    "addressTwo": "Co. Kerry",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "08687J",
    "schoolName": "S N Muire Gan Smal",
    "addressOne": "Coars",
    "addressTwo": "Mastergeehy",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "08791E",
    "schoolName": "Lissivigeen Mxd N S",
    "addressOne": "Lissivigeen",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09209B",
    "schoolName": "Castledrum N S",
    "addressOne": "Castledrum",
    "addressTwo": "Castlemain",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09260B",
    "schoolName": "S N Lios Teilic",
    "addressOne": "Listellick",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09302O",
    "schoolName": "Glenflesk N S",
    "addressOne": "Glenflesk",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09304S",
    "schoolName": "Raheen National School",
    "addressOne": "Raheen",
    "addressTwo": "Headford",
    "addressThree": "Killarney",
    "addressFour": "Co. Kerry",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09708T",
    "schoolName": "Knocknagoshel National School",
    "addressOne": "Knocknagoshel",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09782I",
    "schoolName": "Ballymacelligott 1 N S",
    "addressOne": "Flemby",
    "addressTwo": "Ballymacelligott",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09837H",
    "schoolName": "The Black Valley National School",
    "addressOne": "Dunloe Upper",
    "addressTwo": "Beaufort",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09841V",
    "schoolName": "S N Bhaile An Chrosaigh",
    "addressOne": "Ballyduff",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09878V",
    "schoolName": "Aghatubrid N S",
    "addressOne": "Direen",
    "addressTwo": "Caherciveen",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09938N",
    "schoolName": "Curranes N S",
    "addressOne": "Scoil Ide",
    "addressTwo": "Curranes",
    "addressThree": "Castleisland",
    "addressFour": "Co.Kerry",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "10014Q",
    "schoolName": "Coolick National School",
    "addressOne": "Coolick",
    "addressTwo": "Kilcummin",
    "addressThree": "Killarney",
    "addressFour": "Co. Kerry",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "10049M",
    "schoolName": "Loughguitane N S",
    "addressOne": "Dromickbane",
    "addressTwo": "Muckross",
    "addressThree": "Killarney",
    "addressFour": "Co. Kerry",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "10050U",
    "schoolName": "Scoil Bhríde",
    "addressOne": "Scartlea",
    "addressTwo": "Muckross",
    "addressThree": "Killarney",
    "addressFour": "Co Kerry",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "10239R",
    "schoolName": "Crochan Naofa N S",
    "addressOne": "Caherdaniel",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "10326M",
    "schoolName": "Scoil Nuachabhail",
    "addressOne": "Gortatlea",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "10394G",
    "schoolName": "Scoil Mhuire B&c",
    "addressOne": "Cordal",
    "addressTwo": "Castleisland",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "10501E",
    "schoolName": "Scoil Easa Dhuibhe",
    "addressOne": "Asdee",
    "addressTwo": "Co. Kerry",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "10755M",
    "schoolName": "Scoil An Fhirtearaigh",
    "addressOne": "Baile an Fheirtéaraigh",
    "addressTwo": "Co. Chiarraí",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "10775S",
    "schoolName": "Scoil Chorp Chríost",
    "addressOne": "Knockanure",
    "addressTwo": "Moyvane",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "10957B",
    "schoolName": "S N Mhuire De Lourdes",
    "addressOne": "The Village",
    "addressTwo": "Lixnaw",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "11067T",
    "schoolName": "S N Naomh Brid",
    "addressOne": "Duagh",
    "addressTwo": "Listowel",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "11248A",
    "schoolName": "Sn An Ghleanna",
    "addressOne": "Glin South",
    "addressTwo": "Dingle",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "11363A",
    "schoolName": "Scoil Atha Na Mblath",
    "addressOne": "Kilcummin",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "11405N",
    "schoolName": "Faha National School",
    "addressOne": "Faha",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "11419B",
    "schoolName": "Scoil Bhreanainn",
    "addressOne": "Portmagee",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "11746Q",
    "schoolName": "Castlegregory Mxd N S",
    "addressOne": "Strand Road",
    "addressTwo": "Castlegregory",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "12354E",
    "schoolName": "Clogher Mxd N S",
    "addressOne": "Ballydwyer East",
    "addressTwo": "Ballymecelligott",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "12587E",
    "schoolName": "Coolard Mxd N S",
    "addressOne": "Shronowen",
    "addressTwo": "Coolard",
    "addressThree": "Listowel",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "12701W",
    "schoolName": "Scoil An Fhaill Mor",
    "addressOne": "Foilduff",
    "addressTwo": "Caherciveen",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "12820H",
    "schoolName": "Scoil Realt Na Mara",
    "addressOne": "Cromane",
    "addressTwo": "Killorglin",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "12832O",
    "schoolName": "Scoil Mhuire B",
    "addressOne": "Killorglin",
    "addressTwo": "Co. Kerry",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "12875J",
    "schoolName": "Douglas National School",
    "addressOne": "Douglas",
    "addressTwo": "Killorglin",
    "addressThree": "Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "13150Q",
    "schoolName": "Knockanes Mxd N S",
    "addressOne": "Knockanes",
    "addressTwo": "Headford",
    "addressThree": "Killarney",
    "addressFour": "Co. Kerry",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "13530D",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Moyderwell",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "13540G",
    "schoolName": "Murhur N S",
    "addressOne": "Listowel Road",
    "addressTwo": "Moyvane",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "13615L",
    "schoolName": "Scoil Eoin",
    "addressOne": "Balloonagh",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "14002G",
    "schoolName": "Knockaclarig Mxd N S",
    "addressOne": "Knockaclarig",
    "addressTwo": "Brosna",
    "addressThree": "Tralee",
    "addressFour": "Co. Kerry",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "14366V",
    "schoolName": "Loughfouder N S",
    "addressOne": "Knocknagoshel",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "14767Q",
    "schoolName": "Aghacasla N S",
    "addressOne": "Aughacasla",
    "addressTwo": "Camp",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "14952L",
    "schoolName": "Muire Gan Smal",
    "addressOne": "Old Chapel Lane",
    "addressTwo": "Castleisland",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "14987H",
    "schoolName": "S N An Chlochan",
    "addressOne": "An Clochán",
    "addressTwo": "Co. Chiarraí",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "14998M",
    "schoolName": "Lyre A Crompane N S",
    "addressOne": "Glashanacree",
    "addressTwo": "Lyracrompane",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "15033W",
    "schoolName": "S N Treasa Naofa Mxd",
    "addressOne": "Castletown",
    "addressTwo": "Kilflynn",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "15592M",
    "schoolName": "Sn Ceann Tra Meascaithe",
    "addressOne": "Ceann Trá",
    "addressTwo": "Trá Lí",
    "addressThree": "Co. Chiarraí",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "15644F",
    "schoolName": "Tiernaboul N S",
    "addressOne": "KILLARNEY",
    "addressTwo": "CO KERRY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "15878H",
    "schoolName": "Derryquay Mxd N S",
    "addressOne": "Derrymore",
    "addressTwo": "Tralee",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "15945T",
    "schoolName": "Firies Mxd N S",
    "addressOne": "Church Road",
    "addressTwo": "Firies Village",
    "addressThree": "Killarney",
    "addressFour": "Co. Kerry",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "15978L",
    "schoolName": "Curraheen Mxd N S",
    "addressOne": "GLENBEIGH",
    "addressTwo": "CO KERRY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16014A",
    "schoolName": "St Finians",
    "addressOne": "Spunkane",
    "addressTwo": "Waterville",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16217O",
    "schoolName": "An Bhreac Chluain B",
    "addressOne": "ANNASCAUL",
    "addressTwo": "TRALEE",
    "addressThree": "CO KERRY",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16281A",
    "schoolName": "Sn Naomh Gobnait",
    "addressOne": "BAILE AN FHEIRTÉARAIGH",
    "addressTwo": "TRÁ LÍ",
    "addressThree": "CO CHIARRAI",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16456J",
    "schoolName": "Scoil Naisiunta Eirc",
    "addressOne": "Baile an Mhóraigh",
    "addressTwo": "Trá Lí",
    "addressThree": "Co. Chiarraí",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16703A",
    "schoolName": "Scoil Na Mbraithre",
    "addressOne": "An Daingean",
    "addressTwo": "Co. Chiarraí",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16744O",
    "schoolName": "Boheshill Mxd",
    "addressOne": "Boheshil",
    "addressTwo": "Glencar",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16779K",
    "schoolName": "Scoil Caitlin Naofa",
    "addressOne": "Cill Mhic an Domhnaigh,",
    "addressTwo": "Ceann Trá,",
    "addressThree": "Trá Lí,",
    "addressFour": "Co. Chiarraí.",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16871V",
    "schoolName": "S N An Chroi Naofa",
    "addressOne": "Castle Street Upper",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16898S",
    "schoolName": "S N Breandan Naofa",
    "addressOne": "CATHAR UI MHODHRAIN",
    "addressTwo": "TRAIGHLI",
    "addressThree": "CO CHIARRAI",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16917T",
    "schoolName": "Naomh Padraig Measc",
    "addressOne": "CATHAIR LEITHIN",
    "addressTwo": "TRAIGHLI",
    "addressThree": "CO CHIARRAI",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17012B",
    "schoolName": "S N An Fhossa",
    "addressOne": "Fossa",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17072T",
    "schoolName": "S N Cill Cruinn",
    "addressOne": "Listowel",
    "addressTwo": "Co. Kerry",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17161S",
    "schoolName": "Kiltallagh N S",
    "addressOne": "KILTALLAGH",
    "addressTwo": "CASTLEMAINE",
    "addressThree": "CO KERRY",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17231N",
    "schoolName": "S N Cill Cuimin",
    "addressOne": "Clashnagarrane",
    "addressTwo": "Kilcummin",
    "addressThree": "Killarney",
    "addressFour": "Co. Kerry",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17349N",
    "schoolName": "S N An Leana Mhoir",
    "addressOne": "Lenamore",
    "addressTwo": "Ballylongford",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17358O",
    "schoolName": "S N Naomh Eirc",
    "addressOne": "Cill Mhaoile",
    "addressTwo": "Ard Fhearta",
    "addressThree": "Co. Chiarrai",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17365L",
    "schoolName": "Two Mile Community National School",
    "addressOne": "Aughaleemore",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17646T",
    "schoolName": "S N Uaimh Bhreanainn",
    "addressOne": "Cill Dubh",
    "addressTwo": "Tra Li",
    "addressThree": "Co Chiarrai",
    "addressFour": "V92 AE73",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17710C",
    "schoolName": "S N An Chuilleanaig",
    "addressOne": "Beaufort",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17744T",
    "schoolName": "S N Gniomh Go Leith B",
    "addressOne": "RATHMORE",
    "addressTwo": "CO KERRY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17747C",
    "schoolName": "S N Na Srona",
    "addressOne": "Rathmore",
    "addressTwo": "Co. Kerry",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17915U",
    "schoolName": "Freastogail Mhuire Mxd",
    "addressOne": "Killahan",
    "addressTwo": "Abbeydorney",
    "addressThree": "Tralee",
    "addressFour": "Co. Kerry",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18039D",
    "schoolName": "Na Minteoga N S",
    "addressOne": "Meentogues",
    "addressTwo": "Headford",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18049G",
    "schoolName": "Muire Na Mainistreach",
    "addressOne": "New Road",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18084I",
    "schoolName": "Scoil Mhuire Gan Smal",
    "addressOne": "Ballinageragh",
    "addressTwo": "Lixnaw",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18184M",
    "schoolName": "S N Cill Lúraigh",
    "addressOne": "Causeway",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18214S",
    "schoolName": "Sn Cill Conla",
    "addressOne": "CILL CONLA",
    "addressTwo": "BALLYBUNION",
    "addressThree": "CO KERRY",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18233W",
    "schoolName": "St Johns Parochial School",
    "addressOne": "Ashe Street",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18247K",
    "schoolName": "S N Mhuire Na Mbraithre",
    "addressOne": "Clounalour",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18283O",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Knockognoe",
    "addressTwo": "Brosna",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18325E",
    "schoolName": "Naomh Charthaigh",
    "addressOne": "College Road",
    "addressTwo": "Castleisland",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18414D",
    "schoolName": "S N Gleann Beithe",
    "addressOne": "Glenbeigh",
    "addressTwo": "Co. Kerry",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18573A",
    "schoolName": "Scoil Chriost Ri",
    "addressOne": "Drumnacurra",
    "addressTwo": "Causeway",
    "addressThree": "Tralee",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18590A",
    "schoolName": "Scoil Naomh Erc",
    "addressOne": "Glenderry",
    "addressTwo": "Ballyheigue",
    "addressThree": "Tralee",
    "addressFour": "Co. Kerry",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18665F",
    "schoolName": "S N Mainistir O Dtorna",
    "addressOne": "Abbeydorney",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18702I",
    "schoolName": "Spa National School",
    "addressOne": "Ballygarron",
    "addressTwo": "Spa",
    "addressThree": "Tralee",
    "addressFour": "Co. Kerry",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18705O",
    "schoolName": "Tarbert National School",
    "addressOne": "Chapel Street",
    "addressTwo": "Tarbert",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18756I",
    "schoolName": "Fibough National School",
    "addressOne": "CAISLEAN NA MAINGE",
    "addressTwo": "CO CHIARRAI",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18759O",
    "schoolName": "Cahir National School",
    "addressOne": "Caher West",
    "addressTwo": "Kenmare",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18832V",
    "schoolName": "Castlemaine N S",
    "addressOne": "Castlemaine",
    "addressTwo": "Co. Kerry",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18856M",
    "schoolName": "Scoil Naomh Iosef",
    "addressOne": "Ballyheigue",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19304B",
    "schoolName": "S N Cillin Liath",
    "addressOne": "Máistir Gaoithe",
    "addressTwo": "Cill Airne",
    "addressThree": "Co. Chiarraí",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19384C",
    "schoolName": "Ardfert Central N S",
    "addressOne": "Station Road",
    "addressTwo": "Ardfert",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19436S",
    "schoolName": "Sn Naomh Mhichil",
    "addressOne": "Dungeagáin",
    "addressTwo": "Baile 'n Sceilg",
    "addressThree": "CO CHIARRAI",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19448C",
    "schoolName": "S N Realt Na Mara",
    "addressOne": "TUATH O SIOSTA",
    "addressTwo": "CILL AIRNE",
    "addressThree": "CO CHIARRAI",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19458F",
    "schoolName": "Kilgarvan Central Schl",
    "addressOne": "Kilgarvan",
    "addressTwo": "Co. Kerry",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19461R",
    "schoolName": "Eiltin Naofa",
    "addressOne": "LIOS EILTIN",
    "addressTwo": "CO. CHIARRAI",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19483E",
    "schoolName": "S N Dar Earca",
    "addressOne": "Ballyhearny",
    "addressTwo": "Valentia",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19487M",
    "schoolName": "Holy Cross Primary School",
    "addressOne": "New Road",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19492F",
    "schoolName": "Sn Oilibhear Naofa",
    "addressOne": "Ballylongford",
    "addressTwo": "Co. Kerry",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19512I",
    "schoolName": "St Olivers Ns",
    "addressOne": "Ballycasheen",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19536W",
    "schoolName": "Holy Family",
    "addressOne": "Balloonagh",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19631Q",
    "schoolName": "Gaelscoil Mhic Easmainn",
    "addressOne": "Ráth Rónáin",
    "addressTwo": "Trá Lí",
    "addressThree": "Co. Chiarraí",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19659P",
    "schoolName": "St Brendans N S",
    "addressOne": "FENIT",
    "addressTwo": "TRALEE",
    "addressThree": "CO KERRY",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19745I",
    "schoolName": "Scartaglin New Cent Sc",
    "addressOne": "SCAIRTEACH A GHLINNE",
    "addressTwo": "CILL AIRNE",
    "addressThree": "CO CHIARRAÍ",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19846O",
    "schoolName": "Realt Na Maidne",
    "addressOne": "Listowel",
    "addressTwo": "Co. Kerry",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19917L",
    "schoolName": "Gaelscoil Naomh Aogain",
    "addressOne": "Bóthar an Choláiste",
    "addressTwo": "Oileán Ciarraí",
    "addressThree": "Co. Chiarraí",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19941I",
    "schoolName": "Gaelscoil Faithleann",
    "addressOne": "An Pháirc",
    "addressTwo": "Cill Airne",
    "addressThree": "Co. Chiarraí",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19957A",
    "schoolName": "Dromclough N S",
    "addressOne": "Listowell",
    "addressTwo": "Co. Kerry",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19986H",
    "schoolName": "S N Eoin Baiste",
    "addressOne": "Garraí na dTor",
    "addressTwo": "Liopoil",
    "addressThree": "Co. Chiarraí",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20013U",
    "schoolName": "Gaelscoil Lios Tuathail",
    "addressOne": "LIOS TUATHAIL",
    "addressTwo": "CO CHIARRAI",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20111U",
    "schoolName": "Holy Family National School",
    "addressOne": "RATHMORE",
    "addressTwo": "CO KERRY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20112W",
    "schoolName": "Caherciveen N S",
    "addressOne": "O'Connell Street",
    "addressTwo": "Cahirciveen",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20147S",
    "schoolName": "Scoil Mhuire Agus N.treasa",
    "addressOne": "Currow",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20158A",
    "schoolName": "Tralee Educate Together Ns",
    "addressOne": "Collis Sandes House",
    "addressTwo": "Kileen",
    "addressThree": "Oakpark",
    "addressFour": "Tralee",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20196I",
    "schoolName": "Scoil Íosagáin",
    "addressOne": "Church Road",
    "addressTwo": "Ballybunion",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20197K",
    "schoolName": "St Johns Ns",
    "addressOne": "Railway Road",
    "addressTwo": "Kenmare",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20229U",
    "schoolName": "Nagle Rice Primary School",
    "addressOne": "Milltown",
    "addressTwo": "Killarney",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20478S",
    "schoolName": "Ballyduff National School",
    "addressOne": "Ballyduff",
    "addressTwo": "Tralee",
    "addressThree": "Co. Kerry",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19376D",
    "schoolName": "St Itas & St Josephs Ns",
    "addressOne": "ST ITAS/ST JOSEPHS N S",
    "addressTwo": "BALLOONAGH",
    "addressThree": "TRALEE",
    "addressFour": "CO KERRY",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19547E",
    "schoolName": "St Francis Special Sch",
    "addressOne": "BEAUFORT",
    "addressTwo": "CO. KERRY",
    "addressThree": "",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19548G",
    "schoolName": "Nano Nagle N School",
    "addressOne": "NANO NAGLE SPECIAL NS",
    "addressTwo": "LISTOWEL",
    "addressThree": "COUNTY KERRY",
    "addressFour": "",
    "county": "Kerry",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "02007A",
    "schoolName": "Croagh National School",
    "addressOne": "Croagh",
    "addressTwo": "Rathkeale",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "02358G",
    "schoolName": "Templeglantine N S",
    "addressOne": "Templeglantine East",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "02813E",
    "schoolName": "S N Sheanain",
    "addressOne": "Corgrigg",
    "addressTwo": "Foynes",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "04466T",
    "schoolName": "Ballymartin N S",
    "addressOne": "Manister",
    "addressTwo": "Croom",
    "addressThree": "Co Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "04469C",
    "schoolName": "S N Fiodhnach",
    "addressOne": "Feenagh",
    "addressTwo": "Kilmallock",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "06516S",
    "schoolName": "Kildimo National School",
    "addressOne": "Kildimo",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "06539H",
    "schoolName": "Knockea National School",
    "addressOne": "Knockea",
    "addressTwo": "Ballyneety",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "06936R",
    "schoolName": "St Johns Convent",
    "addressOne": "Cathedral Place",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "07117J",
    "schoolName": "S N Loch Guir",
    "addressOne": "Patrickswell",
    "addressTwo": "Bruff",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "07222G",
    "schoolName": "Banogue National School",
    "addressOne": "Dohora",
    "addressTwo": "Banogue",
    "addressThree": "Croom",
    "addressFour": "Co. Limerick",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "07317R",
    "schoolName": "Glengurt N S",
    "addressOne": "Tournafulla",
    "addressTwo": "Ballagh",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "07857D",
    "schoolName": "S N Ailbhe",
    "addressOne": "Killinure",
    "addressTwo": "Brittas",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "07900B",
    "schoolName": "Ballysteen N S",
    "addressOne": "Ballysteen",
    "addressTwo": "Askeaton",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "08342V",
    "schoolName": "Bohermore N S",
    "addressOne": "Srahane",
    "addressTwo": "Ballysimon",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "08419H",
    "schoolName": "Ardpatrick N S",
    "addressOne": "Ardpatrick",
    "addressTwo": "Kilmallock",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "08572P",
    "schoolName": "Bruree National School",
    "addressOne": "Bruree",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "08926B",
    "schoolName": "St Nicholas Church Of Ireland Schoo",
    "addressOne": "Blackabbey",
    "addressTwo": "Adare",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09132P",
    "schoolName": "Carnane Mxd N S",
    "addressOne": "Carnane",
    "addressTwo": "Fedamore",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09296W",
    "schoolName": "Our Ladys Abbey",
    "addressOne": "Blackabbey",
    "addressTwo": "Adare",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09306W",
    "schoolName": "Croom National School",
    "addressOne": "High Street",
    "addressTwo": "Croom",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09401Q",
    "schoolName": "Monogay National School",
    "addressOne": "Monagea",
    "addressTwo": "Newcastle West",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09702H",
    "schoolName": "St James N S",
    "addressOne": "Cappagh",
    "addressTwo": "Askeaton",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09915B",
    "schoolName": "Martinstown N S",
    "addressOne": "Martinstown",
    "addressTwo": "Kilmallock",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "09927I",
    "schoolName": "Granagh National School",
    "addressOne": "Granagh",
    "addressTwo": "Kilmallock",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "10929T",
    "schoolName": "Rathkeale N S 2",
    "addressOne": "Church Street",
    "addressTwo": "Rathkeale",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "10991B",
    "schoolName": "Garrydoolis N S",
    "addressOne": "Garrydoolis",
    "addressTwo": "Pallasgreen",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "11280T",
    "schoolName": "Carrickerry N S",
    "addressOne": "Carrickerry",
    "addressTwo": "Athea",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "11307N",
    "schoolName": "Ballyguiltenane N S",
    "addressOne": "Ballyguiltenane Upper",
    "addressTwo": "Glin",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "11422N",
    "schoolName": "Mahoonagh N S",
    "addressOne": "Main Street",
    "addressTwo": "Castlemahon",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "11809O",
    "schoolName": "S N Cnoch A Deaga",
    "addressOne": "Knockadea",
    "addressTwo": "Ballylanders",
    "addressThree": "Kilmallock",
    "addressFour": "Co. Limerick",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "11955C",
    "schoolName": "Coolcappa N S",
    "addressOne": "Ardagh",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "12368P",
    "schoolName": "S N Naomh Padraig",
    "addressOne": "Knocknasnaa",
    "addressTwo": "Abbeyfeale",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "12613C",
    "schoolName": "Glenbrohane N S",
    "addressOne": "Glenbrohane",
    "addressTwo": "Garryspillane",
    "addressThree": "Kilmallock",
    "addressFour": "Co. Limerick",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "12631E",
    "schoolName": "Pallaskenry N S",
    "addressOne": "Pallas",
    "addressTwo": "Pallaskenry",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "12834S",
    "schoolName": "St Michaels",
    "addressOne": "11 Barrington Street",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "12975N",
    "schoolName": "St Josephs Convent",
    "addressOne": "Lower Knockane Road",
    "addressTwo": "Newcastle West",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "13026P",
    "schoolName": "Kilfinane Convent Primary School",
    "addressOne": "Castle Street",
    "addressTwo": "Kilfinane",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "13459A",
    "schoolName": "S N Gallbhaile",
    "addressOne": "Galbally",
    "addressTwo": "Co. Tipperary",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "13790G",
    "schoolName": "Bulgaden N S",
    "addressOne": "Bulgaden",
    "addressTwo": "Kilmallock",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "14005M",
    "schoolName": "Kilteely N S",
    "addressOne": "Kilteely",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "14067L",
    "schoolName": "Fedamore N S",
    "addressOne": "Ballyea",
    "addressTwo": "Fedamore",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "14075K",
    "schoolName": "S N Molua B",
    "addressOne": "Cross",
    "addressTwo": "Ardagh",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "14231V",
    "schoolName": "Nicker N S",
    "addressOne": "Nicker",
    "addressTwo": "Pallasgreen",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "14305B",
    "schoolName": "Ballylanders N S",
    "addressOne": "Main Street",
    "addressTwo": "Ballylanders",
    "addressThree": "KIlmallock",
    "addressFour": "Co. Limerick",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "14409N",
    "schoolName": "Scoil Neassain",
    "addressOne": "Mungret",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "14625T",
    "schoolName": "Doon Convent N S",
    "addressOne": "Doon",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "15226K",
    "schoolName": "Caherline N S",
    "addressOne": "Caherline",
    "addressTwo": "Caherconlish",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "15320C",
    "schoolName": "St Michaels Ns",
    "addressOne": "Sexton Street",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "15680J",
    "schoolName": "Scoil An Spioraid Naomh",
    "addressOne": "Roxborough",
    "addressTwo": "Ballysheedy",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "15685T",
    "schoolName": "Athea N School",
    "addressOne": "Athea",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "15692Q",
    "schoolName": "Bilboa N School",
    "addressOne": "Bilaboa",
    "addressTwo": "Cappaghmore",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "15700M",
    "schoolName": "Cloverfield N S",
    "addressOne": "Cloverfield",
    "addressTwo": "Dromchaoin",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16118M",
    "schoolName": "Donoughmore N S",
    "addressOne": "DONOUGHMORE",
    "addressTwo": "CO. LIMERICK",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16237U",
    "schoolName": "Dromtrasna N S",
    "addressOne": "Dromtrasna National School",
    "addressTwo": "Dromtrasna North",
    "addressThree": "Abbeyfeale",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16239B",
    "schoolName": "Meenkilly N S",
    "addressOne": "ABBEYFEALE",
    "addressTwo": "CO LIMERICK",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16264A",
    "schoolName": "Abbeyfeale B N S 1",
    "addressOne": "Church Street",
    "addressTwo": "Abbeyfeale",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16439J",
    "schoolName": "Scoil Na Mbearnan",
    "addressOne": "PALLASAGREEN",
    "addressTwo": "CO LIMERICK",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16443A",
    "schoolName": "Scoil Padraig Naofa B",
    "addressOne": "DUBLIN ROAD",
    "addressTwo": "LIMERICK",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16444C",
    "schoolName": "Scoil Padraig Naofa C",
    "addressOne": "Dublin Road",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16508C",
    "schoolName": "Scoil Athain",
    "addressOne": "Laught",
    "addressTwo": "Lisnagry",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16712B",
    "schoolName": "Scoil Naomh Iosaf",
    "addressOne": "Rathkeale Road",
    "addressTwo": "Adare",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16713D",
    "schoolName": "Scoil Na Mbraithre",
    "addressOne": "DOON",
    "addressTwo": "CO LIMERICK",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16715H",
    "schoolName": "St John The Baptist Boys N S",
    "addressOne": "Downey Street",
    "addressTwo": "Pennywell",
    "addressThree": "Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16764U",
    "schoolName": "Kilmeedy N S",
    "addressOne": "KILMEEDY",
    "addressTwo": "CO LIMERICK",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16896O",
    "schoolName": "Scoil Naomh Ide",
    "addressOne": "Ashford",
    "addressTwo": "Ballagh",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16910F",
    "schoolName": "Scoil Iosagain",
    "addressOne": "Sexton Street",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "16913L",
    "schoolName": "Scoil Na Naoinean",
    "addressOne": "EAS GEIPHTINE",
    "addressTwo": "CO LUIMNI",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17060M",
    "schoolName": "Convent Of Mercy N S",
    "addressOne": "Convent Road",
    "addressTwo": "Abbeyfeale",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17068F",
    "schoolName": "Scoil Sean Tsraide",
    "addressOne": "Tuogh",
    "addressTwo": "Adare",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17101A",
    "schoolName": "Scoil Cre Cumhra",
    "addressOne": "Crecora",
    "addressTwo": "Patrickswell",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17138A",
    "schoolName": "Scoil Cill Colmain",
    "addressOne": "ARD ACHADH",
    "addressTwo": "CO LUIMNIGH",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17155A",
    "schoolName": "Athlacca N S",
    "addressOne": "Athlacca",
    "addressTwo": "Kilmallock",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17184H",
    "schoolName": "Oola National School",
    "addressOne": "OOLA",
    "addressTwo": "TIPPERARY",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17212J",
    "schoolName": "Scoil Nais Cnoc Aine",
    "addressOne": "Knockainey",
    "addressTwo": "Hospital",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17298W",
    "schoolName": "Kilbehenny N S",
    "addressOne": "KILBEHENNY",
    "addressTwo": "MITCHELSTOWN",
    "addressThree": "CO CORK",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17299B",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Effin",
    "addressTwo": "Kilmallock",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17364J",
    "schoolName": "Scoil Baile An Aird",
    "addressOne": "Ballinard",
    "addressTwo": "Herbertstown",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17438M",
    "schoolName": "Shanagolden N S",
    "addressOne": "Shanagolden",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17487C",
    "schoolName": "Scoil Nais Cathair Chinn Lis",
    "addressOne": "Caherconlish",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17593B",
    "schoolName": "Scoil Naomh Mhuire",
    "addressOne": "Caherlevoy",
    "addressTwo": "Mountcollins",
    "addressThree": "Abbeyfeale",
    "addressFour": "Co. Limerick",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17737W",
    "schoolName": "Our Lady Queen Of Peace School",
    "addressOne": "Janesboro",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17738B",
    "schoolName": "Scoil Tobar Padraig",
    "addressOne": "Patrickswell",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17814O",
    "schoolName": "Gearoid Ui Ghriobhtha",
    "addressOne": "LOUGHILL",
    "addressTwo": "County Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17871D",
    "schoolName": "Scoil Cill Churnain",
    "addressOne": "Kilcornan",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17937H",
    "schoolName": "Scoil Moin A Lin",
    "addressOne": "Castletroy",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17951B",
    "schoolName": "Scoil O Curain B",
    "addressOne": "Gortboy",
    "addressTwo": "Newcastle West",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17981K",
    "schoolName": "S N Gleann Na Gcreabhar",
    "addressOne": "GLEANN NA GCREABHAR",
    "addressTwo": "CILL MOCHEALLOG",
    "addressThree": "CO LUIMNI",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18030I",
    "schoolName": "Scoil Ailbhe",
    "addressOne": "Caherelly",
    "addressTwo": "Grange",
    "addressThree": "Kilmallock",
    "addressFour": "Co. Limerick",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18142T",
    "schoolName": "Scoil Cnoc Loinge B",
    "addressOne": "KNOCKLONG",
    "addressTwo": "CO LIMERICK",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18161A",
    "schoolName": "Castleconnell N S",
    "addressOne": "Castleconnell",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18177P",
    "schoolName": "Scoil Aine Naofa",
    "addressOne": "Rathkeale",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18236F",
    "schoolName": "Scoil Naomh Mhuire National School",
    "addressOne": "BEAL ATHA DA THUILLE",
    "addressTwo": "CO LUIMNI",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18260C",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Broadford",
    "addressTwo": "Charleville",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18367U",
    "schoolName": "S N Toinn An Tairbh",
    "addressOne": "CAPPAMORE",
    "addressTwo": "CO LIMERICK",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18426K",
    "schoolName": "Scoil Ide Naofa",
    "addressOne": "Raheenagh",
    "addressTwo": "Ballagh",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18516L",
    "schoolName": "S N Lios Na Groi",
    "addressOne": "Lisnagry",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18530F",
    "schoolName": "Askeaton Senior Ns",
    "addressOne": "ASKEATON",
    "addressTwo": "CO LIMERICK",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18600A",
    "schoolName": "S N Ma Rua",
    "addressOne": "Liscreagh",
    "addressTwo": "Murroe",
    "addressThree": "Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18612H",
    "schoolName": "Scoil Mhuire",
    "addressOne": "ACHADH LIN",
    "addressTwo": "AHALIN",
    "addressThree": "BALLINGARRY",
    "addressFour": "CO. LIMERICK",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18653V",
    "schoolName": "Scoil Naomh Iosef",
    "addressOne": "Thomas Street",
    "addressTwo": "Rathkeale",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18677M",
    "schoolName": "Scoil Mathair De",
    "addressOne": "South Circular Road",
    "addressTwo": "Limerick City",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18708U",
    "schoolName": "S N Cill Lachtain",
    "addressOne": "Newcastle West",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18717V",
    "schoolName": "S N Ciarain",
    "addressOne": "Kilfinny",
    "addressTwo": "Adare",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18872K",
    "schoolName": "Scoil Ide",
    "addressOne": "Corbally Road",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18991S",
    "schoolName": "J F K Memorial School",
    "addressOne": "Ennis Road",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19272O",
    "schoolName": "Sn Naomh Iosef",
    "addressOne": "Ballybrown",
    "addressTwo": "Clarina",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19323F",
    "schoolName": "S N Beal Atha Grean",
    "addressOne": "Ballyagran",
    "addressTwo": "Kilmallock",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19331E",
    "schoolName": "Scoil Chriost Ri B",
    "addressOne": "Caherdavin",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19332G",
    "schoolName": "S N Muire Na Heireann",
    "addressOne": "Caherdavin",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19336O",
    "schoolName": "Scoil Phoil Naofa",
    "addressOne": "Dooradoyle",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19475F",
    "schoolName": "St Brigids Ns",
    "addressOne": "Singland",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19667O",
    "schoolName": "Our Lady Of Lourdes N S",
    "addressOne": "ROSBRIEN",
    "addressTwo": "LIMERICK",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19800N",
    "schoolName": "Milford Grange N School",
    "addressOne": "Plassey Park Road",
    "addressTwo": "Castletroy",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19830W",
    "schoolName": "Corpus Christi N S",
    "addressOne": "Moyross",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19883U",
    "schoolName": "Gaelscoil O Doghair",
    "addressOne": "Bóthar an Stáisiúin",
    "addressTwo": "An Caisleán Nua",
    "addressThree": "Co. Luimnigh",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19894C",
    "schoolName": "An Mhodh Scoil",
    "addressOne": "Ascaill Uí Chonaill",
    "addressTwo": "Luimneach",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19931F",
    "schoolName": "Gaelscoil Sheoirse",
    "addressOne": "Bóthar Bhaile an Róistigh",
    "addressTwo": "Luimneach",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19934L",
    "schoolName": "Limerick School Project",
    "addressOne": "O'Connell Avenue",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19952N",
    "schoolName": "Hospital Ns",
    "addressOne": "Castlefarm",
    "addressTwo": "Hospital",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19956V",
    "schoolName": "Gaelscoil Sairseal",
    "addressOne": "Bóthar Shíol Bhroin",
    "addressTwo": "Luimneach",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19966B",
    "schoolName": "St Josephs",
    "addressOne": "Drumcollogher",
    "addressTwo": "Charleville",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20018H",
    "schoolName": "Presentation Primary School",
    "addressOne": "Sexton Street",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20094A",
    "schoolName": "Scoil Chriost An Slanaitheoir",
    "addressOne": "Ballingarry",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20102T",
    "schoolName": "St Fergus Primary School",
    "addressOne": "South Mall",
    "addressTwo": "Glin",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20133H",
    "schoolName": "Scoil Dean Cussen",
    "addressOne": "Kilmallock Road",
    "addressTwo": "Bruff",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20148U",
    "schoolName": "Gaelscoil Chaladh An Treoigh",
    "addressOne": "Caisleán Nua",
    "addressTwo": "Bóthar Bhaile Átha Cliath",
    "addressThree": "Caladh an Treoigh",
    "addressFour": "Co. Luimnigh",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20175A",
    "schoolName": "Limerick City East Educate Together",
    "addressOne": "Dromdarrig",
    "addressTwo": "Mungret",
    "addressThree": "County Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20181S",
    "schoolName": "Gaelscoil An Raithin",
    "addressOne": "An Drom Dearg",
    "addressTwo": "Mungairit",
    "addressThree": "Luimneach",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20193C",
    "schoolName": "Scoil Mocheallóg",
    "addressOne": "Glenfield Road",
    "addressTwo": "Kilmallock",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20233L",
    "schoolName": "Scoil Chaitríona",
    "addressOne": "Main Street",
    "addressTwo": "Cappamore",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20405O",
    "schoolName": "St Mary's National School",
    "addressOne": "Bishop St,",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20459O",
    "schoolName": "Le Chéile National School",
    "addressOne": "Roxboro Road",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20460W",
    "schoolName": "Thomond Primary School",
    "addressOne": "Moylish Road",
    "addressTwo": "Ballynanty",
    "addressThree": "Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20496U",
    "schoolName": "Salesian Primary School",
    "addressOne": "Fernbank",
    "addressTwo": "North Circular Road",
    "addressThree": "Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "18692I",
    "schoolName": "Catherine Mc Auley Sp S",
    "addressOne": "ASHBOURNE AVE",
    "addressTwo": "SOUTH CIRCULAR ROAD",
    "addressThree": "LIMERICK",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19200M",
    "schoolName": "St Vincents Sp School",
    "addressOne": "Lisnagry",
    "addressTwo": "Co. Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19587Q",
    "schoolName": "St Augustines Spec Sch",
    "addressOne": "Sexton Street",
    "addressTwo": "Limerick",
    "addressThree": "",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19603L",
    "schoolName": "St Gabriels School",
    "addressOne": "Crabtree House",
    "addressTwo": "Springfield Drive",
    "addressThree": "Dooradoyle",
    "addressFour": "Limerick",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "19719H",
    "schoolName": "Mid West School For The Deaf",
    "addressOne": "Greenfields",
    "addressTwo": "Rossbrien",
    "addressThree": "Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20117J",
    "schoolName": "St Canices Special School",
    "addressOne": "COOVAGH HOUSE",
    "addressTwo": "ST JOSEPH'S GROUNDS",
    "addressThree": "MULGRAVE ST",
    "addressFour": "CO LIMERICK",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20234N",
    "schoolName": "The Childrens Ark",
    "addressOne": "Midwestern Regional Hospital",
    "addressTwo": "Dooradoyle",
    "addressThree": "Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "20311F",
    "schoolName": "Red Hill School",
    "addressOne": "Red Hill",
    "addressTwo": "Patrickswell",
    "addressThree": "Co. Limerick",
    "addressFour": "",
    "county": "Limerick",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "17324U",
    "schoolName": "Scoil Naomh Breandan",
    "addressOne": "Hollymount",
    "addressTwo": "Rathmore",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "13"
  },
  {
    "rollNumber": "00779U",
    "schoolName": "Presentation Girls Primary School",
    "addressOne": "Main Street",
    "addressTwo": "Maynooth",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "13819E",
    "schoolName": "Primrose Hill Ns",
    "addressOne": "Hazelhatch Road",
    "addressTwo": "Celbridge",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "16566Q",
    "schoolName": "Scoil Naomh Brid",
    "addressOne": "Main Street",
    "addressTwo": "Celbridge",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17341U",
    "schoolName": "Maynooth B N S",
    "addressOne": "Moyglare Road",
    "addressTwo": "Maynooth",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18551N",
    "schoolName": "Scoil Na Mainistreach",
    "addressOne": "Oldtown Road",
    "addressTwo": "Celbridge",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18644U",
    "schoolName": "Straffan N S",
    "addressOne": "The Glebe",
    "addressTwo": "Newtown",
    "addressThree": "Rathangan",
    "addressFour": "Co. Kildare",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19407L",
    "schoolName": "Scoil Bhride",
    "addressOne": "Green Lane",
    "addressTwo": "Leixlip",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19459H",
    "schoolName": "Scoil Mhuire",
    "addressOne": "Green Lane",
    "addressTwo": "Leixlip",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19653D",
    "schoolName": "San Carlo Junior Ns",
    "addressOne": "Confey",
    "addressTwo": "Leixlip",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19680G",
    "schoolName": "Scoil C.ui Dhalaigh",
    "addressOne": "Léim an Bhradáin",
    "addressTwo": "Co. Chill Dara",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19684O",
    "schoolName": "Scoil Eoin Phoil",
    "addressOne": "Green Lane",
    "addressTwo": "Leixlip",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19794V",
    "schoolName": "Aghards N S",
    "addressOne": "Scoil Mochua",
    "addressTwo": "Aghards Road",
    "addressThree": "Celbridge",
    "addressFour": "Co. Kildare",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19875V",
    "schoolName": "San Carlo Senior N S",
    "addressOne": "Captain's Hill",
    "addressTwo": "Leixlip",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19995I",
    "schoolName": "North Kildare Educate Together Sch",
    "addressOne": "Clane Road",
    "addressTwo": "Celbridge",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20058T",
    "schoolName": "Scoil Ui Fhiaich",
    "addressOne": "Bóthar Chill Droichid",
    "addressTwo": "Maigh Nuad",
    "addressThree": "Co. Chill Dara",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20257C",
    "schoolName": "Scoil Naomh Padraig",
    "addressOne": "Hazelhatch",
    "addressTwo": "Celbridge",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20292E",
    "schoolName": "Maynooth Educate Together National School",
    "addressOne": "Celbridge Road",
    "addressTwo": "Maynooth",
    "addressThree": "Co. Kildare",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20526D",
    "schoolName": "Leixlip Primary School",
    "addressOne": "Leixlip ETNS",
    "addressTwo": "Leixlip Amenities",
    "addressThree": "Station Road, Leixlip,",
    "addressFour": "Leixlip",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20527F",
    "schoolName": "Gaelscoil Ruairí",
    "addressOne": "f/ch Gaelscoil Uí Fhiaich",
    "addressTwo": "Bóthar Chill Droichid",
    "addressThree": "Maigh Nuad",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18988G",
    "schoolName": "St Raphaels Special Sch",
    "addressOne": "CLANE ROAD",
    "addressTwo": "CELBRIDGE",
    "addressThree": "",
    "addressFour": "",
    "county": "Kildare",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "00752A",
    "schoolName": "Central Senior Mxd N S",
    "addressOne": "Marlborough Street",
    "addressTwo": "Dublin 1",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "01795A",
    "schoolName": "Central Infs School",
    "addressOne": "Marlborough Street",
    "addressTwo": "Dublin 1",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "04992R",
    "schoolName": "Scoil An Croi Naofa",
    "addressOne": "St Canice's Road",
    "addressTwo": "Ballygall",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "05933G",
    "schoolName": "Presentation Primary School",
    "addressOne": "George's Hill",
    "addressTwo": "Halston Street",
    "addressThree": "Dublin 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "09932B",
    "schoolName": "Stanhope St Convent",
    "addressOne": "Manor Street",
    "addressTwo": "Stoneybatter",
    "addressThree": "Dublin 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "11525A",
    "schoolName": "St Patricks N School",
    "addressOne": "Millbourne Avenue",
    "addressTwo": "Drumcondra",
    "addressThree": "Dublin 9",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "12448N",
    "schoolName": "Gardiner Street Convent",
    "addressOne": "Gardiner Street",
    "addressTwo": "Belvedere Court",
    "addressThree": "Dublin 1",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "13815T",
    "schoolName": "Howth Rd Mxd N S",
    "addressOne": "Clontarf Road",
    "addressTwo": "Clontarf",
    "addressThree": "Dublin 3",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "14463T",
    "schoolName": "St Columbas N S Mxd",
    "addressOne": "North Strand",
    "addressTwo": "Dublin 3",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "14980Q",
    "schoolName": "Glasnevin N S",
    "addressOne": "Botanic Avenue",
    "addressTwo": "Glasnevin",
    "addressThree": "Dublin 9",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "15056L",
    "schoolName": "S N San Vinseann Cailin",
    "addressOne": "North William Street",
    "addressTwo": "Dublin 1",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "15816I",
    "schoolName": "St Vincents Inf Boys",
    "addressOne": "North William Street",
    "addressTwo": "Dublin 1",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "15895H",
    "schoolName": "Drumcondra N S",
    "addressOne": "Church Avenue",
    "addressTwo": "Drumcondra",
    "addressThree": "Dublin 9",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "16177F",
    "schoolName": "Lindsay Road N S",
    "addressOne": "LINDSAY ROAD",
    "addressTwo": "GLASNEVIN",
    "addressThree": "DUBLIN 9",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "16577V",
    "schoolName": "St Brigids Convent",
    "addressOne": "Finglas Road Old",
    "addressTwo": "Glasnevin",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "16659A",
    "schoolName": "St Columbas Con G & I",
    "addressOne": "Iona Road",
    "addressTwo": "Glasnevin",
    "addressThree": "Dublin 9",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "16695E",
    "schoolName": "Scoil Na Mbrathar Boys Senior School",
    "addressOne": "North Brunswick Street",
    "addressTwo": "Dublin 7",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "16753P",
    "schoolName": "St Vincent De Pauls Girls Senior School",
    "addressOne": "Griffith Avenue",
    "addressTwo": "Dublin 9",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "16754R",
    "schoolName": "St Vincents Convent Inf N S",
    "addressOne": "67 Griffith Avenue",
    "addressTwo": "Dublin 9",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "16759E",
    "schoolName": "S N Mhuire Na Mbrathar",
    "addressOne": "Marino",
    "addressTwo": "Dublin 9",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "16792C",
    "schoolName": "St Brigids Convent N S",
    "addressOne": "St.Brigid's Road",
    "addressTwo": "Killester",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "16860Q",
    "schoolName": "Corpus Christi N S",
    "addressOne": "Home Farm Road",
    "addressTwo": "Drumcondra",
    "addressThree": "Dublin 9",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "16988T",
    "schoolName": "Christ The King B N S",
    "addressOne": "Annaly Road",
    "addressTwo": "Cabra",
    "addressThree": "Dublin 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "16989V",
    "schoolName": "Christ The King Girls Senior School",
    "addressOne": "Annaly Road",
    "addressTwo": "Cabra",
    "addressThree": "Dublin 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17110B",
    "schoolName": "Naomh Lorcan O Tuathail Senior Boys",
    "addressOne": "Seville Place",
    "addressTwo": "Dublin 1",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17148D",
    "schoolName": "S N Eoin Baisde Girls Senior",
    "addressOne": "Seafield Road West",
    "addressTwo": "Clontarf",
    "addressThree": "Dublin 3",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17214N",
    "schoolName": "St. Vincent's Primary School",
    "addressOne": "St Philomenas Road",
    "addressTwo": "Glasnevin",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17318C",
    "schoolName": "Scoil An Leinbh Iosa Boys Seniors",
    "addressOne": "Larkhill Road",
    "addressTwo": "Whitehall",
    "addressThree": "Dublin 9",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17367P",
    "schoolName": "Mary, Help Of Christians G.n.s.",
    "addressOne": "Navan Road",
    "addressTwo": "Dublin 7",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17459U",
    "schoolName": "Christ The King I G",
    "addressOne": "Annaly Road",
    "addressTwo": "Cabra",
    "addressThree": "Dublin 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17464N",
    "schoolName": "Fionnbarra Naofa B.n.s.,",
    "addressOne": "Kilkieran Road",
    "addressTwo": "Cabra West",
    "addressThree": "Dublin 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17465P",
    "schoolName": "Dominican Convent Girls Senior School",
    "addressOne": "RATOATH ROAD",
    "addressTwo": "CABRA WEST",
    "addressThree": "DUBLIN 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17466R",
    "schoolName": "St Catherines Infant School",
    "addressOne": "Ratoath Road",
    "addressTwo": "Cabra",
    "addressThree": "Dublin 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17730I",
    "schoolName": "S N Na Lanai Glasa",
    "addressOne": "Seafield Avenue",
    "addressTwo": "Clontarf",
    "addressThree": "Dublin 3",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17732M",
    "schoolName": "Scoil Chiarain",
    "addressOne": "Collins Avenue East",
    "addressTwo": "Killester",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17881G",
    "schoolName": "Scoil Ui Chonaill Boys Seniors",
    "addressOne": "North Richmond Street",
    "addressTwo": "Dublin 1",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17912O",
    "schoolName": "S N Eoin Bosco Buach",
    "addressOne": "Navan Road",
    "addressTwo": "Dublin 7",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17936F",
    "schoolName": "Sn Eoin Baisde Boys Senior School",
    "addressOne": "Seafield Road",
    "addressTwo": "Clontarf",
    "addressThree": "Dublin 3",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18137D",
    "schoolName": "Sn Naomh Feargal Boys Senior",
    "addressOne": "FINGLAS WEST",
    "addressTwo": "DUBLIN 11",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18205R",
    "schoolName": "S N An Pharoiste",
    "addressOne": "Church Street",
    "addressTwo": "Finglas",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18360G",
    "schoolName": "Scoil Bhreandain",
    "addressOne": "McAuley Road",
    "addressTwo": "Artane",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18361I",
    "schoolName": "Sn Caitriona Girls",
    "addressOne": "Measc Avenue",
    "addressTwo": "Coolock",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18362K",
    "schoolName": "S N Caitriona Naionain",
    "addressOne": "Measc Avenue",
    "addressTwo": "Coolock",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18632N",
    "schoolName": "S N Eoin Bosco Nai Buac",
    "addressOne": "Navan Road",
    "addressTwo": "Dublin 7",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18682F",
    "schoolName": "St Canices B N S",
    "addressOne": "Glasanaon Road",
    "addressTwo": "Finglas",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18683H",
    "schoolName": "St Canices G N S",
    "addressOne": "Seamus Ennis Road",
    "addressTwo": "Finglas",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18726W",
    "schoolName": "S N Seosamh Na Mbrathar",
    "addressOne": "Marino Park Avenue",
    "addressTwo": "Fairview",
    "addressThree": "Dublin 3",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18910P",
    "schoolName": "Bantiarna Na Mbuanna Boys",
    "addressOne": "Ballymun Rd",
    "addressTwo": "DUBLIN 9",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18911R",
    "schoolName": "Bantiarna Na Mbuanna Girls",
    "addressOne": "Ballymun Road",
    "addressTwo": "Dublin 9",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19006Q",
    "schoolName": "Eoin Baisde B Sois",
    "addressOne": "Seafield Road",
    "addressTwo": "Clontarf",
    "addressThree": "Dublin 3",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19007S",
    "schoolName": "Eoin Baisde C Naoidh",
    "addressOne": "Seafield Road West",
    "addressTwo": "Clontarf",
    "addressThree": "Dublin 3",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19015R",
    "schoolName": "St Josephs G N S",
    "addressOne": "Barry Avenue",
    "addressTwo": "Finglas West",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19197D",
    "schoolName": "St Kevins B N S",
    "addressOne": "Barry Avenue",
    "addressTwo": "Finglas West",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19242F",
    "schoolName": "Our Lady Of Victories Infant N S",
    "addressOne": "Ballymun Road",
    "addressTwo": "Dublin 9",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19297H",
    "schoolName": "Cromcastle Green B N S",
    "addressOne": "Cromcastle Green",
    "addressTwo": "Kilmore West",
    "addressThree": "DUBLIN 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19298J",
    "schoolName": "Scoil Nais Ide Cailini",
    "addressOne": "Cromcastle Green",
    "addressTwo": "Kilmore West",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19308J",
    "schoolName": "St Brigids Boys N S",
    "addressOne": "Howth Road",
    "addressTwo": "Killester",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19309L",
    "schoolName": "Scoil Neasain",
    "addressOne": "Baile Harmain",
    "addressTwo": "Baile Átha Cliath 5",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19395H",
    "schoolName": "Scoil Mobhi",
    "addressOne": "Bóthar Mobhí",
    "addressTwo": "Glasnaíon",
    "addressThree": "Baile Átha Cliath 9",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19489Q",
    "schoolName": "Sn Naomh Finnin",
    "addressOne": "Glenties Park",
    "addressTwo": "Rivermount",
    "addressThree": "Finglas South",
    "addressFour": "Dublin 11",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19496N",
    "schoolName": "Scoil Fhiachra Soisir",
    "addressOne": "Montrose Park",
    "addressTwo": "Artane",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19546C",
    "schoolName": "St Oliver Plunkett N S",
    "addressOne": "St Helena's Drive",
    "addressTwo": "Finglas",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19619D",
    "schoolName": "St Malachys Ns",
    "addressOne": "St. Helena's Road",
    "addressTwo": "FINGLAS",
    "addressThree": "DUBLIN 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19628E",
    "schoolName": "St Fiachras Sen N S",
    "addressOne": "St. Fiachras SNS",
    "addressTwo": "Montrose Park",
    "addressThree": "Beaumont",
    "addressFour": "Dublin 5",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19774P",
    "schoolName": "St Josephs Mxd N S",
    "addressOne": "East Wall Road",
    "addressTwo": "Dublin 3",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19831B",
    "schoolName": "Scoil Chaoimhin",
    "addressOne": "Sráid Mhaoilbhríde",
    "addressTwo": "Baile Átha Cliath 1",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19845M",
    "schoolName": "North Dublin Ns Project",
    "addressOne": "BALLYMUN ROAD",
    "addressTwo": "DUBLIN 9",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19913D",
    "schoolName": "St Josephs Ns",
    "addressOne": "Macroom Road",
    "addressTwo": "Bonnybrook",
    "addressThree": "Dublin 17",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19919P",
    "schoolName": "St Davids N S",
    "addressOne": "Kilmore Road",
    "addressTwo": "Artane",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19920A",
    "schoolName": "St John Of God N S",
    "addressOne": "Kilmore Road",
    "addressTwo": "Artane",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19926M",
    "schoolName": "Gaelscoil Cholaiste Mhuire",
    "addressOne": "4 Cearnóg Parnell",
    "addressTwo": "Baile Átha Cliath",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19929S",
    "schoolName": "St Brigids Senior Girls",
    "addressOne": "Wellmount Avenue",
    "addressTwo": "Finglas West",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19946S",
    "schoolName": "Rutland National School",
    "addressOne": "Lower Gloucester Place",
    "addressTwo": "Dublin 1",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19981U",
    "schoolName": "St Marys N S",
    "addressOne": "Windsor Avenue",
    "addressTwo": "Fairview",
    "addressThree": "Dublin 3",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20029M",
    "schoolName": "St Brigids Infant N S",
    "addressOne": "Wellmount Avenue",
    "addressTwo": "Finglas West",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20035H",
    "schoolName": "St Gabriels N S",
    "addressOne": "COWPER STREET",
    "addressTwo": "DUBLIN 7",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20047O",
    "schoolName": "Gaelscoil Bharra",
    "addressOne": "Naomh Fionbarra CLG",
    "addressTwo": "Ascal an Fhasaigh",
    "addressThree": "Cabrach",
    "addressFour": "Baile Átha Cliath 7",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20052H",
    "schoolName": "Gaelscoil Cholmcille",
    "addressOne": "Lána na Cúlóige",
    "addressTwo": "Baile Átha Cliath 17",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20059V",
    "schoolName": "Mother Of Divine Grace",
    "addressOne": "Ferndale Avenue",
    "addressTwo": "Ballygall",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20064O",
    "schoolName": "Our Lady Of Consolation Ns",
    "addressOne": "Collins Avenue East",
    "addressTwo": "Donnycarney",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20091R",
    "schoolName": "St Peters Ns",
    "addressOne": "Saint Peter's Road",
    "addressTwo": "Phibsborough",
    "addressThree": "Dublin 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20131D",
    "schoolName": "Dublin 7 Educate Together Ns",
    "addressOne": "Fitzwilliam Place North",
    "addressTwo": "Grangegorman Lower",
    "addressThree": "Dublin 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20152L",
    "schoolName": "North Dublin Muslim Ns Project",
    "addressOne": "Ratoath Road",
    "addressTwo": "Cabra",
    "addressThree": "Dublin 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20168D",
    "schoolName": "Glasnevin Educate Together Ns",
    "addressOne": "Griffith Avenue",
    "addressTwo": "Glasmevim",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20220C",
    "schoolName": "Gaelscoil Ui Earcain",
    "addressOne": "Bóthar Ghlas an Éin",
    "addressTwo": "Fionnghlas",
    "addressThree": "Baile Átha Cliath 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20338C",
    "schoolName": "Holy Child National School",
    "addressOne": "Larkhill Road",
    "addressTwo": "Whitehall",
    "addressThree": "Dublin 9",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20436C",
    "schoolName": "St Mary's Primary School",
    "addressOne": "Dorset Street Upper",
    "addressTwo": "Dublin 7",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20453C",
    "schoolName": "Broombridge Educate Together National School",
    "addressOne": "Bannow Road",
    "addressTwo": "Cabra",
    "addressThree": "Dublin 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20486R",
    "schoolName": "Grace Park Educate Together National School",
    "addressOne": "DCU All Hallows Campus",
    "addressTwo": "Grace Park Road",
    "addressThree": "Drumcondra",
    "addressFour": "Dublin 9",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20502M",
    "schoolName": "Scoil Sinead Ns",
    "addressOne": "Scoil Sinead Pelletstown",
    "addressTwo": "Patrician College Campus,",
    "addressThree": "Deanstown,",
    "addressFour": "Finglas,",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20507W",
    "schoolName": "St Laurence O'toole's National School",
    "addressOne": "St. Laurence Place East,",
    "addressTwo": "Seville Place,",
    "addressThree": "Dublin 1",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20525B",
    "schoolName": "Killester Raheny Clontarf Educate Together National School",
    "addressOne": "Killester Raheny Clontarf ETNS",
    "addressTwo": "C/O Suttonians RFC",
    "addressThree": "Station Road",
    "addressFour": "Dublin 13",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "17890H",
    "schoolName": "Temple St Children's Hospital N S",
    "addressOne": "TEMPLE STREET",
    "addressTwo": "DUBLIN 1",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18317F",
    "schoolName": "Central Remedial Clinic",
    "addressOne": "VERNON AVENUE",
    "addressTwo": "CLONTARF",
    "addressThree": "DUBLIN 3",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18417J",
    "schoolName": "St Josephs For Blind Ns",
    "addressOne": "ST JOSEPHS SCHOOL FOR",
    "addressTwo": "VISUALLY IMPAIRED BOYS",
    "addressThree": "DRUMCONDRA",
    "addressFour": "DUBLIN 9",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "18763F",
    "schoolName": "St Michaels Hse Spec Sc",
    "addressOne": "Ballymun Road",
    "addressTwo": "Dublin 9",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19039I",
    "schoolName": "St Vincents Home Ns",
    "addressOne": "Saint Vincent's Centre",
    "addressTwo": "Navan Road",
    "addressThree": "Dublin 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19316I",
    "schoolName": "St Pauls Hospital Special School",
    "addressOne": "Coolgreena Road",
    "addressTwo": "Beaumont",
    "addressThree": "Dublin 9",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19325J",
    "schoolName": "St Ciarans Spec Sch",
    "addressOne": "ST CANICES RD",
    "addressTwo": "GLASNEVIN",
    "addressThree": "DUBLIN 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19361N",
    "schoolName": "An Taonad Reamhscoile",
    "addressOne": "SRAID RUTHLAND",
    "addressTwo": "BAILE ATHA CLIATH 1",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19409P",
    "schoolName": "Casa Caterina S S",
    "addressOne": "Rathoath Road",
    "addressTwo": "Cabra",
    "addressThree": "Dublin 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19500B",
    "schoolName": "Phoenix Park Specialist School",
    "addressOne": "PHOENIX PARK",
    "addressTwo": "DUBLIN 8",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19594N",
    "schoolName": "Youth Encounter Project",
    "addressOne": "Deanstown Avenue",
    "addressTwo": "Finglas West",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "19819L",
    "schoolName": "St L O Tooles 2 Spec",
    "addressOne": "Aldborough Parade",
    "addressTwo": "North Strand",
    "addressThree": "Dublin 1",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20021T",
    "schoolName": "Henrietta Street School",
    "addressOne": "Henrietta Street",
    "addressTwo": "Dublin 1",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20153N",
    "schoolName": "St Joseph's Adolescent School",
    "addressOne": "St Vincent;s Hospital",
    "addressTwo": "Richmond Road",
    "addressThree": "Dublin 3",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20154P",
    "schoolName": "Beaumont Hospital Special School",
    "addressOne": "St. Raphael's Ward",
    "addressTwo": "Beaumont Hospital",
    "addressThree": "Dublin 9",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20495S",
    "schoolName": "Holy Family School For The Deaf",
    "addressOne": "Navan Road",
    "addressTwo": "Cabra",
    "addressThree": "Dublin 7",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "20517C",
    "schoolName": "Drumcondra Marino Dublin 1 Gaelscoil",
    "addressOne": "Bóthar Naomh Mobhí,",
    "addressTwo": "Droim Conrach,",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "14"
  },
  {
    "rollNumber": "00697S",
    "schoolName": "St Brigids Mxd N S",
    "addressOne": "Beechpark Lawn",
    "addressTwo": "Castleknock",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "09642P",
    "schoolName": "Burrow N S",
    "addressOne": "Howth Road",
    "addressTwo": "Sutton",
    "addressThree": "Dublin 13",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "18046A",
    "schoolName": "Scoil Bride B",
    "addressOne": "Church Avenue",
    "addressTwo": "Blanchardstown",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "18047C",
    "schoolName": "Scoil Bride C",
    "addressOne": "Church Avenue",
    "addressTwo": "Blanchardstown",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "18623M",
    "schoolName": "Scoil Naisunta Chnuacha",
    "addressOne": "Main Street",
    "addressTwo": "Castleknock",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "18646B",
    "schoolName": "Springdale N S",
    "addressOne": "Lough Derg Road",
    "addressTwo": "Raheny",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "18778S",
    "schoolName": "S N Naomh Mochta",
    "addressOne": "Porterstown Road",
    "addressTwo": "Clonsilla",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "18968A",
    "schoolName": "St Malachys B N S",
    "addressOne": "Edenmore Park",
    "addressTwo": "Raheny",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "18969C",
    "schoolName": "St Eithnes Senior Girls Ns",
    "addressOne": "Edenmore",
    "addressTwo": "Raheny",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "18976W",
    "schoolName": "S N Cholmille B",
    "addressOne": "Chapel Lane",
    "addressTwo": "Swords",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "18977B",
    "schoolName": "S N Cholmcille C",
    "addressOne": "Chapel Lane",
    "addressTwo": "Swords",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19037E",
    "schoolName": "St Monicas N S",
    "addressOne": "Edenmore Park",
    "addressTwo": "Raheny",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19208F",
    "schoolName": "Holy Spirit B N S",
    "addressOne": "Sillogue Road",
    "addressTwo": "Ballymun",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19209H",
    "schoolName": "Sn An Spioraid Naiomh C",
    "addressOne": "Sillogue Road",
    "addressTwo": "Ballymun",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19302U",
    "schoolName": "Sn Na Maighdine Muire B",
    "addressOne": "Shangan Road",
    "addressTwo": "Ballymun",
    "addressThree": "Dublin 9",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19303W",
    "schoolName": "Virgin Mary Girls National School",
    "addressOne": "Shangan Road",
    "addressTwo": "Ballymun",
    "addressThree": "Dublin 9",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19393D",
    "schoolName": "Mhuire Iosef Junior",
    "addressOne": "Verbena Avenue,",
    "addressTwo": "Sutton",
    "addressThree": "Dublin 13",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19406J",
    "schoolName": "Holy Trinity Sen N S",
    "addressOne": "Grange Road",
    "addressTwo": "Donaghmede",
    "addressThree": "Dublin 13",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19430G",
    "schoolName": "Scoil An Tseachtar Laoch",
    "addressOne": "Bóthar Bhaile Munna",
    "addressTwo": "Baile Átha Cliath 11",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19431I",
    "schoolName": "St Josephs Jnr",
    "addressOne": "St Joseph's Junior National School",
    "addressTwo": "Balbutcher Lane",
    "addressThree": "Ballymun",
    "addressFour": "Dublin 11",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19435Q",
    "schoolName": "St Francis Xavier J N S",
    "addressOne": "Roselawn Road",
    "addressTwo": "Castleknock",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19454U",
    "schoolName": "Darndale Ns Junior",
    "addressOne": "OUR LADY IMMAC JUN NS",
    "addressTwo": "DARNDALE",
    "addressThree": "MALAHIDE ROAD",
    "addressFour": "DUBLIN 17",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19456B",
    "schoolName": "St Cronans Junior National School",
    "addressOne": "Brackenstown",
    "addressTwo": "Swords",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19470S",
    "schoolName": "St Francis Xavier Senior N S",
    "addressOne": "Roselawn",
    "addressTwo": "Castleknock",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19471U",
    "schoolName": "St Pauls Junior National School",
    "addressOne": "Clonrosse Drive",
    "addressTwo": "Ard na Gréine",
    "addressThree": "Dublin 13",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19473B",
    "schoolName": "Scoil Bhride",
    "addressOne": "Grange Road",
    "addressTwo": "Donaghmede",
    "addressThree": "Dublin 13",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19505L",
    "schoolName": "Sn Oilibheir",
    "addressOne": "An Chúil Mhín",
    "addressTwo": "Cluain Saileach",
    "addressThree": "Baile Átha Cliath 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19524P",
    "schoolName": "Our Lady Immac Sen N S",
    "addressOne": "OUR LADY IMMAC SEN NS",
    "addressTwo": "DARNDALE",
    "addressThree": "DUBLIN 17",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19533Q",
    "schoolName": "S N Muire Agus Iosef",
    "addressOne": "Verbena Ave",
    "addressTwo": "Bayside",
    "addressThree": "Sutton",
    "addressFour": "Dublin 13",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19535U",
    "schoolName": "Brackenstown Senior N S",
    "addressOne": "Brackenstown Road",
    "addressTwo": "Swords",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19538D",
    "schoolName": "St Kevins Junior N S",
    "addressOne": "Newbrook Avenue",
    "addressTwo": "Donaghmede",
    "addressThree": "Dublin 13",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19545A",
    "schoolName": "Corduff N S",
    "addressOne": "Blackcourt Road",
    "addressTwo": "Corduff",
    "addressThree": "Blanchardstown",
    "addressFour": "Dublin 15",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19549I",
    "schoolName": "St Fintans Ns",
    "addressOne": "Carrickbrack Road",
    "addressTwo": "Sutton",
    "addressThree": "Dublin 13",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19583I",
    "schoolName": "St Josephs Senior N S",
    "addressOne": "Balcurris Road",
    "addressTwo": "Ballymun",
    "addressThree": "Dublin 11",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19601H",
    "schoolName": "St Philip The Apostle Junior N S",
    "addressOne": "Mountview",
    "addressTwo": "Clonsilla",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19605P",
    "schoolName": "Scoil Nais Mhuire Sois",
    "addressOne": "Blakestown",
    "addressTwo": "Mulhuddart",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19611K",
    "schoolName": "Scoil Naomh Colmcille",
    "addressOne": "Newbrook Road",
    "addressTwo": "Donaghmede",
    "addressThree": "Dublin 13",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19618B",
    "schoolName": "St Pauls Sen Ns",
    "addressOne": "AYRFIELD",
    "addressTwo": "MALAHIDE RD",
    "addressThree": "DUBLIN 13",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19636D",
    "schoolName": "St Patricks Senior School",
    "addressOne": "CORDUFF",
    "addressTwo": "BLANCHARDSTOWN",
    "addressThree": "DUBLIN 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19643A",
    "schoolName": "St Philips Senior N S",
    "addressOne": "Mountview Road",
    "addressTwo": "Clonsilla",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19644C",
    "schoolName": "St Ciarans N S",
    "addressOne": "Hartstown",
    "addressTwo": "Dublin 15",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19668Q",
    "schoolName": "St Francis Senior N S",
    "addressOne": "Clonshaugh Drive",
    "addressTwo": "Priorswood,",
    "addressThree": "Dublin 17",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19721R",
    "schoolName": "Holy Family Junior N S",
    "addressOne": "Forest Fields",
    "addressTwo": "Swords",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19755L",
    "schoolName": "Sacred Heart N S",
    "addressOne": "Huntstown",
    "addressTwo": "Mulhuddart",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19769W",
    "schoolName": "Scoil Thomais",
    "addressOne": "Laurel Lodge",
    "addressTwo": "Castleknock",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19777V",
    "schoolName": "Gaelscoil Mide",
    "addressOne": "Bóthar an Ghleanntáin Ghlais",
    "addressTwo": "Cill Bharróg",
    "addressThree": "Baile Átha Cliath 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19850F",
    "schoolName": "Ladyswell N S",
    "addressOne": "Dromheath Gardens",
    "addressTwo": "Mulhuddart",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19877C",
    "schoolName": "Holy Family Senior N S",
    "addressOne": "Forest Fields",
    "addressTwo": "River Valley",
    "addressThree": "Swords",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19898K",
    "schoolName": "Gaelscoil An Duinninigh",
    "addressOne": "Draighneán",
    "addressTwo": "Bóthar Fhaol Droma",
    "addressThree": "Sord",
    "addressFour": "Co. Átha Cliath",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19954R",
    "schoolName": "North Bay Educate Together Ns",
    "addressOne": "GREENDALE AVENUE",
    "addressTwo": "KILBARRACK",
    "addressThree": "DUBLIN 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20015B",
    "schoolName": "Gaelscoil Bhaile Munna",
    "addressOne": "Bóthar Choltraí",
    "addressTwo": "Baile Munna",
    "addressThree": "Baile Átha Cliath 9",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20095C",
    "schoolName": "Gaelscoil Bhrian Bóroimhe",
    "addressOne": "Coill na nÚll",
    "addressTwo": "Sord",
    "addressThree": "Co. Átha Cliath",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20098I",
    "schoolName": "Castleknock Educate Together Ns",
    "addressOne": "Beechpark Avenue",
    "addressTwo": "Castleknock",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20130B",
    "schoolName": "St Patricks Ns",
    "addressOne": "St. Patrick's National School",
    "addressTwo": "Diswellstown Lawn",
    "addressThree": "Castleknock",
    "addressFour": "Dublin 15",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20137P",
    "schoolName": "Mary Mother Of Hope Senior Ns",
    "addressOne": "Littlepace",
    "addressTwo": "Castaheany",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20145O",
    "schoolName": "Swords Educate Together Ns",
    "addressOne": "Applewood",
    "addressTwo": "Swords",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20186F",
    "schoolName": "Castaheany Educate Together Ns",
    "addressOne": "Ongar Village",
    "addressTwo": "Dublin 15",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20231H",
    "schoolName": "St Benedicts National School",
    "addressOne": "Ongar",
    "addressTwo": "Dublin 15",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20241K",
    "schoolName": "Scoil Choilm Community Ns",
    "addressOne": "Porterstown Road",
    "addressTwo": "Clonsilla",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20247W",
    "schoolName": "Scoil Ghrainne Community National School",
    "addressOne": "Phibblestown",
    "addressTwo": "Clonee",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20302E",
    "schoolName": "Thornleigh Educate Together National School",
    "addressOne": "Thornleigh Green",
    "addressTwo": "Applewood Village",
    "addressThree": "Swords",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20348F",
    "schoolName": "Holywell Educate Together National School",
    "addressOne": "Holywell Road",
    "addressTwo": "Nevinstown",
    "addressThree": "Swords",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20383H",
    "schoolName": "Hansfield Educate Together National School",
    "addressOne": "Barnwell Road",
    "addressTwo": "Hansfield",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20392I",
    "schoolName": "Pelletstown Etns",
    "addressOne": "Ashtown Road,",
    "addressTwo": "Rathborne",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20528H",
    "schoolName": "River Valley Cns",
    "addressOne": "Swords South",
    "addressTwo": "Dublin",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "16675V",
    "schoolName": "Scoil Naomh Lucais",
    "addressOne": "Hollywood Road",
    "addressTwo": "Tyrellstown",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "17976R",
    "schoolName": "Scoil Assaim Boys Seniors",
    "addressOne": "Raheny",
    "addressTwo": "Dublin 5",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "17977T",
    "schoolName": "Scoil Aine Convent Senior",
    "addressOne": "All Saints Drive",
    "addressTwo": "Raheny",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "17978V",
    "schoolName": "Naiscoil Ide",
    "addressOne": "All Saints Drive",
    "addressTwo": "Raheny",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19694R",
    "schoolName": "Scoil Mhuire Sin",
    "addressOne": "Blakestown",
    "addressTwo": "Mulhuddart",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19935N",
    "schoolName": "Scoil Eoin",
    "addressOne": "Greendale Road",
    "addressTwo": "Dublin 5",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20201V",
    "schoolName": "Tyrrelstown Educate Together National School",
    "addressOne": "Hollywood Road",
    "addressTwo": "Tyrellstown",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20281W",
    "schoolName": "St Benedicts And St Marys National School",
    "addressOne": "Grange Park",
    "addressTwo": "Raheny",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20309S",
    "schoolName": "Mary Mother Of Hope Junior National School",
    "addressOne": "St Charles Houben Building",
    "addressTwo": "Littlepace",
    "addressThree": "Clonee",
    "addressFour": "Dublin 15",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20384J",
    "schoolName": "Powerstown Educate Together National School",
    "addressOne": "Powerstown Road",
    "addressTwo": "Tyrrelstown",
    "addressThree": "Dublin 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19373U",
    "schoolName": "St Michaels Hse Sp Sch",
    "addressOne": "Raheny Road",
    "addressTwo": "Raheny",
    "addressThree": "Dublin 5",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20279M",
    "schoolName": "St Michael's House Special National School Foxfield",
    "addressOne": "Briarfield Villas",
    "addressTwo": "Greendale Road",
    "addressThree": "Kilbarrack",
    "addressFour": "Dublin 5",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20375I",
    "schoolName": "Abacas Kilbarrack",
    "addressOne": "c/o Scoil Eoin",
    "addressTwo": "Greendale Road",
    "addressThree": "Kilbarrack",
    "addressFour": "Dublin 5",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20548N",
    "schoolName": "Danu Community Special School",
    "addressOne": "c/o Hansfield ETSS",
    "addressTwo": "Barnwell Road",
    "addressThree": "Hansfield",
    "addressFour": "Dublin 15",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "01170G",
    "schoolName": "S N Na H-aille",
    "addressOne": "Westown",
    "addressTwo": "Naul",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "09492W",
    "schoolName": "Balscadden N S",
    "addressOne": "Balscadden",
    "addressTwo": "Ring Commons",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "10296G",
    "schoolName": "Scoil Naomh Mearnog",
    "addressOne": "Strand Road",
    "addressTwo": "Portmarnock",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "14180H",
    "schoolName": "Holmpatrick N S",
    "addressOne": "Convent Lane",
    "addressTwo": "Skerries",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "15569R",
    "schoolName": "Scoil Moibhi",
    "addressOne": "Milverton",
    "addressTwo": "Skerries",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "16332O",
    "schoolName": "St Patricks Snr Mixed",
    "addressOne": "Beau Piers Lane",
    "addressTwo": "Skerries",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "16333Q",
    "schoolName": "St Patricks Jnr Mixed",
    "addressOne": "Beau Piers",
    "addressTwo": "Skerries",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "16844S",
    "schoolName": "Scoil N. Breandan",
    "addressOne": "Loughshinny",
    "addressTwo": "Skerries",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19578P",
    "schoolName": "St Helens Junior N S",
    "addressOne": "Martello",
    "addressTwo": "Portmarnock",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19625V",
    "schoolName": "Scoil Réalt Na Mara",
    "addressOne": "Balbriggan Road",
    "addressTwo": "Skerries",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19762I",
    "schoolName": "St Helens Senior N S",
    "addressOne": "Limetree Ave",
    "addressTwo": "Portmarnock",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20307O",
    "schoolName": "Skerries Educate Together National School",
    "addressOne": "Kellys Bay",
    "addressTwo": "Skerries",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20445D",
    "schoolName": "Malahide / Portmarnock Educate Together National School",
    "addressOne": "Malahide Road",
    "addressTwo": "Dublin 17",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19757P",
    "schoolName": "St Michaels Spec Sch",
    "addressOne": "Hacketstown",
    "addressTwo": "Skerries",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20437E",
    "schoolName": "St Laurence's National School",
    "addressOne": "Brookstone Road",
    "addressTwo": "Baldoyle",
    "addressThree": "Dublin 13",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20509D",
    "schoolName": "Lusk Junior National School St Maccullins",
    "addressOne": "Chapel Road",
    "addressTwo": "Lusk",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20519G",
    "schoolName": "Stapolin Educate Together National School",
    "addressOne": "Grange Abbey Road",
    "addressTwo": "Donaghmede",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "18210K",
    "schoolName": "St Michaels House Special School",
    "addressOne": "College Street",
    "addressTwo": "Baldoyle",
    "addressThree": "Dublin 13",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20136N",
    "schoolName": "Crannog Nua Special School",
    "addressOne": "St Ita's Hospital",
    "addressTwo": "Portrane",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "11583O",
    "schoolName": "St Andrews N S",
    "addressOne": "Church Road",
    "addressTwo": "Malahide",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "12358M",
    "schoolName": "Swords Borough N S",
    "addressOne": "Church Road",
    "addressTwo": "Swords",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "15315J",
    "schoolName": "St Georges N S",
    "addressOne": "Naul Road",
    "addressTwo": "Balbriggan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "15650A",
    "schoolName": "Corduff N S",
    "addressOne": "Lusk",
    "addressTwo": "Co. Dublin",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "16267G",
    "schoolName": "St Patricks Boys National School",
    "addressOne": "Donabate",
    "addressTwo": "Co. Dublin",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "16970A",
    "schoolName": "St Molaga Senior Ns",
    "addressOne": "Bremore",
    "addressTwo": "Balbriggan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "16972E",
    "schoolName": "S N Peadar Agus Pol N",
    "addressOne": "Chapel Street",
    "addressTwo": "Balbriggan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "17104G",
    "schoolName": "St Francis Junior National School",
    "addressOne": "Clonshaugh Drive",
    "addressTwo": "Priorswood",
    "addressThree": "Dublin 17",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "17472M",
    "schoolName": "Hedgestown N S",
    "addressOne": "Jordanstown",
    "addressTwo": "Lusk",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "17559B",
    "schoolName": "S N Mhuire",
    "addressOne": "The Green",
    "addressTwo": "Garristown",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "17569E",
    "schoolName": "Balrothery N S",
    "addressOne": "Balrothery",
    "addressTwo": "Balbriggan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "17595F",
    "schoolName": "Cill Coscain",
    "addressOne": "Kilcoscan",
    "addressTwo": "The Ward",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "17785K",
    "schoolName": "San Nioclas Myra",
    "addressOne": "Malahide Road",
    "addressTwo": "Kinsealy",
    "addressThree": "Dublin 17",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "17820J",
    "schoolName": "Brighde Naofa",
    "addressOne": "Rowlestown Drive",
    "addressTwo": "Kilsallaghan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "17914S",
    "schoolName": "St Oliver Plunkett",
    "addressOne": "Grove Road",
    "addressTwo": "Malahide",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "17928G",
    "schoolName": "Sn N Sailbheastar Nfa",
    "addressOne": "Yellow Walls Road",
    "addressTwo": "Malahide",
    "addressThree": "Co Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "17961E",
    "schoolName": "S N Lusca",
    "addressOne": "Chapel Road",
    "addressTwo": "Lusk",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "18038B",
    "schoolName": "St Margarets N S",
    "addressOne": "Sandyhill",
    "addressTwo": "Saint Margaret's",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "18080A",
    "schoolName": "Scoil Mhuire Mxd",
    "addressOne": "Tuckett's Lane",
    "addressTwo": "Howth",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "18412W",
    "schoolName": "S N C Naomh Padraig",
    "addressOne": "Portrane Road",
    "addressTwo": "Donabate",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19001G",
    "schoolName": "Ballyboghill N S",
    "addressOne": "Ballyboughal",
    "addressTwo": "Co. Dublin",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19515O",
    "schoolName": "Sn Naomh Treasa",
    "addressOne": "Hampton",
    "addressTwo": "Balbriggan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19627C",
    "schoolName": "John Paul Ii N S",
    "addressOne": "Sea Road",
    "addressTwo": "Malahide",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "17263D",
    "schoolName": "St Mary's N S",
    "addressOne": "Oldtown",
    "addressTwo": "Co. Dublin",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19624T",
    "schoolName": "Scoil Nais Caitriona",
    "addressOne": "The Avenue",
    "addressTwo": "Rush",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19660A",
    "schoolName": "Rush Ns",
    "addressOne": "Channel Road",
    "addressTwo": "Rush",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "19693P",
    "schoolName": "Mary Queen Of Ireland N S",
    "addressOne": "TOBERBURR",
    "addressTwo": "CO DUBLIN",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20161M",
    "schoolName": "Donabate/portrane Educate Together",
    "addressOne": "Ballisk Common",
    "addressTwo": "Beaverstown Road",
    "addressThree": "Donabate",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20176C",
    "schoolName": "Rush And Lusk Educate Together Ns",
    "addressOne": "Raheny Lane",
    "addressTwo": "Rathmore Road",
    "addressThree": "Lusk",
    "addressFour": "Co. Dublin",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20202A",
    "schoolName": "Balbriggan Educate Together Ns",
    "addressOne": "Hamlet Lane",
    "addressTwo": "Balbriggan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20252P",
    "schoolName": "Gaelscoil Bhaile Brigin",
    "addressOne": "Fearann an Chaisleáin",
    "addressTwo": "Baile Brigín",
    "addressThree": "Co. Átha Cliath",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20269J",
    "schoolName": "Scoil Chormaic",
    "addressOne": "Scoil Chormaic CNS",
    "addressTwo": "Stephenstown",
    "addressThree": "Balbriggan",
    "addressFour": "Co Dublin",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20282B",
    "schoolName": "Bracken Educate Together N.s.",
    "addressOne": "Castlelands",
    "addressTwo": "Balbriggan",
    "addressThree": "Co. Dublin",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20304I",
    "schoolName": "St. Francis Of Assisi National School",
    "addressOne": "Belmayne Avenue",
    "addressTwo": "Balgriffin",
    "addressThree": "Dublin 13",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20308Q",
    "schoolName": "Belmayne Educate Together National School",
    "addressOne": "Belmayne Avenue",
    "addressTwo": "Dublin 13",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20334R",
    "schoolName": "Gaelscoil Ros Eo",
    "addressOne": "Gaelscoil Ros Eo",
    "addressTwo": "CLG Naomh Maur",
    "addressThree": "Ros Eo",
    "addressFour": "Co. Bhaile Átha Cliath",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "20394M",
    "schoolName": "Gaelscoil An Chuilinn",
    "addressOne": "Bóthar Bhaile an Phaoraigh",
    "addressTwo": "Baile an Tirialaigh",
    "addressThree": "Baile Átha Cliath 15",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "15"
  },
  {
    "rollNumber": "00512D",
    "schoolName": "Midleton Convent N S",
    "addressOne": "Midleton",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "02114B",
    "schoolName": "S N Baile Ui Ghiblin",
    "addressOne": "Ballygiblin",
    "addressTwo": "Mitchelstown",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "02278I",
    "schoolName": "Millstreet Convent N S",
    "addressOne": "West End",
    "addressTwo": "Millstreet",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "04054T",
    "schoolName": "Ballindangan Mixed N S",
    "addressOne": "Ballindangan",
    "addressTwo": "Mitchelstown",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "04442F",
    "schoolName": "Kyle N S",
    "addressOne": "Kyle",
    "addressTwo": "Youghal",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "08828B",
    "schoolName": "Kilcorney Mixed N S",
    "addressOne": "Kilcorney",
    "addressTwo": "Rathcoole",
    "addressThree": "Mallow",
    "addressFour": "Co. Cork",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "09872J",
    "schoolName": "Cloghoola Mixed N S",
    "addressOne": "Cloughoulamore",
    "addressTwo": "Millstreet",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "10523O",
    "schoolName": "Fermoy Adair N S",
    "addressOne": "Greenhill",
    "addressTwo": "Fermoy",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "10724B",
    "schoolName": "South Abbey Ns",
    "addressOne": "Golf Links Road",
    "addressTwo": "Youghal",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "11236Q",
    "schoolName": "Newmarket B N S",
    "addressOne": "West End",
    "addressTwo": "Newmarket",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "11249C",
    "schoolName": "Burnfort N S",
    "addressOne": "Burnfort",
    "addressTwo": "Mallow",
    "addressThree": "Co Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "11337W",
    "schoolName": "Kilmagner N S",
    "addressOne": "Kilmagner",
    "addressTwo": "Fermoy",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "12041G",
    "schoolName": "St John The Baptist N S",
    "addressOne": "The Park",
    "addressTwo": "Midleton",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "12446J",
    "schoolName": "S N Gleann Na Huladh",
    "addressOne": "Cloughleafin",
    "addressTwo": "Mitchelstown",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "12447L",
    "schoolName": "Baltydaniel N S",
    "addressOne": "Newtwopothouse",
    "addressTwo": "Mallow",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "13483U",
    "schoolName": "S N Ath Na Lionta",
    "addressOne": "Mourneabbey",
    "addressTwo": "Mallow",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "13779S",
    "schoolName": "S N Dhrom Athain",
    "addressOne": "Dromahane",
    "addressTwo": "Mallow",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "14052V",
    "schoolName": "Kanturk B N S",
    "addressOne": "Kanturk",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "14107U",
    "schoolName": "Castletownroche N S",
    "addressOne": "Close Road",
    "addressTwo": "Castletownroche",
    "addressThree": "Mallow",
    "addressFour": "Co. Cork",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "15380U",
    "schoolName": "Dromagh Mixed N S",
    "addressOne": "Dromagh",
    "addressTwo": "Mallow",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "15516T",
    "schoolName": "Clonpriest N S",
    "addressOne": "Youghal",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "15594Q",
    "schoolName": "Grange Fermoy N S",
    "addressOne": "Grange",
    "addressTwo": "Fermoy",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "15701O",
    "schoolName": "Bartlemy N S",
    "addressOne": "RATHCORMAC",
    "addressTwo": "FERMOY",
    "addressThree": "CO CORK",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "15903D",
    "schoolName": "Killavullen N S",
    "addressOne": "Killavullen",
    "addressTwo": "Mallow",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "16109L",
    "schoolName": "Scoil Realt Na Mara",
    "addressOne": "Ballycotton",
    "addressTwo": "Midleton",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "16128P",
    "schoolName": "Bunscoil Na Toirbhirte",
    "addressOne": "Church Road",
    "addressTwo": "Mitchelstown",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "16159D",
    "schoolName": "Convent Girls Senior National School",
    "addressOne": "Mallow",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "16648S",
    "schoolName": "Ath Treasna G N S",
    "addressOne": "Poundhill",
    "addressTwo": "Newmarket",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "16683U",
    "schoolName": "Bishop Murphy Memorial Boys Senior National School",
    "addressOne": "McCurtain Street",
    "addressTwo": "Fermoy",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "16684W",
    "schoolName": "Scoil Na Mbraithre Boys Senior School",
    "addressOne": "Rosary Place",
    "addressTwo": "Midleton",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17087J",
    "schoolName": "S N An Chlochair",
    "addressOne": "Church Street",
    "addressTwo": "Kanturk",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17300G",
    "schoolName": "Lios Maighir",
    "addressOne": "Lismire",
    "addressTwo": "Newmarket",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17362F",
    "schoolName": "S N Breandan Naofa",
    "addressOne": "RATHCOOLE",
    "addressTwo": "MALLOW",
    "addressThree": "CO CORK",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17527L",
    "schoolName": "Scoil Bhrugh Thuinne",
    "addressOne": "Churchtown",
    "addressTwo": "Mallow",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17533G",
    "schoolName": "S N Rathain",
    "addressOne": "Mallow",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17579H",
    "schoolName": "S N Ath Fhada",
    "addressOne": "Aghada",
    "addressTwo": "Midleton",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17600S",
    "schoolName": "S N Na Scairte Leithe",
    "addressOne": "Saleen",
    "addressTwo": "Midleton",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17609N",
    "schoolName": "Rathcormac N S",
    "addressOne": "Rathcormac",
    "addressTwo": "Fermoy",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17639W",
    "schoolName": "Scoil Na Mbraithre",
    "addressOne": "Brigown",
    "addressTwo": "Mitchelstown",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17706L",
    "schoolName": "S N An Mhaoilinn",
    "addressOne": "Meelin",
    "addressTwo": "Newmarket",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17794L",
    "schoolName": "S N An Phairc",
    "addressOne": "YOUGHAL",
    "addressTwo": "CO CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17823P",
    "schoolName": "Glantane B N S",
    "addressOne": "Brittas",
    "addressTwo": "Lombardstown",
    "addressThree": "Mallow",
    "addressFour": "Co. Cork",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17868O",
    "schoolName": "Scoil Freastogail Muire",
    "addressOne": "College Road",
    "addressTwo": "Fermoy",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17887S",
    "schoolName": "S N Naomh Padraig Boys Senior School",
    "addressOne": "New Road",
    "addressTwo": "Mallow",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17921P",
    "schoolName": "S N Moing Na Miol",
    "addressOne": "Clashbee",
    "addressTwo": "Mallow",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17950W",
    "schoolName": "Shanagarry N S",
    "addressOne": "SHANAGARRY",
    "addressTwo": "MIDLETON",
    "addressThree": "CO CORK",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17955J",
    "schoolName": "Muire Gan Smal",
    "addressOne": "CLUAIN DILLEAIN",
    "addressTwo": "FERMOY",
    "addressThree": "CO CORK",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18128C",
    "schoolName": "S N Mhuire Mxd",
    "addressOne": "Kiskeam",
    "addressTwo": "Mallow",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18301N",
    "schoolName": "Sn Mhuire Bns",
    "addressOne": "Killarney Road",
    "addressTwo": "Millstreet",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18377A",
    "schoolName": "Iosef Naofa",
    "addressOne": "Barrack Hill",
    "addressTwo": "St. Bernard's Place",
    "addressThree": "Fermoy",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18472R",
    "schoolName": "Scoil Chaitriona",
    "addressOne": "Ballynoe",
    "addressTwo": "Mallow",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18549D",
    "schoolName": "S N Cnoc Na Croighe",
    "addressOne": "CNOC NA GROIGHE",
    "addressTwo": "MALLOW",
    "addressThree": "CO CORK",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18883P",
    "schoolName": "S N Iosagain",
    "addressOne": "Spa Glen",
    "addressTwo": "Mallow",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19256Q",
    "schoolName": "Scoil Ghobnatan",
    "addressOne": "Bellevue",
    "addressTwo": "Mallow",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19307H",
    "schoolName": "Derrinagree N S",
    "addressOne": "Derrinagree",
    "addressTwo": "Mallow",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19342J",
    "schoolName": "Scoil Naomh Eoin Baiste",
    "addressOne": "KILBRIN",
    "addressTwo": "KANTURK",
    "addressThree": "CO CORK",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19526T",
    "schoolName": "Sn Cill Dairbhre",
    "addressOne": "FERMOY ROAD",
    "addressTwo": "KILDORRERY",
    "addressThree": "MITCHELSTOWN",
    "addressFour": "P67 VY68",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20024C",
    "schoolName": "Bun Scoil Muire",
    "addressOne": "O'Brien's Place",
    "addressTwo": "Youghal",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20336V",
    "schoolName": "Midleton Educate Together School",
    "addressOne": "Broomfield West",
    "addressTwo": "Mill Road",
    "addressThree": "Midleton",
    "addressFour": "Co. Cork",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20440Q",
    "schoolName": "Scoil Aonghusa Community National School",
    "addressOne": "Kingsfort Avenue",
    "addressTwo": "Castlepark Village",
    "addressThree": "Mallow",
    "addressFour": "Co. Cork",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "01867W",
    "schoolName": "Castlelyons B N S",
    "addressOne": "Castlelyons",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "02452V",
    "schoolName": "Cloyne National School",
    "addressOne": "Cloyne",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "02803B",
    "schoolName": "Banteer N S",
    "addressOne": "Banteer",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "03704E",
    "schoolName": "S N Sean Baile Mor",
    "addressOne": "Main Street",
    "addressTwo": "Shanballymore",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "04118T",
    "schoolName": "Leamlara Mixed N S",
    "addressOne": "Leamlara",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "04230N",
    "schoolName": "Scoil Naomh Eoin",
    "addressOne": "Ballincurrig",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "04953H",
    "schoolName": "Ballyhass Mixed N S",
    "addressOne": "Ballyhass",
    "addressTwo": "Cecilstown",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "06295F",
    "schoolName": "Freemount Mixed N S",
    "addressOne": "Freemount",
    "addressTwo": "Charleville",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "06342L",
    "schoolName": "Vicarstown Mixed N S",
    "addressOne": "Gilclough",
    "addressTwo": "Vicarstown",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "07242M",
    "schoolName": "Cloghroe Mixed N S",
    "addressOne": "Cloghroe",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "08393P",
    "schoolName": "S N Rath Dubh",
    "addressOne": "Rathduff",
    "addressTwo": "Grenagh",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "09815U",
    "schoolName": "Tullaslease Mixed N S",
    "addressOne": "Tullylease",
    "addressTwo": "Charleville",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "10771K",
    "schoolName": "Cobh N S",
    "addressOne": "Bellevue",
    "addressTwo": "Cobh",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "11262R",
    "schoolName": "Druimne N S",
    "addressOne": "Dromina",
    "addressTwo": "Charleville",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "11496T",
    "schoolName": "Shandrum National School",
    "addressOne": "Newtownshandrum",
    "addressTwo": "Charleville",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "11992I",
    "schoolName": "Whitegate Mixed N S",
    "addressOne": "Corkbeg",
    "addressTwo": "Whitegate",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "12015F",
    "schoolName": "Liscarrol N S",
    "addressOne": "Liscarroll",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17829E",
    "schoolName": "S N Ath An Mhuillinn",
    "addressOne": "Kilbolane",
    "addressTwo": "Milford",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17848I",
    "schoolName": "S N Mhuire",
    "addressOne": "Glenville",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18000W",
    "schoolName": "Scoil Mhuire Naofa",
    "addressOne": "Carrigtwohill",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18196T",
    "schoolName": "St Patrick's National School",
    "addressOne": "Boherash",
    "addressTwo": "Glanworth",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18266O",
    "schoolName": "Ballygown Ns",
    "addressOne": "CASTLETOWNROCHE",
    "addressTwo": "CO. CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18316D",
    "schoolName": "S N Fhursa",
    "addressOne": "Lyre",
    "addressTwo": "Banteer",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18422C",
    "schoolName": "Scoil Na Nog",
    "addressOne": "GLEANN MAGHAIR",
    "addressTwo": "CO CHORCAI",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18475A",
    "schoolName": "S N Baile Ui Chroinin",
    "addressOne": "BALLYCRONEEN",
    "addressTwo": "CLOYNE",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18497K",
    "schoolName": "Little Island Ns",
    "addressOne": "Little Island",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18567F",
    "schoolName": "S N Inis",
    "addressOne": "Ballymakibbot",
    "addressTwo": "Inch",
    "addressThree": "Killeagh",
    "addressFour": "Co. Cork",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18786R",
    "schoolName": "Scoil Iosagain",
    "addressOne": "Knockpogue Avenue",
    "addressTwo": "Farranree",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19351K",
    "schoolName": "S N Cill Criodain",
    "addressOne": "Kilcredan",
    "addressTwo": "Ladysbridge",
    "addressThree": "Co. Corik",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19404F",
    "schoolName": "Sn Mhuire",
    "addressOne": "Main Street",
    "addressTwo": "Ballyhooly",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19415K",
    "schoolName": "Scoil An Athar Tadhg",
    "addressOne": "Carraig na bhFear",
    "addressTwo": "Co. Chorcaí",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19426P",
    "schoolName": "S N Mharcuis B",
    "addressOne": "An Gleann",
    "addressTwo": "Corcaigh",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19427R",
    "schoolName": "S N Bhreanndain C",
    "addressOne": "Glen Avenue",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20038N",
    "schoolName": "Scoil Aiseiri Chriost",
    "addressOne": "Farranree",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20140E",
    "schoolName": "Scoil Mhuire Fatima Boys Senior",
    "addressOne": "North Monastery Road",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20151J",
    "schoolName": "Gaelscoil Mhuscraí",
    "addressOne": "Sean Íochtarach",
    "addressTwo": "An Bhlárna",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20239A",
    "schoolName": "Gaelscoil Ui Drisceoil",
    "addressOne": "Dunkettle",
    "addressTwo": "Glanmire",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20399W",
    "schoolName": "Scoil Chroí Íosa",
    "addressOne": "St Anne's Road",
    "addressTwo": "Blarney",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20442U",
    "schoolName": "Scoil Chliodhna Community National School",
    "addressOne": "C/O Carrigtwohill GAA Club",
    "addressTwo": "Carrigtwohill",
    "addressThree": "",
    "addressFour": "Co Cork",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20493O",
    "schoolName": "Canon Sheehan Primary School",
    "addressOne": "Turnpike",
    "addressTwo": "Doneraile",
    "addressThree": "Co.Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20497W",
    "schoolName": "Scoil Mhuire Agus Eoin",
    "addressOne": "Boherboy Road",
    "addressTwo": "Lotabeg",
    "addressThree": "Mayfield",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20511N",
    "schoolName": "Fermoy Educate Together Ns",
    "addressOne": "Fermoy Educate Together N.S.",
    "addressTwo": "Waterloo Lane",
    "addressThree": "Fermoy",
    "addressFour": "Fermoy",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18458A",
    "schoolName": "St Bernadettes Spec Sch",
    "addressOne": "Bonnington",
    "addressTwo": "Montenotte",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18586J",
    "schoolName": "Scoil Eanna",
    "addressOne": "MONTENOTTE",
    "addressTwo": "CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19203S",
    "schoolName": "St Paul's School",
    "addressOne": "Beech Hill",
    "addressTwo": "Montenotte",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19410A",
    "schoolName": "St Killians Spec Sch",
    "addressOne": "Old Youghal Road",
    "addressTwo": "Mayfield",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19433M",
    "schoolName": "Holy Family S S",
    "addressOne": "Bakers Road",
    "addressTwo": "Charleville",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19760E",
    "schoolName": "Scoil Triest",
    "addressOne": "LOTA",
    "addressTwo": "GLANMIRE",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20354A",
    "schoolName": "Cara Junior School",
    "addressOne": "Cara Junior School",
    "addressTwo": "Banduff Road",
    "addressThree": "Mayfield",
    "addressFour": "Cork",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "12281D",
    "schoolName": "Walterstown N S",
    "addressOne": "Walterstown",
    "addressTwo": "Cobh",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "12382J",
    "schoolName": "Curriglass Ns",
    "addressOne": "Blackpool",
    "addressTwo": "Curraglass",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "12598J",
    "schoolName": "Ardagh Boys Senior National School",
    "addressOne": "Ardagh",
    "addressTwo": "Rosscarbery",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "12676D",
    "schoolName": "Clogheen Mxd N S",
    "addressOne": "Kerry Pike",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "13031I",
    "schoolName": "St Josephs Convent N S",
    "addressOne": "Smith's Road",
    "addressTwo": "Charleville",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "13402R",
    "schoolName": "Knockraha N S",
    "addressOne": "Knockraha",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "13450F",
    "schoolName": "Bunscoil Rinn An Chabhlaigh",
    "addressOne": "Rushbrooke",
    "addressTwo": "Cobh",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "13647B",
    "schoolName": "Castlemartyr N S",
    "addressOne": "Gortnahomnamore",
    "addressTwo": "Castlemartyr",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "13663W",
    "schoolName": "Lower Glanmire N S",
    "addressOne": "New Inn",
    "addressTwo": "Glanmire",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "13696O",
    "schoolName": "St Vincents Convent N S",
    "addressOne": "Wolfe Tone Street",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "13747F",
    "schoolName": "Riverstown N S",
    "addressOne": "Riverstown",
    "addressTwo": "Glanmire",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "14014N",
    "schoolName": "S N Baile Deasmumhan",
    "addressOne": "Church Road",
    "addressTwo": "Ballydesmond",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "14813U",
    "schoolName": "Roscarbery Con N S",
    "addressOne": "Ardagh East",
    "addressTwo": "Rosscarbery",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "15140A",
    "schoolName": "Ballingree Mxd N S",
    "addressOne": "Ballinagree",
    "addressTwo": "Macroom,",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "15165Q",
    "schoolName": "Ballintotas N S",
    "addressOne": "Ballintotis",
    "addressTwo": "Castlemartyr",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "15484J",
    "schoolName": "Glounthaune Mixed N.s.",
    "addressOne": "Glounthane",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "15718I",
    "schoolName": "S N Seosamh Cobh",
    "addressOne": "Cobh",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "15792U",
    "schoolName": "Upper Glanmire N S",
    "addressOne": "Ballinvriskig",
    "addressTwo": "White's Cross",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "16271U",
    "schoolName": "Watergrasshill N S",
    "addressOne": "Watergrasshill",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "16335U",
    "schoolName": "Rylane N S",
    "addressOne": "RYLANE",
    "addressTwo": "CO CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "16396R",
    "schoolName": "Boherbue N S",
    "addressOne": "Boherbue",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "16680O",
    "schoolName": "Scoil Colmcille",
    "addressOne": "Blarney Street",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "16681Q",
    "schoolName": "Scoil Na Mbraithre Boys Snr Sch",
    "addressOne": "Baker's Road",
    "addressTwo": "Charleville",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "16945B",
    "schoolName": "Lisgriffin N S",
    "addressOne": "BUTTEVANT",
    "addressTwo": "CO CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17045Q",
    "schoolName": "St Patricks B N S",
    "addressOne": "Gardiner's Hill",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17079K",
    "schoolName": "S N Carraig An Ime",
    "addressOne": "Carriganima,",
    "addressTwo": "Macroom,",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17103E",
    "schoolName": "Scoil Chlochair Mhuire National School",
    "addressOne": "Carrigtwohill",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17112F",
    "schoolName": "Ballyhea N S",
    "addressOne": "Ballyhea",
    "addressTwo": "Rathluirc",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17156C",
    "schoolName": "Scoil Naomh Aine Girls Senior School",
    "addressOne": "SMITH'S ROAD",
    "addressTwo": "Charleville",
    "addressThree": "CO. CORK",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17171V",
    "schoolName": "S N Cuilinn Ui Caoimh",
    "addressOne": "Mullaghroe",
    "addressTwo": "Cullen",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17297U",
    "schoolName": "S N Fearghail Naofa",
    "addressOne": "Main Street",
    "addressTwo": "Killeagh",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17360B",
    "schoolName": "S N Mhuire",
    "addressOne": "Rathpeacon",
    "addressTwo": "Mallow Road",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17505B",
    "schoolName": "S N Cill Ruadhain",
    "addressOne": "Glanmire",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17515E",
    "schoolName": "S N Achadh Bolg",
    "addressOne": "Aghabullogue",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17528N",
    "schoolName": "S N Naomh Caitriona",
    "addressOne": "Conna",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "11"
  },
  {
    "rollNumber": "17602W",
    "schoolName": "S N Naomh Lachtin",
    "addressOne": "DOMHNACH MOR",
    "addressTwo": "CO CHORCAI",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17667E",
    "schoolName": "S N Padraig Naofa",
    "addressOne": "Farranastig",
    "addressTwo": "Whitechurch",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17754W",
    "schoolName": "S N Naomh Eoin B",
    "addressOne": "Station Road",
    "addressTwo": "Ballincollig",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17755B",
    "schoolName": "Scoil Naomh Mhuire C",
    "addressOne": "Ballincollig",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17761T",
    "schoolName": "S N Mhuire",
    "addressOne": "Billeragh",
    "addressTwo": "Araglin",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17763A",
    "schoolName": "Rockchapel N S",
    "addressOne": "Lyraneag",
    "addressTwo": "Rockchapel",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17765E",
    "schoolName": "S N Re Na Scrine",
    "addressOne": "Maulyregan",
    "addressTwo": "Rosscarbery",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "17790D",
    "schoolName": "Curraghagalla N S",
    "addressOne": "Curraghagalla",
    "addressTwo": "Kilworth",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18153B",
    "schoolName": "S N Padraig Naofa C",
    "addressOne": "Gardiner's Hill",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "18154D",
    "schoolName": "S N Padraig Naofa",
    "addressOne": "Dillon's Cross",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19714U",
    "schoolName": "Mhuire Ar Chnoc Haoine",
    "addressOne": "Knocknaheeny",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19730S",
    "schoolName": "Scoil Oilibheir",
    "addressOne": "Ballyvolane",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19761G",
    "schoolName": "Dungourney Central N Sc",
    "addressOne": "DUNGOURNEY",
    "addressTwo": "CO CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19771J",
    "schoolName": "Scoil Barra",
    "addressOne": "Innishmore",
    "addressTwo": "Ballincollig",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19833F",
    "schoolName": "Gaelscoil Chorain,",
    "addressOne": "Sráid na Trá",
    "addressTwo": "Eochaill",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19851H",
    "schoolName": "Gaelscoil De Hide",
    "addressOne": "Dún Eala",
    "addressTwo": "Dún Teachain",
    "addressThree": "Mainistir Fheatmaí",
    "addressFour": "Co. Chorcarí",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19881Q",
    "schoolName": "Gaelscoil Charraig Ui Leighin",
    "addressOne": "Bridgemount",
    "addressTwo": "Carraig Uí Leighin",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19886D",
    "schoolName": "Gael Scoil Thomais Daibhis",
    "addressOne": "Cnoc an tSamhraidh",
    "addressTwo": "Magh Ealla",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19899M",
    "schoolName": "Gaelscoile Cobh",
    "addressOne": "COBH",
    "addressTwo": "CO CHORCAI",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19909M",
    "schoolName": "Gaelscoil Peig Sayers",
    "addressOne": "Campas Oideachais",
    "addressTwo": "Fearann Phiarais",
    "addressThree": "Bothar Ri na hAoine",
    "addressFour": "Corcaigh",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19989N",
    "schoolName": "Scoil Mhuire Na Trocaire",
    "addressOne": "CILL NA MULLACH",
    "addressTwo": "CO CHORCAI",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19993E",
    "schoolName": "Gaelscoil An Ghoirt Alainn",
    "addressOne": "Aibhinne Murmont",
    "addressTwo": "Gort Álainn",
    "addressThree": "Corcaigh",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20036J",
    "schoolName": "North Presentation Primary School",
    "addressOne": "Cathedral Walk",
    "addressTwo": "Gerald Griffin Street",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "20107G",
    "schoolName": "Gaelscoil Mhainistir Na Corann",
    "addressOne": "Bán Sheáin",
    "addressTwo": "Mainitir na Corann",
    "addressThree": "Co. Chorcaí",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "01692N",
    "schoolName": "Firmount Mixed N S",
    "addressOne": "Firmount",
    "addressTwo": "Donoughmore",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "02707F",
    "schoolName": "Sundays Well G N S",
    "addressOne": "Strawberry Hill",
    "addressTwo": "Blarney Road",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "07006A",
    "schoolName": "Ballyclough Mixed N S",
    "addressOne": "Ballyclough",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "01197D",
    "schoolName": "Strawberry Hill B N S",
    "addressOne": "Blarney Street",
    "addressTwo": "Sunday's Well",
    "addressThree": "Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "14000C",
    "schoolName": "Scoil Naomh Mhuire",
    "addressOne": "Bishop Street",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "16339F",
    "schoolName": "S N Iosef Naofa",
    "addressOne": "Mardyke Walk",
    "addressTwo": "Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "16377N",
    "schoolName": "St Marys National School",
    "addressOne": "Orilia Terrace,",
    "addressTwo": "Cobh",
    "addressThree": "Co. Cork",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "19906G",
    "schoolName": "Cork Educate Together National School",
    "addressOne": "GRATTAN STREET",
    "addressTwo": "CORK",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "16"
  },
  {
    "rollNumber": "04186N",
    "schoolName": "S N Bhiorainn",
    "addressOne": "Berrings",
    "addressTwo": "Co. Cork",
    "addressThree": "",
    "addressFour": "",
    "county": "Cork",
    "cecDistrict": "12"
  },
  {
    "rollNumber": "13217W",
    "schoolName": "Holy Family National School",
    "addressOne": "Rathcoole",
    "addressTwo": "Co. Dublin",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "#VALUE!"
  },
  {
    "rollNumber": "20518E",
    "schoolName": "Gaelscoil Laighean",
    "addressOne": "Lána na Cille",
    "addressTwo": "Cill na Gráinsí",
    "addressThree": "An Charraig Dhubh",
    "addressFour": "Co. Bhaile Átha Cliath",
    "county": "Dublin",
    "cecDistrict": "#VALUE!"
  },
  {
    "rollNumber": "20520O",
    "schoolName": "Dublin 6 Clonskeagh And Dublin 6w Primary School",
    "addressOne": "151/153 Harold's Cross Road",
    "addressTwo": "Dublin 6w",
    "addressThree": "",
    "addressFour": "",
    "county": "Dublin",
    "cecDistrict": "#VALUE!"
  },
  {
    "rollNumber": "20523U",
    "schoolName": "Goatstown Stillorgan Primary School",
    "addressOne": "Grafton House",
    "addressTwo": "Ballymoss Road",
    "addressThree": "Sandyford Industrial Estate",
    "addressFour": "D. 18",
    "county": "Dublin",
    "cecDistrict": "#VALUE!"
  },
  {
    "rollNumber": "19207D",
    "schoolName": "The Adelaide & Meath Hospital",
    "addressOne": "INCORPORATING THE NATIONAL",
    "addressTwo": "CHILDRENS HOSPITAL SPECIAL SCHOOL",
    "addressThree": "TALLAGHT",
    "addressFour": "DUBLIN 24",
    "county": "Dublin",
    "cecDistrict": "#VALUE!"
  },
  {
    "rollNumber": "13086K",
    "schoolName": "Rathgar Junior School",
    "addressOne": "Dublin City South East",
    "addressTwo": "",
    "addressThree": "",
    "addressFour": "",
    "county": "",
    "cecDistrict": ""
  }
]

  export default SCHOOLS_DETAILS;