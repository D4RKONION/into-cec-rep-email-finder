export const DISTRICT_REP_DETAILS = {
  "1": {
    name: "Seamus Hanna",
    email: "shanna@into.ie"
  },
  "2": {
    name: "Dorothy McGinley",
    email: "dmcginley@into.ie"
  },
  "3": {
    name: "Aine McGinley",
    email: "amcginley@into.ie"
  },
  "4": {
    name: "Vincent Duffy",
    email: "vduffy@into.ie"
  },
  "5": {
    name: "Adrian Kelly",
    email: "akelly@into.ie"
  },
  "6": {
    name: "Tommy Greally",
    email: "tgreally@into.ie"
  },
  "7": {
    name: "Carmel Browne",
    email: "cbrowne@into.ie"
  },
  "8": {
    name: "Gerry Brown",
    email: "gbrown@into.ie"
  },
  "9": {
    name: "Órlaith Ní Fhoghlú",
    email: "onifhoghlu@into.ie"
  },
  "10": {
    name: "Deirdre Fleming",
    email: "dfleming@into.ie"
  },
  "11": {
    name: "Brendan Horan",
    email: "bhoran@into.ie"
  },
  "12": {
    name: "Edel Polly",
    email: "epolly@into.ie"
  },
  "13": {
    name: "Anne Horan",
    email: "ahoran@into.ie"
  },
  "14": {
    name: "Gregor Kerr",
    email: "gkerr@into.ie"
  },
  "15": {
    name: "Máire Lineen",
    email: "mlineen@into.ie"
  },
  "16": {
    name: "Siobhan Buckley",
    email: "sbuckley@into.ie"
  }
}

export const OTHER_EMAILS = [
  {
    name: "Joe McKeown",
    position: "President",
    email: "jmckeown@into.ie",
  },
  {
    name: "John Driscoll",
    position: "Vice President",
    email: "jdriscoll@into.ie",
  },
  {
    name: "John Boyle",
    position: "General Secretary",
    email: "jboyle@into.ie",
  },
  {
    name: "Deirdre O'Connor",
    position: "General Treasurer",
    email: "doconnor@into.ie",
  },
  {
    name: "Gerry Murphy",
    position: "Northern Secretary",
    email: "gmurphy@into.ie",
  }
]