import Button from "./Button.jsx"
import ConfirmButtonImage from '../../img/chevron-right.svg';

const Table = ({ SCHOOLS_DETAILS, pattern, onSchoolSelect, isSearched }) =>
  <div className="table">
    <div className="table-header">
        <span className="school-name">School</span>
        <span className="school-rollnumber">Roll Number</span>
        <span className="school-district">CEC District</span>
        <span className="confirm-button"></span>
    </div>
    {SCHOOLS_DETAILS.filter(isSearched(pattern)).map(item =>
      <div key={item.rollNumber} className="table-row" onClick={() => onSchoolSelect(item)}>
        <span className="school-name">{item.schoolName}</span>
        <span className="school-rollnumber">{item.rollNumber}</span>
        <span className="school-district">{item.cecDistrict}</span>
        <span className="confirm-button">
          <Button 
            className="button-inline"
          >
            <img src={ConfirmButtonImage}/>
          </Button>
        </span>
      </div>
    )}
  </div>

export default Table;