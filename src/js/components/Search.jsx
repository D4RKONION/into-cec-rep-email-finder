const Search = ({ value, onChange, children }) =>
  <form onSubmit={(event) =>  event.preventDefault()}>
    {children} <input
      type="text"
      value={value} 
      onChange={onChange}
      placeholder="Type your school's roll/name/address..."
    />
  </form>

export default Search;