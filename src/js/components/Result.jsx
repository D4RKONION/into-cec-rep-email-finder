import {
    OTHER_EMAILS,
    DISTRICT_REP_DETAILS
} from '../constants/Emails';


const Result = ({ selectedSchool }) => 
  <div id="results">
    <div>
      <h1>Your School details</h1>
      <p>School Name: {selectedSchool.schoolName}</p>
      <p>Roll Number: {selectedSchool.rollNumber}</p>
      <p>County: {selectedSchool.county}</p>
    </div>
    <div id="districtResult">
      <h1>You are in District {selectedSchool.cecDistrict}</h1>
      <p id="mistakeWarning"><em>Probably... if you find an error, <a href="mailto:teacher@fullmeter.com">Let me know!</a></em></p>
      <p>Your CEC Rep is <b>{DISTRICT_REP_DETAILS[selectedSchool.cecDistrict].name}</b></p>
      <p>You can email them at <a href={`mailto:${DISTRICT_REP_DETAILS[selectedSchool.cecDistrict].email}`}>{DISTRICT_REP_DETAILS[selectedSchool.cecDistrict].email}</a></p>
    </div>
    <div>
      <h2>Other Useful Emails</h2>
      {OTHER_EMAILS.map(person =>
        <p>{person.name} (<i>{person.position}</i>): <a href={`mailto:${person.email}`}>{person.email}</a></p>  
      )}
    </div>
  </div>

export default Result;