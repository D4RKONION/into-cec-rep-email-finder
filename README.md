# CEC Searach

A quick and simple search to find your INTO CEC rep

## Build Using React

The project was made with create-react-app. If you'd like to contribute, fork the project and from the project directory run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.